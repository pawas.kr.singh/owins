from .base import *

ALLOWED_HOSTS = ['owimslive.do.viewyoursite.net', ]

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'owimslive',
        'USER': 'owimslive_user',
        'PASSWORD': 'tvVPhQJcnd',
        'HOST': 'localhost',
        'PORT': '',
    }
}

STATIC_ROOT = os.path.join(os.path.dirname(BASE_DIR), 'static')
SITE_NAME = 'Owims Live - Client Review site'
