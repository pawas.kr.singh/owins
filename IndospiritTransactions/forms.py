from django import forms

from . import models


class OrderHeadForm(forms.ModelForm):
    class Meta:
        model = models.OrderHead
        fields = '__all__'
        # fields = ('dealer', 'brand', 'sr_id', 'status', 'order_number','total_amount',
        #           'discount_amount', 'payable_amount')


class OrderDetailsForm(forms.ModelForm):
    class Meta:
        model = models.OrderDetails
        fields = '__all__'

        # fields = ('order_id', 'product_id', 'sku_code', 'product_short_name', 'size_capacity',
        #           'size_capacity_unit', 'gst_rate', 'sgst_rate', 'cgst_rate', 'other_tax_rate', 'pattern_color',
        #           'product_description_feature', 'warranty_months', 'best_buying_price', 'dealer_price',
        #           'max_retail_price', 'quantity', 'discount_amount_per_unit', 'discount_percent_per_unit',
        #           'total_amount')


class OrderAllowanceDetailsForm(forms.ModelForm):
    class Meta:
        model = models.OrderAllowanceDetails
        # fields = '__all__'
        fields = ('order_detail_id', 'product_id', 'sku_code', 'order_id', 'allowance_percent', 'allowance_amount',
                  'max_allowance_percent', 'max_allowance_amount')


class OrderTieUpDetailsForm(forms.ModelForm):
    class Meta:
        model = models.OrderTieUpDetails
        fields = '__all__'
        # fields = ('order_detail_id', 'product_id', 'sku_code', 'order_id', 'tie_up_percent', 'tie_up_amount')


class OrderSchemeDetailsSellThroughForm(forms.ModelForm):
    class Meta:
        model = models.OrderSchemeDetailsSellThrough
        fields = ('order_detail_id', 'product_id', 'sku_code', 'order_id', 'scheme_percent', 'scheme_amount',
                  'scheme_code', 'scheme_name', 'max_scheme_percent', 'max_scheme_amount')


class CashDiscountTransactionForm(forms.ModelForm):
    class Meta:
        model = models.CashDiscountTransaction

        fields = ('dealer', 'order', 'discount_percent', 'discount_amount', 'days')


class CreditNotesForm(forms.ModelForm):
    class Meta:
        model = models.CreditNotes
        # fields = '__all__'

        fields = ('dealer', 'note_type', 'credit_amount', 'order_id', 'description')


class OrderDispatchHeadForm(forms.ModelForm):
    class Meta:
        model = models.OrderDispatchHead
        fields = ('order_id', 'dispatched_by', 'dispatched_on')


class OrderDispatchDetailsForm(forms.ModelForm):
    class Meta:
        model = models.OrderDispatchDetail
        fields = ('dispatch_id', 'sku_code', 'quantity', 'warehouse_id')
