from django.contrib.auth.decorators import login_required
from django.urls import path

from . import views

urlpatterns = [

    path('order_head/<str:order_status>/', login_required(views.order_head), name='order_head'),
    path('order_head/create', login_required(views.create_order_head), name='create_order_head'),
    path('order_head/<str:status>/edit/<int:Ohid>', login_required(views.edit_order_head), name='edit_order_head'),
    path('order_details', login_required(views.order_details), name='order_details'),
    path('order_details/create', login_required(views.create_order_details), name='create_order_details'),
    path('order_allowance', login_required(views.order_allowance), name='order_allowance'),
    path('order_allowance/create', login_required(views.create_order_allowance), name='create_order_allowance'),
    path('order_tie_up', login_required(views.order_tie_up), name='order_tie_up'),
    path('order_tie_up/create', login_required(views.create_order_tie_up), name='create_order_tie_up'),
    path('order_scheme', login_required(views.order_scheme), name='order_scheme'),
    path('order_scheme/create', login_required(views.create_order_scheme), name='create_order_scheme'),
    path('cash_discount_transaction', login_required(views.cash_discount_transaction),
         name='cash_discount_transaction'),
    path('cash_discount_transaction/create', login_required(views.create_cash_discount_transaction),
         name='create_cash_discount_transaction'),
    path('credit_note', login_required(views.credit_note), name='credit_note'),
    path('credit_note/create', login_required(views.create_credit_note), name='create_credit_note'),
    path('dispatch_head', login_required(views.dispatch_head), name='dispatch_head'),
    path('dispatch_head/create', login_required(views.create_dispatch_head), name='create_dispatch_head'),
    path('dispatch_detail', login_required(views.dispatch_detail), name='dispatch_detail'),
    path('dispatch_detail/create', login_required(views.create_dispatch_detail), name='create_dispatch_detail'),

]
