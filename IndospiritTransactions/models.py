from django.conf import settings
from django.db import models
from IndospritCore.models import BrandMaster, ProductMaster, CreditNoteTypeMaster, WarehouseMaster
import datetime
today = datetime.datetime.now()


class OrderHead(models.Model):
    ORDER_STATUS = (
        ('Draft', 'Draft'),
        ('Processing', 'Processing'),
        ('SchemeApplied', 'Scheme Applied'),
        ('Deleted', 'Deleted'),
        ('OrderPlaced', 'Order Placed'),
        ('InWarehouse', 'In Warehouse'),
        ('Reject', 'Reject'),
        ('OnHold', 'On Hold'),
        ('Dispatched', 'Dispatched'),
        ('Delivered', 'Delivered'),
        ('PartialDispatched', 'Partial Dispatched'),
        ('PaymentDone', 'Payment Done')
    )

    order_id = models.BigAutoField(primary_key=True)
    dealer = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.SET_NULL, null=True,
                               related_name='dealer_by_order_head')
    brand = models.ForeignKey(BrandMaster, null=True, on_delete=models.SET_NULL, related_name='brand_by_order_head')
    sr_id = models.ForeignKey(settings.AUTH_USER_MODEL, null=True, on_delete=models.SET_NULL, related_name='order_sr')
    status = models.CharField(choices=ORDER_STATUS, default='Draft', max_length=30)
    order_number = models.IntegerField(default=0, null=True, blank=True)
    order_date = models.DateTimeField(null=True, blank=True)
    total_amount = models.DecimalField(default=0, max_digits=15, decimal_places=2, null=True, blank=True)
    discount_amount = models.DecimalField(default=0, max_digits=15, decimal_places=2, null=True, blank=True)
    payable_amount = models.DecimalField(default=0, max_digits=15, decimal_places=2, null=True, blank=True)
    is_active = models.BooleanField(default=True, null=True, blank=True)
    is_deleted = models.BooleanField(default=False, null=True, blank=True)
    DMS_order_number = models.CharField(max_length=100, blank=True, null=True)
    remarks = models.CharField(null=True, blank=True, max_length=200)

    warehouse_code = models.CharField(max_length=60, null=True, blank=True)
    warehouse_address = models.CharField(max_length=250, null=True, blank=True)
    warehouse_manager = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.SET_NULL,
                                          related_name='warehouse_manager', null=True, blank=True)

    def __str__(self):
        return str(self.order_number)


class OrderDetails(models.Model):
    order_detail_id = models.BigAutoField(primary_key=True)
    order_id = models.ForeignKey(OrderHead, on_delete=models.PROTECT, related_name='order_id_by_detail')
    product_id = models.ForeignKey(ProductMaster, on_delete=models.PROTECT, related_name='product_by_order', null=True)
    sku_code = models.CharField(max_length=60, null=True, blank=True)
    product_short_name = models.CharField(null=True, blank=True, max_length=250)
    size_capacity = models.DecimalField(blank=True, max_digits=50, decimal_places=2, null=True)
    size_capacity_unit = models.CharField(max_length=25, null=True, blank=True)
    gst_rate = models.DecimalField(blank=True, max_digits=4, decimal_places=2)
    sgst_rate = models.DecimalField(blank=True, max_digits=4, decimal_places=2)
    cgst_rate = models.DecimalField(blank=True, max_digits=4, decimal_places=2)
    other_tax_rate = models.DecimalField(blank=True, max_digits=4, decimal_places=2)
    pattern_color = models.CharField(max_length=20,default="black")
    product_description_feature = models.CharField(max_length=1000,blank=True, null=True,default=None)
    warranty_months = models.IntegerField(blank=True, null=True)
    best_buying_price = models.DecimalField(blank=True, max_digits=15, decimal_places=2)
    dealer_price = models.DecimalField(null=True, blank=True, max_digits=15, decimal_places=2)
    max_retail_price = models.DecimalField(blank=True, max_digits=15, decimal_places=2)
    created_on = models.DateTimeField(auto_now_add=True)
    created_by = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.SET_NULL, null=True,
                                   related_name='created_by_order')
    modified_on = models.DateTimeField(auto_now=True)
    modified_by = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.SET_NULL, null=True,
                                    related_name='modified_by_order')
    quantity = models.IntegerField(default=0, null=True, blank=True)
    discount_amount_per_unit = models.IntegerField(blank=True, null=True, default=0)
    discount_percent_per_unit = models.DecimalField(blank=True, max_digits=4, decimal_places=2)
    total_amount = models.IntegerField(default=0, null=True, blank=True)
    is_active = models.BooleanField(default=True, null=True, blank=True)
    is_deleted = models.BooleanField(default=False, null=True, blank=True)

    def __str__(self):
        return str(self.order_detail_id)

    def __int__(self):
        return self.order_detail_id


class OrderAllowanceDetails(models.Model):
    order_allowance_id = models.BigAutoField(primary_key=True)
    order_detail_id = models.ForeignKey(OrderDetails, on_delete=models.SET_NULL, null=True,
                                        related_name='order_detail_by_allowance')
    product_id = models.ForeignKey(ProductMaster, null=True, on_delete=models.PROTECT)
    sku_code = models.CharField(max_length=60, null=True, blank=True)
    order_id = models.ForeignKey(OrderHead, on_delete=models.PROTECT, related_name='order_id_by_allowance')
    allowance_percent = models.DecimalField(blank=True, max_digits=4, decimal_places=2)
    allowance_amount = models.IntegerField(default=0, null=True, blank=True)
    max_allowance_percent = models.DecimalField(null=True, blank=True, max_digits=4, decimal_places=2)
    max_allowance_amount = models.IntegerField(default=0, null=True, blank=True)
    is_active = models.BooleanField(default=True, null=True, blank=True)
    is_deleted = models.BooleanField(default=False, null=True, blank=True)

    def __str__(self):
        return str(self.order_allowance_id)

    def __int__(self):
        return self.order_allowance_id


class OrderTieUpDetails(models.Model):
    order_tie_up_id = models.BigAutoField(primary_key=True)
    order_detail_id = models.ForeignKey(OrderDetails, on_delete=models.SET_NULL, null=True,
                                        related_name='order_detail_by_tie_up')
    product_id = models.ForeignKey(BrandMaster, on_delete=models.PROTECT)
    sku_code = models.CharField(max_length=60, null=True, blank=True)
    order_id = models.ForeignKey(OrderHead, on_delete=models.PROTECT, related_name='order_id_by_tie_up')
    tie_up_percent = models.DecimalField(blank=True, max_digits=4, decimal_places=2)
    tie_up_amount = models.IntegerField(default=0, null=True, blank=True)
    max_tie_up_percent = models.DecimalField(null=True, blank=True, max_digits=4, decimal_places=2)
    max_tie_up_amount = models.IntegerField(default=0, null=True, blank=True)
    is_active = models.BooleanField(default=True, null=True, blank=True)
    is_deleted = models.BooleanField(default=False, null=True, blank=True)

    def __str__(self):
        return str(self.order_tie_up_id)

    def __int__(self):
        return self.order_tie_up_id


class OrderSchemeDetailsSellThrough(models.Model):
    order_scheme_id = models.BigAutoField(primary_key=True)
    order_detail_id = models.ForeignKey(OrderDetails, on_delete=models.SET_NULL, null=True,
                                        related_name='order_detail_by_scheme')
    product_id = models.ForeignKey(BrandMaster, on_delete=models.PROTECT)
    sku_code = models.CharField(max_length=60, null=True, blank=True)
    order_id = models.ForeignKey(OrderHead, on_delete=models.PROTECT, related_name='order_id_by_scheme')
    scheme_percent = models.DecimalField(blank=True, max_digits=4, decimal_places=2)
    scheme_amount = models.IntegerField(blank=True, null=True, default=0)
    scheme_code = models.CharField(null=True, blank=True, max_length=250)
    scheme_name = models.CharField(null=True, blank=True, max_length=100)
    max_scheme_percent = models.DecimalField(null=True, blank=True, max_digits=4, decimal_places=2)
    max_scheme_amount = models.IntegerField(default=0, null=True, blank=True)
    is_active = models.BooleanField(default=True, null=True, blank=True)
    is_deleted = models.BooleanField(default=False, null=True, blank=True)

    def __str__(self):
        return str(self.scheme_name)


class CashDiscountTransaction(models.Model):
    transaction_id = models.BigAutoField(primary_key=True)
    dealer = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.SET_NULL, null=True,
                               related_name='dealer_by_credit_trans')
    order = models.ForeignKey(OrderHead, on_delete=models.PROTECT, related_name='order_id_by_cash')
    discount_percent = models.DecimalField(blank=True, max_digits=4, decimal_places=2)
    discount_amount = models.IntegerField(blank=True, null=True, default=0)
    days = models.IntegerField(default=0, blank=True, null=True)
    is_active = models.BooleanField(default=True, null=True, blank=True)
    is_deleted = models.BooleanField(default=False, null=True, blank=True)

    def __str__(self):
        return str(self.dealer)


class CreditNotes(models.Model):
    credit_note_id = models.BigAutoField(primary_key=True)
    dealer = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.SET_NULL, null=True,
                               related_name='dealer_by_credit_notes')
    note_type = models.ForeignKey(CreditNoteTypeMaster, on_delete=models.SET_NULL, null=True,
                                  related_name='dealer_by_credit_notes')
    # credit_amount = models.IntegerField(default=0, null=True, blank=True)
    credit_amount = models.DecimalField(blank=True, max_digits=10, decimal_places=2, default=0)
    order_id = models.ForeignKey(OrderHead, on_delete=models.PROTECT, related_name='order_id_by_credit_notes')
    order_detail_id = models.ForeignKey(OrderDetails, on_delete=models.SET_NULL, null=True,
                                        related_name='order_detail_by_credit_notes')
    description = models.CharField(null=True, blank=True, max_length=250)
    is_active = models.BooleanField(default=True, null=True, blank=True)
    is_deleted = models.BooleanField(default=False, null=True, blank=True)

    tieup_sell_through = models.IntegerField(default=0, null=True, blank=True)
    created_on = models.DateTimeField(auto_now_add=True, null=True, blank=True)
    created_by = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.SET_NULL,
                                   related_name='created_by_credit_note', null=True, blank=True)
    modified_on = models.DateTimeField(auto_now=True, null=True, blank=True)
    modified_by = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.SET_NULL,
                                    related_name='modified_by_credit_note', null=True, blank=True)

    def __str__(self):
        return str(self.credit_note_id)

    def __int__(self):
        return self.credit_note_id


class OrderDispatchHead(models.Model):
    dispatch_id = models.BigAutoField(primary_key=True)
    order_id = models.ForeignKey(OrderHead, on_delete=models.PROTECT, related_name='order_id_by_dispatch_head')
    dispatched_by = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.SET_NULL, null=True,
                                      related_name='dispatched_by_head')
    dispatched_on = models.DateTimeField()
    is_active = models.BooleanField(default=True, null=True, blank=True)
    is_deleted = models.BooleanField(default=False, null=True, blank=True)

    def __str__(self):
        return str(self.dispatch_id)

    def __int__(self):
        return self.dispatch_id


class OrderDispatchDetail(models.Model):
    dispatch_detail_id = models.BigAutoField(primary_key=True)
    dispatch_id = models.ForeignKey(OrderDispatchHead, on_delete=models.PROTECT,
                                    related_name='dispatch_id_by_dispatch_head')
    sku_code = models.CharField(max_length=60, null=True, blank=True)
    quantity = models.IntegerField(default=0, null=True, blank=True)
    warehouse_id = models.ForeignKey(WarehouseMaster, on_delete=models.PROTECT,
                                     related_name='warehouse_by_dispatch_detail')
    is_active = models.BooleanField(default=True, null=True, blank=True)
    is_deleted = models.BooleanField(default=False, null=True, blank=True)

    def __str__(self):
        return str(self.dispatch_detail_id)

    def __int__(self):
        return self.dispatch_detail_id


class OrderEventHistory(models.Model):
    order_id = models.ForeignKey(OrderHead, on_delete=models.PROTECT, related_name='order_id_event_history')
    order_status = models.CharField(max_length=60, null=True, blank=True)
    created_on = models.DateTimeField(auto_now_add=True, null=True, blank=True)
    created_by = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.SET_NULL,
                                   related_name='created_by_event_history', null=True, blank=True)


class LedgerHead(models.Model):
    VOUCHER_TYPE = (
        ('Sales', 'Sales'),
        ('CreditNote', 'Credit Note'),
        ('DebitNote', 'Debit Note'),
        ('Payment', 'Payment')
    )

    ledger_head_id = models.AutoField(primary_key=True)
    voucher_type = models.CharField(choices=VOUCHER_TYPE, default='Sales', max_length=30)
    voucher_date = models.DateField(null=True, blank=True)
    created_on = models.DateTimeField(auto_now_add=True, null=True, blank=True)


class Ledger(models.Model):
    LEDGER_TYPE = (
        ('Dealer', 'Dealer'),
        ('Sales', 'Sales'),
        ('Discount', 'Discount'),
        ('Bank', 'Bank'),
        ('CreditNote', 'Credit Note'),
        ('DebitNote', 'Debit Note'),
        ('DealerTransaction', 'Dealer Transaction'),
        ('DealerOrders', 'DealerOrders'),
        ('Unadjusted', 'Unadjusted')
    )

    ledger_id = models.BigAutoField(primary_key=True)
    ledger_head = models.ForeignKey(LedgerHead, related_name="ledger_head", on_delete=models.PROTECT, null=True,
                                    blank=True)
    ledger_type = models.CharField(choices=LEDGER_TYPE, default='Sales', max_length=30)
    remarks = models.TextField(null=True, blank=True)
    dr_amount = models.DecimalField(max_digits=20, decimal_places=2, default=0)
    cr_amount = models.DecimalField(max_digits=20, decimal_places=2, default=0)
    balance = models.DecimalField(max_digits=20, decimal_places=2, default=0)
    order_number = models.IntegerField(default=0, null=True, blank=True)
    dealer = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.SET_NULL,
                               related_name='ledger_dealer', null=True, blank=True)
    voucher_number = models.CharField(max_length=30, null=True, blank=True)
    created_on = models.DateTimeField(auto_now_add=True, null=True, blank=True)
    created_by = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.SET_NULL,
                                   related_name='created_by_ledger', null=True, blank=True)
    is_active = models.BooleanField(default=True, null=True, blank=True)
    is_deleted = models.BooleanField(default=False, null=True, blank=True)


class Payment(models.Model):
    PAYMENT_MODE = (
        ('Cash', 'Cash'),
        ('Cheque', 'Cheque'),
        ('DD', 'DD'),
        ('Other', 'Other')
    )

    payment_id = models.BigAutoField(primary_key=True)
    payment_type = models.CharField(choices=PAYMENT_MODE, max_length=30)
    reference = models.CharField(max_length=100, null=True, blank=True)
    amount = models.DecimalField(max_digits=20, decimal_places=2, default=0)
    payment_date = models.DateField(null=True, blank=True)
    dealer = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.SET_NULL,
                               related_name='payment_dealer', null=True, blank=True)
    created_on = models.DateTimeField(auto_now_add=True, null=True, blank=True)
    created_by = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.SET_NULL,
                                   related_name='created_by_payment', null=True, blank=True)
    is_active = models.BooleanField(default=True, null=True, blank=True)
    is_deleted = models.BooleanField(default=False, null=True, blank=True)


class SpacialDiscount(models.Model):
    STATUS = (
        ('Pending', 'Pending'),
        ('Rejected', 'Rejected'),
        ('Approved', 'Approved')
    )

    discount_id = models.BigAutoField(primary_key=True)
    status = models.CharField(default='Pending', choices=STATUS, max_length=30)
    comment = models.TextField(null=True, blank=True)
    amount = models.DecimalField(max_digits=20, decimal_places=2, default=0)
    order_number = models.IntegerField(default=0, null=True, blank=True)
    dealer = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.SET_NULL,
                               related_name='discount_dealer', null=True, blank=True)
    created_on = models.DateTimeField(auto_now_add=True, null=True, blank=True)
    created_by = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.SET_NULL,
                                   related_name='created_by_discount', null=True, blank=True)
    is_active = models.BooleanField(default=True, null=True, blank=True)
    is_deleted = models.BooleanField(default=False, null=True, blank=True)


class SpecialCDType(models.Model):
    STATUS = (
        ('Pending', 'Pending'),
        ('Rejected', 'Rejected'),
        ('Approved', 'Approved')
    )

    DISCOUNT_TYPE = (
        ('AdditionalDiscount', 'Additional Discount'),
    )

    cd_id = models.BigAutoField(primary_key=True)
    series_no = models.CharField(max_length=50, null=True, blank=True)
    discount_type = models.CharField(default='AdditionalDiscount', choices=DISCOUNT_TYPE, max_length=50)
    status = models.CharField(default='Pending', choices=STATUS, max_length=30)
    amount = models.DecimalField(max_digits=20, decimal_places=2, default=0)
    dealer = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.SET_NULL,
                               related_name='cd_dealer', null=True, blank=True)
    remarks = models.TextField(null=True, blank=True)
    created_on = models.DateTimeField(auto_now_add=True, null=True, blank=True)
    created_by = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.SET_NULL,
                                   related_name='created_by_cd', null=True, blank=True)
    is_active = models.BooleanField(default=True, null=True, blank=True)
    is_deleted = models.BooleanField(default=False, null=True, blank=True)

    def save(self, *args, **kwargs):
        try:
            prime_number = str(SpecialCDType.objects.latest('pk').pk + 1)
        except SpecialCDType.DoesNotExist:
            prime_number = str(1)
        current_year = int(str(today.year)[-2:])
        if len(prime_number) == 1:
            prime_number = "00{0}".format(prime_number)
        elif len(prime_number) == 2:
            prime_number = "0{0}".format(prime_number)
        else:
            prime_number = prime_number
        series = "CN/{0}-{1}/{2}".format(current_year, current_year + 1, prime_number)
        self.series_no = series
        super(SpecialCDType, self).save(*args, **kwargs)


class DebitNote(models.Model):
    STATUS = (
        ('Pending', 'Pending'),
        ('Rejected', 'Rejected'),
        ('Approved', 'Approved')
    )

    TYPE = (
        ('DebitNote', 'Debit Note'),
    )

    debit_id = models.BigAutoField(primary_key=True)
    type = models.CharField(choices=TYPE, max_length=30)
    status = models.CharField(default='Pending', choices=STATUS, max_length=30)
    reference = models.CharField(max_length=100, null=True, blank=True)
    amount = models.DecimalField(max_digits=20, decimal_places=2, default=0)
    debit_date = models.DateField(null=True, blank=True)
    dealer = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.SET_NULL,
                               related_name='debit_dealer', null=True, blank=True)
    created_on = models.DateTimeField(auto_now_add=True, null=True, blank=True)
    created_by = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.SET_NULL,
                                   related_name='created_by_debit', null=True, blank=True)
    is_active = models.BooleanField(default=True, null=True, blank=True)
    is_deleted = models.BooleanField(default=False, null=True, blank=True)
