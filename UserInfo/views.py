from django.shortcuts import render
import csv
import datetime
import os

from ClassicUserAccounts.models import User
from django.shortcuts import render

from IndospritCore.models import BrandMaster, DealerBusinessMaster, DealersBrand
from Owims.settings import import_settings
from UserInfo.models import DealerProfile, SRDealer
from . import forms
from . import models


def create_sr(request):
    form_class = forms.CreateSalesRep
    return render(request, 'create_sr.html', {'form': form_class})

#profile 
def dealer_profile(request):
    dealer_profile = models.DealerProfile.objects.all()
    msg = ''
    Inserted = 0
    not_insert = 0
    update = 0
    result = []
    lblMsg = ''
    filexls = ''
    invalid_rows = []
    invalids = len(invalid_rows)
    not_exist = []
    if request.method == 'POST':
        if request.FILES:
            dealer_profile= import_dealer_profile(request)
            not_exist=dealer_profile[4]
            Inserted=dealer_profile[2]
            not_insert=dealer_profile[3]
    context={
        'Inserted':str(Inserted),
        'not_insert':str(not_insert),
        'update':str(update),
        'not_exist':not_exist,
        'dealer_info': dealer_profile,
        'invalid_rows': invalid_rows,
    }
    return render(request, 'dealer_info.html', context)


def import_dealer_profile(request):
    invalid_rows = []
    index = 0
    Inserted=0
    not_insert=0
    update=0
    not_exist=[]
    decoded_file = request.FILES['dealer'].read().decode('utf-8').splitlines()
    reader = csv.DictReader(decoded_file)
    for row in reader:
        if index == 0:
            flag = True
            for x in import_settings.DEALER_DATA_IMPORT_KEY:
                if x not in row.keys():
                    notin = {}
                    notin['notexist'] = x
                    not_exist.append(notin)
        set_exist = len(not_exist)
        if set_exist == 0:
            count = 0
            if row['dealer_code'] != '' and row['brand_name'] != '' and row['company_name'] != '' and row[
                'owner_name'] != '' and row['primary_contact'] != '':
                try:
                    users = User.objects.get(user_code=row['dealer_code'])
                except User.DoesNotExist:
                    users = users = User()
                users.email = row['email']
                users.first_name = row['owner_name']
                users.mobile = row['primary_contact']
                users.date_joined = datetime.datetime.now()
                users.state = row['state']
                users.city = row['city']
                users.user_code = row['dealer_code']
                users.save()
                try:
                    DP = DealerProfile.objects.get(dealer_code=row['dealer_code'])
                except DealerProfile.DoesNotExist:
                    DP = DP = DealerProfile()
                    Inserted += 1
                DP.dealer = User.objects.get(id=users.pk)
                DP.dealer_code = row['dealer_code']
                DP.owner_name = row['owner_name']
                DP.company_name = row['company_name']
                DP.region_id = row['region_id']
                DP.shipping_address = row['shipping_address']
                DP.pincode = int(row['pincode'])
                DP.email = row['email']
                DP.primary_contact = int(row['primary_contact'])
                alternate_contact = row['alternate_contact']
                if alternate_contact == '' or alternate_contact == None:
                    DP.alternate_contact = 0
                else:
                    DP.alternate_contact = int(alternate_contact)
                DP.gst_no = row['gst_no']
                DP.pan_no = row['pan_no']
                DP.aadhaar_no = row['aadhaar_no']
                DP.created_by = User.objects.get(id=users.id, is_active=True)
                DP.created_on = datetime.datetime.now()
                DP.modified_by = User.objects.get(id=users.id, is_active=True)
                DP.modified_on = datetime.datetime.now()
                DP.save()
                try:
                    SRD = SRDealer.objects.get(dealer_id=DP.pk, sr_id=DP.dealer)
                except SRDealer.DoesNotExist:
                    SRD = SRDealer()
                SRD.sr_id = User.objects.get(id=users.id, is_active=True)
                SRD.dealer_id = DealerProfile.objects.get(pk=DP.pk)
                SRD.save()
                code = row['brand_name'][0:3]
                count += 1
                code = code + '10' + str(count)
                try:
                    BM = BrandMaster.objects.get(brand_name=row['brand_name'], brand_code=code, is_active=True)
                except BrandMaster.DoesNotExist:
                    BM = BrandMaster()
                BM.brand_name = row['brand_name']
                BM.brand_code = code
                BM.created_by = User.objects.get(id=users.id, is_active=True)
                BM.created_on = datetime.datetime.now()
                BM.modified_by = User.objects.get(id=users.id, is_active=True)
                BM.modified_on = datetime.datetime.now()
                BM.save()
                try:
                    DB = DealersBrand.objects.get(brand_id=BM.brand_code, is_active=True)
                except DealersBrand.DoesNotExist:
                    DB = ''
                if DB == '':
                    DB = DealersBrand()
                DB.user = User.objects.get(id=users.id, is_active=True)
                DB.brand = BrandMaster.objects.get(brand_code=BM.brand_code, is_active=True)
                DB.save()
                try:
                    DBM = DealerBusinessMaster.objects.get(brand_id=BM.brand_code, is_active=True)
                except DealerBusinessMaster.DoesNotExist:
                    DBM = DealerBusinessMaster()
                DBM.dealer = User.objects.get(id=users.id, is_active=True)
                DBM.brand = BrandMaster.objects.get(brand_code=BM.brand_code, is_active=True)
                DBM.min_order_amount = row['min_order_amount']
                DBM.max_outstanding_threshold = row['max_outstanding_threshold']
                DBM.credit_days = row['credit_days']
                DBM.created_by = User.objects.get(id=users.id, is_active=True)
                DBM.created_on = datetime.datetime.now()
                DBM.modified_by = User.objects.get(id=users.id, is_active=True)
                DBM.modified_on = datetime.datetime.now()
                DBM.save()
    return [invalid_rows, Inserted, not_insert, update, not_exist]


def handle_uploaded_file(request, imgfile, paths):
    # pic_path='upload/'
    if not os.path.exists(paths):
        os.makedirs(paths)
    if imgfile != '':
        if os.listdir(paths) != []:
            picfolder = str(os.listdir(paths))
            picfolder = picfolder.replace('[', '').replace(']', '').replace("'", '')
            os.remove(paths + picfolder)
        with open(paths + imgfile.name, 'wb+') as destination:
            for chunk in imgfile.chunks():
                destination.write(chunk)


def create_dealer_profile(request):
    form_class = forms.DealerProfileForm
    if request.method == 'GET':
        return render(request, 'dealer_info_create.html', {'form': form_class})
    elif request.method == 'POST':
        form_class = forms.DealerProfileForm(request.POST)
        if form_class.is_valid():
            dealer = form_class.save(commit=False)
            dealer.created_by = request.user
            dealer.save()
        return render(request, 'dealer_info_create.html', {'form': form_class})
    elif request.method == 'PUT':
        form_class = forms.DealerProfileForm(request.POST)
        if form_class.is_valid():
            dealer = form_class.save(commit=False)
            dealer.created_by = request.user
            dealer.save()
        return render(request, 'dealer_info_create.html', {'form': form_class})


#
# def dealer_address(request):
#     dealer_address = models.DealerAddress.objects.all()
#     return render(request, 'dealer_address.html', {'dealer_address': dealer_address})
#
#
# def create_dealer_address(request):
#     form_class = forms.DealerAddressForm
#     if request.method == 'GET':
#         return render(request, 'dealer_address_create.html', {'form': form_class})
#     elif request.method == 'POST':
#         form_class = forms.DealerAddressForm(request.POST)
#         if form_class.is_valid():
#             dealer_address = form_class.save(commit=False)
#             dealer_address.created_by = request.user
#             dealer_address.save()
#         return render(request, 'dealer_address_create.html', {'form': form_class})
#     elif request.method == 'PUT':
#         form_class = forms.DealerAddressForm(request.POST)
#         if form_class.is_valid():
#             dealer_address = form_class.save(commit=False)
#             dealer_address.created_by = request.user
#             dealer_address.save()
#         return render(request, 'dealer_address_create.html', {'form': form_class})
#


def sr_dealer(request):
    sr_dealer = models.SRDealer.objects.all()
    return render(request, 'sr_dealer.html', {'sr_dealer': sr_dealer})


def create_sr_dealer(request):
    form_class = forms.SRDealerForm
    if request.method == 'GET':
        return render(request, 'sr_dealer_create.html', {'form': form_class})
    elif request.method == 'POST':
        form_class = forms.SRDealerForm(request.POST)
        if form_class.is_valid():
            sr_dealer = form_class.save(commit=False)
            sr_dealer.created_by = request.user
            sr_dealer.save()
        return render(request, 'sr_dealer_create.html', {'form': form_class})
    elif request.method == 'PUT':
        form_class = forms.SRDealerForm(request.POST)
        if form_class.is_valid():
            sr_dealer = form_class.save(commit=False)
            sr_dealer.created_by = request.user
            sr_dealer.save()
        return render(request, 'sr_dealer_create.html', {'form': form_class})


def dealer_documents(request):
    dealer_documents = models.UserDocuments.objects.all()
    return render(request, 'dealer_documents.html', {'dealer_documents': dealer_documents})


def create_dealer_documents(request):
    form_class = forms.DealerDocumentsForm
    if request.method == 'GET':
        return render(request, 'dealer_documents_add.html', {'form': form_class})
    elif request.method == 'POST':
        form_class = forms.DealerDocumentsForm(request.POST)
        if form_class.is_valid():
            dealer_doc = form_class.save(commit=False)
            dealer_doc.created_by = request.user
            dealer_doc.save()
        return render(request, 'dealer_documents_add.html', {'form': form_class})
    elif request.method == 'PUT':
        form_class = forms.DealerDocumentsForm(request.POST)
        if form_class.is_valid():
            dealer_doc = form_class.save(commit=False)
            dealer_doc.created_by = request.user
            dealer_doc.save()
        return render(request, 'dealer_documents_add.html', {'form': form_class})
