from .base import *

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
    }
}
# DATABASES = {
#    'default': {
#        'ENGINE': 'django.db.backends.postgresql',
#        'NAME': 'Owims',
#        'USER': 'postgres',
#        'PASSWORD': 'winslate',
#        'HOST': 'localhost',
#        'PORT': '5432',
#    }
# }