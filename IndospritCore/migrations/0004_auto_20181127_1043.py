# Generated by Django 2.1.1 on 2018-11-27 10:43

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('IndospritCore', '0003_auto_20181127_0949'),
    ]

    operations = [
        migrations.AlterField(
            model_name='allowancemaster',
            name='discount_amount',
            field=models.IntegerField(blank=True, default=0, null=True),
        ),
        migrations.AlterField(
            model_name='allowancemaster',
            name='discount_percent',
            field=models.DecimalField(blank=True, decimal_places=2, default=0, max_digits=4, null=True),
        ),
    ]
