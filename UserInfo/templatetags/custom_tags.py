from django import template
from UserInfo.views.dealer.dealer_views import addcart

register = template.Library()


@register.filter(name='has_group')
def has_group(user, group_name):
    groups = user.groups.all()
    groups = [group.name for group in groups]
    return True if group_name in groups else False


@register.filter(name='has_category')
def has_category(counter):
    return counter - 1


# @register.filter(name='has_cart')
# def has_cart(request, group_name):
#     listdata = addcart(request)
#     return listdata
