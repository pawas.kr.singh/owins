from django.contrib import admin
from . import models


class DealerProfileAdmin(admin.ModelAdmin):
    list_display = ('dealer', 'dealer_code', 'owner_name', 'region_id', 'company_name', 'shipping_address', 'pincode', 'email',
                    'primary_contact', 'alternate_contact', 'gst_no', 'pan_no', 'Aadhaar_No','cd_slab_code')


admin.site.register(models.DealerProfile, DealerProfileAdmin)


# class DealerAddressAdmin(admin.ModelAdmin):
#     list_display = ('dealer', 'address_type')
#
#
# admin.site.register(models.DealerAddress, DealerAddressAdmin)
#

class SRDealerAdmin(admin.ModelAdmin):
    list_display = ('sr_id', 'dealer_id')


admin.site.register(models.SRDealer, SRDealerAdmin)


class DealerDocumentsAdmin(admin.ModelAdmin):
    list_display = ('user', 'document_type', 'document_number', 'document')


admin.site.register(models.UserDocuments, DealerDocumentsAdmin)


class OrderNotesAdmin(admin.ModelAdmin):
    list_display = ('order_id', 'dealer_id', 'sr_id', 'created_by', 'description', 'created_on')


admin.site.register(models.OrderNotes, OrderNotesAdmin)


""" ================= EMAIL AND SMS ========================= """


class AlertMasterAdmin(admin.ModelAdmin):
    list_per_page = 1000
    show_full_result_count = False
    list_display = [f.name for f in models.AlertMaster._meta.fields]


admin.site.register(models.AlertMaster, AlertMasterAdmin)


class SendAlertAdmin(admin.ModelAdmin):
    list_per_page = 1000
    show_full_result_count = False
    list_display = [f.name for f in models.SendAlert._meta.fields]


admin.site.register(models.SendAlert, SendAlertAdmin)


class AlertTemplateMasterAdmin(admin.ModelAdmin):
    list_per_page = 1000
    show_full_result_count = False
    list_display = [f.name for f in models.AlertTemplateMaster._meta.fields]


admin.site.register(models.AlertTemplateMaster, AlertTemplateMasterAdmin)


# class AlertsAdmin(admin.ModelAdmin):
#     list_per_page = 1000
#     show_full_result_count = False
#     list_display = [f.name for f in models.Alerts._meta.fields]
#
#
# admin.site.register(models.Alerts, AlertsAdmin)
#
#
# class ActionAdmin(admin.ModelAdmin):
#     list_per_page = 1000
#     show_full_result_count = False
#     list_display = [f.name for f in models.Action._meta.fields]
#
#
# admin.site.register(models.Action, ActionAdmin)
#
#
# class Notification_roleAdmin(admin.ModelAdmin):
#     list_per_page = 1000
#     show_full_result_count = False
#     list_display = [f.name for f in models.Notification_role._meta.fields]
#
#
# admin.site.register(models.Notification_role, Notification_roleAdmin)
#
#
# class EmailTemplateMasterAdmin(admin.ModelAdmin):
#     list_per_page = 1000
#     show_full_result_count = False
#     list_display = [f.name for f in models.EmailTemplateMaster._meta.fields]
#
#
# admin.site.register(models.EmailTemplateMaster, EmailTemplateMasterAdmin)