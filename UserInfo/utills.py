from django.conf import settings
from IndospritCore.models import ProductCategoryMaster, \
    ProductMaster, StockLevelMaster, StockRegister
from IndospiritTransactions.models import OrderDetails
from UserInfo import status_in
from django.core.mail import EmailMultiAlternatives
from django.db.models import Sum
import requests, json


def reverse_category(category_id, categoryList):
    """ RETURN PRODUCT CATEGORY NAME WITH SEQUENCE """
    try:
        productCategory = ProductCategoryMaster.objects.get(pk=category_id)
        categoryList.append(productCategory.category_name)
        reverse_category(productCategory.parent, categoryList)
    except ProductCategoryMaster.DoesNotExist:
        pass
    categoryList.reverse()
    reverse_name = ", ".join(map(str, categoryList))
    return reverse_name


def send_sms(numbers, message):
    """ SEND SMS AND RETURN RESPONSE WITH STATUS """
    querystring = {
        "method": settings.METHOD, "api_key": settings.API_KEY,
        "sender": settings.SENDER, "to": numbers, "message": message
    }
    response = requests.request("GET", settings.SEND_URL, params=querystring)
    response = json.loads(response.text)
    return response


def send_email(subject, to_email, html_content):
    """ SEND EMAIL AND RETURN RESPONSE """
    try:
        email = EmailMultiAlternatives(subject, html_content, settings.DEFAULT_FROM_EMAIL, to=to_email)
        email.attach_alternative(html_content, "text/html")
        response = email.send()
        return response
    except Exception as error:
        return str(error)


def check_stock_out(list_of_sku, orders=None):
    products = ProductMaster.objects.filter(
        sku_code__in=list_of_sku, is_active=True, is_deleted=False)
    status_in_stock, sku_code = None, None
    for product in products:
        list_dict = {
            "sku_code": product.sku_code, "sellable": 0, "non_sellable": 0,'assigned':0,
            "total_stock": 0, "available": 0, "blocked": 0, "remaining_stock": 0,
            "minimum_stock_level": 0, "required": 0, "stock_out": 0
        }

        sku_qty = 0
        if orders:
            sku_qty = orders.filter(product_id=product).first()
            sku_qty = sku_qty.quantity
        try:
            stock_level = StockLevelMaster.objects.filter(
                product=product, is_active=True, is_deleted=False).first()
            list_dict["minimum_stock_level"] = int(stock_level.min_stock_level)
        except Exception:
            list_dict["minimum_stock_level"] = int(0)

        totalStock = StockRegister.objects.filter(
            product=product, stock_type__in=["Sellable", "NonSellable"],
            is_active=True, is_deleted=False).values("stock_type").annotate(
            count=Sum("stock_in"))

        outStock = StockRegister.objects.filter(
            product=product, stock_type='stock_out',
            is_active=True, is_deleted=False).values(
            'stock_type').annotate(count=Sum('stock_out'))

        for item in totalStock:
            if item["stock_type"] == "Sellable":
                list_dict["sellable"] = int(item["count"])
                list_dict["total_stock"] += int(item["count"])
            elif item["stock_type"] == "NonSellable":
                list_dict["non_sellable"] = int(item["count"])
                list_dict["total_stock"] += int(item["count"])

        for item in outStock:
            list_dict["stock_out"] += int(item["count"])

        blocked_order_detail = OrderDetails.objects.filter(
            order_id__status__in=status_in.BlockedOrder(),
            product_id=product, is_active=True, is_deleted=False).values(
            "product_id").annotate(count=Sum("quantity"))
        for block in blocked_order_detail:
            list_dict["blocked"] += int(block["count"])

        assigned_order_detail = OrderDetails.objects.filter(
            order_id__status__in=status_in.AssignOrder(),
            product_id=product, is_active=True, is_deleted=False).values(
            "product_id").annotate(count=Sum("quantity"))
        for block in assigned_order_detail:
            list_dict["assigned"] += int(block["count"])
        list_dict["available"] = list_dict["sellable"] - list_dict["stock_out"] - list_dict["assigned"]
        list_dict["remaining_stock"] = list_dict["available"] - list_dict["blocked"]
        list_dict["required"] = list_dict["minimum_stock_level"] - list_dict["remaining_stock"]

        if orders:
            if sku_qty > list_dict["available"]:
                status_in_stock = False
                sku_code = product.sku_code
                break
            else:
                status_in_stock = True
        else:
            if list_dict["remaining_stock"] < list_dict["minimum_stock_level"]:
                status_in_stock = False
                sku_code = product.sku_code
                break
            else:
                status_in_stock = True
    return status_in_stock, sku_code
