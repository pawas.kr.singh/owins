from django.core.mail import EmailMultiAlternatives
from django.conf import settings
from django.template.loader import render_to_string
from django.contrib.auth import get_user_model
from IndospiritTransactions.models import OrderHead
from UserInfo.models import Alerts, Action, Notification_role, EmailTemplateMaster

User = get_user_model()


def send_email(subject, ctx, to_email):
    try:
        subject=to_email[1].email_subject
        html_content = to_email[1].content
        to_email=to_email[0]
        email = EmailMultiAlternatives(subject, html_content, settings.DEFAULT_FROM_EMAIL, to=to_email)
        # if obj.attachment:
        #     attachment = request.FILES['attachment']
        #     email.attach(attachment.name, obj.attachment.read(), attachment.content_type)
        # email.attach_alternative(html_content, "text/html")
        msg = email.send()
        return msg
    except:
        pass


def send_email_or_sms(status, order, obj_status=None):
    obj_status = obj_status.split('-')
    subject = ''
    if obj_status[0] == 'Order_Cancelled':
        subject = "Order Cancelled"
    elif obj_status[0] == 'New_order_placed':
        if obj_status[1] == 'Processing':
            subject = "Order Processing"
        if obj_status[1] == 'OrderPlaced':
            subject = "Order Placed"
        if obj_status[1] == 'SchemeApplied':
            subject = "Order Scheme Applied"
    elif obj_status[0] == 'Dispatch_Delivered':
        if obj_status[1] == 'InWarehouse':
            subject = "Order In Warehouse"
        if obj_status[1] == 'Dispatched':
            subject = "Order Dispatched"
    mail_to = get_email_list(status, order, obj_status[0])
    ctx = {"message": subject}
    if len(mail_to[0]) > 0:
        send_email(subject, ctx, mail_to)


def get_email_list(status, order, obj_status=None):
    try:
        alert_status = obj_status
        alert = Alerts.objects.get(alerts_type=alert_status, is_active=True, is_deleted=False)
        action = Action.objects.filter(alerts=alert, is_active=True, is_deleted=False)
        actiondata=''
        for obj in action:
            if obj.send_type=='email' or obj.send_type=='both':
                actiondata=obj.pk
        notification = Notification_role.objects.get(action=actiondata, is_active=True, is_deleted=False)
        
        orderHead = OrderHead.objects.get(order_number=order, status=status, is_active=True, is_deleted=False)
        emails = [orderHead.dealer.email, orderHead.sr_id.email]
        email_list = notification.optional_email.split(",")
        email_list = list(filter(None, email_list))
        emails += email_list
        if obj_status=='New_order_placed' or obj_status=='Order_Cancelled':
            if notification.recipient_3=='BusinessHead':
                Userobj=User.objects.filter(is_active=True,groups__name="BusinessHead")
                for b in Userobj:
                    emails.append(b.email)
        try:
            email_sms=EmailTemplateMaster.objects.get(is_active=True,is_deleted=False,action_id=actiondata)
        except Exception:
            pass
        if status in ['Processing', 'SchemeApplied', 'Deleted']:
            pass
        elif status == "OrderPlaced":
            userList = User.objects.filter(groups__name="WHS", is_active=True)
            for user in userList:
                emails.append(user.email)
        elif status in ['InWarehouse', 'Dispatched', 'Delivered']:
            userList = User.objects.filter(groups__name="WHS", is_active=True)
            for user in userList:
                emails.append(user.email)
            emails.append(orderHead.warehouse_manager.email)
        elif status == "Reject":
            pass
        elif status == "OnHold":
            pass
        elif status == "PartialDispatched":
            pass
        # groups = [notification.recipient_1, notification.recipient_2, notification.recipient_3]
        # userList = User.objects.get(groups__name__in=groups, is_active=True)
        # emails = [user.email for user in userList]
        # emails.append(notification.optional_email)
        return [emails,email_sms]
    except Alerts.DoesNotExist:
        return []
    except Action.DoesNotExist:
        return []
    except Notification_role.DoesNotExist:
        return []
    except OrderHead.DoesNotExist:
        return []
