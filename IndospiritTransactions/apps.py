from django.apps import AppConfig


class IndospirittransactionsConfig(AppConfig):
    name = 'IndospiritTransactions'
