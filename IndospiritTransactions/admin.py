from django.contrib import admin

# Register your models here.
from . import models


class OrderHeadAdmin(admin.ModelAdmin):
    list_display = ('order_id', 'dealer', 'brand', 'sr_id', 'status', 'order_number', 'order_date', 'total_amount',
                    'discount_amount', 'payable_amount', 'DMS_order_number')


admin.site.register(models.OrderHead, OrderHeadAdmin)


class OrderDetailsAdmin(admin.ModelAdmin):
    list_display = ('order_detail_id', 'order_id', 'product_id', 'sku_code', 'product_short_name', 'size_capacity',
                    'size_capacity_unit', 'gst_rate', 'sgst_rate', 'cgst_rate', 'other_tax_rate', 'pattern_color',
                    'product_description_feature', 'warranty_months', 'best_buying_price', 'dealer_price',
                    'max_retail_price', 'quantity', 'discount_amount_per_unit', 'discount_percent_per_unit',
                    'total_amount')


admin.site.register(models.OrderDetails, OrderDetailsAdmin)


class OrderAllowanceDetailsAdmin(admin.ModelAdmin):
    list_display = ('order_detail_id', 'product_id', 'sku_code', 'order_id', 'allowance_percent', 'allowance_amount',
                    'max_allowance_percent', 'max_allowance_amount')


admin.site.register(models.OrderAllowanceDetails, OrderAllowanceDetailsAdmin)


class OrderTieUpDetailsAdmin(admin.ModelAdmin):
    list_display = ('order_detail_id', 'product_id', 'sku_code', 'order_id', 'tie_up_percent', 'tie_up_amount',
                    'max_tie_up_percent', 'max_tie_up_amount')


admin.site.register(models.OrderTieUpDetails, OrderTieUpDetailsAdmin)


class OrderSchemeDetailsSellThroughAdmin(admin.ModelAdmin):
    list_display = ('order_detail_id', 'product_id', 'sku_code', 'order_id', 'scheme_percent', 'scheme_amount',
                    'scheme_name', 'scheme_code', 'max_scheme_percent', 'max_scheme_amount')


admin.site.register(models.OrderSchemeDetailsSellThrough, OrderSchemeDetailsSellThroughAdmin)


class CashDiscountTransactionAdmin(admin.ModelAdmin):
    list_display = ('dealer', 'order', 'discount_percent', 'discount_amount', 'days')


admin.site.register(models.CashDiscountTransaction, CashDiscountTransactionAdmin)


class CreditNotesAdmin(admin.ModelAdmin):
    list_display = ('tieup_sell_through', 'dealer', 'note_type', 'credit_amount', 'order_id', 'order_detail_id', 'description')


admin.site.register(models.CreditNotes, CreditNotesAdmin)


class OrderDispatchHeadAdmin(admin.ModelAdmin):
    list_display = ('dispatch_id', 'dispatched_by', 'dispatched_on', 'order_id')


admin.site.register(models.OrderDispatchHead, OrderDispatchHeadAdmin)


class OrderDispatchDetailsAdmin(admin.ModelAdmin):
    list_display = ('dispatch_detail_id', 'sku_code', 'quantity', 'warehouse_id')


admin.site.register(models.OrderDispatchDetail, OrderDispatchDetailsAdmin)


class OrderEventHistoryAdmin(admin.ModelAdmin):
    list_display = ('order_id', 'order_status', 'created_by', 'created_on')
    readonly_fields = ('order_id', 'order_status', 'created_by', 'created_on')
    # list_display_links = None
    # actions = None

    def has_add_permission(self, request):
        return False

    def has_delete_permission(self, request, obj=None):
        return False

    def changeform_view(self, request, object_id=None, form_url='', extra_context=None):
        extra_context = extra_context or {}
        extra_context['show_save_and_continue'] = False
        extra_context['show_save'] = False
        return super(OrderEventHistoryAdmin, self).changeform_view(request, object_id, extra_context=extra_context)


admin.site.register(models.OrderEventHistory, OrderEventHistoryAdmin)


class LedgerAdmin(admin.ModelAdmin):
    list_display = [f.name for f in models.Ledger._meta.fields]


admin.site.register(models.Ledger, LedgerAdmin)


class PaymentAdmin(admin.ModelAdmin):
    list_display = [f.name for f in models.Payment._meta.fields]


admin.site.register(models.Payment, PaymentAdmin)


# class SpacialDiscountAdmin(admin.ModelAdmin):
#     list_display = [f.name for f in models.SpacialDiscount._meta.fields]
#
#
# admin.site.register(models.SpacialDiscount, SpacialDiscountAdmin)


class SpecialCDTypeAdmin(admin.ModelAdmin):
    list_display = [f.name for f in models.SpecialCDType._meta.fields]


admin.site.register(models.SpecialCDType, SpecialCDTypeAdmin)


class DebitNoteAdmin(admin.ModelAdmin):
    list_display = [f.name for f in models.DebitNote._meta.fields]


admin.site.register(models.DebitNote, DebitNoteAdmin)
