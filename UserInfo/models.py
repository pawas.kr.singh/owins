from django.conf import settings
from django.db import models

from IndospritCore.models import DocumentTypeMaster, BrandMaster, CityMaster, StateMaster, CashDiscountSlabsMaster


class DealerProfile(models.Model):
    profile_id = models.BigAutoField(primary_key=True)
    dealer = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.SET_NULL, null=True, related_name='dealer')
    brand = models.ForeignKey(BrandMaster, on_delete=models.PROTECT, blank=True, null=True)
    dealer_code = models.CharField(max_length=50, null=True)
    shipping_address = models.CharField(max_length=250, null=True, blank=True)
    company_address = models.CharField(max_length=250, null=True, blank=True)
    owner_name = models.CharField(max_length=100, null=True)
    region_id = models.CharField(max_length=50, null=True, blank=True)
    pincode = models.IntegerField(null=True, blank=True)
    email = models.CharField(max_length=100, null=True, blank=True)
    primary_contact = models.CharField(max_length=25, null=True)
    alternate_contact = models.CharField(max_length=25, null=True, blank=True)
    gst_no = models.CharField(max_length=50, null=True, blank=True)
    pan_no = models.CharField(max_length=50, null=True, blank=True)
    Aadhaar_No = models.CharField(max_length=50, null=True, blank=True)
    created_on = models.DateTimeField(auto_now_add=True)
    created_by = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.SET_NULL, null=True,
                                   related_name='dealer_created_by')
    modified_on = models.DateTimeField(auto_now=True)
    modified_by = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.SET_NULL, null=True,
                                    related_name='dealer_modified_by')
    company_name = models.CharField(max_length=250, null=True)
    cd_slab_code = models.ForeignKey(CashDiscountSlabsMaster, on_delete=models.SET_NULL, blank=True, null=True,
                                     related_name='cd_slab')
    is_active = models.BooleanField(default=True, null=True, blank=True)
    is_deleted = models.BooleanField(default=False, null=True, blank=True)
                                     
    def __str__(self):
        return self.dealer_code


class SRDealer(models.Model):
    sr_id = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.SET_NULL, null=True,
                              related_name='sr_dealer_id')
    dealer_id = models.ForeignKey(DealerProfile, on_delete=models.SET_NULL, null=True, related_name='sr_dealer')

    def __str__(self):
        return str(self.dealer_id)

    def __int__(self):
        return self.dealer_id


def upload_to(instance, filename):
    return 'user/documents/%s' % filename


class UserDocuments(models.Model):
    document_id = models.BigAutoField(primary_key=True)
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.SET_NULL, null=True,
                             related_name='dealer_document_created_by')
    document_type = models.ForeignKey(DocumentTypeMaster, on_delete=models.SET_NULL, null=True, related_name='doc_type')
    document_number = models.CharField(max_length=100, null=True, blank=True)
    document = models.CharField(max_length=300, null=True, blank=True)
    document_file = models.FileField(upload_to=upload_to, null=True)

    def __str__(self):
        return str(self.document)

    def __int__(self):
        return self.document


class OrderNotes(models.Model):
    id = models.AutoField(primary_key=True)
    order_id = models.IntegerField(null=True, blank=True)
    dealer_id = models.IntegerField(null=True, blank=True)
    sr_id = models.IntegerField(null=True, blank=True)
    created_by = models.IntegerField(null=True, blank=True)
    created_on = models.DateTimeField(auto_now_add=True, null=True, blank=True)
    description = models.TextField(blank=True, null=True)
    is_active = models.BooleanField(default=True, null=True, blank=True)
    is_deleted = models.BooleanField(default=False, null=True, blank=True)


class Alerts(models.Model):
    alerts_type=models.CharField(max_length=100,blank=True, null=True)
    is_active = models.BooleanField(default=True, null=True, blank=True)
    is_deleted = models.BooleanField(default=False, null=True, blank=True)


class Action(models.Model):
    alerts=models.ForeignKey(Alerts, on_delete=models.SET_NULL, null=True, related_name='alertspk')
    send_type=models.CharField(max_length=100,blank=True, null=True)
    is_active = models.BooleanField(default=True, null=True, blank=True)
    is_deleted = models.BooleanField(default=False, null=True, blank=True)


class Notification_role(models.Model):
    action=models.ForeignKey(Action, on_delete=models.SET_NULL, null=True, related_name='actionpk')
    recipient_1=models.CharField(max_length=100,blank=True, null=True)
    recipient_2=models.CharField(max_length=100,blank=True, null=True)
    recipient_3=models.CharField(max_length=100,blank=True, null=True)
    optional_email=models.CharField(max_length=100,blank=True, null=True)
    is_active = models.BooleanField(default=True, null=True, blank=True)
    is_deleted = models.BooleanField(default=False, null=True, blank=True)


class EmailTemplateMaster(models.Model):
    action = models.ForeignKey(Action, on_delete=models.SET_NULL, null=True, related_name='action_pk')
    subject = models.CharField(max_length=100, blank=True, null=True)
    email_content = models.TextField(blank=True, null=True)
    sms_content = models.TextField(blank=True, null=True)
    is_active = models.BooleanField(default=True, null=True, blank=True)
    is_deleted = models.BooleanField(default=False, null=True, blank=True)


""" EMAIL AND SMS SETTINGS AND TEMPLATES """


class AlertMaster(models.Model):
    ALERT_TYPES = (
        ('OrderPlaced', 'New Order Placed'),
        ('Deleted', 'Order Cancelled'),
        ('Dispatched', 'Order Dispatched'),
        ('PaymentDone', 'Payment Received'),
        ('SchemeApplied', 'Scheme Applied'),
        ('Processing', 'Submitted For Scheme'),
        ('OnHold', 'Order On Hold'),
        ('InWarehouse', 'In Warehouse'),
        ('CDType3Approval', 'Credit Note Type 3 Approval'),
        ('CDType3Approved', 'Credit Note Type 3 Approved'),
        ('DebitNoteApproval', 'Debit Note Approval'),
        ('DebitNoteApproved', 'Debit Note Approved')
    )

    ACTION_TYPES = (
        ('SMS', 'SMS'),
        ('EMAIL', 'Email'),
        ('BOTH', 'SMS & Email')
    )

    alert_type = models.CharField(choices=ALERT_TYPES, max_length=100)
    action_type = models.CharField(choices=ACTION_TYPES, default="EMAIL", max_length=100)
    optional_email = models.TextField(blank=True, null=True)
    is_active = models.BooleanField(default=True, null=True, blank=True)
    is_deleted = models.BooleanField(default=False, null=True, blank=True)

    def __str__(self):
        return self.alert_type


class SendAlert(models.Model):
    action = models.ForeignKey(AlertMaster, on_delete=models.CASCADE)
    role_name = models.CharField(max_length=100, blank=True, null=True)
    is_active = models.BooleanField(default=True, null=True, blank=True)
    is_deleted = models.BooleanField(default=False, null=True, blank=True)

    def __str__(self):
        return self.role_name


class AlertTemplateMaster(models.Model):
    alert = models.ForeignKey(AlertMaster, on_delete=models.CASCADE)
    subject = models.CharField(max_length=200, blank=True, null=True)
    email_content = models.TextField(blank=True, null=True)
    sms_content = models.TextField(blank=True, null=True)
    is_active = models.BooleanField(default=True, null=True, blank=True)
    is_deleted = models.BooleanField(default=False, null=True, blank=True)
