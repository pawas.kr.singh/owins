import csv
import datetime
import re
import os,json
import decimal
from . import forms, models
from ClassicUserAccounts.models import User
from IndospiritTransactions.models import OrderDetails
from IndospritCore.models import AllowanceMaster, BrandMaster, \
    CashDiscountSlabsMaster, CityMaster, DealerSalesTarget, DealersBrand, \
    ProductCategoryMaster, ProductMaster, ProductPriceHistory, \
    SchemeCategoryMaster, SchemeMaster, SchemeProductMaster, \
    SchemeSellThroughMaster, StateMaster, StockLevelMaster, StockRegister, \
    TaxSlabMaster, TieUpMaster, WarehouseAdmin, WarehouseMaster
from Owims.settings import import_settings
from UserInfo.models import DealerProfile
from django.conf import settings
from django.contrib import messages
from django.contrib.auth.models import Group
from django.core.paginator import EmptyPage, PageNotAnInteger, Paginator
from django.db.models import ProtectedError, Q, Count, Max, Q, Sum
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import get_object_or_404, redirect, render
from django.core.mail import EmailMultiAlternatives
from django.conf import settings
from UserInfo.models import SRDealer
from UserInfo import status_in


class MenuList():
    menu = None
    children = []


menu_list = None

def load_city(request,cid):
    if request.is_ajax():
        street = CityMaster.objects.filter(state_id__state_name=cid,is_active=True,is_deleted=False)
        streetlist = []
        for getstreet in street:
            street = {}
            street['cityid'] = getstreet.city_id
            street['cityname'] = getstreet.city_name
            street['citycode'] = str(getstreet.city_code)
            streetlist.append(street)
        data = json.dumps(streetlist)
    else:
        street = CityMaster.objects.filter(state_id__state_name=cid,is_active=True,is_deleted=False)
        streetlist = []
        for getstreet in street:
            street = {}
            street['cityid'] = getstreet.city_id
            street['cityname'] = getstreet.city_name
            street['citycode'] = str(getstreet.city_code)
            streetlist.append(street)
        data = json.dumps(streetlist)
    mimetype = 'application/json'
    return HttpResponse(data, mimetype)


def generate_category(request):
    json_data = ""
    items = models.ProductCategoryMaster.objects.all()
    parents = items.filter(parent__isnull=True)
    unordered_list = generate_child(parents, items, json_data)
    return HttpResponse(unordered_list)


def generate_child(menu, table, sb):
    sb = sb + '['
    if len(menu) > 0:
        for m in menu:
            sb = sb + '{"id":"' + str(m.pk) + '", "label": "' + m.category_name + '"'

            parents = table.filter(parent=m.pk)
            if len(parents) > 0:
                sb_submenu = ', "children":'
                sb = sb + generate_child(parents, table, sb_submenu)

            sb = sb + '},'
    sb = sb[:-1]
    sb = sb + ']'
    return sb


def brands(request):
    brands = models.BrandMaster.objects.filter(is_active=True)
    return render(request, 'brand/brand.html', {'brands': brands})


def create_brand(request):
    form_class = forms.BrandMasterForm
    if request.method == 'GET':
        return render(request, 'brand/brand_create.html', {'form': form_class})
    elif request.method == 'POST':
        form_class = forms.BrandMasterForm(request.POST)
        if form_class.is_valid():
            brand = form_class.save(commit=False)
            brand.created_by = request.user
            brand.is_active = True
            brand.save()
        else:
            return render(request, 'brand/brand_create.html', {'form': form_class})
    elif request.method == 'PUT':
        form_class = forms.BrandMasterForm(request.POST)
        if form_class.is_valid():
            brand = form_class.save(commit=False)
            brand.created_by = request.user
            brand.save()
        return render(request, 'brand/brand_create.html', {'form': form_class})


def edit_brand(request, Bid):
    if request.method == 'GET':
        instance = get_object_or_404(BrandMaster, brand_code=Bid)
        form_class = forms.BrandMasterForm(request.POST or None, instance=instance)
        return render(request, 'brand/brand_create.html', {'form': form_class})

    elif request.method == 'POST':
        instance = get_object_or_404(BrandMaster, brand_code=Bid)
        form_class = forms.BrandMasterForm(request.POST or None, instance=instance)
        if form_class.is_valid():
            form_class.save()
        return redirect('/master/brand')


def delete_brand(Bid):
    brand = BrandMaster.objects.get(brand_code=Bid)
    brand.delete()
    return HttpResponseRedirect('/master/brand')


def product_category(request):
    product_category = models.ProductCategoryMaster.objects.all().order_by('category_id', 'parent')
    return render(request, 'product_category/product_category.html', {'product_category': product_category})


def create_product_category(request):
    form_class = forms.ProductCategoryMasterForm
    if request.method == 'GET':
        return render(request, 'product_category/product_category_create.html',
                      {'product_category_create_form': form_class})
    elif request.method == 'POST':
        form_class = forms.ProductCategoryMasterForm(request.POST)
        if form_class.is_valid():
            product_category = form_class.save(commit=False)
            product_category.created_by = request.user
            if form_class.cleaned_data["parent"] is not None and form_class.cleaned_data["parent"] is not '':
                product_category.parent = form_class.cleaned_data["parent"].pk
            product_category.save()
        return render(request, 'product_category/product_category_create.html',
                      {'product_category_create_form': form_class})
    elif request.method == 'PUT':
        form_class = forms.ProductCategoryMasterForm(request.POST)
        if form_class.is_valid():
            product_category = form_class.save(commit=False)
            product_category.created_by = request.user
            product_category.save()
        return render(request, 'product_category/product_category_create.html',
                      {'product_category_create_form': form_class})


def edit_product_category(request, PCid):
    if request.method == 'GET':
        instance = get_object_or_404(ProductCategoryMaster, category_id=PCid)
        form_class = forms.ProductCategoryMasterForm(request.POST or None, instance=instance)
        return render(request, 'product_category/product_category_create.html',
                      {'product_category_create_form': form_class})

    elif request.method == 'POST':
        instance = get_object_or_404(ProductCategoryMaster, category_id=PCid)
        form_class = forms.ProductCategoryMasterForm(request.POST or None, instance=instance)
        if form_class.is_valid():
            form_class.save()
        else:
            return render(request, 'product_category/product_category_create.html',
                          {'product_category_create_form': form_class})
        return redirect('/master/product_category')


def product_list(request):
    searchValue, querycount = "", ""
    if 'selected_brand' in request.session:
        products = models.ProductMaster.objects.filter(
            brand=request.session["selected_brand"],
            is_active=True).order_by('-created_on').order_by('-modified_on')
        # productCount = models.ProductMaster.objects.filter(
        #     brand=request.session["selected_brand"], is_active=True).count()
    else:
        products = models.ProductMaster.objects.filter(
            is_active=True).order_by('-created_on').order_by('-modified_on')
        # productCount = models.ProductMaster.objects.filter(is_active=True).count()

    if request.method == 'POST':
        if 'search_product' in request.POST:
            if "search" in request.POST:
                capacity = ''
                unit = ''
                searchValue = request.POST.get('search', None)

                # .................................
                products = products.filter(
                    Q(sku_code__icontains=searchValue) |
                    Q(max_retail_price__icontains=searchValue) |
                    Q(size_capacity__icontains=searchValue) |
                    Q(size_capacity_unit__icontains=searchValue) |
                    Q(product_category__category_name__icontains=searchValue) |
                    Q(TaxSlabCode__slab_name__icontains=searchValue))


        if 'export' in request.POST:
            product = export_product(request)
            file_path = os.getcwd()
            paths = file_path + '/upload/product_not_insert/product.csv'
            if os.path.exists(paths):
                with open(paths, "rb") as output:
                    response = HttpResponse(output.read(), content_type="application/vnd.ms-excel")
                    response['Content-Disposition'] = 'attachment; filename="owims_sku.csv"'
                    return response

    # stockLevel = StockLevelMaster.objects.filter(
    #     product__in=products, is_active=True, is_deleted=False)
    # ............................................
    groups = request.user.groups.all()
    groups = [group.name for group in groups]
    if "WHSUSER" in groups:
        stockLevel = models.StockLevelMaster.objects.filter(
            product__in=products, is_active=True, is_deleted=False)
        self_warehouses = models.WarehouseAdmin.objects.filter(
            user=request.user, is_active=True, is_deleted=False)
        self_warehouses = [x.warehouse for x in self_warehouses]
        stockLevel = stockLevel.filter(warehouse__in=self_warehouses)
        products = stockLevel
    # ............................................
    data = []
    for _x_item in products:
        if "WHSUSER" in groups:
            PM = _x_item.product
        else:
            PM = _x_item

        get_product = {}
        get_product['product_id'] = PM.pk
        get_product['sku_code'] = PM.sku_code
        get_product['max_retail_price'] = PM.max_retail_price
        get_product['size_capacity'] = PM.size_capacity
        get_product['size_capacity_unit'] = PM.size_capacity_unit
        get_product['TaxSlabCode'] = PM.TaxSlabCode
        category = ProductCategoryMaster.objects.filter(is_active=True, is_deleted=False)
        getproduct = []
        for PC in category:
            if PM.product_category is not None:
                pass
            else:
                continue
            CID = PM.product_category_id
            PCM = ProductCategoryMaster.objects.get(pk=CID)
            getproduct.append(PCM.category_name)
            PID = PCM.parent
            try:
                PCM = ProductCategoryMaster.objects.get(pk=PID)
            except Exception:
                PCM = ''
            if PCM != '':
                getproduct.append(PCM.category_name)
                PID = PCM.parent
            try:
                PCM = ProductCategoryMaster.objects.get(pk=PID)
            except Exception:
                PCM = ''
            if PCM != '':
                getproduct.append(PCM.category_name)
                PID = PCM.parent
            try:
                PCM = ProductCategoryMaster.objects.get(pk=PID)
            except Exception:
                PCM = ''
            if PCM != '':
                getproduct.append(PCM.category_name)
                PID = PCM.parent
            try:
                PCM1 = ProductCategoryMaster.objects.get(pk=PID)
            except Exception:
                PCM1 = ''
            if PCM1 != '':
                getproduct.append(PCM1.category_name)
                PID = PCM1.parent
            break
        getproduct.reverse()
        datas = ''
        count = 0
        getcount = len(getproduct)
        for i in getproduct:
            count += 1
            if count == getcount:
                datas += '<b>%s</b>-->' % (str(i))
            else:
                datas += str(i) + '-->'
        get_product['product_category'] = datas.rstrip('-->')
        data.append(get_product)

    querycount = str(len(data)) + ' Record found.'
    page_style = request.GET.get("page_style", settings.DEFAULT_PAGING_VIEW)
    # paginator = Paginator(data, settings.PAGE_SIZE)
    paginator = Paginator(data, 10)
    try:
        page_index = request.GET.get("page", 1)
        data_list = paginator.page(page_index)
    except PageNotAnInteger:
        data_list = paginator.page(1)
    except EmptyPage:
        data_list = paginator.page(paginator.num_pages)

    context = {
        'data_list': data_list,
        'search': searchValue,
        'querycount': querycount,
        'productCount': len(data)
    }
    if request.GET.get('list', None):
        context['flagList'] = True
    else:
        context['flagList'] = False
    if page_style == 'grid':
        return render(request, 'products/product_grid_view.html', context)
    else:
        return render(request, 'products/product_list_view.html', context)


def create_product(request):
    form_class = forms.ProductMasterForm
    queryset = models.SchemeCategoryMaster.objects.filter(is_active=True)
    if request.method == 'GET':
        context = {'product_create_form': form_class, 'queryset': queryset}
        return render(request, 'products/products_create.html', context=context)
    elif request.method == 'POST':
        try:
            products = ProductMaster.objects.get(
                sku_code=request.POST.get('sku_code'), is_active=True, is_deleted=False)
            messages.add_message(request, messages.ERROR, 'sku_code already Exists!!!')
        except ProductMaster.DoesNotExist:
            products = None
        if products is None:
            form_class = forms.ProductMasterForm(request.POST)
            if form_class.is_valid():
                product = form_class.save(commit=False)
                product.created_by = request.user
                product.sku_code = request.POST.get('sku_code')
                product.save()
                category_code=SchemeProductMaster()
                #category_code = category_code.save(commit=False)
                category_code.scheme_category_code = str(request.POST.get('product_category_code'))
                category_code.brand = product.brand
                category_code.product=product
                category_code.save()
                messages.add_message(request, messages.SUCCESS, 'Product created successfully...')
            else:
                context = {'product_create_form': form_class}
                return render(request, 'products/products_create.html', context=context)
            return redirect('/master/products?page_style=list')
        else:
            context = {'product_create_form': form_class,}
            return render(request, 'products/products_create.html', context=context)
            messages.add_message(request, messages.ERROR, 'sku_code already Exists!!!')

def edit_product(request, Pid):
    dprice=''
    rprice=''
    bestprice=''
    if request.method == 'GET':
        instance = get_object_or_404(ProductMaster, product_id=Pid)
        instance1 = get_object_or_404(SchemeProductMaster, product_id=Pid)
        queryset=models.SchemeCategoryMaster.objects.filter(is_active=True)
        form_class = forms.ProductMasterForm(request.POST or None, instance=instance)
        context = {'product_create_form': form_class, 'product': instance, 'disabled': True,'instance1':instance1,'queryset': queryset}
        return render(request, 'products/products_create.html', context=context)

    elif request.method == 'POST':
        instance = ProductMaster.objects.get(product_id=Pid)
        dprice=instance.dealer_price
        rprice=instance.max_retail_price
        bestprice=instance.best_buying_price
        form_class = forms.ProductMasterForm(request.POST or None, instance=instance)
        if form_class.is_valid():
            product = form_class.save(commit=False)
            category_code=SchemeProductMaster.objects.get(product_id=Pid)
            category_code.scheme_category_code = str(request.POST.get('product_category_code'))
            category_code.brand = product.brand
            category_code.product=product
            category_code.save()
            form_class.save()

            """ need to changed """
            # businessheadobj=User.objects.filter(is_active=True,groups__name="BusinessHead")
            # html_content=''
            # if businessheadobj:
            #     emails=[]
            #     for user in businessheadobj:
            #         emails.append(user.email)
            #     if dprice!=product.dealer_price:
            #         html_content+= "Previous Dealer Price ₹"+str(dprice)+" Current Dealer Price ₹"+str(product.dealer_price)+" , "
            #     if rprice!=product.max_retail_price:
            #         html_content+= " Previous Max Retail Price ₹"+str(dprice)+" Current Max Retail Price ₹"+str(product.max_retail_price)+" , "
            #     if bestprice!=product.best_buying_price:
            #         html_content+= "Previous Best Buying Price ₹"+str(bestprice)+" Current Best Buying Price ₹"+str(product.best_buying_price)+" , "
            #     html_content=html_content.rstrip(',')+"is updated"
            #     subject=" financial number changed"
            #     email = EmailMultiAlternatives(subject, html_content, settings.DEFAULT_FROM_EMAIL, to=emails)
            #     email.send()
            messages.add_message(request, messages.SUCCESS, 'Product updated successfully...')
        else:
            context = {'product_create_form': form_class, 'product': instance, 'disabled': True}
            return render(request, 'products/products_create.html', context=context)
        return redirect('/master/products?page_style=list')


def delete_product(request, Pid):
    try:
        stock = StockLevelMaster.objects.get(
            product_id=Pid, is_active=True, is_deleted=False)
    except Exception:
        stock = None
    if stock is None:
        success_flag, error_flag = False, False
        try:
            product = ProductMaster.objects.get(
                product_id=Pid, is_active=True, is_deleted=False)
            product.is_active = False
            product.is_deleted = True
            product.save()
            success_flag = True
        except ProductMaster.DoesNotExist:
            pass
        except ProtectedError:
            error_flag = True

        try:
            Scheme = SchemeProductMaster.objects.get(
                product_id=Pid, is_active=True, is_deleted=False)
            Scheme.is_active = False
            Scheme.is_deleted = True
            Scheme.save()
            success_flag = True
        except SchemeProductMaster.DoesNotExist:
            pass
        except ProtectedError:
            error_flag = True

        try:
            sell = SchemeSellThroughMaster.objects.get(
                product_id=Pid, is_active=True, is_deleted=False)
            sell.is_active = False
            sell.is_deleted = True
            sell.save()
            success_flag = True
        except SchemeSellThroughMaster.DoesNotExist:
            pass
        except ProtectedError:
            error_flag = True

        if error_flag:
            messages.add_message(request, messages.ERROR, 'This Product cannot be deactivated!!!')
        elif success_flag:
            messages.add_message(request, messages.SUCCESS, 'Product deactivated successfully...')

        # try:
        #     messages.add_message(request, messages.SUCCESS, 'Product deactivated successfully...')
        # except ProtectedError:
        #     messages.add_message(request, messages.ERROR, 'This Product cannot be deactivated!!!')
    else:
        messages.add_message(request, messages.ERROR, 'This Product cannot be deactivated!!!')
    #return JsonResponse(error_message)
    #product.delete()
    #Scheme.delete()
    return HttpResponseRedirect('/master/products?page_style=list')


def import_products_target(request):
    invalid_rows = []
    index = 0
    insert = 0
    not_insert = 0
    update = 0
    process = 0
    not_exist = []
    lblMsg = ''
    filexlsx = False
    if request.method == 'POST':

        if 'Save' in request.POST:
            file = request.FILES.get('Product_Master')
            if file != None and file != '':
                filexlsx = str(file._name).endswith('csv')
            if filexlsx == True:
                decoded_file = request.FILES['Product_Master'].read().decode('utf-8').splitlines()
                reader = csv.DictReader(decoded_file)
                for row in reader:
                    process += 1
                    if index == 0:
                        flag = True
                        for x in import_settings.PRODUCT_MASTER_IMPORT_KEY:
                            if x not in row.keys():
                                not_exist.append(x)
                    set_exist = len(not_exist)
                    if set_exist == 0:
                        TSM = ''
                        if row['brand'] != '' and row['SKU'] != '' and row['main_category'] != '' and \
                                row['category'] != '' and row['sub_category-1'] != ''  \
                                and row['MRP'] != '' and row['DP'] != '':
                            row['MRP']=row['MRP'].replace(',','')
                            row['DP']=row['DP'].replace(',','')
                            
                            if float(row['MRP']) >= 0 and float(row['DP']) >= 0:
                                PCM5 = ProductCategoryMaster.objects.order_by('-pk').values_list('pk')
                                if not PCM5:
                                    pk_id = 0
                                else:
                                    pk_id = PCM5[0][0]

                                myBrand = None
                                try:
                                    # brand = BrandMaster.objects.get(brand_name=row['brand'].lower())
                                    brand = BrandMaster.objects.filter(brand_name__icontains=row['brand']).first()
                                    myBrand = brand
                                    TSM = TaxSlabMaster.objects.get(slab_name=row['TaxSlabCode'])
                                    if 'main_category' in row.keys():
                                        try:
                                            PCM = ProductCategoryMaster.objects.get(category_name=row['main_category'])
                                            # PCM = ProductCategoryMaster.objects.filter(
                                            #     category_name=row['main_category']).first()
                                        except Exception:
                                            PCM = ProductCategoryMaster()
                                        # PCM.brand = BrandMaster.objects.get(brand_name__icontains=row['brand'])
                                        PCM.brand = myBrand
                                        PCM.category_name = row['main_category']
                                        PCM.created_by = User.objects.get(id=request.user.id, is_active=True)
                                        PCM.created_on = datetime.datetime.now()
                                        PCM.modified_by = User.objects.get(id=request.user.id, is_active=True)
                                        PCM.modified_on = datetime.datetime.now()
                                        PCM.save()
                                    if 'category' in row.keys():
                                        try:
                                            PCM1 = ProductCategoryMaster.objects.get(category_name=row['category'])
                                            # PCM1 = ProductCategoryMaster.objects.filter(
                                            #     category_name=row['category']).first()
                                            PCM1.parent = PCM.pk
                                        except Exception:
                                            PCM1 = ProductCategoryMaster()
                                            PCM1.parent = PCM.pk
                                        # PCM1.brand = BrandMaster.objects.get(brand_name__icontains=row['brand'])
                                        PCM1.brand = myBrand
                                        PCM1.category_name = row['category']
                                        PCM1.created_by = User.objects.get(id=request.user.id, is_active=True)
                                        PCM1.created_on = datetime.datetime.now()
                                        PCM1.modified_by = User.objects.get(id=request.user.id, is_active=True)
                                        PCM1.modified_on = datetime.datetime.now()
                                        PCM1.save()
                                        # if PCM1.parent==pk_id:
                                        #     pk_id = PCM1.pk
                                    if 'sub_category-1' in row.keys():
                                        try:
                                            PCM2 = ProductCategoryMaster.objects.get(category_name=row['sub_category-1'])
                                            # PCM2 = ProductCategoryMaster.objects.filter(
                                            #     category_name=row['sub_category-1']).first()
                                            PCM2.parent = PCM1.pk
                                        except Exception:
                                            PCM2 = ProductCategoryMaster()
                                            PCM2.parent = PCM1.pk
                                        # PCM2.brand = BrandMaster.objects.get(brand_name=row['brand'].lower())
                                        PCM2.brand = myBrand
                                        PCM2.category_name = row['sub_category-1']
                                        PCM2.created_by = User.objects.get(id=request.user.id, is_active=True)
                                        PCM2.created_on = datetime.datetime.now()
                                        PCM2.modified_by = User.objects.get(id=request.user.id, is_active=True)
                                        PCM2.modified_on = datetime.datetime.now()
                                        PCM2.save()
                                        # if PCM2.parent==pk_id:
                                        #     pk_id = PCM2.pk
                                    if 'sub_category-2' in row.keys():
                                        try:
                                            # PCM3 = ProductCategoryMaster.objects.filter(
                                            #     category_name=row['sub_category-2'], is_active=True).first()
                                            PCM3 = ProductCategoryMaster.objects.get(
                                                category_name=row['sub_category-2'], is_active=True)
                                            PCM3.parent = PCM2.pk
                                        except ProductCategoryMaster.DoesNotExist:
                                            PCM3 = ProductCategoryMaster()
                                            PCM3.parent = PCM2.pk
                                        # PCM3.brand = BrandMaster.objects.get(brand_name=row['brand'].lower())
                                        PCM3.brand = myBrand
                                        PCM3.category_name = row['sub_category-2']
                                        PCM3.created_by = User.objects.get(id=request.user.id, is_active=True)
                                        PCM3.created_on = datetime.datetime.now()
                                        PCM3.modified_by = User.objects.get(id=request.user.id, is_active=True)
                                        PCM3.modified_on = datetime.datetime.now()
                                        PCM3.save()
                                        pk_id = PCM3.pk
                                    if 'sub_category-3' in row.keys():
                                        if row['sub_category-3'] != '':
                                            try:
                                                PCM4 = ProductCategoryMaster.objects.get(
                                                    category_name=row['sub_category-3'], is_active=True)
                                                # PCM4 = ProductCategoryMaster.objects.filter(
                                                #     category_name=row['sub_category-3'], is_active=True).first()
                                                PCM4.parent = PCM3.pk
                                            except ProductCategoryMaster.DoesNotExist:
                                                PCM4 = ProductCategoryMaster()
                                                PCM4.parent = PCM3.pk
                                            #PCM4.brand = BrandMaster.objects.get(brand_name=row['brand'].lower())
                                            PCM4.brand = myBrand
                                            PCM4.category_name = row['sub_category-3']
                                            PCM4.created_by = User.objects.get(id=request.user.id, is_active=True)
                                            PCM4.created_on = datetime.datetime.now()
                                            PCM4.modified_by = User.objects.get(id=request.user.id, is_active=True)
                                            PCM4.modified_on = datetime.datetime.now()
                                            PCM4.save()
                                            pk_id = PCM4.pk
                                    try:
                                        PM = ProductMaster.objects.get(sku_code=str(row['SKU']))
                                        if row['MRP'] != '' and row['DP'] != '':
                                            if PM.dealer_price != float(row['DP']) or PM.max_retail_price != float(row['MRP']):
                                                PPH = ProductPriceHistory()
                                                insert += 1
                                                PPH.product = PM
                                                PPH.dealer_price = row['DP']
                                                PPH.max_retail_price = row['MRP']
                                                if 'best_buying_price' in row.keys():
                                                    row['best_buying_price']=row['best_buying_price'].replace(',','')
                                                    if row["best_buying_price"] != '' and float(row["best_buying_price"]) > 0:
                                                        PPH.best_buying_price = row["best_buying_price"]
                                                    else:
                                                        PPH.best_buying_price = 0
                                                elif 'Best Buying Price' in row.keys():
                                                    row['Best Buying Price']=row['Best Buying Price'].replace(',','')
                                                    if row["Best Buying Price"] != '' and float(row["Best Buying Price"]) > 0:
                                                        PPH.best_buying_price = row["Best Buying Price"]
                                                    else:
                                                        PPH.best_buying_price = 0
                                                else:
                                                    PPH.best_buying_price = 0
                                                PPH.gst_rate = TSM.gst_rate
                                                PPH.sgst_rate = TSM.sgst_rate
                                                PPH.cgst_rate = TSM.cgst_rate
                                                PPH.other_tax_rate = TSM.other_tax_rate
                                                PPH.created_by = User.objects.get(id=request.user.id, is_active=True)
                                                PPH.created_on = datetime.datetime.now()
                                                PPH.modified_by = User.objects.get(id=request.user.id, is_active=True)
                                                PPH.modified_on = datetime.datetime.now()
                                                PPH.save()
                                        else:
                                            error_MRP = {}
                                            if row['MRP'] == '':
                                                error_MRP['error_message'] = 'Column <MRP> at row ' + str(
                                                    process) + ' contain null values'
                                                invalid_rows.append(error_MRP)
                                            error_DP = {}
                                            if row['DP'] == '':
                                                error_DP['error_message'] = 'Column <DP> at row ' + str(
                                                    process) + ' contain null values'
                                                invalid_rows.append(error_DP)
                                            not_insert += 1
                                        update += 1
                                    except ProductMaster.DoesNotExist:
                                        PM = ProductMaster()
                                        insert += 1
                                        # PM.brand = BrandMaster.objects.get(brand_name=row['brand'].lower())
                                    PM.brand = myBrand
                                    PM.product_category = ProductCategoryMaster.objects.get(
                                        pk=pk_id, is_active=True)
                                    PM.sku_code = row['SKU']
                                    size_capacity = row['size_capacity_ltr']
                                    capacity = re.split(r'\s+(?=\d)|(?<=\d)\s+', size_capacity)
                                    if '-' in capacity[0]:
                                        match = re.match(r"([0-9]+)([a-z]+)", capacity[0], re.I)
                                        if match:
                                            items = match.groups()
                                            PM.size_capacity = items[0]
                                            PM.size_capacity_unit = items[1] + '-' + capacity[1]
                                        elif '/' in capacity[0]:
                                            capcity = re.split('/', capacity[0])
                                            match = re.match(r"([0-9]+)([a-z]+)", capcity[1], re.I)
                                            if match:
                                                items = match.groups()
                                                PM.size_capacity = capcity[0] + '/' + items[0]
                                                PM.size_capacity_unit = items[1] + '-' + capacity[1]

                                        else:
                                            PM.size_capacity = capacity[0]
                                            PM.size_capacity_unit = capacity[1].lower()
                                    elif len(capacity) == 1:
                                        match = re.match(r"([0-9]+)([a-z]+)", capacity[0], re.I)
                                        if match:
                                            items = match.groups()
                                            PM.size_capacity = items[0]
                                            PM.size_capacity_unit = items[1]
                                    else:
                                        PM.size_capacity = capacity[0]
                                        PM.size_capacity_unit = capacity[1].lower()
                                    PM.TaxSlabCode = TaxSlabMaster.objects.get(pk=TSM.pk)
                                    PM.pattern_color = row['series_colour'].lower()
                                    PM.product_description_feature = row['features'].lower()
                                    PM.dealer_price = row['DP'].lower()
                                    PM.max_retail_price = row['MRP'].lower()
                                    PM.warranty_months = 0
                                    if 'best_buying_price' in row.keys():
                                        row['best_buying_price']=row['best_buying_price'].replace(',','')
                                        if row["best_buying_price"] != '' and float(row["best_buying_price"]) > 0:
                                            PM.best_buying_price = float(row["best_buying_price"])
                                        else:
                                            PM.best_buying_price = 0
                                    elif 'Best Buying Price' in row.keys():
                                        row['Best Buying Price']=row['Best Buying Price'].replace(',','')
                                        if row["Best Buying Price"] != '' and float(row["Best Buying Price"]) > 0:
                                            PM.best_buying_price = row["Best Buying Price"]
                                        else:
                                            PM.best_buying_price = 0
                                    else:
                                        PM.best_buying_price = 0
                                    PM.created_by = User.objects.get(id=request.user.id, is_active=True)
                                    PM.created_on = datetime.datetime.now()
                                    PM.modified_by = User.objects.get(id=request.user.id, is_active=True)
                                    PM.modified_on = datetime.datetime.now()
                                    PM.save()
                                    allowance = row['product_category_code']
                                    code = ''
                                    if 'B' in allowance:
                                        code = allowance[0:1].upper()
                                    if 'M' in allowance:
                                        code = allowance[0:1].upper()
                                    if 'V' in allowance:
                                        code = allowance[0:3].upper()
                                    try:
                                        SCM = SchemeCategoryMaster.objects.get(scheme_category_code=code)
                                    except SchemeCategoryMaster.DoesNotExist:
                                        SCM = ''
                                    if SCM != '':
                                        try:
                                            SPM = SchemeProductMaster.objects.get(product_id=PM.product_id)
                                        except Exception:
                                            SPM = SchemeProductMaster()
                                        SPM.brand = myBrand
                                        SPM.scheme_category_code = code
                                        SPM.product = ProductMaster.objects.get(product_id=PM.product_id)
                                        SPM.save()
                                except BrandMaster.DoesNotExist:
                                    error_brand = {}
                                    error_brand['error_message'] = 'Column <brand> at row ' + str(
                                        process) + ' does not exist.'
                                    invalid_rows.append(error_brand)
                                    not_insert += 1
                                except TaxSlabMaster.DoesNotExist:
                                    error_tax = {}
                                    error_tax['error_message'] = 'Column <TaxSlabCode> at row ' + str(
                                        process) + ' does not exist.'
                                    invalid_rows.append(error_tax)
                                    not_insert += 1
                            else:
                                error_b = {}
                                if float(row['MRP']) <= 0:
                                    error_b['error_message'] = 'Column <MRP> at row ' + str(
                                        process) + ' contain negative values'
                                    invalid_rows.append(error_b)
                                error_m = {}
                                if float(row['DP']) <= 0:
                                    error_m['error_message'] = 'Column <DP> at row ' + str(
                                        process) + ' contain negative values'
                                    invalid_rows.append(error_m)
                                not_insert += 1
                        else:
                            error_code = {}
                            if row['brand'] == '':
                                error_code['error_message'] = 'Column <brand> at row ' + str(
                                    process) + ' contain null values'
                                invalid_rows.append(error_code)
                            error_ab = {}
                            if row['SKU'] == '':
                                error_ab['error_message'] = 'Column <SKU> at row ' + str(
                                    process) + ' contain null val   ues'
                                invalid_rows.append(error_ab)
                            error_am = {}
                            if row['main_category'] == '':
                                error_am['error_message'] = 'Column <main_category> at row ' + str(
                                    process) + ' contain null values'
                                invalid_rows.append(error_am)
                            error_aval = {}
                            if row['category'] == '':
                                error_aval['error_message'] = 'Column <category> at row ' + str(
                                    process) + ' contain null values'
                                invalid_rows.append(error_aval)
                            error_avol = {}
                            if row['sub_category-1'] == '':
                                error_avol['error_message'] = 'Column <sub_category-1> at row ' + str(
                                    process) + ' contain null values'
                                invalid_rows.append(error_avol)
                            error_avl = {}
                            if row['sub_category-2'] == '':
                                error_avl['error_message'] = 'Column <sub_category-2> at row ' + str(
                                    process) + ' contain null values'
                                invalid_rows.append(error_avl)
                            if row['DP'] == '':
                                dp_error = {'error_message': 'Column <DP> at row ' + str(process) + ' contain null values'}
                                invalid_rows.append(dp_error)
                            if row['MRP'] == '':
                                mrp_error = {'error_message': 'Column <MRP> at row ' + str(process) + ' contain null values'}
                                invalid_rows.append(mrp_error)
                            not_insert += 1
                        if len(invalid_rows) >= 0:
                            # not_insert = len(invalid_rows)
                            filespath = os.getcwd()
                            paths = filespath + '/upload/product_not_insert/'
                            if not os.path.exists(paths):
                                os.makedirs(paths)
                            file_path = filespath + '/upload/product_not_insert/product_not_insert.csv'
                            keys = ['error_message']
                            with open(file_path, "w") as output:
                                dict_writer = csv.DictWriter(output, keys)
                                dict_writer.writeheader()
                                if len(invalid_rows) > 0:
                                    dict_writer.writerows(invalid_rows)
                        if insert != 0 or not_insert != 0 or update != 0:
                            lblMsg = "<div class='wright_msg'  style='max-width: 665px;margin: 0 auto;'>" \
                                     "<ul>" \
                                     "<li style='color: #5cb85c;font-size: 21px;font-weight: 700; text-align:center'> <span class='glyphicon glyphicon-ok-sign'></span> File  Processed Successfully .</li>" \
                                     "<li style='color: #5cb85c;font-size: 21px;font-weight: 700;'> <span class='glyphicon glyphicon-arrow-right'></span> Total Record(s) Processed =" + str(
                                process) + \
                                     "<li  style='color: #5cb85c;font-size: 21px;font-weight: 700;' > <span class='glyphicon glyphicon-arrow-right'></span> Total Record(s) Inserted =" + str(
                                insert) + \
                                     "</li ><li  style='color: #5cb85c;font-size: 21px;font-weight: 700;'> <span class='glyphicon glyphicon-arrow-right'></span> Total Record(s) Updated =" + str(
                                update) + \
                                     "</li ><li  style='color: #5cb85c;font-size: 21px;font-weight: 700;'> <span class='glyphicon glyphicon-arrow-right'></span> Total Record(s) Not Inserted =" + str(
                                not_insert) + \
                                     "</li ></ul>" \
                                     " For Errors, <input value='Click to download' type='submit' " \
                                     "name='download' style=' background-color: #ecf0f5; padding: 5px 15px; text-align: center;font-size: 15px; cursor: pointer;display: inline-block; border-radius: 4px;font-weight: 600;'></div>"

                    else:
                        lblMsg = "<div class='wrong_msg ' style='max-width: 665px;margin: 0 auto;'>" \
                                 "<ul>" \
                                 "<li style='text-align:center'><span class='glyphicon glyphicon-remove-sign'></span>Error! </li>" \
                                 "<li><span class='glyphicon glyphicon-arrow-right'></span>  Sheet must have these columns !!</li>" \
                                 "<li> <span class='glyphicon glyphicon-arrow-right'></span>  brand, main_category, category, sub_category-1, sub_category-2, SKU</li>" \
                                 "</ul>" \
                                 "</div>"

                messages.add_message(request, messages.SUCCESS, 'File Processed Successfully...')
            else:
                lblMsg = "<div class='wrong_msg' style='max-width: 665px;margin: 0 auto;'>" \
                         "<ul>" \
                         "<li style='text-align:center'><span class='glyphicon glyphicon-remove-sign'></span>Error! </li>" \
                         "<li><span class='glyphicon glyphicon-arrow-right'></span>  Must contain .csv file!!</li>" \
                         "</ul>" \
                         "</div>"
        if 'download' in request.POST:
            file_path = os.getcwd()
            paths = file_path + '/upload/product_not_insert/product_not_insert.csv'
            if os.path.exists(paths):
                with open(paths, 'rb') as fh:
                    response = HttpResponse(fh.read(), content_type="application/vnd.ms-excel")
                    response['Content-Disposition'] = 'inline; filename=' + os.path.basename(paths)
                    return response
    return render(request, 'products/product_import.html', {'lblMsg': lblMsg})


def export_product(request):
    productinfo = ProductMaster.objects.filter(is_active=True,is_deleted=False).order_by('-created_on')
    result = []
    PCM_id = 0
    for product in productinfo:
        try:
            count = 0
            get_product = {}
            get_product['brand'] = str(product.brand.brand_name)
            get_product['SKU'] = str(product.sku_code)
            get_product['MRP'] = str(product.max_retail_price)
            get_product['DP'] = str(product.dealer_price)
            get_product['best_buying_price'] = str(product.best_buying_price)
            get_product['TaxSlabCode'] = str(product.TaxSlabCode)
            try:
                SPM = SchemeProductMaster.objects.get(product_id=product.product_id)
                if SPM.scheme_category_code.upper() == 'VOL':
                    get_product['product_category_code'] = 'Volume'
                if SPM.scheme_category_code.upper() == 'VAL':
                    get_product['product_category_code'] = 'Value'
                if SPM.scheme_category_code.upper() == 'B':
                    get_product['product_category_code'] = 'Basic'
                if SPM.scheme_category_code.upper() == 'M':
                    get_product['product_category_code'] = 'Mass'
            except Exception:
                get_product['product_category_code'] = 'Not Found'
                get_product['product_category_code'] = 'Not Found'
                get_product['product_category_code'] = 'Not Found'
                get_product['product_category_code'] = 'Not Found'

            category = ProductCategoryMaster.objects.all()
            get_c = []
            for PC in category:
                get_category = {}
                CID = product.product_category_id
                PCM = ProductCategoryMaster.objects.get(pk=CID)
                get_category['category1'] = PCM.category_name
                PID = PCM.parent
                try:
                    PCM = ProductCategoryMaster.objects.get(pk=PID)
                except Exception:
                    PCM = ''
                if PCM != '':
                    get_category['category2'] = PCM.category_name
                    PID = PCM.parent
                try:
                    PCM = ProductCategoryMaster.objects.get(pk=PID)
                except Exception:
                    PCM = ''
                if PCM != '':
                    get_category['category3'] = PCM.category_name
                    PID = PCM.parent
                try:
                    PCM = ProductCategoryMaster.objects.get(pk=PID)
                except Exception:
                    PCM = ''
                if PCM != '':
                    get_category['category4'] = PCM.category_name
                    PID = PCM.parent
                try:
                    PCM1 = ProductCategoryMaster.objects.get(pk=PID)
                except Exception:
                    PCM1 = ''
                if PCM1 != '':
                    get_category['category5'] = PCM1.category_name
                    PID = PCM1.parent
                get_c.append(get_category)
                # break
                # a=get_c.count()
                if len(get_category) == 5:
                    get_product['main_category'] = get_c[0]['category5']
                    get_product['category'] = get_c[0]['category4']
                    get_product['sub_category-1'] = get_c[0]['category3']
                    get_product['sub_category-2'] = get_c[0]['category2']
                    get_product['sub_category-3'] = get_c[0]['category1']
                if len(get_category) == 4:
                    get_product['main_category'] = get_c[0]['category4']
                    get_product['category'] = get_c[0]['category3']
                    get_product['sub_category-1'] = get_c[0]['category2']
                    get_product['sub_category-2'] = get_c[0]['category1']
                    get_product['sub_category-3'] = ''
                if len(get_category) == 3:
                    get_product['main_category'] = get_c[0]['category3']
                    get_product['category'] = get_c[0]['category2']
                    get_product['sub_category-1'] = get_c[0]['category1']
                    get_product['sub_category-2'] = ''
                    get_product['sub_category-3'] = ''
                if len(get_category) == 2:
                    get_product['main_category'] = get_c[0]['category2']
                    get_product['category'] = get_c[0]['category1']
                    get_product['sub_category-1'] = ''
                    get_product['sub_category-2'] = ''
                    get_product['sub_category-3'] = ''
                if len(get_category) == 1:
                    get_product['main_category'] = get_c[0]['category1']
                    get_product['category'] = ''
                    get_product['sub_category-1'] = ''
                    get_product['sub_category-2'] = ''
                    get_product['sub_category-3'] = ''
                break
            size_capacity = str(product.size_capacity)
            size_capacity_unit = str(product.size_capacity_unit)
            if '-' in size_capacity_unit:
                size_capacity_unit=size_capacity_unit.split('-')
                size_capacity_unit=size_capacity_unit[0]+'- '+size_capacity_unit[1]
            get_product['size_capacity_ltr'] = str(size_capacity + '' + size_capacity_unit)
            get_product['series_colour'] = str(product.pattern_color)
            get_product['features'] = str(product.product_description_feature)
        except:
            continue
        result.append(get_product)
    filespath = os.getcwd()
    paths = filespath + '/upload/product_not_insert/'
    if not os.path.exists(paths):
        os.makedirs(paths)
    file_path = filespath + '/upload/product_not_insert/product.csv'
    keys = ['brand', 'main_category', 'category', 'sub_category-1', 'sub_category-2', 'sub_category-3', 'SKU', 'MRP',
            'DP', 'best_buying_price', 'product_category_code', 'size_capacity_ltr', 'series_colour',
            'features', 'TaxSlabCode']
    with open(file_path, "w") as output:
        dict_writer = csv.DictWriter(output, keys)
        dict_writer.writeheader()
        if len(result) > 0:
            dict_writer.writerows(result)
    return result 


def warehouse(request):
    warehouses = models.WarehouseMaster.objects.filter(is_active=True,is_deleted=False).order_by('-created_on').order_by('-modified_on')
    warehousesCount = models.WarehouseMaster.objects.filter(is_active=True,is_deleted=False).count()
    result = []
    searchValue, querycount = "", ""
    for obj in warehouses:
        if obj.warehouse_code!='' and obj.warehouse_code!=None:
            jsdata={}
            jsdata['warehouse_code']=obj.warehouse_code
            jsdata['warehouse_name']=obj.warehouse_name
            jsdata['address_line1']=obj.address_line1
            # if not obj.state or obj.state=='':
            #     jsdata['state']=''
            # else:
            #     jsdata['state']=obj.state.state_name
            jsdata['state'] = obj.state
            jsdata['city']=obj.city
            jsdata['pin_code']=obj.pin_code
            jsdata['contact_number']=obj.contact_number
            try:
                wareobj=WarehouseAdmin.objects.get(warehouse_id=obj.warehouse_code,is_active=True,is_deleted=False)
            except Exception:
                wareobj=''
            if wareobj!='':
                jsdata['user_id']=str(wareobj.user.first_name)+' '+str(wareobj.user.last_name)
            else:
                jsdata['user_id']=''
            result.append(jsdata)
    if request.method == "POST":
        if "search" in request.POST:
            searchValue = request.POST.get('search', None)
            warehouses = models.WarehouseMaster.objects.filter(Q(warehouse_code=searchValue)|Q(warehouse_code__endswith=searchValue)|Q(warehouse_name=searchValue)|Q(contact_number=searchValue)|Q(address_line1__startswith=searchValue)|Q(address_line2__startswith=searchValue),is_active=True,is_deleted=False).order_by('-created_on')
            querycount=str(warehouses.count())+' Record found.'
            result=[]
            for obj in warehouses:
                if obj.warehouse_code!='' and obj.warehouse_code!=None:
                    jsdata={}
                    jsdata['warehouse_code']=obj.warehouse_code
                    jsdata['warehouse_name']=obj.warehouse_name
                    jsdata['address_line1']=obj.address_line1
                    jsdata['state']=obj.state
                    jsdata['city']=obj.city
                    jsdata['pin_code']=obj.pin_code
                    jsdata['contact_number']=obj.contact_number
                    try:
                        wareobj=WarehouseAdmin.objects.get(warehouse_id=obj.warehouse_code,is_active=True,is_deleted=False)
                    except Exception:
                        wareobj=''
                    if wareobj!='':
                        jsdata['user_id']=str(wareobj.user.first_name)+' '+str(wareobj.user.last_name)
                    else:
                        jsdata['user_id']=''
                    result.append(jsdata)

        # if request.GET.get('list', None):
        #     result['listflag'] = True
        # else:
        #     result['listflag'] = False

        if 'export' in request.POST:
            if len(result) > 0:
                final_list = []
                for order in result:
                    order_dict = {
                        "Warehouse Code": order["warehouse_code"], "Name": order["warehouse_name"],
                        "Address": order["address_line1"], "State": order["state"], "City": order["city"],
                        "Pincode": order["pin_code"], "Contact No": order["contact_number"],
                        "WH User Name": order["user_id"]
                    }
                    final_list.append(order_dict)

                response = HttpResponse(content_type='text/csv')
                response['Content-Disposition'] = 'attachment;filename=warehouses_list.csv'
                keys = ["Warehouse Code", "Name", "Address", "State", "City",
                        "Pincode", "Contact No", "WH User Name"]
                writer = csv.DictWriter(response, keys)
                writer.writeheader()
                writer.writerows(final_list)
                return response
    context = {
        'warehouses': result,
        'warehousesCount': warehousesCount,
        'search': searchValue,
        'querycount': querycount
    }
    return render(request, 'warehouse/warehouse.html', context=context)


def WarehouseDashboard(request):
    # if request.user.is_superuser:
    #     price=0
    #     dealer=User.objects.filter(is_active=True).values_list('pk')   
    #     product=ProductMaster.objects.filter(is_active=True,is_deleted=False).count()
    #     orderheadactive=OrderHead.objects.filter(dealer_id__in=dealer ,status='Processing').count()
    #     orderhead=OrderHead.objects.filter(dealer_id__in=dealer,status='Processing')
    #     for rs in orderhead:
    #         price+=rs.total_amount
    #     try:
    #         objdbm=DealerBusinessMaster.objects.get(dealer_id__in=dealer,is_active=True)
    #     except Exception as e:
    #         objdbm=0
    #     target=DealerSalesTarget.objects.filter(dealer_id__in=dealer)
    #     get_target=[]
    #     for DST in target:
    #         monthly_target=0
    #         order_price=0
    #         DST_get={}
    #         DST_get['category']=DST.category.category_name
    #         monthly_target+=DST.monthly_target
    #         order=OrderHead.objects.filter(Q(status='Confirm')|Q(status='Delivered'),dealer_id__in=dealer)
    #         get_product=[]
    #         if order!='':
    #             for amt in order:
    #                 get_cat={}
    #                 try:
    #                     OD=OrderDetails.objects.get(order_id_id=amt.pk)
    #                 except Exception:
    #                     OD=''
    #                 try:
    #                     PM=ProductMaster.objects.get(pk=OD.product_id_id)
    #                 except Exception:
    #                     PM=''
    #                 if PM!='':
    #                     category = ProductCategoryMaster.objects.filter(is_active=True,is_deleted=False)

    #                     for PC in category:
    #                         #get_category={}
    #                         CID=PM.product_category_id
    #                         PCM=ProductCategoryMaster.objects.get(pk=CID)
    #                         get_product.append(PCM.category_name)
    #                         PID=PCM.parent
    #                         try:
    #                             PCM=ProductCategoryMaster.objects.get(pk=PID)
    #                         except Exception:
    #                             PCM=''
    #                         if PCM!='':
    #                             get_product.append(PCM.category_name)
    #                             PID=PCM.parent
    #                         try:
    #                             PCM=ProductCategoryMaster.objects.get(pk=PID)
    #                         except Exception:
    #                             PCM=''
    #                         if PCM!='':
    #                             get_product.append(PCM.category_name)
    #                             PID=PCM.parent
    #                         try:
    #                             PCM=ProductCategoryMaster.objects.get(pk=PID)
    #                         except Exception:
    #                             PCM=''
    #                         if PCM!='':
    #                             get_product.append(PCM.category_name)
    #                             PID=PCM.parent
    #                         try:
    #                             PCM1=ProductCategoryMaster.objects.get(pk=PID)
    #                         except Exception:
    #                             PCM1=''
    #                         if PCM1!='':
    #                             get_product.append(PCM1.category_name)
    #                             PID=PCM1.parent
    #                         break
    #                     get_product.reverse()
    #                     order_price+=amt.total_amount
    #         if DST.category.category_name in get_product:
    #             DST_get['complete_target']=order_price
    #             DST_get['percent']=(order_price*100)/monthly_target
    #         else:
    #             DST_get['percent']=0
    #             DST_get['complete_target']=0
    #         DST_get['monthly_target']=monthly_target

    #         get_target.append(DST_get)
    #     pay=OrderHead.objects.filter(Q(status='Confirm')|Q(status='Delivered'),dealer_id__in=dealer).aggregate(Sum('total_amount'))
    #     objdst=DealerSalesTarget.objects.filter(dealer_id__in=dealer).aggregate(Sum('monthly_target'))
    #     if pay['total_amount__sum']==None:
    #         pay['total_amount__sum']=0
    #     if objdst['monthly_target__sum']==None:
    #         objdst['monthly_target__sum']=0
    #     outstanding=int(objdst['monthly_target__sum'])-int(pay['total_amount__sum'])

    # context={
    #     'objdst':objdst,
    #     'objdbm':objdbm,
    #     'orderhead':orderhead,
    #     'get_target':get_target,
    #     'orderheadactive':orderheadactive,
    #     'outstanding':price,
    #     'product':product,
    # } 
    return render(request, 'warehouse_admin/warehouse_dashboard.html')


def create_warehouse(request):
    warehouse_code = request.GET.get('warehouse_code')
    userobj = User.objects.filter(groups__name='WHSUSER', is_active=True)
    form_class = forms.WarehouseMasterForm
    state=StateMaster.objects.filter(is_active=True, is_deleted=False)
    city=CityMaster.objects.filter(is_active=True, is_deleted=False)
    if request.method == 'GET':
        if warehouse_code != '' and warehouse_code is not None:
            
            try:
                instance = WarehouseMaster.objects.get(warehouse_code=warehouse_code,is_active=True,is_deleted=False)
            except Exception:
                instance=''
            try:
                wareobj=WarehouseAdmin.objects.get(warehouse_id=warehouse_code,is_active=True,is_deleted=False)
            except Exception:
                wareobj=''
            if instance!='':
                form_class = forms.WarehouseMasterForm(request.POST or None, instance=instance)
            else:
                form_class = forms.WarehouseMasterForm
        else:
            form_class = forms.WarehouseMasterForm
            instance=''
            wareobj=''
        context = {
            'warehouse_create_form': form_class,
            'instance': instance,
            'userobj': userobj,
            'state':state,
            'city':city,
            'wareobj': wareobj
        }
        return render(request, 'warehouse/warehouse_create.html', context)
    elif request.method == 'POST':
        check=0
        try:
            userdata=User.objects.get(pk=request.POST.get('user'), groups__name='WHSUSER', is_active=True)
        except Exception:
            userdata=''

        if userdata!='':
            try:
                instance = WarehouseMaster.objects.get(
                    warehouse_code=warehouse_code, is_active=True, is_deleted=False)
            except Exception:
                instance=''

            try:
                wareobj=WarehouseAdmin.objects.get(
                    warehouse_id=warehouse_code, is_active=True, is_deleted=False)
            except Exception:
                wareobj=''

            if warehouse_code != '' and warehouse_code is not None:
                if instance != '':
                    form_class = forms.WarehouseMasterForm(request.POST, instance=instance)
                    check=1
                else:
                    check=0
            else:
                form_class = forms.WarehouseMasterForm(request.POST)
                check=1
                wareobj=''
            if check!=0:
                if form_class.is_valid():
                    warehouse = form_class.save(commit=False)
                    warehouse.created_by = request.user
                    warehouse.save()

                    if wareobj=='':
                        wareobj=WarehouseAdmin()
                    wareobj.user_id=request.POST.get('user')
                    wareobj.warehouse=warehouse
                    wareobj.created_by=request.user
                    wareobj.is_active=True
                    wareobj.save()
                
                return HttpResponseRedirect('/master/warehouse')
                #return render(request, 'warehouse/warehouse_create.html', {'warehouse_create_form': form_class})
            else:
                return HttpResponseRedirect('/master/warehouse/create')
        else:
            messages.add_message(request, messages.ERROR, 'User Does Not Exists!!!')
            return render(request, 'warehouse/warehouse_create.html', {'warehouse_create_form': form_class,'userobj':userobj})
    # elif request.method == 'PUT':
    #     form_class = forms.WarehouseMasterForm(request.POST)
    #     if form_class.is_valid():
    #         warehouse = form_class.save(commit=False)
    #         warehouse.created_by = request.user
    #         warehouse.save()
    #     return render(request, 'warehouse/warehouse_create.html', {'warehouse_create_form': form_class})


def delete_warehouse(request,wareid=None):
    if wareid!=None:
        try:
            stock=StockLevelMaster.objects.get(warehouse_id=wareid,is_active=True,is_deleted=False)
        except Exception:
            stock=''
        if stock=='':
            try:
                wareobj=WarehouseAdmin.objects.get(warehouse_id=wareid,is_active=True,is_deleted=False)
            except Exception:
                wareobj=''
            if wareobj!='':
                wareobj.delete()
            try:
                instance=WarehouseMaster.objects.get(warehouse_code=wareid,is_active=True,is_deleted=False)
            except Exception:
                instance=''
            if instance!='':
                try:
                    instance.delete()
                    messages.add_message(request, messages.SUCCESS, 'Warehouse deleted successfully...')
                except Exception:
                    messages.add_message(request, messages.ERROR, 'Warehouse cannot be deleted!!!')

        else:
            messages.add_message(request, messages.ERROR, 'Warehouse cannot be deleted!!!')
    else:
        messages.add_message(request, messages.ERROR, 'Warehouse Code Does not Exists!!!')
    return HttpResponseRedirect('/master/warehouse')


def stock_level(request):
    stock_level = models.StockLevelMaster.objects.all()
    return render(request, 'stock_level/stock_level.html', {'stock_level': stock_level})


def create_stock_level(request):
    form_class = forms.StockLevelMasterForm
    if request.method == 'GET':
        return render(request, 'stock_level/stock_level_create.html', {'stock_level_create_form': form_class})
    elif request.method == 'POST':
        form_class = forms.StockLevelMasterForm(request.POST)
        if form_class.is_valid():
            stock_level = form_class.save(commit=False)
            stock_level.created_by = request.user
            stock_level.save()
        return render(request, 'stock_level/stock_level_create.html', {'stock_level_create_form': form_class})
    elif request.method == 'PUT':
        form_class = forms.StockLevelMasterForm(request.POST)
        if form_class.is_valid():
            stock_level = form_class.save(commit=False)
            stock_level.created_by = request.user
            stock_level.save()
        return render(request, 'stock_level/stock_level_create.html', {'stock_level_create_form': form_class})


def stock_register(request):
    stock_register = models.StockRegister.objects.all()

    return render(request, 'stock_register/stock_register.html', {'stock_register': stock_register})


def create_stock_register(request):
    form_class = forms.StockRegisterForm
    if request.method == 'GET':
        return render(request, 'stock_register/stock_register_create.html', {'stock_register_create_form': form_class})
    elif request.method == 'POST':
        form_class = forms.StockRegisterForm(request.POST)
        if form_class.is_valid():
            stock_register = form_class.save(commit=False)
            stock_register.created_by = request.user
            stock_register.save()
        return render(request, 'stock_register/stock_register_create.html', {'stock_register_create_form': form_class})
    elif request.method == 'PUT':
        form_class = forms.StockRegisterForm(request.POST)
        if form_class.is_valid():
            stock_register = form_class.save(commit=False)
            stock_register.created_by = request.user
            stock_register.save()
        return render(request, 'stock_register/stock_register_create.html', {'stock_register_create_form': form_class})


def dealer_business(request):
    dealer_business = models.DealerBusinessMaster.objects.all()
    return render(request, 'dealer_business/dealer_business.html', {'dealer_business': dealer_business})


def create_dealer_business(request):
    form_class = forms.DealerBusinessForm
    if request.method == 'GET':
        return render(request, 'dealer_business/dealer_business_create.html',
                      {'dealer_business_create_form': form_class})
    elif request.method == 'POST':
        form_class = forms.DealerBusinessForm(request.POST)
        if form_class.is_valid():
            dealer_business = form_class.save(commit=False)
            dealer_business.created_by = request.user
            dealer_business.save()
        return render(request, 'dealer_business/dealer_business_create.html',
                      {'dealer_business_create_form': form_class})
    elif request.method == 'PUT':
        form_class = forms.DealerBusinessForm(request.POST)
        if form_class.is_valid():
            dealer_business = form_class.save(commit=False)
            dealer_business.created_by = request.user
            dealer_business.save()
        return render(request, 'dealer_business/dealer_business_create.html',
                      {'dealer_business_create_form': form_class})


def dealer_sales_target(request):
    searchValue, querycount, unique_months = "", "", []
    oType = request.GET.get('month', -1)
    oType = datetime.datetime.now().month if int(oType) < 0 else oType
    if 'selected_brand' in request.session:
        dealerSalesTarget = models.DealerSalesTarget.objects.filter(
            brand=request.session["selected_brand"],
            is_deleted=False, is_active=True).order_by('-dealer')
    else:
        dealerSalesTarget = models.DealerSalesTarget.objects.filter(
            is_deleted=False, is_active=True).order_by('-dealer')

    months = [{"created_on": _x.created_on, "month": _x.created_on.month} for _x in dealerSalesTarget]
    for _x in months:
        if any(d.get('month', None) == _x["month"] for d in unique_months):
            pass
        else:
            unique_months.append(_x)
    months = sorted(unique_months, key=lambda k: k['month'], reverse=True)

    dealerSalesTarget = dealerSalesTarget.filter(created_on__month=oType)
    if request.method == 'POST':
        if "search" in request.POST:
            searchValue = request.POST.get('search', None)
            # dealerSalesTarget = models.DealerSalesTarget.objects.filter(
            #     Q(dealer__user_code=searchValue) |
            #     Q(dealer__user_code__endswith=searchValue) |
            #     Q(category__category_name=searchValue),
            #     is_deleted=False, is_active=True).order_by('-pk')
            # querycount = str(dealerSalesTarget.count()) + " Record found."

            dealerSalesTarget = dealerSalesTarget.filter(
                Q(dealer__user_code__icontains=searchValue) |
                Q(category__category_name__icontains=searchValue) |
                Q(monthly_target__icontains=searchValue))

        if 'export' in request.POST:
            sales = export_sales_target(request)
            file_path = os.getcwd()
            paths = file_path + '/upload/target_not_insert/sales_target.csv'
            if os.path.exists(paths):
                with open(paths, "rb") as output:
                    response = HttpResponse(output.read(), content_type="application/vnd.ms-excel")
                    response['Content-Disposition'] = 'attachment; filename="owims_dealer_monthly_sales_target.csv"'
                    return response

    for _y in dealerSalesTarget:
        try:
            d_profile = DealerProfile.objects.get(
                dealer=_y.dealer, is_active=True, is_deleted=False)
            company_name = d_profile.company_name
        except DealerProfile.DoesNotExist:
            company_name = ""
        _y.__dict__["company"] = company_name

    page_style = request.GET.get("page_style", settings.DEFAULT_PAGING_VIEW)
    paginator = Paginator(dealerSalesTarget, 10)
    try:
        page_index = request.GET.get("page", 1)
        data_list = paginator.page(page_index)
    except PageNotAnInteger:
        data_list = paginator.page(1)
    except EmptyPage:
        data_list = paginator.page(paginator.num_pages)

    context = {
        'msg': '',
        'data_list': data_list,
        'targetCount': len(data_list),
        'search': searchValue,
        'querycount': querycount,
        "months": months,
        "oType": int(oType)
    }
    if request.GET.get('list', None):
        context['listflag'] = True
    else:
        context['listflag'] = False
    if page_style == 'grid':
        return render(request, 'dealer_sales_target/sales_grid_view.html', context)
    else:
        return render(request, 'dealer_sales_target/sales_list_view.html', context)


def create_dealer_sales_target(request):
    form_class = forms.DealerSalesTargetForm
    if request.method == 'GET':
        dealer_Target = DealerSalesTarget.objects.filter(
            created_on__month=datetime.datetime.now().month,
            category__category_name='CE', is_active=True,
            is_deleted=False).values_list('dealer_id')
        if not dealer_Target:
            dealer_Target_AV = DealerSalesTarget.objects.filter(
                created_on__month=datetime.datetime.now().month,
                category__category_name='AV', is_active=True,
                is_deleted=False).values_list('dealer_id')
            dealer_Target = DealerSalesTarget.objects.filter(
                created_on__month=datetime.datetime.now().month,
                category__category_name='HA', dealer_id__in=dealer_Target_AV,
                is_active=True, is_deleted=False).values_list('dealer_id')
        users = User.objects.filter(groups__name="DEL", is_active=True).exclude(pk__in=dealer_Target)
        users = list(users)
        for user in users:
            # print("{0}/{1}".format(user.email, user.user_code))
            dealerTarget = DealerSalesTarget.objects.filter(
                dealer=user, category__category_name='CE',
                created_on__month=datetime.datetime.now().month,
                is_active=True, is_deleted=False)
            if len(dealerTarget) > 0:
                users.remove(user)
        context = {'dealer_sales_target_create_form': form_class, 'users': users}
        return render(request, 'dealer_sales_target/dealer_sales_target_create.html', context=context)
    elif request.method == 'POST':
        try:
            DST = DealerSalesTarget.objects.get(
                created_on__month=datetime.datetime.now().month,
                dealer=request.POST.get('dealer'),
                category=request.POST.get('category'),
                is_active=True, is_deleted=False)
        except Exception:
            DST = None
        users = User.objects.filter(groups__name="DEL", is_active=True)
        users = list(users)
        for user in users:
            # print("{0}/{1}".format(user.email, user.user_code))
            dealerTarget = DealerSalesTarget.objects.filter(
                created_on__month=datetime.datetime.now().month,
                dealer=user, category__category_name='CE',
                is_active=True, is_deleted=False)
            if len(dealerTarget) > 0:
                users.remove(user)
        if DST is None:
            # users = User.objects.filter(~Q(user_code=None))
            form_class = forms.DealerSalesTargetForm(request.POST)
            if form_class.is_valid():
                dealer = User.objects.get(id=request.POST['dealer'], groups__name="DEL", is_active=True)
                try:
                    DealerSalesTarget.objects.get(
                        created_on__month=datetime.datetime.now().month,
                        dealer=dealer, brand=request.POST['brand'],
                        category__category_id=request.POST['category'],
                        is_active=True, is_deleted=False)
                    messages.add_message(request, messages.ERROR, 'Target already exists...')
                    context = {'dealer_sales_target_create_form': form_class, 'users': users}
                    return render(request, 'dealer_sales_target/dealer_sales_target_create.html', context=context)
                except DealerSalesTarget.DoesNotExist:
                    dealer_sales = form_class.save(commit=False)
                    dealer_sales.created_by = request.user
                    dealer_sales.category = form_class.cleaned_data['category']
                    dealer_sales.save()
                    messages.add_message(request, messages.SUCCESS, 'Target created successfully...')
            else:
                context = {'dealer_sales_target_create_form': form_class, 'users': users}
                return render(request, 'dealer_sales_target/dealer_sales_target_create.html', context=context)
            # messages.success(request, 'Details added successfully')
            # return render(request, 'dealer_sales_target/dealer_sales_target_create.html',
            #               {'dealer_sales_target_create_form': form_class})
            # messages.success(request, 'Details added successfully.')
            return redirect('/master/dealer_sales_target?page_style=list')
        else:
            messages.add_message(request, messages.ERROR, 'This product already allocated to Dealer...')
            context = {'dealer_sales_target_create_form': form_class, 'users': users}
            return render(request, 'dealer_sales_target/dealer_sales_target_create.html', context=context)


def edit_dealer_sales_target(request, DSTid):
    monthlyamt=''
    if request.method == 'GET':
        instance = get_object_or_404(DealerSalesTarget, pk=DSTid)
        form_class = forms.DealerSalesTargetForm(request.POST or None, instance=instance)
        context = {'dealer_sales_target_create_form': form_class, 'sales': instance}
        return render(request, 'dealer_sales_target/dealer_sales_target_create.html', context=context)

    elif request.method == 'POST':
        instance = DealerSalesTarget.objects.get(pk=DSTid)
        monthlyamt = instance.monthly_target
        form_class = forms.DealerSalesTargetForm(request.POST, instance=instance)
        if form_class.is_valid():
            form_class.save()

            """ need to changed """
            # businessheadobj=User.objects.filter(is_active=True,groups__name="BusinessHead")
            # html_content=''
            # if businessheadobj:
            #     emails=[]
            #     for user in businessheadobj:
            #         emails.append(user.email)
            #     if monthlyamt!=form_class.cleaned_data['monthly_target']:
            #         html_content+= "Previous Monthly Target "+str(monthlyamt)+" Current Monthly Target "+str(form_class.cleaned_data['monthly_target'])+" , "
            #     html_content=html_content.rstrip(',')+"is updated"
            #     subject=" financial number changed"
            #     email = EmailMultiAlternatives(subject, html_content, settings.DEFAULT_FROM_EMAIL, to=emails)
            #     email.send()
            messages.add_message(request, messages.SUCCESS, 'Target updated successfully...')
        else:
            context = {'dealer_sales_target_create_form': form_class}
            return render(request, 'dealer_sales_target/dealer_sales_target_create.html', context=context)
        return redirect('/master/dealer_sales_target?page_style=list')


def delete_dealer_sales_target(request, DSTid):
    dealer_sales = DealerSalesTarget.objects.get(pk=DSTid)
    dealer_sales.delete()
    messages.add_message(request, messages.SUCCESS, 'Target deleted successfully...')
    return HttpResponseRedirect('/master/dealer_sales_target?page_style=list')


    # dealer_sales = DealerSalesTarget.objects.get(pk=DSTid)
    # if request.method == 'POST':
    #     dealer_sales.delete()
    #     return HttpResponseRedirect('/master/dealer_sales_target?page_style=list')
    # else:
    #     return HttpResponseRedirect('/master/dealer_sales_target?page_style=list')

def import_dealer_sales_target(request):
    invalid_rows = []
    index = 0
    insert = 0
    not_insert = 0
    filexlsx = False
    update = 0
    process = 0
    not_exist = []
    lblMsg = ''
    Msg = ''
    month=datetime.datetime.now().month
    if request.method == 'POST':
        if 'Save' in request.POST:
            getSl_num = {}
            file = request.FILES.get('dealer_sales')
            if file != None and file != '':
                filexlsx = str(file._name).endswith('csv')
            if filexlsx == True:
                decoded_file = request.FILES['dealer_sales'].read().decode('utf-8').splitlines()
                reader = csv.DictReader(decoded_file)
                for row in reader:
                    process += 1
                    if index == 0:
                        flag = True
                        for x in import_settings.DEALER_SALES_TARGET_IMPORT_KEY:
                            if x not in row.keys():
                                not_exist.append(x)
                    set_exist = len(not_exist)
                    if set_exist == 0:
                        if row["dealer_code"] != '' and row['brand'] != '' and row['product_category'] != '' and row[
                            'monthly_target'] != '':
                            try:
                                # user = User.objects.get(user_code=row["dealer_code"], is_active=True)
                                if float(row['monthly_target']) >= 0:
                                    try:
                                        dealer_profile = DealerProfile.objects.get(
                                            dealer_code=row["dealer_code"])  # required fields checks
                                        code = DealersBrand.objects.get(user=dealer_profile.dealer_id)
                                        brand = BrandMaster.objects.get(brand_code=code.brand_id)
                                        PCM = ProductCategoryMaster.objects.get(
                                            category_name=row['product_category'],
                                            brand_id=brand.brand_code, is_active=True)
                                        if not PCM:
                                            PCM = ''
                                        if PCM != '':
                                            try:
                                                DST = DealerSalesTarget.objects.get(
                                                    dealer_id=dealer_profile.dealer_id,
                                                    category_id=PCM.pk, brand_id=brand.brand_code,
                                                    created_on__month=month)
                                                update += 1
                                            except DealerSalesTarget.DoesNotExist:
                                                insert += 1
                                                DST = DealerSalesTarget()
                                            DST.dealer = dealer_profile.dealer
                                            # DST.brand = BrandMaster.objects.get(brand_name=row['brand'].lower())
                                            DST.brand = BrandMaster.objects.get(brand_name=row['brand'])
                                            DST.monthly_target = row['monthly_target']
                                            DST.category = ProductCategoryMaster.objects.get(category_id=PCM.pk)
                                            DST.save()
                                        else:
                                            error_msg1 = {}
                                            error_msg1['error_message'] = 'Column <product_category> at row ' + str(
                                                process) + ' does not exist.'
                                            invalid_rows.append(error_msg1)
                                            not_insert += 1
                                    except ProductCategoryMaster.DoesNotExist:
                                        category_error_msg = {
                                            'error_message': 'Column <product_category> at row ' + str(process) + ' does not exist.'
                                        }
                                        invalid_rows.append(category_error_msg)
                                        not_insert += 1
                                    except DealersBrand.DoesNotExist:
                                        dealer_brand_error_msg = {
                                            'error_message': 'Dealer Brand does not exist against Column <dealer_code> at row ' + str(
                                                process) + ' '
                                        }
                                        invalid_rows.append(dealer_brand_error_msg)
                                        not_insert += 1
                                    except DealerProfile.DoesNotExist:
                                        error_msg2 = {}
                                        error_msg2['error_message'] = 'Column <dealer_code> at row ' + str(
                                            process) + ' does not exist.'
                                        invalid_rows.append(error_msg2)
                                        not_insert += 1
                                else:
                                    error_b = {}
                                    if float(row['monthly_target'].rstrip('%')) <= 0:
                                        error_b['error_message'] = 'Column <monthly_target> at row ' + str(
                                            process) + ' contain negative values'
                                        invalid_rows.append(error_b)
                                    not_insert += 1
                            except Exception:
                                errorusr = {}
                                errorusr['error_message'] = 'This user_code = '+str(row["dealer_code"])+' at row ' + str(
                                    process) + ' is not active'
                                invalid_rows.append(errorusr)
                                not_insert += 1
                        else:
                            error_code = {}
                            if row['dealer_code'] == '':
                                error_code['error_message'] = 'Column <dealer_code> at row ' + str(
                                    process) + ' contain null values'
                                invalid_rows.append(error_code)
                            error_ab = {}
                            if row['brand'] == '':
                                error_ab['error_message'] = 'Column <brand> at row ' + str(
                                    process) + ' contain null values'
                                invalid_rows.append(error_ab)
                            error_am = {}
                            if row['product_category'] == '':
                                error_am['error_message'] = 'Column <product_category> at row ' + str(
                                    process) + ' contain null values'
                                invalid_rows.append(error_am)
                            error_aval = {}
                            if row['monthly_target'] == '':
                                error_aval['error_message'] = 'Column <monthly_target> at row ' + str(
                                    process) + ' contain null values'
                                invalid_rows.append(error_aval)
                            not_insert += 1
                        if len(invalid_rows) >= 0:
                            filespath = os.getcwd()
                            paths = filespath + '/upload/target_not_insert/'
                            if not os.path.exists(paths):
                                os.makedirs(paths)
                            file_path = filespath + '/upload/target_not_insert/target_not_insert.csv'
                            keys = ['error_message']
                            with open(file_path, "w") as output:
                                dict_writer = csv.DictWriter(output, keys)
                                dict_writer.writeheader()
                                if len(invalid_rows) > 0:
                                    dict_writer.writerows(invalid_rows)
                        if insert != 0 or not_insert != 0 or update != 0:
                            lblMsg = "<div class='wright_msg'  style='max-width: 665px;margin: 0 auto;'>" \
                                     "<ul>" \
                                     "<li style='color: #5cb85c;font-size: 21px;font-weight: 700; text-align:center'> <span class='glyphicon glyphicon-ok-sign'></span> File  Processed Successfully .</li>" \
                                     "<li style='color: #5cb85c;font-size: 21px;font-weight: 700;'> <span class='glyphicon glyphicon-arrow-right'></span> Total Record(s) Processed =" + str(
                                process) + \
                                     "<li  style='color: #5cb85c;font-size: 21px;font-weight: 700;' > <span class='glyphicon glyphicon-arrow-right'></span> Total Record(s) Inserted =" + str(
                                insert) + \
                                     "</li ><li  style='color: #5cb85c;font-size: 21px;font-weight: 700;'> <span class='glyphicon glyphicon-arrow-right'></span> Total Record(s) Updated =" + str(
                                update) + \
                                     "</li ><li  style='color: #5cb85c;font-size: 21px;font-weight: 700;'> <span class='glyphicon glyphicon-arrow-right'></span> Total Record(s) Not Inserted =" + str(
                                not_insert) + \
                                     "</li ></ul>" \
                                     " For Errors, <input value='Click to download' type='submit' " \
                                     "name='download' style=' background-color: #ecf0f5; padding: 5px 15px; text-align: center;font-size: 15px; cursor: pointer;display: inline-block; border-radius: 4px;font-weight: 600;'></div>"

                    else:
                        lblMsg = "<div class='wrong_msg ' style='max-width: 665px;margin: 0 auto;'>" \
                                 "<ul>" \
                                 "<li style='text-align:center'><span class='glyphicon glyphicon-remove-sign'></span>Error! </li>" \
                                 "<li><span class='glyphicon glyphicon-arrow-right'></span>  Sheet must have these columns !!</li>" \
                                 "<li> <span class='glyphicon glyphicon-arrow-right'></span> 'dealer_code', 'product_category', 'monthly_target'</li>" \
                                 "</ul>" \
                                 "</div>"

                messages.add_message(request, messages.SUCCESS, 'File Processed Successfully...')
            else:
                lblMsg = "<div class='wrong_msg' style='max-width: 665px;margin: 0 auto;'>" \
                         "<ul>" \
                         "<li style='text-align:center'><span class='glyphicon glyphicon-remove-sign'></span>Error! </li>" \
                         "<li><span class='glyphicon glyphicon-arrow-right'></span>  Must contain .csv file!!</li>" \
                         "</ul>" \
                         "</div>"
        if 'download' in request.POST:
            file_path = os.getcwd()
            paths = file_path + '/upload/target_not_insert/target_not_insert.csv'
            if os.path.exists(paths):
                with open(paths, 'rb') as fh:
                    response = HttpResponse(fh.read(), content_type="application/vnd.ms-excel")
                    response['Content-Disposition'] = 'inline; filename=' + os.path.basename(paths)
                    return response
    context = {
        'lblMsg': lblMsg,
        'Msg': Msg,
    }
    return render(request, 'dealer_sales_target/sales_import.html', context)


def export_sales_target(request):
    sales = DealerSalesTarget.objects.filter(
        is_active=True, is_deleted=False).order_by('-pk')
    result = []
    for target in sales:
        get_target = {
            "brand": str(target.brand),
            "dealer_code": str(target.dealer.user_code),
            "monthly_target": str(target.monthly_target),
            "product_category": str(target.category.category_name)
        }
        result.append(get_target)
    filespath = os.getcwd()
    paths = filespath + '/upload/target_not_insert/'
    if not os.path.exists(paths):
        os.makedirs(paths)
    file_path = filespath + '/upload/target_not_insert/sales_target.csv'
    keys = ['dealer_code', 'brand', 'product_category', 'monthly_target']
    with open(file_path, "w") as output:
        dict_writer = csv.DictWriter(output, keys)
        dict_writer.writeheader()
        if len(result) > 0:
            dict_writer.writerows(result)
    return result


def allowance(request):
    searchValue = ''
    querycount = ''
    if 'selected_brand' in request.session:
        allowanceList = models.AllowanceMaster.objects.filter(
            brand=request.session["selected_brand"],
            dealer__is_active=True, is_active=True,
            is_deleted=False).order_by('-created_on').order_by('-modified_on')
    else:
        allowanceList = models.AllowanceMaster.objects.filter(
            dealer__is_active=True, is_active=True,
            is_deleted=False).order_by('-created_on').order_by('-modified_on')

    groups = request.user.groups.all()
    groups = [group.name for group in groups]
    if 'SR' in groups:
        srDealer = SRDealer.objects.filter(sr_id=request.user)
        # dealerList = [item.dealer_id.dealer for item in srDealer]
        dealerList = []
        for item in srDealer:
            if item.dealer_id is not None:
                if item.dealer_id.dealer is not None:
                    dealerList.append(item.dealer_id.dealer)
        allowanceList = allowanceList.filter(dealer__in=dealerList)

    data_list = None
    if request.method == "POST":
        if "search" in request.POST:
            searchValue = request.POST.get('search', None)
            # allowanceList = models.AllowanceMaster.objects.filter(Q(dealer__user_code=searchValue)|Q(dealer__user_code__endswith=searchValue)|Q(scheme_category_code__scheme_category_code=searchValue),
            #     dealer__is_active=True, is_active=True, is_deleted=False).order_by('-pk')

            allowanceList = allowanceList.filter(
                Q(dealer__user_code__icontains=searchValue) |
                Q(scheme_category_code__scheme_category_code__icontains=searchValue) |
                Q(scheme_category_code__scheme_category_name__icontains=searchValue) |
                Q(discount_percent__icontains=searchValue) |
                Q(discount_amount__icontains=searchValue))

            # querycount=str(allowanceList.count())+' Record found.'

        if 'export' in request.POST:
            allowance = export_allowance(request)
            file_path = os.getcwd()
            paths = file_path + '/upload/not_insert_allowance/allowance.csv'
            if os.path.exists(paths):
                with open(paths, "rb") as output:
                    response = HttpResponse(output.read(), content_type="application/vnd.ms-excel")
                    response['Content-Disposition'] = 'attachment; filename="owims_allowance.csv"'
                    return response

    # ............................
    data, sum_of_allowances = [], 0
    for item in allowanceList:
        if any(d.get('dealer', None) == item.dealer.user_code for d in data):
            index = [i for i, _ in enumerate(data) if _['dealer'] == item.dealer.user_code][0]

            if item.scheme_category_code.scheme_category_code == "VOL":
                x_dict = {"percent": item.discount_percent, 'amount': item.discount_amount}
                data[index]['vol'] = x_dict
                sum_of_allowances += 1
            elif item.scheme_category_code.scheme_category_code == "VAL":
                x_dict = {"percent": item.discount_percent, 'amount': item.discount_amount}
                data[index]['val'] = x_dict
                sum_of_allowances += 1
            elif item.scheme_category_code.scheme_category_code == "B":
                x_dict = {"percent": item.discount_percent, 'amount': item.discount_amount}
                data[index]['b'] = x_dict
                sum_of_allowances += 1
            elif item.scheme_category_code.scheme_category_code == "M":
                x_dict = {"percent": item.discount_percent, 'amount': item.discount_amount}
                data[index]['m'] = x_dict
                sum_of_allowances += 1
        else:
            item_dict = {"dealer": item.dealer.user_code, 'vol': {}, 'm': {},
                         'val': {}, 'b': {}, 'allowance_master_id': item.pk}

            if item.scheme_category_code.scheme_category_code == "VOL":
                x_dict = {"percent": item.discount_percent, 'amount': item.discount_amount}
                item_dict['vol'] = x_dict
                sum_of_allowances += 1
            elif item.scheme_category_code.scheme_category_code == "VAL":
                x_dict = {"percent": item.discount_percent, 'amount': item.discount_amount}
                item_dict['val'] = x_dict
                sum_of_allowances += 1
            elif item.scheme_category_code.scheme_category_code == "B":
                x_dict = {"percent": item.discount_percent, 'amount': item.discount_amount}
                item_dict['b'] = x_dict
                sum_of_allowances += 1
            elif item.scheme_category_code.scheme_category_code == "M":
                x_dict = {"percent": item.discount_percent, 'amount': item.discount_amount}
                item_dict['m'] = x_dict
                sum_of_allowances += 1

            # ..............................................................................
            """ COMPANY FROM DEALER PROFILE """
            try:
                dealer_profile = DealerProfile.objects.get(
                    dealer=item.dealer, is_active=True, is_deleted=False)
                item_dict["company_name"] = dealer_profile.company_name
            except Exception:
                item_dict["company_name"] = ""
            # ..............................................................................

            data.append(item_dict)
    # ............................

    page_style = request.GET.get("page_style", settings.DEFAULT_PAGING_VIEW)
    # paginator = Paginator(allowanceList, 10)
    paginator = Paginator(data, 10)
    try:
        page_index = request.GET.get("page", 1)
        data_list = paginator.page(page_index)
    except PageNotAnInteger:
        data_list = paginator.page(1)
    except EmptyPage:
        data_list = paginator.page(paginator.num_pages)

    context = {
        'msg': '',
        'data_list': data_list,
        'search': searchValue,
        'querycount': querycount,
        'number_of_dealers': len(data_list),
        'sum_of_allowances': sum_of_allowances
    }
    if request.GET.get('list', None):
        context['flagList'] = True
    else:
        context['flagList'] = False
    if page_style == 'grid':
        return render(request, 'allowance/allowance_grid_view.html', context)
    else:
        return render(request, 'allowance/allowance_list_view.html', context)


def get_states(request, state_id):
    cities = models.CityMaster.objects.filter(state_id=state_id)
    return render(request, 'city_dropdown_list_options.html', {'cities': cities})


def create_allowance(request):
    form_class = forms.AllowanceMasterForm
    if request.method == 'GET':
        users = User.objects.filter(groups__name='DEL', is_active=True).order_by('-pk')
        context = {'allowance_create_form': form_class, 'users': users}
        return render(request, 'allowance/allowance_create.html', context=context)

    elif request.method == 'POST':
        users = User.objects.filter(groups__name='DEL', is_active=True).order_by('-pk')
        form_class = forms.AllowanceMasterForm(request.POST)
        allowances = AllowanceMaster.objects.filter(dealer__id=request.POST.get('dealer'))
        x_dealer = request.POST.get('dealer', 0)
        curr_discount = request.POST.get('discount_value')
        context = {
            'allowance_create_form': form_class, 'users': users,
            'x_id': int(x_dealer), 'discount': curr_discount
        }
        for allow in allowances:
            if allow.scheme_category_code_id == request.POST.get('scheme_category_code'):
                messages.add_message(request, messages.ERROR, 'Product category code for dealer is already exists...')
                # context = {'allowance_create_form': form_class, 'users': users}
                return render(request, 'allowance/allowance_create.html', context=context)

        if form_class.is_valid():
            newAllowance = form_class.save(commit=False)
            newAllowance.created_by = request.user
            if request.POST.get('discount') == 'percent':
                percent = float(request.POST.get('discount_value'))
                newAllowance.discount_percent = decimal.Decimal(percent)
            elif request.POST.get('discount') == 'amount':
                newAllowance.discount_amount = float(request.POST.get('discount_value'))
            else:
                pass
            try:
                newAllowance.save()
                messages.add_message(request, messages.SUCCESS, 'Allowance created successfully...')
            except Exception:
                if request.POST.get('discount') == 'percent':
                    messages.add_message(request, messages.ERROR, 'Percent should be max 4 digits with 2 decimal places.')
                else:
                    messages.add_message(request, messages.ERROR, 'Please enter valid amount')
                # context = {'allowance_create_form': form_class, 'users': users}
                return render(request, 'allowance/allowance_create.html', context=context)
        else:
            # context = {'allowance_create_form': form_class, 'users': users}
            return render(request, 'allowance/allowance_create.html', context=context)

        return redirect('/master/allowance?page_style=list')


def edit_allowance(request, allow_id):
    dispercent=''
    disamount=''
    if request.method == 'GET':
        instance = get_object_or_404(AllowanceMaster, allowance_master_id=allow_id)
        form_class = forms.AllowanceMasterForm(request.POST or None, instance=instance)
        # ....................
        schemes = form_class.fields['scheme_category_code'].__dict__['_queryset']
        categories = AllowanceMaster.objects.filter(dealer=instance.dealer)
        vol, val, m, b = 0, 0, 0, 0
        for item in categories:
            if item.scheme_category_code.scheme_category_code == 'VOL':
                vol = item.pk
            elif item.scheme_category_code.scheme_category_code == 'VAL':
                val = item.pk
            elif item.scheme_category_code.scheme_category_code == 'B':
                b = item.pk
            elif item.scheme_category_code.scheme_category_code == 'M':
                m = item.pk

        for i in schemes:
            if i.scheme_category_code == 'VOL':
                i.__dict__["code_id"] = vol
            elif i.scheme_category_code == 'VAL':
                i.__dict__["code_id"] = val
            elif i.scheme_category_code == 'B':
                i.__dict__["code_id"] = b
            elif i.scheme_category_code == 'M':
                i.__dict__["code_id"] = m
        # ....................
        context = {'allowance_create_form': form_class, 'allowance': instance, 'category': schemes}
        return render(request, 'allowance/allowance_create.html', context=context)

    elif request.method == 'POST':
        instance = AllowanceMaster.objects.get(allowance_master_id=allow_id)
        form_class = forms.AllowanceMasterForm(request.POST, instance=instance)
        disamount=instance.discount_amount
        dispercent=instance.discount_percent

        # ....................
        schemes = form_class.fields['scheme_category_code'].__dict__['_queryset']
        categories = AllowanceMaster.objects.filter(dealer=instance.dealer)
        vol, val, m, b = 0, 0, 0, 0
        for item in categories:
            if item.scheme_category_code.scheme_category_code == 'VOL':
                vol = item.pk
            elif item.scheme_category_code.scheme_category_code == 'VAL':
                val = item.pk
            elif item.scheme_category_code.scheme_category_code == 'B':
                b = item.pk
            elif item.scheme_category_code.scheme_category_code == 'M':
                m = item.pk

        for i in schemes:
            if i.scheme_category_code == 'VOL':
                i.__dict__["code_id"] = vol
            elif i.scheme_category_code == 'VAL':
                i.__dict__["code_id"] = val
            elif i.scheme_category_code == 'B':
                i.__dict__["code_id"] = b
            elif i.scheme_category_code == 'M':
                i.__dict__["code_id"] = m
        # ....................
        context = {
            'allowance_create_form': form_class,
            'allowance': instance, 'category': schemes,
        }

        if instance.scheme_category_code_id == request.POST.get('scheme_category_code'):
            pass
        else:
            allowances = AllowanceMaster.objects.filter(dealer__id=request.POST.get('dealer'))
            for allowance in allowances:
                if allowance.scheme_category_code_id == request.POST.get('scheme_category_code'):
                    # context = {'allowance_create_form': form_class, 'allowance': instance}
                    messages.add_message(request, messages.ERROR, 'Scheme category code for dealer is already exists...')
                    return render(request, 'allowance/allowance_create.html', context=context)

        if form_class.is_valid():
            updateAllowance = form_class.save()

            if request.POST.get('discount') == 'percent':
                percent = float(request.POST.get('discount_value'))
                updateAllowance.discount_percent = decimal.Decimal(percent)
                updateAllowance.discount_amount = float(0)
            elif request.POST.get('discount') == 'amount':
                updateAllowance.discount_amount = float(request.POST.get('discount_value'))
                updateAllowance.discount_percent = float(0)
            else:
                pass

            try:
                updateAllowance.save()
                """ need to changed """
                # businessheadobj=User.objects.filter(is_active=True,groups__name="BusinessHead")
                # html_content=''
                # if businessheadobj:
                #     emails=[]
                #     try:
                #         for user in businessheadobj:
                #             emails.append(user.email)
                #         if disamount!=updateAllowance.discount_amount:
                #             html_content+= "Previous Discount Amount ₹"+str(disamount)+" Current Discount Amount ₹"+str(updateAllowance.discount_amount)+" , "
                #         if dispercent!=updateAllowance.discount_percent:
                #             html_content+= "Previous Discount Percent %"+str(dispercent)+" Current Discount Percent %"+str(updateAllowance.discount_percent)+" , "
                #         html_content=html_content.rstrip(',')+"is updated"
                #         subject=" financial number changed"
                #         email = EmailMultiAlternatives(subject, html_content, settings.DEFAULT_FROM_EMAIL, to=emails)
                #         email.send()
                #     except Exception as error:
                #         messages.add_message(request, messages.ERROR, str(error))
                messages.add_message(request, messages.SUCCESS, 'Allowance updated successfully...')
            except Exception as error:
                if request.POST.get('discount') == 'percent':
                    messages.add_message(request, messages.ERROR, 'Percent should be max 4 digits with 2 decimal places.')
                elif request.POST.get('discount') == 'amount':
                    messages.add_message(request, messages.ERROR, 'Please enter valid amount')
                else:
                    messages.add_message(request, messages.ERROR, str(error))
                # context = {'allowance_create_form': form_class, 'allowance': instance}
                return render(request, 'allowance/allowance_create.html', context=context)
        else:
            # context = {'allowance_create_form': form_class, 'allowance': instance}
            return render(request, 'allowance/allowance_create.html', context=context)
        return redirect('/master/allowance?page_style=list')


def delete_allowance(request, allow_id):
    allowance = AllowanceMaster.objects.get(allowance_master_id=allow_id)
    allowance.delete()
    messages.add_message(request, messages.SUCCESS, 'Allowance deleted successfully...')
    return HttpResponseRedirect('/master/allowance?page_style=list')


def import_allowances(request):
    invalid_rows = []
    index = 0
    not_exist = []
    insert = 0
    not_insert = 0
    update = 0
    process = 0
    lblMsg = ''
    filexlsx = False

    if request.method == 'POST':
        if 'Save' in request.POST:
            #getSl_num = {}
            file = request.FILES.get('allowance')
            if file != None and file != '':
                filexlsx = str(file._name).endswith('csv')
            if filexlsx == True:
                decoded_file = request.FILES['allowance'].read().decode('utf-8').splitlines()
                reader = csv.DictReader(decoded_file)
                for row in reader:
                    process += 1
                    if index == 0:
                        flag = True
                        for x in import_settings.DEALER_ALLOWANCE_IMPORT_KEYS:
                            if x not in row.keys():
                                not_exist.append(x)
                        # return invalid file from here
                        for allowance_code in row.keys():
                            if 'allowance_' in allowance_code:
                                allowance_code = allowance_code.replace('allowance_', '').upper().rstrip('_amt')
                                if '_AMT' in allowance_code:
                                    allowance_code = allowance_code.replace('_AMT', '').upper()
                                scheme_catgs = SchemeCategoryMaster.objects.filter(scheme_category_code=allowance_code)
                                if len(scheme_catgs) <= 0:
                                    SchemeCategoryMaster.objects.create(scheme_category_code=allowance_code,
                                                                        scheme_category_name=allowance_code)
                                    index = index + 1
                    set_exist = len(not_exist)
                    if set_exist == 0:
                        if row['dealer_code'] != '' and row['allowance_b'] != '' and row[
                            'allowance_m'] != '' and row['allowance_val'] != '' and row['allowance_vol'] != '':
                            if float(row['allowance_b'].rstrip('%')) >= 0 and float(row['allowance_m'].rstrip('%')) >= 0 \
                                    and float(row['allowance_val'].rstrip('%')) >= 0 and float(
                                row['allowance_vol'].rstrip('%')) >= 0:
                                try:
                                    user=User.objects.get(user_code=row["dealer_code"], is_active=True)
                                    try:
                                        dealer_profile = DealerProfile.objects.get(
                                            dealer_code=row["dealer_code"])  # required fields checks
                                        code = DealersBrand.objects.get(user=dealer_profile.dealer_id)

                                        Brand = BrandMaster.objects.get(brand_code=code.brand_id)
                                        cnt = 0
                                        up = 0

                                        for column in row.keys():

                                            if 'allowance_' in column:
                                                SchemeCategory = SchemeCategoryMaster.objects.all().values_list(
                                                    'scheme_category_code')
                                                scheme_code = column.replace('allowance_', '')
                                                if '_amt' in scheme_code:
                                                    scheme_code = scheme_code.replace('_amt', '')
                                                try:
                                                    # obj_allowance = AllowanceMaster.objects.filter(
                                                    #     scheme_category_code=scheme_code.upper(), brand_id=code.brand_id,
                                                    #     dealer_id=dealer_profile.dealer_id)
                                                    # obj_allowance = obj_allowance[0]
                                                    obj_allowance = AllowanceMaster.objects.get(
                                                        scheme_category_code=scheme_code.upper(), brand_id=code.brand_id,
                                                        dealer_id=dealer_profile.dealer_id)
                                                    up += 1
                                                except AllowanceMaster.DoesNotExist:
                                                    obj_allowance = AllowanceMaster()
                                                    cnt += 1
                                                obj_allowance.dealer = User.objects.get(id=dealer_profile.dealer_id,
                                                                                        is_active=True)
                                                obj_allowance.brand = BrandMaster.objects.get(brand_code=Brand.brand_code)
                                                obj_allowance.scheme_category_code = SchemeCategoryMaster.objects.get(
                                                    scheme_category_code=scheme_code.upper())
                                                if '_amt' in column:
                                                    if row[column] != '':
                                                        obj_allowance.discount_amount = float(row[column])
                                                    else:
                                                        obj_allowance.discount_amount = 0
                                                else:
                                                    if row[column] != '':
                                                        obj_allowance.discount_percent = float(row[column].rstrip('%'))
                                                    else:
                                                        obj_allowance.discount_percent = 0
                                                obj_allowance.save()
                                        if cnt == 4:
                                            insert += 1
                                        if cnt == 8:
                                            insert += 1
                                        if up == 4:
                                            update += 1
                                        if up == 8:
                                            update += 1
                                    except DealerProfile.DoesNotExist:
                                        error1 = {}
                                        error1['error_message'] = 'Column <dealer_code> at row ' + str(
                                            process) + ' does not exist'
                                        invalid_rows.append(error1)
                                        not_insert += 1
                                except Exception:
                                        errorusr = {}
                                        errorusr['error_message'] = 'This user_code = '+str(row["dealer_code"])+' at row ' + str(
                                            process) + ' is not active'
                                        invalid_rows.append(errorusr)
                                        not_insert += 1
                            else:
                                error_b = {}
                                if float(row['allowance_b'].rstrip('%')) < 0:
                                    error_b['error_message'] = 'Column <allowance_b> at row ' + str(
                                        process) + ' contain negative values'
                                    invalid_rows.append(error_b)
                                error_m = {}
                                if float(row['allowance_m'].rstrip('%')) < 0:
                                    error_m['error_message'] = 'Column <allowance_m> at row ' + str(
                                        process) + ' contain negative values'
                                    invalid_rows.append(error_m)
                                error_val = {}
                                if float(row['allowance_val'].rstrip('%')) < 0:
                                    error_val['error_message'] = 'Column <allowance_val> at row ' + str(
                                        process) + ' contain negative values'
                                    invalid_rows.append(error_val)
                                error_vol = {}
                                if float(row['allowance_vol'].rstrip('%')) < 0:
                                    error_vol['error_message'] = 'Column <allowance_vol> at row ' + str(
                                        process) + ' contain negative values'
                                    invalid_rows.append(error_vol)
                                not_insert += 1
                                # row['error_message']='Field contain negative values'

                        else:
                            error_code = {}
                            if row['dealer_code'] == '':
                                error_code['error_message'] = 'Column <dealer_code> at row ' + str(
                                    process) + ' contain null values'
                                invalid_rows.append(error_code)
                            error_ab = {}
                            if row['allowance_b'] == '':
                                error_ab['error_message'] = 'Column <allowance_b> at row ' + str(
                                    process) + ' contain null values'
                                invalid_rows.append(error_ab)
                                error_am = {}
                            if row['allowance_m'] == '':
                                error_am['error_message'] = 'Column <allowance_m> at row ' + str(
                                    process) + ' contain null values'
                                invalid_rows.append(error_am)
                            error_aval = {}
                            if row['allowance_val'] == '':
                                error_aval['error_message'] = 'Column <allowance_val> at row ' + str(
                                    process) + ' contain null values'
                                invalid_rows.append(error_aval)
                            error_avol = {}
                            if row['allowance_vol'] == '':
                                error_avol['error_message'] = 'Column <allowance_vol> at row ' + str(
                                    process) + ' contain null values'
                                invalid_rows.append(error_avol)
                            not_insert += 1
                    else:
                        lblMsg = "<div class='wrong_msg ' style='max-width: 665px;margin: 0 auto;'>" \
                                 "<ul>" \
                                 "<li style='text-align:center'><span class='glyphicon glyphicon-remove-sign'></span>Error! </li>" \
                                 "<li><span class='glyphicon glyphicon-arrow-right'></span>  Sheet must have these columns !!</li>" \
                                 "<li> <span class='glyphicon glyphicon-arrow-right'></span>  dealer_code, allowance_b, allowance_m, allowance_vol, allowance_val</li>" \
                                 "</ul>" \
                                 "</div>"
                    if len(invalid_rows) >= 0:
                        filespath = os.getcwd()
                        paths = filespath + '/upload/not_insert_allowance/'
                        if not os.path.exists(paths):
                            os.makedirs(paths)
                        file_path = filespath + '/upload/not_insert_allowance/not_insert_allowance.csv'
                        keys = ['error_message']
                    with open(file_path, "w") as output:
                        dict_writer = csv.DictWriter(output, keys)
                        dict_writer.writeheader()
                        if len(invalid_rows) > 0:
                            dict_writer.writerows(invalid_rows)

                messages.add_message(request, messages.SUCCESS, 'File Processed Successfully...')
            else:
                lblMsg = "<div class='wrong_msg' style='max-width: 665px;margin: 0 auto;'>" \
                         "<ul>" \
                         "<li style='text-align:center'><span class='glyphicon glyphicon-remove-sign'></span>Error! </li>" \
                         "<li><span class='glyphicon glyphicon-arrow-right'></span>  Must contain .csv file!!</li>" \
                         "</ul>" \
                         "</div>"
            if insert != 0 or not_insert != 0 or update != 0:
                lblMsg = "<div class='wright_msg'  style='max-width: 665px;margin: 0 auto;'>" \
                         "<ul>" \
                         "<li style='color: #5cb85c;font-size: 21px;font-weight: 700; text-align:center'> <span class='glyphicon glyphicon-ok-sign'></span> File  Processed Successfully .</li>" \
                         "<li style='color: #5cb85c;font-size: 21px;font-weight: 700;'> <span class='glyphicon glyphicon-arrow-right'></span> Total Record(s) Processed =" + str(
                    process) + \
                         "<li  style='color: #5cb85c;font-size: 21px;font-weight: 700;' > <span class='glyphicon glyphicon-arrow-right'></span> Total Record(s) Inserted =" + str(
                    insert) + \
                         "</li ><li  style='color: #5cb85c;font-size: 21px;font-weight: 700;'> <span class='glyphicon glyphicon-arrow-right'></span> Total Record(s) Updated =" + str(
                    update) + \
                         "</li ><li  style='color: #5cb85c;font-size: 21px;font-weight: 700;'> <span class='glyphicon glyphicon-arrow-right'></span> Total Record(s) Not Inserted =" + str(
                    not_insert) + \
                         "</li ></ul>" \
                         "For Errors,<input value='Click to download' type='submit' " \
                         "name='download' style=' background-color: #ecf0f5; padding: 5px 15px; text-align: center;font-size: 15px; cursor: pointer;display: inline-block; border-radius: 4px;font-weight: 600;'></div>"
        if 'download' in request.POST:
            file_path = os.getcwd()
            paths = file_path + '/upload/not_insert_allowance/not_insert_allowance.csv'
            if os.path.exists(paths):
                with open(paths, 'rb') as fh:
                    response = HttpResponse(fh.read(), content_type="application/vnd.ms-excel")
                    response['Content-Disposition'] = 'inline; filename=' + os.path.basename(paths)
                    return response
    return render(request, 'allowance/import_allowance.html', {'lblMsg': lblMsg})


def export_allowance(request):
    # if 'selected_brand' in request.session:
    #     allow_info = models.AllowanceMaster.objects.filter(
    #         brand=request.session["selected_brand"]).order_by('-pk')
    # else:
    #     allow_info = models.AllowanceMaster.objects.all().order_by('-pk')
    # for allowance in allow_info:
    #     get_allowance = {}
    #     code = str(allowance.scheme_category_code.scheme_category_code)
    #     get_allowance['dealer_code'] = str(allowance.dealer.user_code)
    #     if code == 'B':
    #         get_allowance['allowance_b'] = str(allowance.discount_percent) + '%'
    #     if code == 'M':
    #         get_allowance['allowance_m'] = str(allowance.discount_percent) + '%'
    #     if code == 'VOL':
    #         get_allowance['allowance_vol'] = str(allowance.discount_percent) + '%'
    #     if code == 'VAL':
    #         get_allowance['allowance_val'] = str(allowance.discount_percent) + '%'

    dealer = DealerProfile.objects.filter(is_active=True)#.order_by('-created_on')
    result = []
    for DP in dealer:
        allow_info = AllowanceMaster.objects.filter(dealer_id=DP.dealer_id)
        get_allowance = {}
        for allowance in allow_info:
            code = str(allowance.scheme_category_code.scheme_category_code)
            get_allowance['dealer_code'] = str(allowance.dealer.user_code)
            if code == 'B':
                get_allowance['allowance_b'] = str(allowance.discount_percent) + '%'
            if code == 'M':
                get_allowance['allowance_m'] = str(allowance.discount_percent) + '%'
            if code == 'VOL':
                get_allowance['allowance_vol'] = str(allowance.discount_percent) + '%'
            if code == 'VAL':
                get_allowance['allowance_val'] = str(allowance.discount_percent) + '%'
        if allow_info:
            result.append(get_allowance)
        
        
    filespath = os.getcwd()
    paths = filespath + '/upload/not_insert_allowance/'
    if not os.path.exists(paths):
        os.makedirs(paths)
    file_path = filespath + '/upload/not_insert_allowance/allowance.csv'
    keys = ['dealer_code', 'allowance_b', 'allowance_m', 'allowance_vol', 'allowance_val']
    with open(file_path, "w") as output:
        dict_writer = csv.DictWriter(output, keys)
        dict_writer.writeheader()
        if len(result) > 0:
            dict_writer.writerows(result)
    return result


def tie_up(request):
    searchValue, querycount = "", ""
    if 'selected_brand' in request.session:
        tieUp = models.TieUpMaster.objects.filter(
            brand=request.session["selected_brand"],
            dealer__is_active=True, is_active=True,
            is_deleted=False).order_by('-created_on').order_by('-modified_on')
    else:
        tieUp = models.TieUpMaster.objects.filter(
            dealer__is_active=True, is_active=True,
            is_deleted=False).order_by('-created_on').order_by('-modified_on')

    groups = request.user.groups.all()
    groups = [group.name for group in groups]
    if 'SR' in groups:
        srDealer = SRDealer.objects.filter(sr_id=request.user)
        # dealerList = [item.dealer_id.dealer for item in srDealer]
        dealerList = []
        for item in srDealer:
            if item.dealer_id is not None:
                if item.dealer_id.dealer is not None:
                    dealerList.append(item.dealer_id.dealer)
        tieUp = tieUp.filter(dealer__in=dealerList)

    if request.method == "POST":
        if "search" in request.POST:
            searchValue = request.POST.get('search', None)
            # tieUp = models.TieUpMaster.objects.filter(Q(dealer__user_code=searchValue)|Q(dealer__user_code__endswith=searchValue)|Q(scheme_category_code__scheme_category_code=searchValue),
            #     dealer__is_active=True, is_active=True, is_deleted=False).order_by('-pk')
            # querycount=str(tieUp.count())+' Record found.'

            tieUp = tieUp.filter(
                    Q(dealer__user_code__icontains=searchValue) |
                    Q(scheme_category_code__scheme_category_code__icontains=searchValue) |
                    Q(scheme_category_code__scheme_category_name__icontains=searchValue) |
                    Q(discount_percent__icontains=searchValue) |
                    Q(discount_amount__icontains=searchValue))

        if 'export' in request.POST:
            tie_up = export_tie_up(request)
            file_path = os.getcwd()
            paths = file_path + '/upload/tie_up/tie_up.csv'
            if os.path.exists(paths):
                with open(paths, "rb") as output:
                    response = HttpResponse(output.read(), content_type="application/vnd.ms-excel")
                    response['Content-Disposition'] = 'attachment; filename="owims_tie_up.csv"'
                    return response

    # ............................
    data, sum_of_tieup = [], 0
    for item in tieUp:
        if any(d.get('dealer', None) == item.dealer.user_code for d in data):
            index = [i for i, _ in enumerate(data) if _['dealer'] == item.dealer.user_code][0]

            if item.scheme_category_code.scheme_category_code == "VOL":
                x_dict = {"percent": item.discount_percent, 'amount': item.discount_amount}
                data[index]['vol'] = x_dict
                sum_of_tieup += 1
            elif item.scheme_category_code.scheme_category_code == "VAL":
                x_dict = {"percent": item.discount_percent, 'amount': item.discount_amount}
                data[index]['val'] = x_dict
                sum_of_tieup += 1
            elif item.scheme_category_code.scheme_category_code == "B":
                x_dict = {"percent": item.discount_percent, 'amount': item.discount_amount}
                data[index]['b'] = x_dict
                sum_of_tieup += 1
            elif item.scheme_category_code.scheme_category_code == "M":
                x_dict = {"percent": item.discount_percent, 'amount': item.discount_amount}
                data[index]['m'] = x_dict
                sum_of_tieup += 1
        else:
            item_dict = {"dealer": item.dealer.user_code, 'vol': {}, 'm': {},
                         'val': {}, 'b': {}, 'tie_up_master_id': item.pk}

            if item.scheme_category_code.scheme_category_code == "VOL":
                x_dict = {"percent": item.discount_percent, 'amount': item.discount_amount}
                item_dict['vol'] = x_dict
                sum_of_tieup += 1
            elif item.scheme_category_code.scheme_category_code == "VAL":
                x_dict = {"percent": item.discount_percent, 'amount': item.discount_amount}
                item_dict['val'] = x_dict
                sum_of_tieup += 1
            elif item.scheme_category_code.scheme_category_code == "B":
                x_dict = {"percent": item.discount_percent, 'amount': item.discount_amount}
                item_dict['b'] = x_dict
                sum_of_tieup += 1
            elif item.scheme_category_code.scheme_category_code == "M":
                x_dict = {"percent": item.discount_percent, 'amount': item.discount_amount}
                item_dict['m'] = x_dict
                sum_of_tieup += 1

            # ..............................................................................
            """ COMPANY FROM DEALER PROFILE """
            try:
                dealer_profile = DealerProfile.objects.get(
                    dealer=item.dealer, is_active=True, is_deleted=False)
                item_dict["company_name"] = dealer_profile.company_name
            except Exception:
                item_dict["company_name"] = ""
            # ..............................................................................

            data.append(item_dict)
    # ............................

    # paginator = Paginator(tieUp, 10)
    paginator = Paginator(data, 10)
    page_style = request.GET.get("page_style", settings.DEFAULT_PAGING_VIEW)
    try:
        page_index = request.GET.get("page", 1)
        data_list = paginator.page(page_index)
    except PageNotAnInteger:
        data_list = paginator.page(1)
    except EmptyPage:
        data_list = paginator.page(paginator.num_pages)

    context = {
        'msg': '', 'data_list': data_list,
        'search': searchValue, 'querycount': querycount,
        'number_of_dealers': len(data_list),
        'sum_of_tieup': sum_of_tieup
    }
    if request.GET.get('list', None):
        context['flagList'] = True
    else:
        context['flagList'] = False
    if page_style == 'grid':
        return render(request, 'tie_up/tie_up_grid_view.html', context)
    else:
        return render(request, 'tie_up/tie_up_list_view.html', context)


def create_tie_up(request):
    form_class = forms.TieUpMasterForm
    if request.method == 'GET':
        users = User.objects.filter(groups__name='DEL', is_active=True).order_by('-pk')
        return render(request, 'tie_up/tie_up_create.html', {'tie_up_create_form': form_class, 'users': users})

    elif request.method == 'POST':
        users = User.objects.filter(groups__name='DEL', is_active=True).order_by('-pk')
        form_class = forms.TieUpMasterForm(request.POST)
        x_dealer = request.POST.get('dealer', 0)
        curr_discount = request.POST.get('discount_value')
        context = {
            'tie_up_create_form': form_class, 'users': users,
            'x_id': int(x_dealer), 'discount': curr_discount
        }
        if request.POST.get('dealer') != '':
            tieUpes = TieUpMaster.objects.filter(dealer_id=request.POST.get('dealer'))
            for tieup in tieUpes:
                if tieup.scheme_category_code_id == request.POST.get('scheme_category_code'):
                    # context = {'tie_up_create_form': form_class, 'users': users}
                    messages.add_message(request, messages.ERROR, 'Product category code for dealer is already exists...')
                    return render(request, 'tie_up/tie_up_create.html', context=context)

            if form_class.is_valid():
                newTieUp = form_class.save(commit=False)
                newTieUp.created_by = request.user
                # ..................
                if request.POST.get('discount') == 'percent':
                    percent = float(request.POST.get('discount_value'))
                    newTieUp.discount_percent = decimal.Decimal(percent)
                elif request.POST.get('discount') == 'amount':
                    newTieUp.discount_amount = float(request.POST.get('discount_value'))
                else:
                    pass
                try:
                    newTieUp.save()
                    messages.add_message(request, messages.SUCCESS, 'Tie Up created successfully...')
                except Exception:
                    if request.POST.get('discount') == 'percent':
                        messages.add_message(request, messages.ERROR,
                                             'Percent should be max 4 digits with 2 decimal places.')
                    else:
                        messages.add_message(request, messages.ERROR, 'Please enter valid amount')
                    # context = {'tie_up_create_form': form_class, 'users': users}
                    return render(request, 'tie_up/tie_up_create.html', context=context)
                # ..................
            else:
                # context = {
                #     'tie_up_create_form': form_class, 'users': users,
                #     'x_id': int(x_dealer), 'discount': curr_discount
                # }
                return render(request, 'tie_up/tie_up_create.html', context=context)
            return redirect('/master/tie_up?page_style=list')
        messages.add_message(request, messages.ERROR, 'Please select dealer code...')
        # context = {'tie_up_create_form': form_class, 'users': users}
        return render(request, 'tie_up/tie_up_create.html', context=context)


def edit_tie_up(request, tieup_id):
    dispercent = ''
    disamount = ''
    if request.method == 'GET':
        instance = get_object_or_404(TieUpMaster, tie_up_master_id=tieup_id)
        form_class = forms.TieUpMasterForm(request.POST or None, instance=instance)

        # ....................
        schemes = form_class.fields['scheme_category_code'].__dict__['_queryset']
        categories = TieUpMaster.objects.filter(dealer=instance.dealer)
        vol, val, b, m = 0, 0, 0, 0
        for item in categories:
            if item.scheme_category_code.scheme_category_code == 'VOL':
                vol = item.pk
            elif item.scheme_category_code.scheme_category_code == 'VAL':
                val = item.pk
            elif item.scheme_category_code.scheme_category_code == 'B':
                b = item.pk
            elif item.scheme_category_code.scheme_category_code == 'M':
                m = item.pk

        for i in schemes:
            if i.scheme_category_code == 'VOL':
                i.__dict__["code_id"] = vol
            elif i.scheme_category_code == 'VAL':
                i.__dict__["code_id"] = val
            elif i.scheme_category_code == 'B':
                i.__dict__["code_id"] = b
            elif i.scheme_category_code == 'M':
                i.__dict__["code_id"] = m
        # ....................

        context = {'tie_up_create_form': form_class, 'tieup': instance, 'category': schemes}
        return render(request, 'tie_up/tie_up_create.html', context=context)

    elif request.method == 'POST':
        users = User.objects.filter(groups__name='DEL', is_active=True).order_by('-pk')
        instance = get_object_or_404(TieUpMaster, tie_up_master_id=tieup_id)
        disamount = instance.discount_amount
        dispercent = instance.discount_percent
        form_class = forms.TieUpMasterForm(request.POST or None, instance=instance)
        # curr_discount = request.POST.get('discount_value')

        # ....................
        schemes = form_class.fields['scheme_category_code'].__dict__['_queryset']
        categories = TieUpMaster.objects.filter(dealer=instance.dealer)
        vol, val, b, m = 0, 0, 0, 0
        for item in categories:
            if item.scheme_category_code.scheme_category_code == 'VOL':
                vol = item.pk
            elif item.scheme_category_code.scheme_category_code == 'VAL':
                val = item.pk
            elif item.scheme_category_code.scheme_category_code == 'B':
                b = item.pk
            elif item.scheme_category_code.scheme_category_code == 'M':
                m = item.pk

        for i in schemes:
            if i.scheme_category_code == 'VOL':
                i.__dict__["code_id"] = vol
            elif i.scheme_category_code == 'VAL':
                i.__dict__["code_id"] = val
            elif i.scheme_category_code == 'B':
                i.__dict__["code_id"] = b
            elif i.scheme_category_code == 'M':
                i.__dict__["code_id"] = m
        # ....................

        context = {
            'tie_up_create_form': form_class,
            'tieup': instance, 'users': users,
            'category': schemes,
            # "discount": curr_discount
        }

        if instance.scheme_category_code_id == request.POST.get('scheme_category_code'):
            pass
        else:
            tieUpes = TieUpMaster.objects.filter(dealer__id=request.POST.get('dealer'))
            for tieup in tieUpes:
                if tieup.scheme_category_code_id == request.POST.get('scheme_category_code'):
                    # context = {'tie_up_create_form': form_class, 'tie_up': instance, 'users': users}
                    messages.add_message(request, messages.ERROR,
                                         'Scheme category code for dealer is already exists...')
                    return render(request, 'tie_up/tie_up_create.html', context=context)

        if form_class.is_valid():
            updateTieUp = form_class.save(commit=False)
            updateTieUp.created_by = request.user
            if request.POST.get('discount') == 'percent':
                percent = float(request.POST.get('discount_value'))
                updateTieUp.discount_percent = decimal.Decimal(percent)
                updateTieUp.discount_amount = float(0)
            elif request.POST.get('discount') == 'amount':
                updateTieUp.discount_amount = float(request.POST.get('discount_value'))
                updateTieUp.discount_percent = float(0)
            else:
                pass
            try:
                updateTieUp.save()

                """ need to changed """
                # businessheadobj=User.objects.filter(is_active=True,groups__name="BusinessHead")
                # html_content=''
                # if businessheadobj:
                #     emails=[]
                #     for user in businessheadobj:
                #         emails.append(user.email)
                #     if disamount!=updateTieUp.discount_amount:
                #         html_content+= "Previous Discount Amount ₹"+str(disamount)+" Current Discount Amount ₹"+str(updateTieUp.discount_amount)+" , "
                #     if dispercent!=updateTieUp.discount_percent:
                #         html_content+= "Previous Discount Percent %"+str(dispercent)+" Current Discount Percent %"+str(updateTieUp.discount_percent)+" , "
                #     html_content=html_content.rstrip(',')+"is updated"
                #     subject=" financial number changed"
                #     email = EmailMultiAlternatives(subject, html_content, settings.DEFAULT_FROM_EMAIL, to=emails)
                #     email.send()
                messages.add_message(request, messages.SUCCESS, 'Tie Up updated successfully...')
            except Exception:
                if request.POST.get('discount') == 'percent':
                    messages.add_message(request, messages.ERROR,
                                         'Percent should be max 4 digits with 2 decimal places.')
                else:
                    messages.add_message(request, messages.ERROR, 'Please enter valid amount')
                # context = {'tie_up_create_form': form_class, 'tie_up': instance, 'users': users}
                return render(request, 'tie_up/tie_up_create.html', context=context)
        else:
            # context = {
            #     'tie_up_create_form': form_class,
            #     'tie_up': instance, 'users': users,
            #     'x_id': instance.dealer, 'discount': curr_discount
            # }
            return render(request, 'tie_up/tie_up_create.html', context=context)
        return redirect('/master/tie_up?page_style=list')


def delete_tie_up(request, tieup_id):
    TieUp = TieUpMaster.objects.get(tie_up_master_id=tieup_id)
    TieUp.delete()
    messages.add_message(request, messages.SUCCESS, 'Tie Up deleted successfully...')
    return HttpResponseRedirect('/master/tie_up?page_style=list')


def import_tie_up(request):
    invalid_rows = []
    index = 0
    not_exist = []
    insert = 0
    not_insert = 0
    update = 0
    process = 0
    lblMsg = ''
    filexlsx = False

    if request.method == 'POST':
        if 'Save' in request.POST:
            #getSl_num = {}
            file = request.FILES.get('Tie_Up')
            if file != None and file != '':
                filexlsx = str(file._name).endswith('csv')
            if filexlsx == True:
                decoded_file = request.FILES['Tie_Up'].read().decode('utf-8').splitlines()
                reader = csv.DictReader(decoded_file)
                for row in reader:
                    process += 1
                    if index == 0:
                        flag = True
                        for x in import_settings.DEALER_TIE_UP_IMPORT_KEYS:
                            if x not in row.keys():
                                not_exist.append(x)
                        # return invalid file from here
                        for tie_up_code in row.keys():
                            if 'tie_up_' in tie_up_code:
                                tie_up_code = tie_up_code.replace('tie_up_', '').upper()
                                if '_AMT' in tie_up_code:
                                    tie_up_code = tie_up_code.replace('_AMT', '').upper()
                                scheme_catgs = SchemeCategoryMaster.objects.filter(scheme_category_code=tie_up_code)
                                if len(scheme_catgs) <= 0:
                                    SchemeCategoryMaster.objects.create(scheme_category_code=tie_up_code,
                                                                        scheme_category_name=tie_up_code)
                                    index = index + 1
                    set_exist = len(not_exist)

                    if set_exist == 0:
                        if row['dealer_code'] != '' and row['tie_up_b'] != '' and row[
                            'tie_up_m'] != '' and row['tie_up_vol'] != '' and row['tie_up_val'] != '':
                            if float(row['tie_up_b'].rstrip('%')) >= 0 and float(row['tie_up_m'].rstrip('%')) >= 0 \
                                    and float(row['tie_up_vol'].rstrip('%')) >= 0 and float(
                                row['tie_up_val'].rstrip('%')) >= 0:
                                try:
                                    user=User.objects.get(user_code=row["dealer_code"], is_active=True)
                                    try:
                                        dealer_profile = DealerProfile.objects.get(
                                            dealer_code=row["dealer_code"])  # required fields checks
                                        code = DealersBrand.objects.get(user=dealer_profile.dealer_id)
                                        Brand = BrandMaster.objects.get(brand_code=code.brand_id)

                                        up = 0
                                        cnt = 0
                                        for column in row.keys():
                                            if 'tie_up_' in column:
                                                scheme_code = column.replace('tie_up_', '')
                                                if '_amt' in scheme_code:
                                                    scheme_code = scheme_code.replace('_amt', '')
                                                try:
                                                    TUM = TieUpMaster.objects.filter(dealer_id=dealer_profile.dealer_id,
                                                                                     brand_id=Brand.brand_code,
                                                                                     scheme_category_code=scheme_code.upper())
                                                    if len(TUM)>1:
                                                        TUM = TUM[0]
                                                    else:
                                                        TUM = TieUpMaster.objects.get(dealer_id=dealer_profile.dealer_id,
                                                                                     brand_id=Brand.brand_code,
                                                                                     scheme_category_code=scheme_code.upper())
                                                    up += 1
                                                except TieUpMaster.DoesNotExist:
                                                    TUM = TieUpMaster()
                                                    cnt += 1
                                                TUM.dealer = User.objects.get(id=dealer_profile.dealer_id, is_active=True)
                                                TUM.brand = BrandMaster.objects.get(brand_code=Brand.brand_code)
                                                TUM.scheme_category_code = SchemeCategoryMaster.objects.get(scheme_category_code=scheme_code.upper())
                                                if '_amt' in column:
                                                    if row[column] != '':
                                                        TUM.discount_amount = float(row[column])
                                                    else:
                                                        TUM.discount_amount = 0
                                                else:
                                                    if row[column] != '':
                                                        TUM.discount_percent = float(row[column].rstrip('%'))
                                                    else:
                                                        TUM.discount_percent = 0
                                                TUM.save()
                                        if cnt == 4:
                                            insert += 1
                                        if cnt == 8:
                                            insert += 1
                                        if up == 4:
                                            update += 1
                                        if up == 8:
                                            update += 1
                                    except DealerProfile.DoesNotExist:
                                        error1 = {}
                                        error1['error_message'] = 'Column <dealer_code> at row ' + str(
                                            process) + ' does not exist'
                                        invalid_rows.append(error1)
                                except Exception:
                                        errorusr = {}
                                        errorusr['error_message'] = 'This user_code = '+str(row["dealer_code"])+' at row ' + str(
                                            process) + ' is not active'
                                        invalid_rows.append(errorusr)
                                        not_insert += 1
                            else:
                                error_b = {}
                                if float(row['tie_up_b'].rstrip('%')) < 0:
                                    error_b['error_message'] = 'Column <tie_up_b> at row ' + str(
                                        process) + ' contain negative values'
                                    invalid_rows.append(error_b)
                                error_m = {}
                                if float(row['tie_up_m'].rstrip('%')) < 0:
                                    error_m['error_message'] = 'Column <tie_up_m> at row ' + str(
                                        process) + ' contain negative values'
                                    invalid_rows.append(error_m)
                                error_val = {}
                                if float(row['tie_up_val'].rstrip('%')) < 0:
                                    error_val['error_message'] = 'Column <tie_up_val> at row ' + str(
                                        process) + ' contain negative values'
                                    invalid_rows.append(error_val)
                                error_vol = {}
                                if float(row['tie_up_vol'].rstrip('%')) < 0:
                                    error_vol['error_message'] = 'Column <tie_up_vol> at row ' + str(
                                        process) + ' contain negative values'
                                    invalid_rows.append(error_vol)
                            # row['error_message']='Field contain negative values'

                        else:
                            error_code = {}
                            if row['dealer_code'] == '':
                                error_code['error_message'] = 'Column <dealer_code> at row ' + str(
                                    process) + ' contain null values'
                                invalid_rows.append(error_code)
                            error_ab = {}
                            if row['tie_up_b'] == '':
                                error_ab['error_message'] = 'Column <tie_up_b> at row ' + str(
                                    process) + ' contain null values'
                                invalid_rows.append(error_ab)
                            error_am = {}
                            if row['tie_up_m'] == '':
                                error_am['error_message'] = 'Column <tie_up_m> at row ' + str(
                                    process) + ' contain null values'
                                invalid_rows.append(error_am)
                            error_aval = {}
                            if row['tie_up_val'] == '':
                                error_aval['error_message'] = 'Column <tie_up_val> at row ' + str(
                                    process) + ' contain null values'
                                invalid_rows.append(error_aval)
                            error_avol = {}
                            if row['tie_up_vol'] == '':
                                error_avol['error_message'] = 'Column <tie_up_vol> at row ' + str(
                                    process) + ' contain null values'
                                invalid_rows.append(error_avol)
                    else:
                        lblMsg = "<div class='wrong_msg ' style='max-width: 665px;margin: 0 auto;'>" \
                                 "<ul>" \
                                 "<li style='text-align:center'><span class='glyphicon glyphicon-remove-sign'></span>Error! </li>" \
                                 "<li><span class='glyphicon glyphicon-arrow-right'></span>  Sheet must have these columns !!</li>" \
                                 "<li> <span class='glyphicon glyphicon-arrow-right'></span>  dealer_code, tie_up_b, tie_up_m, tie_up_vol, tie_up_val</li>" \
                                 "</ul>" \
                                 "</div>"
                    if len(invalid_rows) >= 0:
                        # invalid_rows.append(error)
                        filespath = os.getcwd()
                        paths = filespath + '/upload/tie_up/'
                        if not os.path.exists(paths):
                            os.makedirs(paths)
                        file_path = filespath + '/upload/tie_up/not_insert_tie_up.csv'
                        keys = ['error_message']
                    with open(file_path, "w") as output:
                        dict_writer = csv.DictWriter(output, keys)
                        dict_writer.writeheader()
                        if len(invalid_rows) > 0:
                            dict_writer.writerows(invalid_rows)

                messages.add_message(request, messages.SUCCESS, 'File Processed Successfully...')
            else:
                lblMsg = "<div class='wrong_msg' style='max-width: 665px;margin: 0 auto;'>" \
                         "<ul>" \
                         "<li style='text-align:center'><span class='glyphicon glyphicon-remove-sign'></span>Error! </li>" \
                         "<li><span class='glyphicon glyphicon-arrow-right'></span>  Must contain .csv file!!</li>" \
                         "</ul>" \
                         "</div>"
            if insert != 0 or not_insert != 0 or update != 0:
                lblMsg = "<div class='wright_msg'  style='max-width: 665px;margin: 0 auto;'>" \
                         "<ul>" \
                         "<li style='color: #5cb85c;font-size: 21px;font-weight: 700; text-align:center'> <span class='glyphicon glyphicon-ok-sign'></span> File  Processed Successfully .</li>" \
                         "<li style='color: #5cb85c;font-size: 21px;font-weight: 700;'> <span class='glyphicon glyphicon-arrow-right'></span> Total Record(s) Processed =" + str(
                    process) + \
                         "<li  style='color: #5cb85c;font-size: 21px;font-weight: 700;' > <span class='glyphicon glyphicon-arrow-right'></span> Total Record(s) Inserted =" + str(
                    insert) + \
                         "</li ><li  style='color: #5cb85c;font-size: 21px;font-weight: 700;'> <span class='glyphicon glyphicon-arrow-right'></span> Total Record(s) Updated =" + str(
                    update) + \
                         "</li ><li  style='color: #5cb85c;font-size: 21px;font-weight: 700;'> <span class='glyphicon glyphicon-arrow-right'></span> Total Record(s) Not Inserted =" + str(
                    not_insert) + \
                         "</li ></ul>" \
                         "For Errors, <input value='Click to download' type='submit' " \
                         "name='download' style=' background-color: #ecf0f5; padding: 5px 15px; text-align: center;font-size: 15px; cursor: pointer;display: inline-block; border-radius: 4px;font-weight: 600;'></div>"
        if 'download' in request.POST:
            file_path = os.getcwd()
            paths = file_path + '/upload/tie_up/not_insert_tie_up.csv'
            if os.path.exists(paths):
                with open(paths, 'rb') as fh:
                    response = HttpResponse(fh.read(), content_type="application/vnd.ms-excel")
                    response['Content-Disposition'] = 'inline; filename=' + os.path.basename(paths)
                    return response
    return render(request, 'tie_up/import_tie_up.html', {'lblMsg': lblMsg})


def export_tie_up(request):
    # if 'selected_brand' in request.session:
    #     tie_up_info = models.TieUpMaster.objects.filter(
    #         brand=request.session["selected_brand"]).order_by('-pk')
    # else:
    #     tie_up_info = models.TieUpMaster.objects.all().order_by('-pk')
    # for tie_up in tie_up_info:
    #     get_tie_up = {}
    #     code = str(tie_up.scheme_category_code.scheme_category_code)
    #     get_tie_up['dealer_code'] = str(tie_up.dealer.user_code)
    #     if code == 'B':
    #         get_tie_up['tie_up_b'] = str(tie_up.discount_percent) + '%'
    #     if code == 'M':
    #         get_tie_up['tie_up_m'] = str(tie_up.discount_percent) + '%'
    #     if code == 'VOL':
    #         get_tie_up['tie_up_vol'] = str(tie_up.discount_percent) + '%'
    #     if code == 'VAL':
    #         get_tie_up['tie_up_val'] = str(tie_up.discount_percent) + '%'

    dealer = DealerProfile.objects.filter(is_active=True)
    result = []
    for DP in dealer:
        tie_up_info = TieUpMaster.objects.filter(dealer_id=DP.dealer_id)

        get_tie_up = {}
        for tie_up in tie_up_info:
            code = str(tie_up.scheme_category_code.scheme_category_code)
            get_tie_up['dealer_code'] = str(tie_up.dealer.user_code)
            if code == 'B':
                get_tie_up['tie_up_b'] = str(tie_up.discount_percent) + '%'
            if code == 'M':
                get_tie_up['tie_up_m'] = str(tie_up.discount_percent) + '%'
            if code == 'VOL':
                get_tie_up['tie_up_vol'] = str(tie_up.discount_percent) + '%'
            if code == 'VAL':
                get_tie_up['tie_up_val'] = str(tie_up.discount_percent) + '%'
        if tie_up_info:
            result.append(get_tie_up)
    filespath = os.getcwd()
    paths = filespath + '/upload/tie_up/'
    if not os.path.exists(paths):
        os.makedirs(paths)
    file_path = filespath + '/upload/tie_up/tie_up.csv'
    keys = ['dealer_code', 'tie_up_b', 'tie_up_m', 'tie_up_vol', 'tie_up_val']
    with open(file_path, "w") as output:
        dict_writer = csv.DictWriter(output, keys)
        dict_writer.writeheader()
        if len(result) > 0:
            dict_writer.writerows(result)
    return result


def scheme(request):
    scheme = models.SchemeMaster.objects.all()

    return render(request, 'scheme/scheme.html', {'scheme': scheme})


def create_scheme(request):
    form_class = forms.SchemeMasterForm
    if request.method == 'GET':
        return render(request, 'scheme/scheme_create.html', {'scheme_create_form': form_class})

    elif request.method == 'POST':
        form_class = forms.SchemeMasterForm(request.POST)
        if form_class.is_valid():
            scheme = form_class.save(commit=False)
            scheme.created_by = request.user
            scheme.save()
        else:
            return render(request, 'scheme/scheme_create.html', {'scheme_create_form': form_class})
    elif request.method == 'PUT':
        form_class = forms.SchemeMasterForm(request.POST)
        if form_class.is_valid():
            scheme = form_class.save(commit=False)
            scheme.created_by = request.user
            scheme.save()
        return render(request, 'scheme/scheme_create.html', {'scheme_create_form': form_class})


def warehouse_admin(request):
    warehouse_admin = models.WarehouseAdmin.objects.filter(is_active=True,is_deleted=False)

    return render(request, 'warehouse_admin/warehouse_admin.html', {'warehouse_admin': warehouse_admin})


def create_warehouse_admin(request):
    form_class = forms.WarehouseAdminForm
    userobj=User.objects.filter(groups__name='Warehouse_user', is_active=True)
    if request.method == 'GET':
        context={'warehouse_admin_create_form': form_class,'userobj':userobj}
        return render(request, 'warehouse_admin/warehouse_admin_create.html',context)
    elif request.method == 'POST':
        form_class = forms.WarehouseAdminForm(request.POST)
        if form_class.is_valid():
            warehouse_admin = form_class.save(commit=False)
            warehouse_admin.created_by = request.user
            warehouse_admin.is_active=True
            warehouse_admin.save()
            messages.add_message(request, messages.SUCCESS, 'Warehouse User created successfully...')
        return render(request, 'warehouse_admin/warehouse_admin_create.html',
                      {'warehouse_admin_create_form': form_class})
    # elif request.method == 'PUT':
    #     form_class = forms.WarehouseAdminForm(request.POST)
    #     if form_class.is_valid():
    #         warehouse_admin = form_class.save(commit=False)
    #         warehouse_admin.created_by = request.user
    #         warehouse_admin.save()
    #     return render(request, 'warehouse_admin/warehouse_admin_create.html',
    #                   {'warehouse_admin_create_form': form_class})


def edit_warehouse_admin(request, wareid):
    if request.method == 'GET':
        userobj = User.objects.filter(groups__name='Warehouse_user', is_active=True)
        try:
            instance=WarehouseAdmin.objects.get(is_active=True,is_deleted=False,id=wareid)
        except Exception:
            instance=''
        if instance!='':
            form_class = forms.WarehouseAdminForm(request.POST or None, instance=instance)
        else:
            form_class = forms.WarehouseAdminForm
        context={'warehouse_admin_create_form': form_class,'userobj':userobj,'instance':instance}
        return render(request, 'warehouse_admin/warehouse_admin_create.html',context)
    
    if request.method=='POST':
        check=0
        try:
            instance=WarehouseAdmin.objects.get(is_active=True,is_deleted=False,id=wareid)
        except Exception:
            instance=''
        if instance!='':
            form_class = forms.WarehouseAdminForm(request.POST, instance=instance)
            check=1
        else:
            check=0
        if check!=0:
            if form_class.is_valid():
                warehouse_admin = form_class.save(commit=False)
                warehouse_admin.created_by = request.user
                warehouse_admin.is_active=True
                warehouse_admin.save()
                messages.add_message(request, messages.SUCCESS, 'Warehouse User created successfully...')
                return HttpResponseRedirect('/master/warehouse_admin')
            return render(request, 'warehouse_admin/warehouse_admin_create.html',
                      {'warehouse_admin_create_form': form_class})
        else:
            return HttpResponseRedirect('/master/warehouse_admin/create')


def product_price_history(request):
    product_price_history = models.ProductPriceHistory.objects.all()
    return render(request, 'product_price_history/product_price_history.html',
                  {'product_price_history': product_price_history})


def create_product_price_history(request):
    form_class = forms.ProductPriceHistoryForm
    if request.method == 'GET':
        return render(request, 'product_price_history/product_price_history_create.html',
                      {'product_price_history_create_form': form_class})
    elif request.method == 'POST':
        form_class = forms.ProductPriceHistoryForm(request.POST)
        if form_class.is_valid():
            product_history = form_class.save(commit=False)
            product_history.created_by = request.user
            product_history.save()
        return render(request, 'product_price_history/product_price_history_create.html',
                      {'product_price_history_create_form': form_class})

    elif request.method == 'PUT':
        form_class = forms.ProductPriceHistoryForm(request.POST)
        if form_class.is_valid():
            product_history = form_class.save(commit=False)
            product_history.created_by = request.user
            product_history.save()
        return render(request, 'product_price_history/product_price_history_create.html',
                      {'product_price_history_create_form': form_class})


def dealers_brand(request):
    dealers_brand = models.DealersBrand.objects.all()

    return render(request, 'dealers_brand/dealers_brand.html', {'dealers_brand': dealers_brand})


def create_dealers_brand(request):
    form_class = forms.DealersBrandForm
    if request.method == 'GET':
        return render(request, 'dealers_brand/dealers_brand_create.html', {'dealers_brand_create_form': form_class})
    elif request.method == 'POST':

        form_class = forms.DealersBrandForm(request.POST)
        if form_class.is_valid():
            dealers_brand = form_class.save(commit=False)
            dealers_brand.created_by = request.user
            dealers_brand.save()
        return render(request, 'dealers_brand/dealers_brand_create.html', {'dealers_brand_create_form': form_class})
    elif request.method == 'PUT':
        form_class = forms.DealersBrandForm(request.POST)
        if form_class.is_valid():
            dealers_brand = form_class.save(commit=False)
            dealers_brand.created_by = request.user
            dealers_brand.save()
        return render(request, 'dealers_brand/dealers_brand_create.html', {'dealers_brand_create_form': form_class})


def edit_dealers_brand(request, DBid):
    if request.method == 'GET':
        instance = get_object_or_404(DealersBrand, pk=DBid)
        form_class = forms.DealersBrandForm(request.POST or None, instance=instance)
        return render(request, 'dealers_brand/dealers_brand_create.html', {'dealers_brand_create_form': form_class})


def scheme_product(request):
    scheme_product = models.SchemeProductMaster.objects.all()

    return render(request, 'scheme_product/scheme_product.html', {'scheme_product': scheme_product})


def create_scheme_product(request):
    form_class = forms.SchemeProductMasterForm
    if request.method == 'GET':
        return render(request, 'scheme_product/scheme_product_create.html', {'scheme_product_create_form': form_class})
    elif request.method == 'POST':
        form_class = forms.SchemeProductMasterForm(request.POST)
        if form_class.is_valid():
            scheme_product = form_class.save(commit=False)
            scheme_product.created_by = request.user
            scheme_product.save()
        return render(request, 'scheme_product/scheme_product_create.html', {'scheme_product_create_form': form_class})
    elif request.method == 'PUT':
        form_class = forms.SchemeProductMasterForm(request.POST)
        if form_class.is_valid():
            scheme_product = form_class.save(commit=False)
            scheme_product.created_by = request.user
            scheme_product.save()
        return render(request, 'scheme_product/scheme_product_create.html', {'scheme_product_create_form': form_class})


def scheme_category(request):
    scheme_category = models.SchemeCategoryMaster.objects.all()

    return render(request, 'scheme_category/scheme_category.html',
                  {'scheme_category': scheme_category})


def create_scheme_category(request):
    form_class = forms.SchemeCategoryMasterForm
    if request.method == 'GET':
        context = {'scheme_category_create_form': form_class}
        return render(request, 'scheme_category/scheme_category_create.html', context=context)

    elif request.method == 'POST':
        form_class = forms.SchemeCategoryMasterForm(request.POST)
        if form_class.is_valid():
            schemeCategory = form_class.save(commit=False)
            schemeCategory.created_by = request.user
            schemeCategory.save()
        context = {'scheme_category_create_form': form_class}
        return render(request, 'scheme_category/scheme_category_create.html', context=context)

    elif request.method == 'PUT':
        form_class = forms.SchemeCategoryMasterForm(request.POST)
        if form_class.is_valid():
            schemeCategory = form_class.save(commit=False)
            schemeCategory.created_by = request.user
            schemeCategory.save()
        context = {'scheme_category_create_form': form_class}
        return render(request, 'scheme_category/scheme_category_create.html', context=context)


def edit_scheme_category(request, SCid):
    if request.method == 'GET':
        instance = get_object_or_404(SchemeCategoryMaster, pk=SCid)
        form_class = forms.SchemeCategoryMasterForm(request.POST or None, instance=instance)
        return render(request, 'scheme_category/scheme_category_create.html',
                      {'scheme_category_create_form': form_class})

today = datetime.date.today()
def scheme_sell_through(request):
    searchValue = ''
    querycount = ''
    if 'selected_brand' in request.session:
        schemeSellThrough = models.SchemeSellThroughMaster.objects.filter(
            brand=request.session["selected_brand"]).order_by(
            '-scheme_end_date')
    else:
        schemeSellThrough = models.SchemeSellThroughMaster.objects.filter(
            is_active=True, is_deleted=False).order_by(
            '-scheme_end_date')

    groups = request.user.groups.all()
    groups = [group.name for group in groups]
    if 'SR' in groups:
        schemeSellThrough = models.SchemeSellThroughMaster.objects.filter(
            is_active=True, is_deleted=False,scheme_end_date__gte=today).order_by(
            '-scheme_end_date')
    #     srDealer = SRDealer.objects.filter(sr_id=request.user)
    #     dealerList = [item.dealer_id.dealer for item in srDealer]
    #     schemeSellThrough = schemeSellThrough.filter(dealer__in=dealerList)

    data_list = None
    if request.method == "POST":
        if "search" in request.POST:
            searchValue = request.POST.get('search', None)
            schemeSellThrough = schemeSellThrough.filter(
                    Q(state_code__state_name__icontains=searchValue) |
                    Q(city_code__city_name__icontains=searchValue) |
                    Q(product__sku_code__icontains=searchValue) |
                    Q(discount_percent__icontains=searchValue) |
                    Q(discount_amount__icontains=searchValue) |
                    Q(scheme_name__icontains=searchValue) |
                    Q(scheme_code__icontains=searchValue))

        if 'export' in request.POST:
            export_sell_through(request)
            file_path = os.getcwd()
            paths = file_path + '/upload/sell_through/scheme_sell_through.csv'
            if os.path.exists(paths):
                with open(paths, "rb") as output:
                    response = HttpResponse(output.read(), content_type="application/vnd.ms-excel")
                    response['Content-Disposition'] = 'attachment; filename="owims_scheme_sellthro.csv"'
                    return response

    page_style = request.GET.get("page_style", settings.DEFAULT_PAGING_VIEW)
    paginator = Paginator(schemeSellThrough, 10)
    try:
        page_index = request.GET.get("page", 1)
        data_list = paginator.page(page_index)
    except PageNotAnInteger:
        data_list = paginator.page(1)
    except EmptyPage:
        data_list = paginator.page(paginator.num_pages)

    context = {
        'msg': '',
        'data_list': data_list,
        'search': searchValue,
        'querycount': querycount,
        'sellCount': len(data_list)
    }
    if request.GET.get('list', None):
        context['flagList'] = True
    else:
        context['flagList'] = False

    if page_style == 'grid':
        return render(request, 'sell_through/sell_thru_grid_view.html', context)
    else:
        return render(request, 'sell_through/sell_thru_list_view.html', context)


def create_scheme_sell_through(request):
    form_class = forms.SchemeSellThroughMasterForm
    if request.method == 'GET':
        products = ProductMaster.objects.filter(~Q(sku_code=None))
        context = {'scheme_sell_through_create_form': form_class, 'products': products}
        return render(request, 'sell_through/scheme_sell_through_create.html', context=context)

    elif request.method == 'POST':
        if request.POST['city_code']:
            city = models.CityMaster.objects.get(city_id=request.POST['city_code'])
        form_class = forms.SchemeSellThroughMasterForm(request.POST)
        products = ProductMaster.objects.filter(~Q(sku_code=None))

        if form_class.is_valid():
            try:
                skucode = request.POST['product']
                scheme_code = request.POST['scheme_code']
                SchemeSellThroughMaster.objects.get(
                    product_id=skucode, scheme_code=scheme_code.strip(),
                    is_active=True, is_deleted=False)
                messages.add_message(request, messages.ERROR, 'Scheme Sell Through already exists...')
                context = {'scheme_sell_through_create_form': form_class, 'products': products, 'item': int(skucode)}
                return render(request, 'sell_through/scheme_sell_through_create.html', context=context)
            except SchemeSellThroughMaster.DoesNotExist:
                schemeSellThrough = form_class.save(commit=False)
                schemeSellThrough.created_by = request.user
                schemeSellThrough.save()
                messages.add_message(request, messages.SUCCESS, 'Scheme Sell Through created successfully...')
        else:
            context = {'scheme_sell_through_create_form': form_class, 'products': products}
            return render(request, 'sell_through/scheme_sell_through_create.html', context=context)

    return redirect('/master/scheme_sell_through/?page_style=list')


def edit_sell_through(request, SSTid):
    disamount=''
    dispercent=''
    if request.method == 'GET':
        instance = get_object_or_404(SchemeSellThroughMaster, master_id=SSTid)
        form_class = forms.SchemeSellThroughMasterForm(request.POST or None, instance=instance)
        context = {'scheme_sell_through_create_form': form_class, 'sell_thru': instance}
        return render(request, 'sell_through/scheme_sell_through_create.html', context=context)

    elif request.method == 'POST':
        products = ProductMaster.objects.filter(~Q(sku_code=None))
        instance = SchemeSellThroughMaster.objects.get(master_id=SSTid)
        disamount=instance.discount_amount
        dispercent=instance.discount_percent
        form_class = forms.SchemeSellThroughMasterForm(request.POST, instance=instance)
        if form_class.is_valid():
            schemeSellThrough = form_class.save(commit=False)
            schemeSellThrough.dealer = request.POST.get('dealer')
            # .......................
            try:
                skucode = request.POST['product']
                scheme_code = request.POST['scheme_code']
                newScheme = SchemeSellThroughMaster.objects.get(
                    product_id=skucode, scheme_code=scheme_code.strip(),
                    is_active=True, is_deleted=False)
                if newScheme.pk != schemeSellThrough.pk:
                    messages.add_message(request, messages.ERROR, 'Scheme Sell Through already exists...')
                    context = {'scheme_sell_through_create_form': form_class, 'products': products,
                               'item': int(skucode)}
                    return render(request, 'sell_through/scheme_sell_through_create.html', context=context)
            except SchemeSellThroughMaster.DoesNotExist:
                pass
            # .......................
            form_class.save()

            """ need to changed """
            # businessheadobj=User.objects.filter(is_active=True,groups__name="BusinessHead")
            # html_content=''
            # if businessheadobj:
            #     emails=[]
            #     for user in businessheadobj:
            #         emails.append(user.email)
            #     if disamount!=form_class.cleaned_data['discount_amount']:
            #         html_content+= "Previous Discount Amount ₹"+str(disamount)+" Current Discount Amount ₹"+str(form_class.cleaned_data['discount_amount'])+" , "
            #     if dispercent!=form_class.cleaned_data['discount_percent']:
            #         html_content+= "Previous Discount Percent %"+str(dispercent)+" Current Discount Percent %"+str(form_class.cleaned_data['discount_percent'])+" , "
            #     html_content=html_content.rstrip(',')+"is updated"
            #     subject=" financial number changed"
            #     email = EmailMultiAlternatives(subject, html_content, settings.DEFAULT_FROM_EMAIL, to=emails)
            #     email.send()
            messages.add_message(request, messages.SUCCESS, 'Scheme Sell Through updated successfully...')
        else:
            context = {'scheme_sell_through_create_form': form_class, 'products': products}
            return render(request, 'sell_through/scheme_sell_through_create.html', context=context)
        return redirect('/master/scheme_sell_through/?page_style=list')


def delete_sell_through(request, SSTid):
    schemeSellThrough = SchemeSellThroughMaster.objects.get(master_id=SSTid)
    schemeSellThrough.delete()
    messages.add_message(request, messages.SUCCESS, 'Scheme Sell Through deleted successfully...')
    return HttpResponseRedirect('/master/scheme_sell_through?page_style=list')


def import_scheme_sell_through(request):
    invalid_rows = []
    index = 0
    not_exist = []
    insert = 0
    not_insert = 0
    update = 0
    process = 0
    lblMsg = ''
    Msg = ''
    msgcheck=0
    filexlsx = False
    if request.method == 'POST':
        if 'Save' in request.POST:
            getSl_num = {}
            file = request.FILES.get('Sell_Through')
            if file != None and file != '':
                filexlsx = str(file._name).endswith('csv')
            if filexlsx == True:
                decoded_file = request.FILES['Sell_Through'].read().decode('utf-8').splitlines()
                reader = csv.DictReader(decoded_file)
                for row in reader:
                    process += 1
                    if index == 0:
                        flag = True
                        for x in import_settings.SCHEME_SELL_THROUGH_IMOPRT:
                            if x not in row.keys():
                                not_exist.append(x)
                        # return invalid file from here
                    set_exist = len(not_exist)
                    if set_exist == 0:
                        if row['sku_code'] != '' and row['scheme_start_date'] != '' and row['scheme_end_date'] != '' and \
                                row['scheme_name'] != '' and row['scheme_code'] != '':
                            sdatecount=0
                            if '/' in row['scheme_start_date']:
                                sdate = row['scheme_start_date'].split('/')
                                sdatecount=len(sdate[2])
                            elif '-' in row['scheme_start_date']:
                                sdate = row['scheme_start_date'].split('-')
                                sdatecount=len(sdate[2])
                            edatecount=0
                            if '/' in row['scheme_end_date']:
                                edate = row['scheme_end_date'].split('/')
                                edatecount=len(edate[2])
                            elif '-' in row['scheme_end_date']:
                                edate = row['scheme_end_date'].split('-')
                                edatecount=len(edate[2])
                            if sdatecount == 4 and edatecount==4:
                                discountAmt = 0
                                discountper = 0
                                error_disc = ''
                                if row['discount_percent'] != '':
                                    discountper = float(row['discount_percent'].rstrip('%'))
                                else:
                                    row['discount_percent']=0
                                if row['discount_amount'] != "":
                                    discountAmt = int(row['discount_amount'])
                                else:
                                    error_disc = 'No value'
                                    row['discount_amount']=0

                                # if float(row['discount_percent'].rstrip('%')) >= 0 or int(row['discount_amount']) >= 0:
                                if discountAmt >= 0 and discountper >=0:
                                    try:
                                        # Dealer = DealerProfile.objects.get(dealer_code=row['dealer'])
                                        Brand = BrandMaster.objects.get(brand_name__icontains=row['brand'], is_active=True)
                                        PM = ProductMaster.objects.get(sku_code=str(row['sku_code']), is_active=True)
                                        try:
                                            Scheme = SchemeMaster.objects.get(scheme_name=row['scheme_name'],
                                                                              scheme_code=row['scheme_code'])
                                        except Exception:
                                            Scheme = SchemeMaster()
                                        Scheme.state = row['state']
                                        Scheme.city = row['city']
                                        Scheme.product = ''
    
                                        # Scheme.discount_percent = float(row['discount_percent'].rstrip('%'))
                                        # Scheme.discount_amount = float(row['discount_amount'].rstrip('%'))
                                        if row['discount_percent'] == '':
                                            Scheme.discount_percent=0
                                        else:
                                            Scheme.discount_percent = float(row['discount_percent'].rstrip('%'))

                                        if row['discount_amount'] == '':
                                            Scheme.discount_amount=0
                                        else:
                                            Scheme.discount_amount = float(row['discount_amount'].rstrip('%'))
                                        sdatecount=0
                                        if '/' in row['scheme_start_date']:
                                            sdate = row['scheme_start_date'].split('/')
                                            sdatecount=len(sdate[2])
                                        elif '-' in row['scheme_start_date']:
                                            sdate = row['scheme_start_date'].split('-')
                                            sdatecount=len(sdate[2])
                                        if sdatecount == 4:
                                            if '/' in row['scheme_start_date']:
                                                startdate =datetime.datetime.strptime(str(row['scheme_start_date']), '%d/%m/%Y').strftime('%Y/%m/%d')
                                                Scheme.scheme_start_date = datetime.datetime.strptime(startdate,'%Y/%m/%d')
                                            elif '-' in row['scheme_start_date']:
                                                startdate =datetime.datetime.strptime(str(row['scheme_start_date']), '%d-%m-%Y').strftime('%Y-%m-%d')
                                                Scheme.scheme_start_date = datetime.datetime.strptime(startdate,'%Y-%m-%d')
                                        edatecount=0
                                        if '/' in row['scheme_end_date']:
                                            edate = row['scheme_end_date'].split('/')
                                            edatecount=len(edate[2])
                                        elif '-' in row['scheme_end_date']:
                                            edate = row['scheme_end_date'].split('-')
                                            edatecount=len(edate[2])
                                        if edatecount == 4:
                                            if '/' in row['scheme_end_date']:
                                                enddate = datetime.datetime.strptime(str(row['scheme_end_date']), '%d/%m/%Y').strftime('%Y/%m/%d')
                                                Scheme.scheme_end_date = datetime.datetime.strptime(enddate, '%Y/%m/%d')
                                            elif '-' in row['scheme_end_date']:
                                                enddate = datetime.datetime.strptime(str(row['scheme_end_date']), '%d-%m-%Y').strftime('%Y-%m-%d')
                                                Scheme.scheme_end_date = datetime.datetime.strptime(enddate, '%Y-%m-%d')
                                            Scheme.discount_amount = int(row['discount_amount'].rstrip('%'))
                                            Scheme.scheme_name = row['scheme_name']
                                            Scheme.scheme_code = row['scheme_code']
                                            Scheme.save()
                                        try:
                                            # obj_Scheme = SchemeSellThroughMaster.objects.get(product=PM.pk)
                                            obj_Scheme = SchemeSellThroughMaster.objects.get(
                                                product=PM.pk, scheme_code=row['scheme_code'],
                                                is_active=True, is_deleted=False)
                                            update += 1
                                        except Exception:
                                            obj_Scheme = SchemeSellThroughMaster()
                                            insert += 1
                                        # obj_Scheme.dealer = User.objects.get(id=Dealer.dealer_id, is_active=True)
                                        obj_Scheme.brand = BrandMaster.objects.get(brand_name=Brand.brand_name)
                                        obj_Scheme.product = ProductMaster.objects.get(sku_code=PM.sku_code)
                                        #obj_Scheme.discount_percent = float(row['discount_percent'].rstrip('%'))
                                        #obj_Scheme.discount_amount = int(row['discount_amount'].rstrip('%'))
                                        obj_Scheme.discount_percent = discountper
                                        obj_Scheme.discount_amount = discountAmt
                                        try:
                                            State=StateMaster.objects.get(state_code = row['state'])
                                            obj_Scheme.state_code = StateMaster.objects.get(pk = State.pk)
                                        except Exception:
                                            State=''
                                        try:
                                            City=CityMaster.objects.get(city_code = row['city'])
                                            obj_Scheme.city_code = CityMaster.objects.get(pk = City.pk)
                                        except Exception:
                                            state=''
                                        #obj_Scheme.city = row['city']
                                        sdatecount=0
                                        if '/' in row['scheme_start_date']:
                                            sdate = row['scheme_start_date'].split('/')
                                            sdatecount=len(sdate[2])
                                        elif '-' in row['scheme_start_date']:
                                            sdate = row['scheme_start_date'].split('-')
                                            sdatecount=len(sdate[2])
                                        if sdatecount == 4:
                                            if '/' in row['scheme_start_date']:
                                                startdate =datetime.datetime.strptime(str(row['scheme_start_date']), '%d/%m/%Y').strftime('%Y/%m/%d')
                                                obj_Scheme.scheme_start_date = datetime.datetime.strptime(startdate,'%Y/%m/%d')
                                            elif '-' in row['scheme_start_date']:
                                                startdate =datetime.datetime.strptime(str(row['scheme_start_date']), '%d-%m-%Y').strftime('%Y-%m-%d')
                                                obj_Scheme.scheme_start_date = datetime.datetime.strptime(startdate,'%Y-%m-%d')
                                        edatecount=0
                                        if '/' in row['scheme_end_date']:
                                            edate = row['scheme_end_date'].split('/')
                                            edatecount=len(edate[2])
                                        elif '-' in row['scheme_end_date']:
                                            edate = row['scheme_end_date'].split('-')
                                            edatecount=len(edate[2])
                                        if edatecount == 4:
                                            if '/' in row['scheme_end_date']:
                                                enddate = datetime.datetime.strptime(str(row['scheme_end_date']), '%d/%m/%Y').strftime('%Y/%m/%d')
                                                obj_Scheme.scheme_end_date = datetime.datetime.strptime(enddate, '%Y/%m/%d')
                                            elif '-' in row['scheme_end_date']:
                                                enddate = datetime.datetime.strptime(str(row['scheme_end_date']), '%d-%m-%Y').strftime('%Y-%m-%d')
                                                obj_Scheme.scheme_end_date = datetime.datetime.strptime(enddate, '%Y-%m-%d')
                                            obj_Scheme.scheme_name = row['scheme_name']
                                            obj_Scheme.scheme_code = row['scheme_code']
                                            obj_Scheme.save()
                                    except ProductMaster.DoesNotExist:
                                        error_brand = {}
                                        error_brand['error_message'] = 'Column <sku_code> at row ' + str(
                                            process) + ' does not exist.'
                                        invalid_rows.append(error_brand)
                                        not_insert += 1
                                    except BrandMaster.DoesNotExist:
                                        Brand = ''
                                    # except Exception:
                                    #     error_dealer = {}
                                    #     error_dealer['error_message'] = 'Column <dealer> at row ' + str(
                                    #         process) + ' does not exist.'
                                    #     invalid_rows.append(error_dealer)
                                    #     not_insert += 1

                                else:
                                    error_obj = {}
                                    if error_disc == 'No value':
                                        error_obj['error_message'] = 'Column <discount_amount> and <discount_percent> at row ' + str(
                                            process) + ' contain blank values'
                                        invalid_rows.append(error_obj)
                                    else:
                                        if float(row['discount_percent'].rstrip('%')) <= 0:
                                            error_obj['error_message'] = 'Column <discount_percent> at row ' + str(
                                                process) + ' contain negative values'
                                            invalid_rows.append(error_obj)
                                        error_m = {}
                                        if int(row['discount_amount']) <= 0:
                                            error_m['error_message'] = 'Column <discount_amount> at row ' + str(
                                                process) + ' contain negative values'
                                            invalid_rows.append(error_m)
                                    not_insert += 1
                            else:
                                Msg = "<div class='wrong_msg ' style='max-width: 665px;margin: 0 auto;'>" \
                                        "<ul>" \
                                        "<li style='text-align:center'><span class='glyphicon glyphicon-remove-sign'></span>Error! </li>" \
                                        "<li><span class='glyphicon glyphicon-arrow-right'></span> date format:-DD/MM/YYYY or DD-MM-YYYY !!</li>" \
                                        "</ul>" \
                                        "</div>"
                        else:
                            error_code = {}
                            if row['sku_code'] == '':
                                error_code['error_message'] = 'Column <sku_code> at row ' + str(
                                    process) + ' contain null values'
                                invalid_rows.append(error_code)
                            error_ab = {}
                            if row['scheme_start_date'] == '':
                                error_ab['error_message'] = 'Column <scheme_start_date> at row ' + str(
                                    process) + ' contain null values'
                                invalid_rows.append(error_ab)
                            error_am = {}
                            if row['scheme_end_date'] == '':
                                error_am['error_message'] = 'Column <scheme_end_date> at row ' + str(
                                    process) + ' contain null values'
                                invalid_rows.append(error_am)
                            error_aval = {}
                            if row['scheme_name'] == '':
                                error_aval['error_message'] = 'Column <scheme_name> at row ' + str(
                                    process) + ' contain null values'
                                invalid_rows.append(error_aval)
                            error_avol = {}
                            if row['scheme_code'] == '':
                                error_avol['error_message'] = 'Column <scheme_code> at row ' + str(
                                    process) + ' contain null values'
                                invalid_rows.append(error_avol)
                            not_insert += 1
                        if len(invalid_rows) >= 0:
                            # not_insert = len(invalid_rows)
                            filespath = os.getcwd()
                            paths = filespath + '/upload/sell_through/'
                            if not os.path.exists(paths):
                                os.makedirs(paths)
                            file_path = filespath + '/upload/sell_through/sell_through_not_inserted.csv'
                            keys = ['error_message']
                            with open(file_path, "w") as output:
                                dict_writer = csv.DictWriter(output, keys)
                                dict_writer.writeheader()
                                if len(invalid_rows) > 0:
                                    dict_writer.writerows(invalid_rows)
                        if insert != 0 or not_insert != 0 or update != 0:
                            msgcheck=1
                            lblMsg = "<div class='wright_msg'  style='max-width: 665px;margin: 0 auto;'>" \
                                     "<ul>" \
                                     "<li style='color: #5cb85c;font-size: 21px;font-weight: 700; text-align:center'> <span class='glyphicon glyphicon-ok-sign'></span> File  Processed Successfully .</li>" \
                                     "<li style='color: #5cb85c;font-size: 21px;font-weight: 700;'> <span class='glyphicon glyphicon-arrow-right'></span> Total Record(s) Processed =" + str(
                                process) + \
                                     "<li  style='color: #5cb85c;font-size: 21px;font-weight: 700;' > <span class='glyphicon glyphicon-arrow-right'></span> Total Record(s) Inserted =" + str(
                                insert) + \
                                     "</li ><li  style='color: #5cb85c;font-size: 21px;font-weight: 700;'> <span class='glyphicon glyphicon-arrow-right'></span> Total Record(s) Updated =" + str(
                                update) + \
                                     "</li ><li  style='color: #5cb85c;font-size: 21px;font-weight: 700;'> <span class='glyphicon glyphicon-arrow-right'></span> Total Record(s) Not Inserted =" + str(
                                not_insert) + \
                                     "</li ></ul>" \
                                     " For Errors, <input value='Click to download' type='submit' " \
                                     "name='download' style=' background-color: #ecf0f5; padding: 5px 15px; text-align: center;font-size: 15px; cursor: pointer;display: inline-block; border-radius: 4px;font-weight: 600;'></div>"
                    else:
                        lblMsg = "<div class='wrong_msg ' style='max-width: 665px;margin: 0 auto;'>" \
                                 "<ul>" \
                                 "<li style='text-align:center'><span class='glyphicon glyphicon-remove-sign'></span>Error! </li>" \
                                 "<li><span class='glyphicon glyphicon-arrow-right'></span>  Sheet must have these columns !!</li>" \
                                 "<li> <span class='glyphicon glyphicon-arrow-right'></span>  sku_code, brand, scheme_name, scheme_code</li>" \
                                 "</ul>" \
                                 "</div>"
                if msgcheck==1:
                    messages.add_message(request, messages.SUCCESS, 'File Processed Successfully...')
            else:
                lblMsg = "<div class='wrong_msg' style='max-width: 665px;margin: 0 auto;'>" \
                         "<ul>" \
                         "<li style='text-align:center'><span class='glyphicon glyphicon-remove-sign'></span>Error! </li>" \
                         "<li><span class='glyphicon glyphicon-arrow-right'></span>  Must contain .csv file!!</li>" \
                         "</ul>" \
                         "</div>"
        if 'download' in request.POST:
            file_path = os.getcwd()
            paths = file_path + '/upload/sell_through/sell_through_not_inserted.csv'
            if os.path.exists(paths):
                with open(paths, 'rb') as fh:
                    response = HttpResponse(fh.read(), content_type="application/vnd.ms-excel")
                    response['Content-Disposition'] = 'inline; filename=' + os.path.basename(paths)
                    return response
    return render(request, 'sell_through/import_sell_thru.html',
                  {'scheme_sell_through': scheme_sell_through, 'lblMsg': lblMsg, 'Msg': Msg})


def export_sell_through(request):
    result = []
    sell = SchemeSellThroughMaster.objects.filter(is_active=True, is_deleted=False).order_by('-pk')
    for sell_through in sell:
        get_sell = {'brand': str(sell_through.brand.brand_name)}
        # get_sell['dealer'] = str(sell_through.dealer.user_code)
        try:
            product = ProductMaster.objects.get(pk=sell_through.product_id)
        except Exception:
            product = ''
        if product != '':
            get_sell['sku_code'] = str(sell_through.product.sku_code)
        else:
            get_sell['sku_code'] = 'Product Not Exist'
        get_sell['discount_percent'] = str(sell_through.discount_percent)
        get_sell['discount_amount'] = str(sell_through.discount_amount)
        get_sell['scheme_start_date'] = str(datetime.datetime.strptime(str(sell_through.scheme_start_date.date()), '%Y-%m-%d').strftime('%d/%m/%Y'))
        get_sell['scheme_end_date'] = str(datetime.datetime.strptime(str(sell_through.scheme_end_date.date()), '%Y-%m-%d').strftime('%d/%m/%Y'))
        scheme_name = str(sell_through.scheme_name)
        if scheme_name != '':
            get_sell['scheme_name'] = scheme_name
        else:
            get_sell['scheme_name'] = 'Not Found'
        scheme_code = str(sell_through.scheme_code)
        if scheme_code != '':
            get_sell['scheme_code'] = scheme_code
        else:
            get_sell['scheme_code'] = 'Not Found'
        try:
            get_sell['state'] = StateMaster.objects.get(pk = sell_through.state_code_id)
        except Exception:
            get_sell['state'] = ''
        try:
            get_sell['city'] = CityMaster.objects.get(pk = sell_through.city_code_id)
        except Exception:
            get_sell['city'] = ''
        result.append(get_sell)
    filespath = os.getcwd()
    paths = filespath + '/upload/sell_through/'
    if not os.path.exists(paths):
        os.makedirs(paths)
    file_path = filespath + '/upload/sell_through/scheme_sell_through.csv'
    keys = ['brand', 'state', 'city', 'sku_code', 'discount_percent', 'discount_amount', 'scheme_start_date',
            'scheme_end_date', 'scheme_name', 'scheme_code']
    with open(file_path, "w") as output:
        dict_writer = csv.DictWriter(output, keys)
        dict_writer.writeheader()
        if len(result) > 0:
            dict_writer.writerows(result)
    return result


def cash_discount_slabs(request):
    searchValue = ''
    querycount = ''
    if 'selected_brand' in request.session:
        # brand = request.session["selected_brand"]
        cashDiscountSlabs = models.CashDiscountSlabsMaster.objects.filter(
            is_active=True, is_deleted=False).order_by(
            '-created_on').order_by('-modified_on')
    else:
        cashDiscountSlabs = models.CashDiscountSlabsMaster.objects.filter(
            is_active=True, is_deleted=False).order_by(
            '-created_on').order_by('-modified_on')

    # .......................
    # groups = request.user.groups.all()
    # groups = [group.name for group in groups]
    # dealerScheme
    # if 'SR' in groups:
    #     srDealer = SRDealer.objects.filter(sr_id=request.user)
    #     # dealerList = [item.dealer_id.dealer for item in srDealer]
    #     dealerList = []
    #     for item in srDealer:
    #         if item.dealer_id is not None:
    #             if item.dealer_id.dealer is not None:
    #                 dealerList.append(item.dealer_id.dealer)
    #     tieUp = tieUp.filter(dealer__in=dealerList)
    # .......................

    data_list = None
    if request.method == "POST":
        if "search" in request.POST:
            searchValue = request.POST.get('search', None)
            # cash_discount_slabs = models.CashDiscountSlabsMaster.objects.filter(
            # slab_code=searchValue,is_active=True,is_deleted=False).order_by('-pk')
            # querycount=str(cash_discount_slabs.count())+' Record found.'

            cashDiscountSlabs = cashDiscountSlabs.filter(
                    Q(slab_code__icontains=searchValue) |
                    Q(days_1__icontains=searchValue) |
                    Q(cd1__icontains=searchValue) |
                    Q(days_2__icontains=searchValue) |
                    Q(cd2__icontains=searchValue) |
                    Q(days_3__icontains=searchValue) |
                    Q(cd3__icontains=searchValue) |
                    Q(days_4__icontains=searchValue) |
                    Q(cd4__icontains=searchValue))

        if 'export' in request.POST:
            CD_Data = export_cd(request)
            file_path = os.getcwd()
            paths = file_path + '/upload/CD_Data/CD_Data.csv'
            if os.path.exists(paths):
                with open(paths, "rb") as output:
                    response = HttpResponse(output.read(), content_type="application/vnd.ms-excel")
                    response['Content-Disposition'] = 'attachment; filename="owims_cash_discount.csv"'
                    return response

    page_style = request.GET.get("page_style", settings.DEFAULT_PAGING_VIEW)
    paginator = Paginator(cashDiscountSlabs, 10)
    try:
        page_index = request.GET.get("page", 1)
        data_list = paginator.page(page_index)
    except PageNotAnInteger:
        data_list = paginator.page(1)
    except EmptyPage:
        data_list = paginator.page(paginator.num_pages)

    context = {
        'msg': '',
        'data_list': data_list,
        'search': searchValue,
        'querycount': querycount,
        'SlabCount': len(data_list)
    }
    if request.GET.get('list', None):
        context['flagList'] = True
    else:
        context['flagList'] = False
    if page_style == 'grid':
        return render(request, 'cash_discount_slabs/cd_slabs_grid.html', context)
    else:
        return render(request, 'cash_discount_slabs/cd_slabs_list.html', context)


def create_cd_slab(request):
    form_class = forms.CashDiscountSlabsMasterForm
    if request.method == 'GET':
        return render(request, 'cash_discount_slabs/cash_discount_slabs_create.html',
                      {'cash_discount_slabs_create_form': form_class})
    elif request.method == 'POST':
        try:
            instanceobj = CashDiscountSlabsMaster.objects.get(slab_code=request.POST.get('slab_code'),is_active=True,is_deleted=False)
        except Exception:
            instanceobj=''
        if instanceobj=='' or instanceobj==None:
            form_class = forms.CashDiscountSlabsMasterForm(request.POST)
            if form_class.is_valid():
                cd_slab = form_class.save(commit=False)
                cd_slab.created_by = request.user
                cd_slab.save()
                messages.add_message(request, messages.SUCCESS, 'Cash discount slab created successfully...')
            else:
                return render(request, 'cash_discount_slabs/cash_discount_slabs_create.html',
                          {'cash_discount_slabs_create_form': form_class})
            return redirect('/master/cash_discount_slab?page_style=list')
        else:
            messages.add_message(request, messages.ERROR, 'Slab_code already Exists!!!')
            return render(request, 'cash_discount_slabs/cash_discount_slabs_create.html',
                          {'cash_discount_slabs_create_form': form_class})
        


def edit_cd_slab(request, CDid):
    days1,cd1,days2,cd2,days3,cd3,days4,cd4='','','','','','','',''
    if request.method == 'GET':
        instance = get_object_or_404(CashDiscountSlabsMaster, cash_discount_slabs_id=CDid)
        form_class = forms.CashDiscountSlabsMasterForm(request.POST or None, instance=instance)
        return render(request, 'cash_discount_slabs/cash_discount_slabs_create.html',
                      {'cash_discount_slabs_create_form': form_class, 'cd': instance})
    elif request.method == 'POST':
        instance = get_object_or_404(CashDiscountSlabsMaster, cash_discount_slabs_id=CDid)
        days1=instance.days_1
        cd1=instance.cd1
        days2=instance.days_2
        cd2=instance.cd2
        days3=instance.days_3
        cd3=instance.cd3
        days4=instance.days_4
        cd4=instance.cd4
        form_class = forms.CashDiscountSlabsMasterForm(request.POST or None, instance=instance)
        if form_class.is_valid():
            form_class.save()

            """ need to changed """
            # businessheadobj=User.objects.filter(is_active=True,groups__name="BusinessHead")
            # html_content=''
            # if businessheadobj:
            #     emails=[]
            #     for user in businessheadobj:
            #         emails.append(user.email)
            #     if days1!=form_class.cleaned_data['days_1']:
            #         html_content+= "Previous Days 1 ₹"+str(days1)+" Current Days 1 ₹"+str(form_class.cleaned_data['days_1'])+" , "
            #     if cd1!=form_class.cleaned_data['cd1']:
            #         html_content+= "Previous CD 1 ₹"+str(cd1)+" Current CD 1 ₹"+str(form_class.cleaned_data['cd1'])+" , "
            #     if days1!=form_class.cleaned_data['days_2']:
            #         html_content+= "Previous Days 2 ₹"+str(days2)+" Current Days 2 ₹"+str(form_class.cleaned_data['days_2'])+" , "
            #     if days1!=form_class.cleaned_data['cd2']:
            #         html_content+= "Previous CD 2 ₹"+str(cd2)+" Current CD 2 ₹"+str(form_class.cleaned_data['cd2'])+" , "
            #     if days1!=form_class.cleaned_data['days_3']:
            #         html_content+= "Previous Days 3 ₹"+str(days3)+" Current Days 3 ₹"+str(form_class.cleaned_data['days_3'])+" , "
            #     if days1!=form_class.cleaned_data['cd3']:
            #         html_content+= "Previous CD 3 ₹"+str(cd3)+" Current CD 3 ₹"+str(form_class.cleaned_data['cd3'])+" , "
            #     if days1!=form_class.cleaned_data['days_4']:
            #         html_content+= "Previous Days 4 ₹"+str(days4)+" Current Days 4 ₹"+str(form_class.cleaned_data['days_4'])+" , "
            #     if days1!=form_class.cleaned_data['cd4']:
            #         html_content+= "Previous CD 4 ₹"+str(cd4)+" Current CD 4 ₹"+str(form_class.cleaned_data['cd4'])+" , "
            #
            #     html_content=html_content.rstrip(',')+"is updated"
            #     subject=" financial number changed"
            #     email = EmailMultiAlternatives(subject, html_content, settings.DEFAULT_FROM_EMAIL, to=emails)
            #     email.send()
            messages.add_message(request, messages.SUCCESS, 'Cash discount slab updated successfully...')
        else:
            return render(request, 'cash_discount_slabs/cash_discount_slabs_create.html',
                          {'cash_discount_slabs_create_form': form_class, 'cd': instance})
        return redirect('/master/cash_discount_slab')


def delete_cd_slab(request, CDid):
    try:
        cash_discount_slabs = CashDiscountSlabsMaster.objects.get(cash_discount_slabs_id=CDid)
        cash_discount_slabs.delete()
        messages.add_message(request, messages.SUCCESS, 'Cash discount slab deleted successfully...')
    except Exception as err:
        messages.add_message(request, messages.ERROR, str(err))
    return HttpResponseRedirect('/master/cash_discount_slab?page_style=list')


def cd_import(request):
    invalid_rows = []
    index = 0
    insert = 0
    not_insert = 0
    update = 0
    process = 0
    not_exist = []
    lblMsg = ''
    filexlsx = False
    if request.method == 'POST':
        if 'Save' in request.POST:
            getSl_num = {}
            file = request.FILES.get('cd_slab')
            if file != None and file != '':
                filexlsx = str(file._name).endswith('csv')
            if filexlsx == True:
                decoded_file = request.FILES['cd_slab'].read().decode('utf-8').splitlines()
                reader = csv.DictReader(decoded_file)
                for row in reader:
                    process += 1
                    if index == 0:
                        flag = True
                        for x in import_settings.CASH_DISCOUNT_SLABS_IMPORT_KEY:
                            if x not in row.keys():
                                not_exist.append(x)
                    set_exist = len(not_exist)
                    if set_exist == 0:

                        if row['slabcode'] != '' and row['days1'] != '' and row['cd1'] != '' and row['days2'] != '' and \
                                row['cd2'] != '' and row['days3'] != '' and row['cd3'] != '' and row['days4'] != '' and \
                                row['cd4'] != '':
                            if int(row['days1']) >= 0 and float(row['cd1'].rstrip('%')) >= 0 and int(
                                    row['days2']) >= 0 and float(row['cd2'].rstrip('%')) >= 0 \
                                    and int(row['days3']) >= 0 and float(row['cd3'].rstrip('%')) >= 0 and int(
                                row['days4']) >= 0 \
                                    and float(row['cd4'].rstrip('%')) >= 0:
                                try:
                                    CDS = CashDiscountSlabsMaster.objects.get(slab_code=row['slabcode'])
                                    update += 1
                                except CashDiscountSlabsMaster.DoesNotExist:
                                    CDS = CashDiscountSlabsMaster()
                                    insert += 1
                                CDS.slab_code = row['slabcode']
                                CDS.days_1 = int(row['days1'])
                                CDS.cd1 = float(row['cd1'].rstrip('%'))
                                CDS.days_2 = int(row['days2'])
                                CDS.cd2 = float(row['cd2'].rstrip('%'))
                                CDS.days_3 = int(row['days3'])
                                CDS.cd3 = float(row['cd3'].rstrip('%'))
                                CDS.days_4 = int(row['days4'])
                                CDS.cd4 = float(row['cd4'].rstrip('%'))
                                CDS.save()

                            else:
                                error_b = {}
                                if int(row['days1']) <= 0:
                                    error_b['error_message'] = 'Column <days1> at row ' + str(
                                        process) + ' contain negative values'
                                    invalid_rows.append(error_b)
                                error_m = {}
                                if float(row['cd1'].rstrip('%')) <= 0:
                                    error_m['error_message'] = 'Column <cd1> at row ' + str(
                                        process) + ' contain negative values'
                                    invalid_rows.append(error_m)
                                error_val = {}
                                if int(row['days2']) <= 0:
                                    error_val['error_message'] = 'Column <days2> at row ' + str(
                                        process) + ' contain negative values'
                                    invalid_rows.append(error_val)
                                error_vol = {}
                                if float(row['cd2'].rstrip('%')) <= 0:
                                    error_vol['error_message'] = 'Column <cd2> at row ' + str(
                                        process) + ' contain negative values'
                                    invalid_rows.append(error_vol)
                                error_b1 = {}
                                if int(row['days3']) <= 0:
                                    error_b1['error_message'] = 'Column <days3> at row ' + str(
                                        process) + ' contain negative values'
                                    invalid_rows.append(error_b1)
                                error_m1 = {}
                                if float(row['cd3'].rstrip('%')) <= 0:
                                    error_m1['error_message'] = 'Column <cd3> at row ' + str(
                                        process) + ' contain negative values'
                                    invalid_rows.append(error_m1)
                                error_val1 = {}
                                if int(row['days4']) <= 0:
                                    error_val1['error_message'] = 'Column <days4> at row ' + str(
                                        process) + ' contain negative values'
                                    invalid_rows.append(error_val1)
                                error_vol1 = {}
                                if float(row['cd4'].rstrip('%')) <= 0:
                                    error_vol1['error_message'] = 'Column <cd4> at row ' + str(
                                        process) + ' contain negative values'
                                    invalid_rows.append(error_vol1)
                                not_insert += 1
                        else:
                            error_code = {}
                            if row['slabcode'] == '':
                                error_code['error_message'] = 'Column <slabcode> at row ' + str(
                                    process) + ' contain null values'
                                invalid_rows.append(error_code)
                            error_ab = {}
                            if row['days1'] == '':
                                error_ab['error_message'] = 'Column <days1> at row ' + str(
                                    process) + ' contain null values'
                                invalid_rows.append(error_ab)
                            error_am = {}
                            if row['cd1'] == '':
                                error_am['error_message'] = 'Column <cd1> at row ' + str(
                                    process) + ' contain null values'
                                invalid_rows.append(error_am)
                            error_aval = {}
                            if row['days2'] == '':
                                error_aval['error_message'] = 'Column <days2> at row ' + str(
                                    process) + ' contain null values'
                                invalid_rows.append(error_aval)
                            error_avol = {}
                            if row['cd2'] == '':
                                error_avol['error_message'] = 'Column <cd2> at row ' + str(
                                    process) + ' contain null values'
                                invalid_rows.append(error_avol)
                            error_avl = {}
                            if row['days3'] == '':
                                error_avl['error_message'] = 'Column <days3> at row ' + str(
                                    process) + ' contain null values'
                                invalid_rows.append(error_avl)
                            error_cd3 = {}
                            if row['cd3'] == '':
                                error_cd3['error_message'] = 'Column <cd3> at row ' + str(
                                    process) + ' contain null values'
                                invalid_rows.append(error_cd3)
                            error_days4 = {}
                            if row['days4'] == '':
                                error_days4['error_message'] = 'Column <days4> at row ' + str(
                                    process) + ' contain null values'
                                invalid_rows.append(error_days4)
                            error_cd4 = {}
                            if row['cd4'] == '':
                                error_cd4['error_message'] = 'Column <cd4> at row ' + str(
                                    process) + ' contain null values'
                                invalid_rows.append(error_cd4)
                            not_insert += 1
                        if len(invalid_rows) >= 0:
                            not_insert = len(invalid_rows)
                            filespath = os.getcwd()
                            paths = filespath + '/upload/CD_Data/'
                            if not os.path.exists(paths):
                                os.makedirs(paths)
                            file_path = filespath + '/upload/CD_Data/CD_not_import.csv'
                            keys = ['error_message']
                            with open(file_path, "w") as output:
                                dict_writer = csv.DictWriter(output, keys)
                                dict_writer.writeheader()
                                if len(invalid_rows) > 0:
                                    dict_writer.writerows(invalid_rows)
                        if insert != 0 or not_insert != 0 or update != 0:
                            lblMsg = "<div class='wright_msg'  style='max-width: 665px;margin: 0 auto;'>" \
                                     "<ul>" \
                                     "<li style='color: #5cb85c;font-size: 21px;font-weight: 700; text-align:center'> <span class='glyphicon glyphicon-ok-sign'></span> File  Processed Successfully .</li>" \
                                     "<li style='color: #5cb85c;font-size: 21px;font-weight: 700;'> <span class='glyphicon glyphicon-arrow-right'></span> Total Record(s) Processed =" + str(
                                process) + \
                                     "<li  style='color: #5cb85c;font-size: 21px;font-weight: 700;' > <span class='glyphicon glyphicon-arrow-right'></span> Total Record(s) Inserted =" + str(
                                insert) + \
                                     "</li ><li  style='color: #5cb85c;font-size: 21px;font-weight: 700;'> <span class='glyphicon glyphicon-arrow-right'></span> Total Record(s) Updated =" + str(
                                update) + \
                                     "</li ><li  style='color: #5cb85c;font-size: 21px;font-weight: 700;'> <span class='glyphicon glyphicon-arrow-right'></span> Total Record(s) Not Inserted =" + str(
                                not_insert) + \
                                     "</li ></ul>" \
                                     " For Errors, <input value='Click to download' type='submit' " \
                                     "name='download' style=' background-color: #ecf0f5; padding: 5px 15px; text-align: center;font-size: 15px; cursor: pointer;display: inline-block; border-radius: 4px;font-weight: 600;'></div>"
                    else:
                        lblMsg = "<div class='wrong_msg ' style='max-width: 665px;margin: 0 auto;'>" \
                                 "<ul>" \
                                 "<li style='text-align:center'><span class='glyphicon glyphicon-remove-sign'></span>Error! </li>" \
                                 "<li><span class='glyphicon glyphicon-arrow-right'></span>  Sheet must have these columns !!</li>" \
                                 "<li> <span class='glyphicon glyphicon-arrow-right'></span>slabcode, days1, cd1, days2, cd2, days3, cd3, days4, cd4</li>" \
                                 "</ul>" \
                                 "</div>"
            else:
                lblMsg = "<div class='wrong_msg' style='max-width: 665px;margin: 0 auto;'>" \
                         "<ul>" \
                         "<li style='text-align:center'><span class='glyphicon glyphicon-remove-sign'></span>Error! </li>" \
                         "<li><span class='glyphicon glyphicon-arrow-right'></span>  Must contain .csv file!!</li>" \
                         "</ul>" \
                         "</div>"
        if 'download' in request.POST:
            file_path = os.getcwd()
            paths = file_path + '/upload/CD_Data/CD_not_import.csv'
            if os.path.exists(paths):
                with open(paths, 'rb') as fh:
                    response = HttpResponse(fh.read(), content_type="application/vnd.ms-excel")
                    response['Content-Disposition'] = 'inline; filename=' + os.path.basename(paths)
                    return response

    return render(request, 'cash_discount_slabs/import_cd.html',
                  {'cash_discount_slabs': cash_discount_slabs, 'lblMsg': lblMsg})


def export_cd(request):
    CashDiscount = CashDiscountSlabsMaster.objects.filter(is_active=True,is_deleted=False).order_by('-pk')
    result = []
    for CD in CashDiscount:
        C_D = {}
        C_D['slabcode'] = str(CD.slab_code)
        C_D['days1'] = CD.days_1
        C_D['cd1'] = CD.cd1
        C_D['days2'] = CD.days_2
        C_D['cd2'] = CD.cd2
        C_D['days3'] = CD.days_3
        C_D['cd3'] = CD.cd3
        C_D['days4'] = CD.days_4
        C_D['cd4'] = CD.cd4
        result.append(C_D)
    filespath = os.getcwd()
    paths = filespath + '/upload/CD_Data/'
    if not os.path.exists(paths):
        os.makedirs(paths)
    file_path = filespath + '/upload/CD_Data/CD_Data.csv'
    keys = ['slabcode', 'days1', 'cd1', 'days2', 'cd2', 'days3', 'cd3', 'days4', 'cd4']
    with open(file_path, "w") as output:
        dict_writer = csv.DictWriter(output, keys)
        dict_writer.writeheader()
        if len(result) > 0:
            dict_writer.writerows(result)
    return result


def credit_note_type(request):
    credit_note_type = models.CreditNoteTypeMaster.objects.all()

    return render(request, 'credit_note_type/credit_note_type.html', {'credit_note_type': credit_note_type})


def create_credit_note_type(request):
    form_class = forms.CreditNoteTypeMasterForm
    if request.method == 'GET':
        return render(request, 'credit_note_type/credit_note_type_create.html',
                      {'credit_note_type_create_form': form_class})
    elif request.method == 'POST':
        form_class = forms.CreditNoteTypeMasterForm(request.POST)
        if form_class.is_valid():
            credit_note = form_class.save(commit=False)
            credit_note.created_by = request.user
            credit_note.save()
        return render(request, 'credit_note_type/credit_note_type_create.html',
                      {'credit_note_type_create_form': form_class})
    elif request.method == 'PUT':
        form_class = forms.CreditNoteTypeMasterForm(request.POST)
        if form_class.is_valid():
            credit_note = form_class.save(commit=False)
            credit_note.created_by = request.user
            credit_note.save()
        return render(request, 'credit_note_type/credit_note_type_create.html',
                      {'credit_note_type_create_form': form_class})


def document_type(request):
    document_type = models.DocumentTypeMaster.objects.all()

    return render(request, 'document_type/document_type.html', {'document_type': document_type})


def create_document_type(request):
    form_class = forms.DocumentTypeMasterForm
    if request.method == 'GET':
        return render(request, 'document_type/document_type_create.html', {'form': form_class})
    elif request.method == 'POST':
        form_class = forms.DocumentTypeMasterForm(request.POST)
        if form_class.is_valid():
            doc_type = form_class.save(commit=False)
            doc_type.created_by = request.user
            doc_type.save()
        return render(request, 'document_type/document_type_create.html', {'form': form_class})
    elif request.method == 'PUT':
        form_class = forms.DocumentTypeMasterForm(request.POST)
        if form_class.is_valid():
            doc_type = form_class.save(commit=False)
            doc_type.created_by = request.user
            doc_type.save()
        return render(request, 'document_type/document_type_create.html', {'form': form_class})


def region(request):
    region = models.CityStateRegionMaster.objects.all().order_by('region_id')
    return render(request, 'region/region.html', {'region': region})


def create_region(request):
    form_class = forms.CityStateRegionMasterForm
    if request.method == 'GET':
        return render(request, 'region/region_create.html', {'form': form_class})
    elif request.method == 'POST':
        form_class = forms.CityStateRegionMasterForm(request.POST)
        if form_class.is_valid():
            region = form_class.save(commit=False)
            region.created_by = request.user
            if form_class.cleaned_data["parent"] is not None and form_class.cleaned_data["parent"] is not '':
                region.parent = form_class.cleaned_data["parent"].pk
                region.save()
        return render(request, 'region/region_create.html', {'form': form_class})
    elif request.method == 'PUT':
        form_class = forms.CityStateRegionMasterForm(request.POST)
        if form_class.is_valid():
            region = form_class.save(commit=False)
            region.created_by = request.user
            region.save()
        return render(request, 'region/region_create.html', {'form': form_class})


def inventory_import_head(request):
    pass


def import_inventory(request):
    invalid_rows = []
    index = 0
    insert = 0
    not_insert = 0
    update = 0
    process = 0
    not_exist = []
    lblMsg = ''
    msg = ''
    filexlsx = False
    if request.method == 'POST':
        if 'Save' in request.POST:
            getSl_num = {}
            file = request.FILES.get('Inventory')
            if file is not None and file != '':
                filexlsx = str(file._name).endswith('csv')
            if filexlsx == True:
                decoded_file = request.FILES['Inventory'].read().decode('utf-8').splitlines()
                reader = csv.DictReader(decoded_file)
                for row in reader:
                    process += 1
                    if index == 0:
                        flag = True
                        for x in import_settings.INVENTORY_IMPORT_KEY:
                            if x not in row.keys():
                                not_exist.append(x)
                        # return invalid file from here

                    set_exist = len(not_exist)
                    if set_exist == 0:
                        if row['WAREHOUSE CODE'] != '' and row['PRODUCT CODE(SKU)'] != '' and row['City'] != '' and \
                                row['SALEABLE STOCK'] != '' and row['NON-SALEABLE STOCK'] != '' and \
                                row['MINIMUM STOCK LEVEL'] != '':
                            if float(row['MINIMUM STOCK LEVEL']) >= 0 and float(row['SALEABLE STOCK']) >= 0 and float(
                                    row['NON-SALEABLE STOCK']) >= 0:
                                try:
                                    Brand = BrandMaster.objects.get(brand_name=row['Brand'].lower())
                                except BrandMaster.DoesNotExist:
                                    Brand = ''
                                try:
                                    PM = ProductMaster.objects.get(
                                        sku_code=str(row['PRODUCT CODE(SKU)']),
                                        is_active=True, is_deleted=False)

                                    try:
                                        WHM = WarehouseMaster.objects.get(
                                            warehouse_code=row['WAREHOUSE CODE'],
                                            is_active=True, is_deleted=False)
                                    except Exception:
                                        WHM = WarehouseMaster()
                                        WHM.warehouse_name = row['WAREHOUSE CODE']
                                        WHM.warehouse_code = row['WAREHOUSE CODE']
                                        WHM.city = row['City']
                                        WHM.save()

                                    try:
                                        obj_StockLevel = StockLevelMaster.objects.get(
                                            warehouse_id=WHM.pk, product_id=PM.pk,
                                            is_active=True, is_deleted=False)
                                        update += 1
                                    except Exception:
                                        obj_StockLevel = StockLevelMaster()
                                        insert += 1
                                    # obj_StockLevel.warehouse = WarehouseMaster.objects.get(pk=WHM.pk)
                                    # obj_StockLevel.product = ProductMaster.objects.get(sku_code=PM.sku_code)
                                    obj_StockLevel.warehouse = WHM
                                    obj_StockLevel.product = PM
                                    min_stock_level = row['MINIMUM STOCK LEVEL']
                                    if min_stock_level != '' and min_stock_level is not None:
                                        obj_StockLevel.min_stock_level = int(min_stock_level)
                                    else:
                                        obj_StockLevel.min_stock_level = 0
                                    obj_StockLevel.save()

                                    # try:
                                    #     SR = StockRegister.objects.get(
                                    #         warehouse_id=WHM.pk, product_id=PM.pk,
                                    #         is_active=True, is_deleted=False)
                                    # except Exception:
                                    #     SR = StockRegister()

                                    # try:
                                    #     SR = StockRegister.objects.get(
                                    #         warehouse_id=WHM.pk, product_id=PM.pk,
                                    #         is_active=True, is_deleted=False)
                                    # except Exception:
                                    SR = StockRegister()

                                    # SR.warehouse = WarehouseMaster.objects.get(pk=WHM.pk)
                                    # SR.product = ProductMaster.objects.get(sku_code=PM.sku_code)
                                    SR.warehouse = WHM
                                    SR.product = PM
                                    # stock_in = float(row['SALEABLE STOCK'])
                                    stock_in = row['SALEABLE STOCK']
                                    # if stock_in != '':
                                    #     SR.stock_in = float(stock_in)
                                    #     if stock_in != 0:
                                    #         SR.stock_type = 'Saleable'
                                    #         # SR.stock_type = 'stock_in'
                                    # else:
                                    #     SR.stock_in = 0
                                    # stock_out = float(row['NON-SALEABLE STOCK'])
                                    # if stock_out != '':
                                    #     SR.stock_out = float(stock_out)
                                    #     if stock_out != 0:
                                    #         SR.stock_type = 'stock_out'
                                    # else:
                                    #     SR.stock_out = 0
                                    # SR.save()
                                    if stock_in != '' and stock_in != 0 and stock_in != '0' and stock_in is not None:
                                        SR.stock_in = float(stock_in)
                                        SR.stock_out = float(0)
                                        SR.stock_type = 'Sellable'
                                        SR.save()

                                    stock_out = float(row['NON-SALEABLE STOCK'])
                                    # stock_out = row['NON-SALEABLE STOCK']
                                    if stock_out != '' and stock_out != 0 and stock_out != '0' and stock_out is not None:
                                        SR.pk = None
                                        SR.stock_in = float(stock_out)
                                        SR.stock_out = float(0)
                                        SR.stock_type = 'NonSellable'
                                        SR.save()
                                except ProductMaster.DoesNotExist:
                                    error = {'error_message': 'Column <MINIMUM STOCK SKU_Code> at row ' + str(
                                        process) + ' does not exist'}
                                    invalid_rows.append(error)
                            else:
                                error_b = {}
                                if float(row['MINIMUM STOCK LEVEL']) <= 0:
                                    error_b['error_message'] = 'Column <MINIMUM STOCK LEVEL> at row ' + str(
                                        process) + ' contain negative values'
                                    invalid_rows.append(error_b)
                                error_m = {}
                                if float(row['SALEABLE STOCK']) <= 0:
                                    error_m['error_message'] = 'Column <SALEABLE STOCK> at row ' + str(
                                        process) + ' contain negative values'
                                    invalid_rows.append(error_m)
                                error_val = {}
                                if float(row['NON-SALEABLE STOCK']) <= 0:
                                    error_val['error_message'] = 'Column <NON-SALEABLE STOCK> at row ' + str(
                                        process) + ' contain negative values'
                                    invalid_rows.append(error_val)
                        else:
                            error_code = {}
                            if row['WAREHOUSE CODE'] == '':
                                error_code['error_message'] = 'Column <WAREHOUSE CODE> at row ' + str(
                                    process) + ' contain null values'
                                invalid_rows.append(error_code)
                            error_ab = {}
                            if row['PRODUCT CODE(SKU)'] == '':
                                error_ab['error_message'] = 'Column <PRODUCT CODE(SKU)> at row ' + str(
                                    process) + ' contain null values'
                                invalid_rows.append(error_ab)
                            error_am = {}
                            if row['City'] == '':
                                error_am['error_message'] = 'Column <City> at row ' + str(
                                    process) + ' contain null values'
                                invalid_rows.append(error_am)
                            error_aval = {}
                            if row['SALEABLE STOCK'] == '':
                                error_aval['error_message'] = 'Column <SALEABLE STOCK> at row ' + str(
                                    process) + ' contain null values'
                                invalid_rows.append(error_aval)
                            error_avol = {}
                            if row['NON-SALEABLE STOCK'] == '':
                                error_avol['error_message'] = 'Column <NON-SALEABLE STOCK> at row ' + str(
                                    process) + ' contain null values'
                                invalid_rows.append(error_avol)
                            error_MSL = {}
                            if row['MINIMUM STOCK LEVEL'] == '':
                                error_MSL['error_message'] = 'Column <MINIMUM STOCK LEVEL> at row ' + str(
                                    process) + ' contain null values'
                                invalid_rows.append(error_MSL)
                        if len(invalid_rows) >= 0:
                            not_insert = len(invalid_rows)
                            filespath = os.getcwd()
                            paths = filespath + '/upload/inventory/'
                            if not os.path.exists(paths):
                                os.makedirs(paths)
                            file_path = filespath + '/upload/inventory/inventory_not_import.csv'
                            keys = ['error_message']
                            with open(file_path, "w") as output:
                                dict_writer = csv.DictWriter(output, keys)
                                dict_writer.writeheader()
                                if len(invalid_rows) > 0:
                                    dict_writer.writerows(invalid_rows)
                        if insert != 0 or not_insert != 0 or update != 0:
                            lblMsg = "<div class='wright_msg'  style='max-width: 665px;margin: 0 auto;'>" \
                                     "<ul>" \
                                     "<li style='color: #5cb85c;font-size: 21px;font-weight: 700; text-align:center'> <span class='glyphicon glyphicon-ok-sign'></span> File  Processed Successfully .</li>" \
                                     "<li style='color: #5cb85c;font-size: 21px;font-weight: 700;'> <span class='glyphicon glyphicon-arrow-right'></span> Total Record(s) Processed =" + str(
                                process) + \
                                     "<li  style='color: #5cb85c;font-size: 21px;font-weight: 700;' > <span class='glyphicon glyphicon-arrow-right'></span> Total Record(s) Inserted =" + str(
                                insert) + \
                                     "</li ><li  style='color: #5cb85c;font-size: 21px;font-weight: 700;'> <span class='glyphicon glyphicon-arrow-right'></span> Total Record(s) Updated =" + str(
                                update) + \
                                     "</li ><li  style='color: #5cb85c;font-size: 21px;font-weight: 700;'> <span class='glyphicon glyphicon-arrow-right'></span> Total Record(s) Not Inserted =" + str(
                                not_insert) + \
                                     "</li ></ul>" \
                                     " For Errors, <input value='Click to download' type='submit' " \
                                     "name='download' style=' background-color: #ecf0f5; padding: 5px 15px; text-align: center;font-size: 15px; cursor: pointer;display: inline-block; border-radius: 4px;font-weight: 600;'></div>"
                    else:
                        lblMsg = "<div class='wrong_msg ' style='max-width: 665px;margin: 0 auto;'>" \
                                 "<ul>" \
                                 "<li style='text-align:center'><span class='glyphicon glyphicon-remove-sign'></span>Error! </li>" \
                                 "<li><span class='glyphicon glyphicon-arrow-right'></span>  Sheet must have these columns !!</li>" \
                                 "<li> <span class='glyphicon glyphicon-arrow-right'></span>  sku_code, brand, scheme_name, scheme_code</li>" \
                                 "</ul>" \
                                 "</div>"

                messages.add_message(request, messages.SUCCESS, 'File Processed Successfully...')
            else:
                lblMsg = "<div class='wrong_msg' style='max-width: 665px;margin: 0 auto;'>" \
                         "<ul>" \
                         "<li style='text-align:center'><span class='glyphicon glyphicon-remove-sign'></span>Error! </li>" \
                         "<li><span class='glyphicon glyphicon-arrow-right'></span>  Must contain .csv file!!</li>" \
                         "</ul>" \
                         "</div>"
        if 'download' in request.POST:
            file_path = os.getcwd()
            paths = file_path + '/upload/inventory/inventory_not_import.csv'
            if os.path.exists(paths):
                with open(paths, 'rb') as fh:
                    response = HttpResponse(fh.read(), content_type="application/vnd.ms-excel")
                    response['Content-Disposition'] = 'inline; filename=' + os.path.basename(paths)
                    return response
    return render(request, 'inventory/import_inventory.html', {'lblMsg': lblMsg, 'msg': msg})


def tax_slab(request):
    page_index = request.GET.get("page", 1)
    page_style = request.GET.get("page_style", settings.DEFAULT_PAGING_VIEW)
    tax_slab = models.TaxSlabMaster.objects.all()
    paginator = Paginator(tax_slab, settings.PAGE_SIZE)
    data_list = None
    # if request.method == "POST":
    #     if 'export' in request.POST:
    #         CD_Data = export_cd(request)
    #         file_path = os.getcwd()
    #         paths = file_path + '/upload/CD_Data/CD_Data.csv'
    #         if os.path.exists(paths):
    #             with open(paths, "rb") as output:
    #                 response = HttpResponse(output.read(), content_type="application/vnd.ms-excel")
    #                 response['Content-Disposition'] = 'attachment; filename="owims_cash_discount.csv"'
    #                 return response
    try:
        data_list = paginator.page(page_index)
    except PageNotAnInteger:
        data_list = paginator.page(1)
    except EmptyPage:
        data_list = paginator.page(paginator.num_pages)

    context = {
        'msg': '',
        'data_list': data_list,
    }
    if page_style == 'grid':
        return render(request, 'tax_slab/tax_slab_grid_view.html', context)
    else:
        return render(request, 'tax_slab/tax_slab_list_view.html', context)


def create_tax_slab(request):
    form_class = forms.TaxSlabMasterForm
    if request.method == 'GET':
        return render(request, 'tax_slab/tax_slab_create.html', {'tax_slab_form': form_class})

    elif request.method == 'POST':
        form_class = forms.TaxSlabMasterForm(request.POST)
        if form_class.is_valid():
            tax_slab = form_class.save(commit=False)
            tax_slab.created_by = request.user
            tax_slab.save()
        else:
            return render(request, 'tax_slab/tax_slab_create.html', {'tax_slab_form': form_class})

        return redirect('/master/tax_slab?page_style=list')


def edit_tax_slab(request, Aid):
    if request.method == 'GET':
        instance = get_object_or_404(TaxSlabMaster, tax_slab_id=Aid)
        form_class = forms.TaxSlabMasterForm(request.POST or None, instance=instance)
        return render(request, 'tax_slab/tax_slab_create.html', {'tax_slab_form': form_class})

    elif request.method == 'PUT':
        instance = TaxSlabMaster.objects.get(tax_slab_id=Aid)
        form_class = forms.TaxSlabMasterForm(request.POST, instance=instance)
        if form_class.is_valid():
            form_class.save()
        else:
            return render(request, 'tax_slab/tax_slab_create.html', {'tax_slab_form': form_class})

        return redirect('/master/tax_slab?page_style=list')


def inventory(request):
    products = ProductMaster.objects.filter(is_active=True, is_deleted=False)
    stockLevel = StockLevelMaster.objects.filter(
        is_active=True, is_deleted=False).order_by(
        '-created_on').order_by('-modified_on')

    # stockcount = StockLevelMaster.objects.filter(
    #     is_active=True, is_deleted=False).count()

    # ............................................
    groups = request.user.groups.all()
    groups = [group.name for group in groups]
    if "WHSUSER" in groups:
        self_warehouses = WarehouseAdmin.objects.filter(
            user=request.user, is_active=True, is_deleted=False)
        self_warehouses = [x.warehouse for x in self_warehouses]
        stockLevel = stockLevel.filter(warehouse__in=self_warehouses)
    # ............................................

    inventories, querycount, city = [], "", ""
    sku, warehouse_code, searchValue = "", "", ""
    if request.method == 'POST':
        if "search" in request.POST:
            searchValue = request.POST.get('search', None)


            stockLevel = stockLevel.filter(
                Q(warehouse__warehouse_code__icontains=searchValue) |
                Q(warehouse__warehouse_name__icontains=searchValue) |
                Q(warehouse__city__icontains=searchValue) |
                Q(product__sku_code__icontains=searchValue))


    for stock in stockLevel:
        product = stock.product
        list_dict = {
            "sku_code": stock.product.sku_code, "warehouse": 0,"stock_out": 0,
            "sellable": 0, "non_sellable": 0, "min_stock": 0,"remaining": 0,"blocked": 0,"available": 0
        }
        totalStock = StockRegister.objects.filter(
            product=stock.product, stock_type__in=['Sellable', 'NonSellable'],warehouse=stock.warehouse,
            is_active=True, is_deleted=False).values('stock_type').annotate(
            count=Sum('stock_in'))

        outStock = StockRegister.objects.filter(
            product=stock.product, stock_type='stock_out',
            is_active=True, is_deleted=False).values(
            'stock_type').annotate(count=Sum('stock_out'))

        if len(totalStock) > 0:
            for item in totalStock:
                if item["stock_type"] == "Sellable":
                    list_dict["sellable"] = item["count"]
                elif item["stock_type"] == "NonSellable":
                    list_dict["non_sellable"] = item["count"]

            if len(outStock) > 0:
                for item in outStock:
                    list_dict["sellable"] = list_dict["sellable"] - item["count"]
                    list_dict["stock_out"] = list_dict["stock_out"] + item["count"]
                    list_dict["available"] = list_dict["sellable"] - list_dict["stock_out"]
            else:
                list_dict["available"] = list_dict["sellable"]
            """ NEW FROM Here """
            # =============================================
            order_details = OrderDetails.objects.filter(
                order_id__status__in=status_in.AssignedOrder(),
                product_id=product, is_active=True, is_deleted=False).values(
                'product_id').annotate(count=Sum('quantity'))
            for block in order_details:
                list_dict["blocked"] = list_dict["blocked"] + block["count"]
            # list_dict["available"] = list_dict["total"] - list_dict["non_sale"]
            list_dict["available"] = list_dict["sellable"] - list_dict["stock_out"]
            list_dict["remaining"] = list_dict["available"] - list_dict["blocked"]

            register_item = {
                "warehouse": stock.warehouse.warehouse_code,
                "product": stock.product.sku_code,
                "stock_in": list_dict["sellable"],
                "stock_out": list_dict["non_sellable"],
                "available":list_dict["available"],
                "blocked":list_dict["blocked"],
                'remain': list_dict["remaining"],
                "min_stock": stock.min_stock_level,
                'search': searchValue,
            }
            inventories.append(register_item)

    page_style = request.GET.get("page_style", settings.DEFAULT_PAGING_VIEW)
    # paginator = Paginator(inventories, settings.PAGE_SIZE)
    paginator = Paginator(inventories, 10)
    try:
        page_index = request.GET.get("page", 1)
        data_list = paginator.page(page_index)
    except PageNotAnInteger:
        data_list = paginator.page(1)
    except EmptyPage:
        data_list = paginator.page(paginator.num_pages)

    context = {
        'msg': '', 'data_list': data_list,
        'inventoryCount': len(inventories),
        'search': searchValue, 'querycount': querycount
    }
    if request.GET.get('list', None):
        context['flagList'] = True
    else:
        context['flagList'] = False
    if page_style == 'grid':
        return render(request, 'inventory/inventory_grid_view.html', context)
    else:
        return render(request, 'inventory/inventory_list_view.html', context)


def create_inventory(request):
    form_class = forms.InventoryForm
    products = models.ProductMaster.objects.filter(is_active=True, is_deleted=False)
    warehouses = models.WarehouseMaster.objects.filter(is_active=True, is_deleted=False)
    if request.method == 'GET':
        context = {'form': form_class, 'products': products, 'warehouses': warehouses}
        return render(request, 'inventory/inventory_create.html', context=context)
    elif request.method == 'POST':
        # try:
        #     sku_code = StockRegister.objects.get(
        #         product=request.POST.get('sku_code'),
        #         is_active=True, is_deleted=False)
        # except Exception:
        #     sku_code = None
        # if sku_code is None:
        #     form_class = forms.InventoryForm(request.POST)
        #     if form_class.is_valid():
        #         try:
        #             product = ProductMaster.objects.get(pk=request.POST.get('sku_code'))
        #
        #             warehouse = WarehouseMaster.objects.get(
        #                 warehouse_code=request.POST.get('warehouse_code'))
        #         except WarehouseMaster.DoesNotExist:
        #             warehouse = WarehouseMaster()
        #         warehouse.warehouse_code = request.POST.get('warehouse_code')
        #         warehouse.warehouse_name = request.POST.get('warehouse_code')
        #         # warehouse.city = request.POST.get('city')
        #         warehouse.save()
        #
        #         try:
        #             stock_level = StockLevelMaster.objects.get(
        #                 warehouse_id=warehouse, product_id=product,
        #                 is_active=True, is_deleted=False)
        #         except StockLevelMaster.DoesNotExist:
        #             stock_level = StockLevelMaster()
        #         stock_level.warehouse = warehouse
        #         stock_level.product = product
        #         min_stock_level = request.POST.get('minimum_stock_level')
        #         if min_stock_level != '' and min_stock_level is not None:
        #             stock_level.min_stock_level = int(min_stock_level)
        #         else:
        #             stock_level.min_stock_level = 0
        #         stock_level.save()
        #
        #         try:
        #             stock_register = StockRegister.objects.get(
        #                 warehouse_id=warehouse, product_id=product,
        #                 is_active=True, is_deleted=False)
        #         except StockRegister.DoesNotExist:
        #             stock_register = StockRegister()
        #         stock_register.warehouse = warehouse
        #         stock_register.product = product
        #         stock_in = request.POST.get('saleable_stock')
        #         if stock_in != '' and stock_in != 0:
        #             stock_register.stock_in = float(stock_in)
        #             stock_register.stock_type = 'Sellable'
        #             stock_register.stock_out = float(0)
        #             stock_register.save()
        #
        #         stock_out = request.POST.get('nonsaleable_stock')
        #         if stock_out != '' and stock_out != 0:
        #             stock_register.pk = None
        #             stock_register.stock_out = float(stock_out)
        #             stock_register.stock_type = 'NonSellable'
        #             stock_register.stock_in = float(0)
        #             stock_register.save()
        #         messages.add_message(request, messages.SUCCESS, 'Inventory created successfully...')
        #     else:
        #         context = {'form': form_class, 'products': products}
        #         return render(request, 'inventory/inventory_create.html', context=context)
        #     return redirect('/master/inventory?page_style=list')
        # else:
        #     messages.add_message(request, messages.ERROR, 'SKU_CODE already exist!!!')
        #     context = {'form': form_class, 'products': products}
        #     return render(request, 'inventory/inventory_create.html', context=context)

        """ NEW CODE """

        form_class = forms.InventoryForm(request.POST)
        if form_class.is_valid():
            try:
                product = ProductMaster.objects.get(pk=request.POST.get('sku_code'))

                warehouse = WarehouseMaster.objects.get(
                    warehouse_code=request.POST.get('warehouse_code'))
            except WarehouseMaster.DoesNotExist:
                warehouse = WarehouseMaster()
                warehouse.warehouse_code = request.POST.get('warehouse_code')
                warehouse.warehouse_name = request.POST.get('warehouse_code')
                warehouse.save()

            try:
                stock_level = StockLevelMaster.objects.get(
                    warehouse_id=warehouse, product_id=product,
                    is_active=True, is_deleted=False)
            except StockLevelMaster.DoesNotExist:
                stock_level = StockLevelMaster()
            stock_level.warehouse = warehouse
            stock_level.product = product
            min_stock_level = request.POST.get('minimum_stock_level')
            if min_stock_level != '' and min_stock_level is not None:
                stock_level.min_stock_level = int(min_stock_level)
            else:
                stock_level.min_stock_level = 0
            stock_level.save()

            stock_register = StockRegister()
            stock_register.warehouse = warehouse
            stock_register.product = product
            stock_in = request.POST.get('saleable_stock')
            if stock_in != '' and stock_in != '0' and stock_in is not None:
                stock_register.stock_in = float(stock_in)
                stock_register.stock_type = 'Sellable'
                stock_register.stock_out = float(0)
                stock_register.save()

            stock_out = request.POST.get('nonsaleable_stock')
            if stock_out != '' and stock_out != '0' and stock_out is not None:
                stock_register.pk = None
                stock_register.stock_in = float(stock_out)
                stock_register.stock_type = 'NonSellable'
                stock_register.stock_out = float(0)
                stock_register.save()
            messages.add_message(request, messages.SUCCESS, 'Inventory created successfully...')
        else:
            context = {'form': form_class, 'products': products}
            return render(request, 'inventory/inventory_create.html', context=context)
        return redirect('/master/inventory?page_style=list')


def edit_inventory(request, rid, sid):
    products = models.ProductMaster.objects.filter(is_active=True)
    stock_register = models.StockRegister.objects.get(pk=rid)
    stock_level = StockLevelMaster.objects.get(pk=sid)
    warehouses = models.WarehouseMaster.objects.filter(is_active=True)
    if request.method == 'GET':
        register_item = {
            "warehouse": stock_register.warehouse.warehouse_code,
            "product": stock_register.product.pk,
            "city": stock_register.warehouse.city,
            "stock_in": stock_register.stock_in,
            "stock_out": stock_register.stock_out,
            "min_stock": stock_level.min_stock_level
        }
        context = {'products': products, 'item': register_item, 'warehouses': warehouses}
        return render(request, 'inventory/inventory_create.html', context=context)

    elif request.method == 'POST':
        form_class = forms.InventoryForm(request.POST)
        if form_class.is_valid():
            product = ProductMaster.objects.get(pk=request.POST.get('sku_code'))

            warehouse = WarehouseMaster.objects.get(
                warehouse_code=stock_register.warehouse.warehouse_code)
            warehouse.warehouse_code = request.POST.get('warehouse_code')
            # warehouse.city = request.POST.get('city')
            warehouse.save()

            stock_level.warehouse = warehouse
            stock_level.product = product
            min_stock_level = request.POST.get('minimum_stock_level')
            if min_stock_level != '' and min_stock_level is not None:
                stock_level.min_stock_level = int(min_stock_level)
            else:
                stock_level.min_stock_level = 0
            stock_level.save()

            stock_register.warehouse = warehouse
            stock_register.product = product
            stock_in = request.POST.get('saleable_stock')
            if stock_in != '' and stock_in is not None:
                stock_register.stock_in = float(stock_in)
                # if stock_in != 0:
                #     stock_register.stock_type = 'Sellable'
            # else:
            #     stock_register.stock_in = 0
            stock_out = request.POST.get('nonsaleable_stock')
            if stock_out != '' and stock_out is not None:
                stock_register.stock_out = float(stock_out)
                # if stock_out != 0:
                #     stock_register.stock_type = 'NonSellable'
            # else:
            #     stock_register.stock_out = 0
            stock_register.save()
            messages.add_message(request, messages.SUCCESS, 'Inventory updated successfully...')
        else:
            register_item = {
                "warehouse": request.POST.get('warehouse_code'),
                "product": request.POST.get('sku_code'),
                "city": request.POST.get('city'),
                "stock_in": request.POST.get('saleable_stock'),
                "stock_out": request.POST.get('nonsaleable_stock'),
                "min_stock": request.POST.get('minimum_stock_level')
            }
            context = {'products': products, 'item': register_item}
            return render(request, 'inventory/inventory_create.html', context=context)
        return redirect('/master/inventory?page_style=list')
