"""
Django settings for Owims project.

Generated by 'django-admin startproject' using Django 2.1.1.

For more information on this file, see
https://docs.djangoproject.com/en/2.1/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/2.1/ref/settings/
"""

import os
# print(os.getpid())
# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'Owims.settings')


# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/2.1/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = '-(=2q!j%451gn4kfh4&w=3+qo%m4-s#!h(n(l6hu*7b@v&omee'


# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

ALLOWED_HOSTS = []

AUTH_USER_MODEL = 'ClassicUserAccounts.User'

# Application definition
INSTALLED_APPS = [
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'ClassicUserAccounts',
    'sorl.thumbnail',
    'django.contrib.admin',
    'django.contrib.auth',
    'crispy_forms',
    'IndospritCore',
    'IndospiritTransactions',
    'UserInfo',
    'django.contrib.humanize'
    #'mptt',
]

CRISPY_TEMPLATE_PACK = 'bootstrap3'

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'UserInfo.middleware.BrandMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'ClassicUserAccounts.middleware.ClassicUserAccountsMiddleWare',

]

ROOT_URLCONF = 'Owims.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [
            os.path.join(BASE_DIR, 'templates'),
        ],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
            'libraries': {
                'custom_tags': 'UserInfo.templatetags.custom_tags'
            },
        },
    },
]


WSGI_APPLICATION = 'Owims.wsgi.application'


# Database
# https://docs.djangoproject.com/en/2.1/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
    }
}


# Password validation
# https://docs.djangoproject.com/en/2.1/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]


# Internationalization
# https://docs.djangoproject.com/en/2.1/topics/i18n/

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'Asia/Kolkata'

USE_I18N = True

USE_L10N = True

USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/2.1/howto/static-files/

STATIC_URL = '/static/'
STATIC_ROOT = os.path.join(os.path.dirname(BASE_DIR), 'static')

MEDIA_URL = '/media/'
MEDIA_ROOT = os.path.join(BASE_DIR, 'media')
LOGIN_REDIRECT_URL='/'
STATICFILES_DIRS = [ os.path.join(BASE_DIR, 'static'), ]
SITE_NAME = 'Owims'

DOCUMENT_URL = '/document/'
DOCUMENT_ROOT = os.path.join(BASE_DIR, 'document')

PAGE_SIZE = 50
DEFAULT_PAGING_VIEW = 'list'

# SKINS = ('skin-blue', 'skin-black', 'skin-red', 'skin-yellow', 'skin-purple', 'skin-green', 'skin-blue-light',
#          'skin-black-light', 'skin-red-light', 'skin-yellow-light', 'skin-purple-light', 'skin-green-light')

ROLE_BASED_SKIN = [
   {'role': 'Admin', 'skin_name': 'skin-yellow'},
   {'role': 'Dealer', 'skin_name': 'skin-purple'}
]

THEME_NAME = 'default-theme'
# THEME_NAME = 'theme-2'
USER_BASED_THEME = False


# *************** SMTP Settings Start **************************
EMAIL_BACKEND = "django.core.mail.backends.smtp.EmailBackend"
EMAIL_HOST = 'smtp.gmail.com'
EMAIL_HOST_USER = 'noreply@indospirit.com'
EMAIL_PORT = 587
EMAIL_USE_TLS = True
EMAIL_HOST_PASSWORD = 'Indo@123'
FROM_EMAIL_DEFAULT = 'noreply@indospirit.com'
# EMAIL_DEFAULT = 'Owims Team <postmaster@mg.viewyoursite.net>'
# DEFAULT_FROM_EMAIL = EMAIL_DEFAULT
# *************** SMTP Settings End **************************

# *************** SMS Settings Start **************************
API_KEY = "A04109a576045c6b2b7147c77ef0db97d"
SENDER = "INDOSP"
METHOD = "sms"
SEND_URL = "https://api-alerts.kaleyra.com/v4/"
# *************** SMS Settings End **************************