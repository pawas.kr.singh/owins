# ========================ALLOWANCE-IMPORT====================================
ALLOWANCE_MASTER_IMPORT_KEYS = ['allowance_b', 'allowance_m', 'allowance_vol', 'allowance_val']
DEALER_ALLOWANCE_IMPORT_KEYS = ['dealer_code', 'allowance_b', 'allowance_m', 'allowance_vol', 'allowance_val']

#========================DEALER-SALES-TARGET IMPORT============================
DEALER_SALES_TARGET_IMPORT_KEY = ['dealer_code','product_category','monthly_target']

#=========================DEALER-DATA IMPORT===================================
DEALER_DATA_IMPORT_KEY = ['dealer_code','brand_name','company_name','company_address','shipping_address','state',
                          'region_id','city','pincode','email','owner_name','primary_contact','alternate_contact',
                          'sr_id','GST_no','PAN_no','aadhaar_no','min_order_amount',
                          'max_outstanding_threshold','credit_days','tie_up_b', 'tie_up_m', 'tie_up_vol', 'tie_up_val','CDslab_code']
DEALER_DATA_REQUIRED_FIELD = ['dealer_code','brand_name','company_name','owner_name','primary_contact']
#=========================PRODUCT-MASTER IMPORT===================================
PRODUCT_MASTER_IMPORT_KEY = ['brand','main_category','category','sub_category-1','sub_category-2','sub_category-3','SKU','MRP','DP','product_category_code','size_capacity_ltr','series_colour','features']
#===========================Cash-Discount-Slabs Master===============================
CASH_DISCOUNT_SLABS_IMPORT_KEY=['slabcode','days1','cd1','days2','cd2','days3','cd3','days4','cd4']
#=============================SCHEME-SELL-THROUGH-IMOPRT===============================
SCHEME_SELL_THROUGH_IMOPRT=['brand','state','city','sku_code','discount_percent','discount_amount','scheme_start_date','scheme_end_date','scheme_name','scheme_code']
#=============================INVENTORY-IMPORT====================================
INVENTORY_IMPORT_KEY=['WAREHOUSE CODE','City','Brand','PRODUCT CODE(SKU)','SALEABLE STOCK','NON-SALEABLE STOCK','MINIMUM STOCK LEVEL']
#============================TIE_UP=============================================
DEALER_TIE_UP_IMPORT_KEYS=['dealer_code', 'tie_up_b', 'tie_up_m', 'tie_up_vol', 'tie_up_val']
TIE_UP_MASTER_IMPORT_KEYS = [ 'tie_up_b', 'tie_up_m', 'tie_up_vol', 'tie_up_val']
