from django.shortcuts import render
from django.shortcuts import render, get_object_or_404, redirect
from . import forms
from . import models
from IndospiritTransactions.models import OrderHead,OrderEventHistory
from ClassicUserAccounts.models import User
from UserInfo.models import DealerProfile, SRDealer
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
from django.db.models import Count, Max, Q, Sum
import csv
from UserInfo import status_in
import datetime


def order_head(request, order_status=None):
    dealer = request.GET.get('on_id')
    active = ''
    dealercount = ''
    searchValue = ''
    try:
        dealer_active = User.objects.get(pk=int(dealer), is_active=True, groups__name="DEL")
        active = 'True'
    except Exception:
        active = ''

    edit = ''
    order_head = ''

    if order_status == 'Processing':
        edit = 'Active Orders'
        if request.user.is_superuser:
            dealer = User.objects.filter(is_active=True, groups__name="DEL").values_list('pk')
            order_head = models.OrderHead.objects.filter(
                dealer_id__in=dealer, is_active=True, is_deleted=False,
                status__in=status_in.SRHeadActiveStatus()).order_by('-order_date')
            # order_head = models.OrderHead.objects.filter(status=order_status)
        else:
            edit = 'Active Order'
            try:
                SR = User.objects.get(id=request.user.id, groups__name="SR", is_active=True)
            except Exception:
                SR = ''

            if SR == '':
                order_head = models.OrderHead.objects.filter(
                    dealer_id=int(dealer), status__in=status_in.SRHeadActiveStatus(),
                    is_active=True, is_deleted=False).order_by('-order_date')
            else:
                sr_dealer = SRDealer.objects.filter(sr_id_id=SR.pk).values_list('dealer_id_id')
                dealer = DealerProfile.objects.filter(
                    pk__in=sr_dealer, is_active=True, is_deleted=False).values_list('dealer_id')
                userobj = User.objects.filter(is_active=True, pk__in=dealer, groups__name="DEL").values_list('pk')
                order_head = models.OrderHead.objects.filter(
                    dealer_id__in=userobj, status__in=status_in.SRHeadActiveStatus(),
                    is_active=True, is_deleted=False).order_by('-order_date')
                
            # order_head = models.OrderHead.objects.filter(dealer_id=int(dealer),status=order_status)
    else:
        get_url = request.get_full_path().split('/')[3]
        edit = get_url
        groups = request.user.groups.all()
        groups = [group.name for group in groups]
        if request.user.is_superuser or 'Finance' in groups or 'BusinessHead' in groups:
            if edit == 'Process':
                order_head = models.OrderHead.objects.filter(
                    dealer_id=int(dealer), is_active=True, is_deleted=False,
                    status__in=status_in.SRHeadNullStatus()).order_by('-order_date')
                if not order_head:
                    try:
                        userobj = User.objects.get(id=int(dealer), is_active=True, is_superuser=True)
                    except Exception:
                        userobj = ''
                    if userobj != '':
                        dealer = User.objects.filter(is_active=True, groups__name="DEL").values_list('pk')
                        order_head = models.OrderHead.objects.filter(
                            dealer_id__in=dealer, is_active=True, is_deleted=False,
                            status__in=status_in.SRHeadNullStatus()).order_by('-order_date')
            elif edit == 'Outstanding' or edit == 'outstanding':
                order_head = models.OrderHead.objects.filter(
                    dealer_id=int(dealer), is_active=True, is_deleted=False,
                    status__in=['Dispatched', 'Delivered', 'PartialDispatched']).order_by('-order_date')
                if not order_head:
                    try:
                        userobj = User.objects.get(id=int(dealer), is_active=True, is_superuser=True)
                    except Exception:
                        userobj = ''
                    if userobj != '':
                        dealer = User.objects.filter(is_active=True, groups__name="DEL").values_list('pk')
                        order_head = models.OrderHead.objects.filter(
                            dealer_id__in=dealer, is_active=True, is_deleted=False,
                            status__in=['Dispatched', 'Delivered', 'PartialDispatched']).order_by('-order_date')
            elif edit == 'Delivered':
                order_head = models.OrderHead.objects.filter(
                    dealer_id=int(dealer), is_active=True, is_deleted=False,
                    status__in=['Dispatched', 'Delivered', 'PartialDispatched', 'PaymentDone']).order_by('-order_date')
                if not order_head:
                    try:
                        userobj = User.objects.get(id=int(dealer), is_active=True, is_superuser=True)
                    except Exception:
                        userobj = ''
                    if userobj != '':
                        dealer = User.objects.filter(is_active=True, groups__name="DEL").values_list('pk')
                        order_head = models.OrderHead.objects.filter(
                            dealer_id__in=dealer, is_active=True, is_deleted=False,
                            status__in=['Dispatched', 'Delivered', 'PartialDispatched', 'PaymentDone']).order_by('-order_date')
            elif edit == 'On_Hold':
                edit = 'On Hold'
                order_head = models.OrderHead.objects.filter(
                    dealer_id=int(dealer), is_active=True, is_deleted=False,
                    status__in=['OnHold']).order_by('-order_date')
                if not order_head:
                    try:
                        userobj = User.objects.get(id=int(dealer), is_active=True, is_superuser=True)
                    except Exception:
                        userobj = ''
                    if userobj != '':
                        dealer = User.objects.filter(is_active=True, groups__name="DEL").values_list('pk')
                        order_head = models.OrderHead.objects.filter(
                            dealer_id__in=dealer, is_active=True, is_deleted=False,
                            status__in=['OnHold']).order_by('-order_date')
            elif edit == 'InWarehouse':
                order_head = models.OrderHead.objects.filter(
                    dealer_id=int(dealer), is_active=True, is_deleted=False,
                    status__in=['InWarehouse']).order_by('-order_date')
                if not order_head:
                    try:
                        userobj = User.objects.get(id=int(dealer), is_active=True, is_superuser=True)
                    except Exception:
                        userobj = ''
                    if userobj != '':
                        dealer = User.objects.filter(is_active=True, groups__name="DEL").values_list('pk')
                        order_head = models.OrderHead.objects.filter(
                            dealer_id__in=dealer, is_active=True, is_deleted=False,
                            status__in=['InWarehouse']).order_by('-order_date')
            elif edit == 'Deleted':
                order_head = models.OrderHead.objects.filter(
                    dealer_id=int(dealer), is_active=True, is_deleted=False,
                    status__in=['Deleted']).order_by('-order_date')
                if not order_head:
                    try:
                        userobj = User.objects.get(id=int(dealer), is_active=True, is_superuser=True)
                    except Exception:
                        userobj = ''
                    if userobj != '':
                        dealer = User.objects.filter(is_active=True, groups__name="DEL").values_list('pk')
                        order_head = models.OrderHead.objects.filter(
                            dealer_id__in=dealer, is_active=True, is_deleted=False,
                            status__in=['Deleted']).order_by('-order_date')
            else:
                order_head = models.OrderHead.objects.filter(
                    dealer_id=int(dealer), is_active=True, is_deleted=False,
                    status__in=['Dispatched', 'Delivered', 'PartialDispatched']).order_by('-order_date')
                if not order_head:
                    try:
                        userobj = User.objects.get(id=int(dealer), is_active=True, is_superuser=True)
                    except Exception:
                        userobj = ''
                    if userobj != '':
                        dealer = User.objects.filter(is_active=True, groups__name="DEL").values_list('pk')
                        order_head = models.OrderHead.objects.filter(
                            dealer_id__in=dealer, is_active=True, is_deleted=False,
                            status__in=['Dispatched', 'Delivered', 'PartialDispatched']).order_by('-order_date')
        else:
            try:
                SR = User.objects.get(pk=int(dealer), groups__name="SR", is_active=True)
            except Exception:
                SR = ''

            if SR == '':
                if edit == 'Process':
                    edit = 'Active Order'
                    order_head = models.OrderHead.objects.filter(
                        dealer_id=int(dealer), status__in=status_in.SRHeadActiveStatus(),
                        is_active=True, is_deleted=False).order_by('-order_date')
                elif edit == 'Outstanding' or edit == 'outstanding':
                    order_head = models.OrderHead.objects.filter(
                        dealer_id=int(dealer), status__in=['Dispatched', 'Delivered', 'PartialDispatched'],
                        is_active=True, is_deleted=False).order_by('-order_date')
                elif edit == 'Delivered':
                    order_head = models.OrderHead.objects.filter(
                        dealer_id=int(dealer), status__in=['Dispatched', 'Delivered', 'PartialDispatched', 'PaymentDone'],
                        is_active=True, is_deleted=False).order_by('-order_date')
                elif edit == 'On_Hold':
                    order_head = models.OrderHead.objects.filter(
                        dealer_id=int(dealer), status__in=['OnHold'],
                        is_active=True, is_deleted=False).order_by('-order_date')
                elif edit == 'InWarehouse':
                    order_head = models.OrderHead.objects.filter(
                        dealer_id=int(dealer), status__in=['InWarehouse'],
                        is_active=True, is_deleted=False).order_by('-order_date')
                elif edit == 'Deleted':
                    order_head = models.OrderHead.objects.filter(
                        dealer_id=int(dealer), status__in=['Deleted'],
                        is_active=True, is_deleted=False).order_by('-order_date')
                else:
                    order_head = models.OrderHead.objects.filter(
                        dealer_id=int(dealer), status__in=['Dispatched', 'Delivered', 'PartialDispatched'],
                        is_active=True, is_deleted=False).order_by('-order_date')
            else:
                sr_dealer = SRDealer.objects.filter(sr_id_id=SR.pk).values_list('dealer_id_id')
                dealer = DealerProfile.objects.filter(
                    pk__in=sr_dealer, is_active=True, is_deleted=False).values_list('dealer_id')
                userobj = User.objects.filter(is_active=True, pk__in=dealer, groups__name="DEL").values_list('pk')
                if edit == 'Process':
                    edit = 'Order in process'
                    order_head = models.OrderHead.objects.filter(
                        dealer_id__in=userobj, status__in=status_in.SRHeadActiveStatus(),
                        is_active=True, is_deleted=False).order_by('-order_date')
                elif edit == 'History':
                    order_head = models.OrderHead.objects.filter(
                        dealer_id__in=userobj, status__in=['Delivered', 'PartialDispatched', 'PaymentDone'],
                        is_active=True, is_deleted=False).order_by('-order_date')
                elif edit == 'Outstanding' or edit == 'outstanding':
                    order_head = models.OrderHead.objects.filter(
                        dealer_id__in=userobj, status__in=['Dispatched', 'Delivered', 'PartialDispatched'],
                        is_active=True, is_deleted=False).order_by('-order_date')
                elif edit == 'Delivered':
                    order_head = models.OrderHead.objects.filter(
                        dealer_id__in=userobj, status__in=['Dispatched', 'Delivered', 'PartialDispatched', 'PaymentDone'],
                        is_active=True, is_deleted=False).order_by('-order_date')
                elif edit == 'On_Hold':
                    order_head = models.OrderHead.objects.filter(
                        dealer_id__in=userobj, status__in=['OnHold'],
                        is_active=True, is_deleted=False).order_by('-order_date')
                elif edit == 'InWarehouse':
                    order_head = models.OrderHead.objects.filter(
                        dealer_id__in=userobj, status__in=['InWarehouse'],
                        is_active=True, is_deleted=False).order_by('-order_date')
                elif edit == 'Deleted':
                    order_head = models.OrderHead.objects.filter(
                        dealer_id__in=userobj, status__in=['Deleted'],
                        is_active=True, is_deleted=False).order_by('-order_date')
                else:
                    order_head = models.OrderHead.objects.filter(
                        dealer_id__in=userobj, status__in=['Dispatched', 'Delivered', 'PartialDispatched'],
                        is_active=True, is_deleted=False).order_by('-order_date')
    odsum = {}
    if order_head:
        od = order_head.values_list('dealer_id')
        dealercount = User.objects.filter(pk__in=od, is_active=True).count()
        odsum = order_head.filter(status='Dispatched').aggregate(Sum('payable_amount'))
    else:
        odsum['payable_amount__sum'] = 0

    if odsum['payable_amount__sum'] is None:
        odsum['payable_amount__sum'] = 0
    for obj in order_head:
        try:
            orderPlaced = OrderEventHistory.objects.get(order_id=obj, order_status='Dispatched')
            obj.__dict__['Dispatched_Date'] = orderPlaced.created_on
        except Exception:
            obj.__dict__['Dispatched_Date'] = "---"
    if request.method == 'POST':
        if 'searchbtn' in request.POST:
            if order_head:
                searchValue = request.POST.get('search', None)
                order_head = order_head.filter(
                    Q(dealer__user_code__icontains=searchValue) |
                    Q(sr_id__first_name__icontains=searchValue) |
                    Q(sr_id__last_name__icontains=searchValue) |
                    Q(status__icontains=searchValue) |
                    Q(order_number__icontains=searchValue) |
                    Q(total_amount__icontains=searchValue) |
                    Q(discount_amount__icontains=searchValue) |
                    Q(payable_amount__icontains=searchValue))
                querycount = str(order_head.count())+' Record found.'

        for obj in order_head:
            try:
                orderPlaced = OrderEventHistory.objects.get(order_id=obj, order_status='Dispatched')
                obj.__dict__['Dispatched_Date'] = orderPlaced.created_on
            except Exception:
                obj.__dict__['Dispatched_Date'] = "---"

            try:
                d_profile = DealerProfile.objects.get(
                    dealer=obj.dealer, is_active=True, is_deleted=False)
                company_name = d_profile.company_name
            except DealerProfile.DoesNotExist:
                company_name = ""
            obj.__dict__["company"] = company_name

    # if request.method == 'POST':
        if 'export' in request.POST:
            if len(order_head) > 0:
                result = []
                searchValue = request.POST.get('export', None)
                order_head = order_head.filter(
                    Q(dealer__user_code__icontains=searchValue) |
                    Q(sr_id__first_name__icontains=searchValue) |
                    Q(sr_id__last_name__icontains=searchValue) |
                    Q(status__icontains=searchValue) |
                    Q(order_number__icontains=searchValue) |
                    Q(total_amount__icontains=searchValue) |
                    Q(discount_amount__icontains=searchValue) |
                    Q(payable_amount__icontains=searchValue))
                for obj in order_head:
                    try:
                        orderPlaced = OrderEventHistory.objects.get(order_id=obj, order_status='Dispatched')
                        obj.__dict__['Dispatched_Date'] = orderPlaced.created_on
                    except Exception:
                        obj.__dict__['Dispatched_Date'] = "---"

                    try:
                        d_profile = DealerProfile.objects.get(
                            dealer=obj.dealer, is_active=True, is_deleted=False)
                        company_name = d_profile.company_name
                    except DealerProfile.DoesNotExist:
                        company_name = ""
                    obj.__dict__["company"] = company_name
                for order in order_head:

                    if order.status in ['Draft', 'Processing', 'SchemeApplied']:
                        discountAmount = "---"
                    else:
                        discountAmount = order.discount_amount

                    get_order = {}
                    # .....................
                    groups = request.user.groups.all()
                    groups = [group.name for group in groups]
                    # if 'SR' in groups:
                    #     pass
                    # else:
                    #     get_order['SR Name'] = "{0} {1}".format(order.sr_id.first_name, order.sr_id.last_name)
                    #     get_order['SR No.'] = order.sr_id.mobile
                    # .....................

                    if order.status in ['Dispatched', 'PaymentDone']:
                        payableAmount = order.payable_amount
                    else:
                        payableAmount = order.payable_amount
                        # payableAmount = "---"

                    get_order['Order No'] = order.order_number
                    get_order['DMS No'] = order.DMS_order_number
                    if isinstance(order.order_date, datetime.date):
                        get_order['Order Date'] = order.order_date.date()
                    else:
                        get_order['Order Date'] = order.order_date
                    if isinstance(order.Dispatched_Date, datetime.date):
                        get_order['Dispatched Date'] = order.Dispatched_Date.date()
                    else:
                        get_order['Dispatched Date'] = order.Dispatched_Date
                    get_order['Invoice Value'] = order.total_amount
                    get_order['Discount Amount'] = discountAmount
                    get_order['Payable Amount'] = payableAmount
                    get_order['Status'] = order.status
                    get_order['SR'] = str(order.sr_id.first_name + ' ' + order.sr_id.last_name)
                    get_order['Dealer'] = order.dealer.user_code



                    result.append(get_order)
                if edit == 'Outstanding' or edit == 'outstanding':
                    response = HttpResponse(content_type='text/csv')
                    response['Content-Disposition'] = 'attachment;filename=Outstanding_order_list.csv'
                if edit == 'History' or edit == 'history':
                    response = HttpResponse(content_type='text/csv')
                    response['Content-Disposition'] = 'attachment;filename=History_order_list.csv'
                if order_status == 'Processing':
                    response = HttpResponse(content_type='text/csv')
                    response['Content-Disposition'] = 'attachment;filename=Process_order_list.csv'
                if edit == 'Active Order':
                    response = HttpResponse(content_type='text/csv')
                    response['Content-Disposition'] = 'attachment;filename=Active_order_list.csv'
                if edit == 'Order in process':
                    response = HttpResponse(content_type='text/csv')
                    response['Content-Disposition'] = 'attachment;filename=Active_order_list.csv'
                if edit == 'On Hold':
                    response = HttpResponse(content_type='text/csv')
                    response['Content-Disposition'] = 'attachment;filename=On_Hold_order_list.csv'
                if edit == 'InWarehouse':
                    response = HttpResponse(content_type='text/csv')
                    response['Content-Disposition'] = 'attachment;filename=InWarehouse_order_list.csv'
                if edit == 'Deleted':
                    response = HttpResponse(content_type='text/csv')
                    response['Content-Disposition'] = 'attachment;filename=Deleted_order_list.csv'
                if edit == 'Delivered':
                    response = HttpResponse(content_type='text/csv')
                    response['Content-Disposition'] = 'attachment;filename=Delivered_order_list.csv'
                # keys = ['Order No', 'Dealer', 'Status', 'Invoice Value',
                #         'Discount Amount', 'Payable Amount']
                keys = ['Order No','DMS No' ,'Order Date', 'Dispatched Date', 'Invoice Value','Discount Amount',
                        'Payable Amount', 'Status','SR','Dealer']

                # .....................
                groups = request.user.groups.all()
                groups = [group.name for group in groups]
                # if 'SR' in groups:
                #     pass
                # else:
                #     keys.append('SR')
                #     keys.append('SR No.')
                # .....................
                writer = csv.DictWriter(response, keys)
                writer.writeheader()
                writer.writerows(result)
                return response

    if edit == 'Delivered':
        edit = 'Dispatched'

    my_head_count = len(order_head)

    _x = [a.dealer for a in order_head]
    _x = list(set(_x))
    my_dealer_count = len(_x)
    my_order_head_sum = order_head.filter(status='Dispatched').aggregate(Sum('payable_amount'))
    context = {
        'searchValue': searchValue,
        'order_head': order_head,
        # 'dealercount': dealercount,
        'dealercount': my_dealer_count,
        # 'orderheadsum': odsum['payable_amount__sum'],
        'orderheadsum': my_order_head_sum['payable_amount__sum'],
        # 'orderlen': len(order_head),
        'orderlen': my_head_count,
        'edit': edit, 'active': active
    }
    return render(request, 'order_head/order_head.html', context=context)


def edit_order_head(request, Ohid,status):
    instance = get_object_or_404(OrderHead, pk=Ohid)
    dealer = OrderHead.objects.get(pk=Ohid)
    form_class = forms.OrderHeadForm(request.POST or None, instance=instance)
    if request.method == 'GET':
        return render(request, 'order_head/order_head_create.html', {'create_order_head_form': form_class})
    elif request.method == 'POST':
        form_class = forms.OrderHeadForm(request.POST or None, instance=instance)
        if form_class.is_valid():
            order_head = form_class.save(commit=False)
            order_head.created_by = request.user
            order_head.save()
        dealerid=str(dealer.dealer_id)
        if status!='Processing':
            return redirect( '/transactions/order_head/outstanding/?dealer_id='+dealerid)
        else:
            return redirect( '/transactions/order_head/Processing/?dealer_id='+dealerid)

def create_order_head(request):
    form_class = forms.OrderHeadForm
    if request.method == 'GET':
        return render(request, 'order_head/order_head_create.html', {'create_order_head_form': form_class})
    elif request.method == 'POST':
        form_class = forms.OrderHeadForm(request.POST)
        if form_class.is_valid():
            order_head = form_class.save(commit=False)
            order_head.created_by = request.user
            order_head.save()
        return render(request, 'order_head/order_head_create.html', {'create_order_head_form': form_class})
    elif request.method == 'PUT':
        form_class = forms.OrderHeadForm(request.POST)
        if form_class.is_valid():
            order_head = form_class.save(commit=False)
            order_head.created_by = request.user
            order_head.save()
        return render(request, 'order_head/order_head_create.html', {'create_order_head_form': form_class})


def order_details(request):
    order_details = models.OrderDetails.objects.all()
    return render(request, 'order_details/order_details.html', {'order_details': order_details})


def create_order_details(request):
    form_class = forms.OrderDetailsForm

    if request.method == 'GET':
        return render(request, 'order_details/order_details_create.html', {'form': form_class})

    elif request.method == 'POST':
        form_class = forms.OrderDetailsForm(request.POST)
        if form_class.is_valid():
            order_details = form_class.save(commit=False)
            order_details.created_by = request.user
            order_details.save()
        return render(request, 'order_details/order_details_create.html', {'form': form_class})

    elif request.method == 'PUT':
        form_class = forms.OrderDetailsForm(request.POST)
        if form_class.is_valid():
            order_details = form_class.save(commit=False)
            order_details.created_by = request.user
            order_details.save()
        return render(request, 'order_details/order_details_create.html', {'form': form_class})


def order_allowance(request):
    order_allowance = models.OrderDetails.objects.all()
    return render(request, 'order_allowance/order_allowance.html', {'order_allowance': order_allowance})


def create_order_allowance(request):
    form_class = forms.OrderAllowanceDetailsForm
    if request.method == 'GET':
        return render(request, 'order_allowance/order_allowance_create.html',
                      {'create_order_allowance_form': form_class})
    elif request.method == 'POST':
        form_class = forms.OrderAllowanceDetailsForm(request.POST)
        if form_class.is_valid():
            allowance = form_class.save(commit=False)
            allowance.created_by = request.user
            allowance.save()
        return render(request, 'order_allowance/order_allowance_create.html',
                      {'create_order_allowance_form': form_class})
    elif request.method == 'PUT':
        form_class = forms.OrderAllowanceDetailsForm(request.POST)
        if form_class.is_valid():
            allowance = form_class.save(commit=False)
            allowance.created_by = request.user
            allowance.save()
        return render(request, 'order_allowance/order_allowance_create.html',
                      {'create_order_allowance_form': form_class})


def order_tie_up(request):
    order_tie_up = models.OrderDetails.objects.all()
    return render(request, 'order_tie_up/order_tie_up.html', {'order_tie_up': order_tie_up})


def create_order_tie_up(request):
    form_class = forms.OrderTieUpDetailsForm
    if request.method == 'GET':
        return render(request, 'order_tie_up/order_tie_up_create.html', {'create_order_tie_up_form': form_class})
    elif request.method == 'POST':
        form_class = forms.OrderTieUpDetailsForm(request.POST)
        if form_class.is_valid():
            order_tie_up = form_class.save(commit=False)
            order_tie_up.created_by = request.user
            order_tie_up.save()
        return render(request, 'order_tie_up/order_tie_up_create.html', {'create_order_tie_up_form': form_class})
    elif request.method == 'PUT':
        form_class = forms.OrderTieUpDetailsForm(request.POST)
        if form_class.is_valid():
            order_tie_up = form_class.save(commit=False)
            order_tie_up.created_by = request.user
            order_tie_up.save()
        return render(request, 'order_tie_up/order_tie_up_create.html', {'create_order_tie_up_form': form_class})


def order_scheme(request):
    order_scheme = models.OrderSchemeDetailsSellThrough.objects.all()
    return render(request, 'order_scheme/order_scheme.html', {'order_scheme': order_scheme})


def create_order_scheme(request):
    form_class = forms.OrderSchemeDetailsSellThroughForm
    if request.method == 'GET':
        return render(request, 'order_scheme/order_scheme_create.html', {'form': form_class})
    elif request.method == 'POST':
        form_class = forms.OrderSchemeDetailsSellThroughForm(request.POST)
        if form_class.is_valid():
            order_scheme = form_class.save(commit=False)
            order_scheme.created_by = request.user
            order_scheme.save()
        return render(request, 'order_scheme/order_scheme_create.html', {'create_order_scheme_form': form_class})
    elif request.method == 'PUT':
        form_class = forms.OrderSchemeDetailsSellThroughForm(request.POST)
        if form_class.is_valid():
            order_scheme = form_class.save(commit=False)
            order_scheme.created_by = request.user
            order_scheme.save()
        return render(request, 'order_scheme/order_scheme_create.html', {'create_order_scheme_form': form_class})


def cash_discount_transaction(request):
    cash_discount = models.CashDiscountTransaction.objects.all()
    return render(request, 'cash_discount_transactions/cash_discount_transaction.html',
                  {'cash_discount_transaction': cash_discount_transaction})


def create_cash_discount_transaction(request):
    form_class = forms.CashDiscountTransactionForm
    if request.method == 'GET':
        return render(request, 'cash_discount_transactions/cash_discount_transaction_create.html', {'form': form_class})
    elif request.method == 'POST':
        form_class = forms.CashDiscountTransactionForm(request.POST)
        if form_class.is_valid():
            cd = form_class.save(commit=False)
            cd.created_by = request.user
            cd.save()
        return render(request, 'cash_discount_transactions/cash_discount_transaction_create.html', {'form': form_class})
    elif request.method == 'PUT':
        form_class = forms.CashDiscountTransactionForm(request.POST)
        if form_class.is_valid():
            cd = form_class.save(commit=False)
            cd.created_by = request.user
            cd.save()
        return render(request, 'cash_discount_transactions/cash_discount_transaction_create', {'form': form_class})


def credit_note(request):
    credit_note = models.CreditNotes.objects.all()
    return render(request, 'credit_note/credit_note.html', {'credit_note': credit_note})


def create_credit_note(request):
    form_class = forms.CreditNotesForm
    if request.method == 'GET':
        return render(request, 'credit_note/credit_note_create.html', {'create_credit_notes_form': form_class})
    elif request.method == 'POST':
        form_class = forms.CreditNotesForm(request.POST)
        if form_class.is_valid():
            credit_note = form_class.save(commit=False)
            credit_note.created_by = request.user
            credit_note.save()
        return render(request, 'credit_note/credit_note_create.html', {'create_credit_notes_form': form_class})
    elif request.method == 'PUT':
        form_class = forms.CreditNotesForm(request.POST)
        if form_class.is_valid():
            credit_note = form_class.save(commit=False)
            credit_note.created_by = request.user
            credit_note.save()
        return render(request, 'credit_note/credit_note_create.html', {'create_credit_notes_form': form_class})


def dispatch_head(request):
    dispatch_head = models.OrderDispatchHead.objects.all()
    return render(request, 'dispatch_head/dispatch_head.html', {'dispatch_head': dispatch_head})


def create_dispatch_head(request):
    form_class = forms.OrderDispatchHeadForm
    if request.method == 'GET':
        return render(request, 'dispatch_head/dispatch_head_create.html',
                      {'create_order_dispatch_head_form': form_class})

    elif request.method == 'POST':
        form_class = forms.OrderDispatchHeadForm(request.POST)
        if form_class.is_valid():
            dispatch_head = form_class.save(commit=False)
            dispatch_head.created_by = request.user
            dispatch_head.save()
        return render(request, 'dispatch_head/dispatch_head_create.html',
                      {'create_order_dispatch_head_form': form_class})
    elif request.method == 'PUT':
        form_class = forms.OrderDispatchHeadForm(request.POST)
        if form_class.is_valid():
            dispatch_head = form_class.save(commit=False)
            dispatch_head.created_by = request.user
            dispatch_head.save()
        return render(request, 'dispatch_head/dispatch_head_create.html',
                      {'create_order_dispatch_head_form': form_class})


def dispatch_detail(request):
    dispatch_detail = models.OrderDispatchDetail.objects.all()
    return render(request, 'dispatch_detail/dispatch_detail.html', {'dispatch_detail': dispatch_detail})


def create_dispatch_detail(request):
    form_class = forms.OrderDispatchDetailsForm
    if request.method == 'GET':
        return render(request, 'dispatch_detail/dispatch_detail_create.html',
                      {'create_order_dispatch_detail_form': form_class})
    elif request.method == 'POST':
        form_class = forms.OrderDispatchDetailsForm(request.POST)
        if form_class.is_valid():
            dispatch_detail = form_class.save(commit=False)
            dispatch_detail.created_by = request.user
            dispatch_detail.save()
        return render(request, 'dispatch_detail/dispatch_detail_create.html',
                      {'create_order_dispatch_detail_form': form_class})
    elif request.method == 'PUT':
        form_class = forms.OrderDispatchDetailsForm(request.POST)
        if form_class.is_valid():
            dispatch_detail = form_class.save(commit=False)
            dispatch_detail.created_by = request.user
            dispatch_detail.save()
        return render(request, 'dispatch_detail/dispatch_detail_create.html',
                      {'create_order_dispatch_detail_form': form_class})
