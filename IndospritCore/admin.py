from django.contrib import admin
from . import models
from .forms import ProductCategoryMasterForm


class WarehouseMasterAdmin(admin.ModelAdmin):
    list_display = ('warehouse_code', 'warehouse_name', 'address_line1', 'address_line2', 'state', 'city', 'pin_code',
                    'contact_number', 'created_on', 'created_by', 'modified_on', 'modified_by')


admin.site.register(models.WarehouseMaster, WarehouseMasterAdmin)


class BrandMasterAdmin(admin.ModelAdmin):
    list_display = ('brand_name', 'brand_code','is_active', 'created_by', 'created_on', 'modified_on', 'modified_by',
                    'primary_warehouse')


admin.site.register(models.BrandMaster, BrandMasterAdmin)


class ProductCategoryMasterAdmin(admin.ModelAdmin):
    form = ProductCategoryMasterForm
    list_display = ('brand', 'category_name', 'path', 'parent', 'created_on', 'created_by', 'modified_on',
                    'modified_by', 'is_active')


admin.site.register(models.ProductCategoryMaster, ProductCategoryMasterAdmin)


class WarehouseAdmin(admin.ModelAdmin):
    list_display = ('user', 'warehouse', 'is_active', 'created_on', 'created_by', 'modified_on', 'modified_by')


admin.site.register(models.WarehouseAdmin, WarehouseAdmin)


class ProductMasterAdmin(admin.ModelAdmin):
    list_display = ('brand', 'product_category', 'sku_code', 'product_short_name', 'size_capacity',
                    'size_capacity_unit', 'TaxSlabCode', 'pattern_color',
                    'product_description_feature',  'best_buying_price', 'dealer_price',
                    'max_retail_price', 'created_on', 'created_by', 'modified_on', 'modified_by')


admin.site.register(models.ProductMaster, ProductMasterAdmin)


class ProductPriceHistoryAdmin(admin.ModelAdmin):
    list_display = ('product', 'gst_rate', 'sgst_rate', 'cgst_rate', 'other_tax_rate',
                    'best_buying_price', 'dealer_price', 'max_retail_price', 'created_on', 'created_by',
                    'modified_on', 'modified_by')


admin.site.register(models.ProductPriceHistory, ProductPriceHistoryAdmin)


class StockLevelMasterAdmin(admin.ModelAdmin):
    list_display = ('warehouse', 'product', 'min_stock_level')


admin.site.register(models.StockLevelMaster, StockLevelMasterAdmin)


class StockRegisterAdmin(admin.ModelAdmin):
    list_display = ('warehouse', 'product', 'stock_in', 'stock_out', 'reference',
                    'stock_type', 'remarks', 'created_on', 'created_by', 'modified_on', 'modified_by')


admin.site.register(models.StockRegister, StockRegisterAdmin)


class DealersBrandAdmin(admin.ModelAdmin):
    list_display = ('user', 'brand', 'is_active')


admin.site.register(models.DealersBrand, DealersBrandAdmin)


class DealerBusinessMasterAdmin(admin.ModelAdmin):
    list_display = ('dealer', 'brand', 'min_order_amount', 'max_outstanding_threshold', 'credit_days', 'is_active',
                    'created_on', 'created_by', 'modified_on', 'modified_by')


admin.site.register(models.DealerBusinessMaster, DealerBusinessMasterAdmin)


class DealerSalesTargetAdmin(admin.ModelAdmin):
    list_display = ('dealer', 'brand', 'monthly_target', 'category', 'created_on')


admin.site.register(models.DealerSalesTarget, DealerSalesTargetAdmin)


class AllowanceMasterAdmin(admin.ModelAdmin):
    list_display = ('dealer', 'brand', 'scheme_category_code', 'discount_percent', 'discount_amount')


admin.site.register(models.AllowanceMaster, AllowanceMasterAdmin)


class TieUpMasterAdmin(admin.ModelAdmin):
    list_display = ('dealer', 'brand', 'scheme_category_code', 'discount_percent', 'discount_amount')


admin.site.register(models.TieUpMaster, TieUpMasterAdmin)


# class CashDiscountMasterAdmin(admin.ModelAdmin):
#     list_display = ('dealer', 'brand', 'scheme_category_code', 'discount_percent', 'discount_amount')
#
#
# admin.site.register(models.CashDiscountMaster, CashDiscountMasterAdmin)


class SchemeMasterAdmin(admin.ModelAdmin):
    list_display = ('state', 'city', 'product', 'discount_percent', 'discount_amount', 'scheme_start_date',
                    'scheme_end_date', 'scheme_name', 'scheme_code')


admin.site.register(models.SchemeMaster, SchemeMasterAdmin)


class SchemeProductMasterAdmin(admin.ModelAdmin):
    list_display = ('brand', 'product', 'scheme_category_code', 'is_active')


admin.site.register(models.SchemeProductMaster, SchemeProductMasterAdmin)


class SchemeCategoryMasterAdmin(admin.ModelAdmin):
    list_display = ('scheme_category_code', 'scheme_category_name')


admin.site.register(models.SchemeCategoryMaster, SchemeCategoryMasterAdmin)


class SchemeSellThroughMasterAdmin(admin.ModelAdmin):
    list_display = ('brand','state_code','city_code', 'product', 'discount_percent', 'discount_amount', 'scheme_start_date',
                    'scheme_end_date', 'scheme_name', 'scheme_code')


admin.site.register(models.SchemeSellThroughMaster, SchemeSellThroughMasterAdmin)


class CashDiscountSlabsMasterAdmin(admin.ModelAdmin):
    list_display = ( 'cd1', 'days_1', 'cd2', 'days_2', 'cd3', 'days_3', 'cd4', 'days_4','slab_code', 'is_active' )


admin.site.register(models.CashDiscountSlabsMaster, CashDiscountSlabsMasterAdmin)

#
# class CashDiscountSchemeMasterAdmin(admin.ModelAdmin):
#     list_display = ('slab', 'dealer')
#
#
# admin.site.register(models.CashDiscountSchemeMaster, CashDiscountSchemeMasterAdmin)


class CreditNoteTypeMasterAdmin(admin.ModelAdmin):
    list_display = ('note_type_code', 'note_type_name', 'description', 'is_active')


admin.site.register(models.CreditNoteTypeMaster, CreditNoteTypeMasterAdmin)


class DocumentTypeMasterAdmin(admin.ModelAdmin):
    list_display = ('document_type_id', 'document_name')


admin.site.register(models.DocumentTypeMaster, DocumentTypeMasterAdmin)


class CityStateRegionMasterAdmin(admin.ModelAdmin):
    list_display = ('name', 'parent', 'created_on', 'created_by', 'modified_on',
                    'modified_by')


admin.site.register(models.CityStateRegionMaster, CityStateRegionMasterAdmin)


class TaxSlabMasterAdmin(admin.ModelAdmin):
    list_display = ('gst_rate', 'igst_rate', 'sgst_rate', 'cgst_rate', 'other_tax_rate',
                    'slab_name')


admin.site.register(models.TaxSlabMaster, TaxSlabMasterAdmin)


class StateMasterAdmin(admin.ModelAdmin):
    list_display = ('state_name', 'state_code')


admin.site.register(models.StateMaster, StateMasterAdmin)


class CityMasterAdmin(admin.ModelAdmin):
    list_display = ('state_id','city_name', 'city_code')


admin.site.register(models.CityMaster, CityMasterAdmin)


class CashDiscountSchemeMasterAdmin(admin.ModelAdmin):
    list_display = ('slab','dealer')


admin.site.register(models.CashDiscountSchemeMaster, CashDiscountSchemeMasterAdmin)