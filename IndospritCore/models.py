from django.conf import settings
from django.db import models
from django.utils.translation import ugettext as _


class StateMaster(models.Model):
    state_id = models.BigAutoField(primary_key=True)
    state_name = models.CharField(max_length=30)
    state_code = models.CharField(max_length=15)
    is_active = models.BooleanField(default=True, null=True, blank=True)
    is_deleted = models.BooleanField(default=False, null=True, blank=True)

    def __str__(self):
        return self.state_code


class CityMaster(models.Model):
    city_id = models.BigAutoField(primary_key=True)
    state_id = models.ForeignKey(StateMaster, on_delete=models.SET_NULL, null=True, related_name='city_by_state')
    city_code = models.CharField(max_length=15)
    city_name = models.CharField(max_length=30)
    is_active = models.BooleanField(default=True, null=True, blank=True)
    is_deleted = models.BooleanField(default=False, null=True, blank=True)

    def __str__(self):
        return self.city_code


class WarehouseMaster(models.Model):
    warehouse_code = models.CharField(max_length=60, primary_key=True)
    warehouse_name = models.CharField(null=True, blank=True, max_length=25)
    address_line1 = models.CharField(null=True, blank=True, max_length=250)
    address_line2 = models.CharField(null=True, blank=True, max_length=250)
    # state = models.ForeignKey(StateMaster, on_delete=models.SET_NULL, null=True, related_name='state_by_warehouse_master')
    # city = models.ForeignKey(CityMaster, on_delete=models.SET_NULL, null=True, related_name='city_by_warehouse_master')
    state = models.CharField(null=True, blank=True, max_length=40)
    city = models.CharField(null=True, blank=True, max_length=40)
    pin_code = models.CharField(null=True, blank=True, max_length=8)
    contact_number = models.CharField(max_length=25, null=True, blank=True)
    created_on = models.DateTimeField(auto_now_add=True)
    created_by = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.SET_NULL, null=True,
                                   related_name='warehouse_created_by')
    modified_on = models.DateTimeField(auto_now=True)
    modified_by = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.SET_NULL, null=True,
                                    related_name='warehouse_modified_by')
    is_active = models.BooleanField(default=True, null=True, blank=True)
    is_deleted = models.BooleanField(default=False, null=True, blank=True)

    # def __str__(self):
    #     return self.warehouse_name

    def __str__(self):
        if self.warehouse_code is None:
            return "ERROR-WAREHOUSE CODE IS NULL"
        return self.warehouse_code


class TaxSlabMaster(models.Model):
    tax_slab_id = models.BigAutoField(primary_key=True)
    # brand = models.ForeignKey(BrandMaster, on_delete=models.PROTECT)
    gst_rate = models.DecimalField(default=0, max_digits=4, decimal_places=2)
    igst_rate = models.DecimalField(default=0, max_digits=4, decimal_places=2)
    sgst_rate = models.DecimalField(default=0, max_digits=4, decimal_places=2)
    cgst_rate = models.DecimalField(default=0, max_digits=4, decimal_places=2)
    other_tax_rate = models.DecimalField(default=0, max_digits=4, decimal_places=2)
    slab_name = models.CharField(max_length=150)
    is_active = models.BooleanField(default=True, null=True, blank=True)
    is_deleted = models.BooleanField(default=False, null=True, blank=True)

    def __str__(self):
        return self.slab_name


class BrandMaster(models.Model):
    brand_code = models.CharField(max_length=50, primary_key=True)
    brand_name = models.CharField(max_length=150, null=True, blank=True)
    is_active = models.BooleanField(default=True)
    created_on = models.DateTimeField(auto_now_add=True)
    created_by = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.SET_NULL, null=True,
                                   related_name='created_by_brand')
    modified_on = models.DateTimeField(auto_now=True)
    modified_by = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.SET_NULL, null=True,
                                    related_name='modified_by_brand')
    primary_warehouse = models.ForeignKey(WarehouseMaster, on_delete=models.SET_NULL, null=True,
                                          related_name='primary_warehouse_by_brand')
    is_deleted = models.BooleanField(default=False, null=True, blank=True)


    # def __str__(self):
    #     return self.brand_name

    def __str__(self):
        if self.brand_name is None:
            return "ERROR-BRAND NAME IS NULL"
        return self.brand_name

# CE, AV etc. category masters


class ProductCategoryMaster(models.Model):
    category_id = models.AutoField(primary_key=True)
    brand = models.ForeignKey(BrandMaster, on_delete=models.PROTECT)
    category_name = models.CharField(max_length=150)
    is_active = models.BooleanField(default=True)
    path = models.CharField(max_length=500)
    parent = models.IntegerField(null=True, blank=True)
    created_on = models.DateTimeField(auto_now_add=True)
    created_by = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.SET_NULL, null=True,
                                   related_name='product_category_created_by')
    modified_on = models.DateTimeField(auto_now=True)
    modified_by = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.SET_NULL, null=True,
                                    related_name='product_category_modified_by')
    is_deleted = models.BooleanField(default=False, null=True, blank=True)

    def as_json(self):
        return dict(
            id=self.category_id,
            brand=self.brand.brand_name,
            category_name=self.category_name,
            is_active=self.is_active,
            parent=self.parent)

    def __str__(self):
        return self.category_name

    @property
    def get_parent_obj(self):
        if self.parent is not None:
            obj = ProductCategoryMaster.objects.get(category_id=self.parent)
            return obj
        return None


class WarehouseAdmin(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.SET_NULL, null=True,
                             related_name='warehouse_user')
    warehouse = models.ForeignKey(WarehouseMaster, on_delete=models.SET_NULL, null=True,
                                  related_name='warehouse_by_admin')
    is_active = models.BooleanField(default=True)
    created_on = models.DateTimeField(auto_now_add=True)
    created_by = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.SET_NULL, null=True,
                                   related_name='warehouse_admin_created_by')
    modified_on = models.DateTimeField(auto_now=True)
    modified_by = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.SET_NULL, null=True,
                                    related_name='warehouse_admin_modified_by')
    is_deleted = models.BooleanField(default=False, null=True, blank=True)

    def __str__(self):
        return self.warehouse.warehouse_code

    class Meta:
        verbose_name = _("Warehouse Manager")
        verbose_name_plural = _("Warehouse Managers")


class ProductMaster(models.Model):
    product_id = models.BigAutoField(primary_key=True)
    brand = models.ForeignKey(BrandMaster, on_delete=models.SET_NULL, null=True,
                              related_name='brand_name_from_product_master')
    product_category = models.ForeignKey(ProductCategoryMaster, on_delete=models.SET_NULL, null=True,
                                         related_name='category_name_from_product_master')
    sku_code = models.CharField(max_length=60)
    product_short_name = models.CharField(max_length=60)
    size_capacity = models.CharField(max_length=50, null=True, blank=True)
    size_capacity_unit = models.CharField(max_length=25,null=True, blank=True)
    TaxSlabCode = models.ForeignKey(TaxSlabMaster, on_delete=models.SET_NULL, null=True,
                                    related_name='TaxSlab_Code_from_product_master')
    pattern_color = models.CharField(max_length=150, null=True, blank=True)
    product_description_feature = models.CharField(max_length=1000, null=True, blank=True)
    # warranty_months = models.IntegerField(default=0, null=True, blank=True)
    best_buying_price = models.DecimalField(max_digits=15, decimal_places=2, null= True, blank=True)
    dealer_price = models.DecimalField(null=True, max_digits=15, decimal_places=2)
    max_retail_price = models.DecimalField(max_digits=15, decimal_places=2)
    created_on = models.DateTimeField(auto_now_add=True)
    created_by = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.SET_NULL, null=True,
                                   related_name='product_created_by_from_product_master')
    modified_on = models.DateTimeField(auto_now=True)
    modified_by = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.SET_NULL, null=True,
                                    related_name='product_modified_by_from_product_master')
    is_active = models.BooleanField(default=True, null=True, blank=True)
    is_deleted = models.BooleanField(default=False, null=True, blank=True)

    # def __str__(self):
    #     return self.sku_code

    def __str__(self):
        if self.sku_code is None:
            return "ERROR-SKU-CODE IS NULL"
        return self.sku_code


class ProductPriceHistory(models.Model):
    product_history_id = models.BigAutoField(primary_key=True)
    product = models.ForeignKey(ProductMaster, on_delete=models.SET_NULL, null=True,
                                related_name='product_id_from_product_price')
    gst_rate = models.DecimalField(max_digits=4, blank=True, decimal_places=2)
    sgst_rate = models.DecimalField(blank=True, max_digits=4, decimal_places=2)
    cgst_rate = models.DecimalField(blank=True, max_digits=4, decimal_places=2)
    other_tax_rate = models.DecimalField(blank=True, max_digits=4, decimal_places=2)
    best_buying_price = models.DecimalField(blank=True, max_digits=15, decimal_places=2)
    dealer_price = models.DecimalField(blank=True, max_digits=15, decimal_places=2)
    max_retail_price = models.DecimalField(blank=True, max_digits=15, decimal_places=2)
    created_on = models.DateTimeField(auto_now_add=True)
    created_by = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.SET_NULL, null=True,
                                   related_name='product_history_created_by_product_master')
    modified_on = models.DateTimeField(auto_now=True)
    modified_by = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.SET_NULL, null=True,
                                    related_name='product_history_modified_by_product_master')
    is_active = models.BooleanField(default=True, null=True, blank=True)
    is_deleted = models.BooleanField(default=False, null=True, blank=True)

    def __str__(self):
        return self.product_history_id


class StockLevelMaster(models.Model):
    stock_level_id = models.BigAutoField(primary_key=True)
    warehouse = models.ForeignKey(WarehouseMaster, on_delete=models.SET_NULL, null=True,
                                  related_name='warehouse_name_from_stock_master')
    product = models.ForeignKey(ProductMaster, on_delete=models.SET_NULL, null=True,
                                related_name='product_from_stock_master')
    min_stock_level = models.DecimalField(blank=True, max_digits=15, decimal_places=2)
    is_active = models.BooleanField(default=True)
    is_deleted = models.BooleanField(default=False, null=True, blank=True)
    created_on = models.DateTimeField(auto_now_add=True)
    modified_on = models.DateTimeField(auto_now=True)

    # def __str__(self):
    #     return self.product


class DealersBrand(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.SET_NULL, null=True,
                             related_name='user_modified_from_dealer')
    brand = models.ForeignKey(BrandMaster, null=True, on_delete=models.SET_NULL, related_name='brand_name_from_dealer')
    is_active = models.BooleanField(default=True)
    is_deleted = models.BooleanField(default=False, null=True, blank=True)

    def __str__(self):
        if self.brand:
            return self.brand.brand_name
        else:
            return ''


class CashDiscountSlabsMaster(models.Model):
    cash_discount_slabs_id = models.BigAutoField(primary_key=True)
    # brand = models.ForeignKey(BrandMaster, on_delete=models.PROTECT)
    cd1 = models.DecimalField(default=0, max_digits=4, decimal_places=2)
    days_1 = models.IntegerField(default=0)
    cd2 = models.DecimalField(default=0, max_digits=4, decimal_places=2)
    days_2 = models.IntegerField(default=0)
    cd3 = models.DecimalField(default=0, max_digits=4, decimal_places=2)
    days_3 = models.IntegerField(default=0)
    cd4 = models.DecimalField(default=0, max_digits=4, decimal_places=2)
    days_4 = models.IntegerField(default=0)
    # slab_name = models.CharField(max_length=150)
    slab_code = models.CharField(max_length=50, null=True, blank=True)
    created_on = models.DateTimeField(auto_now_add=True)
    modified_on = models.DateTimeField(auto_now=True)
    is_active = models.BooleanField(default=True)
    is_deleted = models.BooleanField(default=False, null=True, blank=True)

    def __str__(self):
        return self.slab_code


class DealerBusinessMaster(models.Model):
    dealer = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.SET_NULL, null=True,
                               related_name='dealer_business_by_master')
    brand = models.ForeignKey(BrandMaster, null=True, on_delete=models.SET_NULL, related_name='brand_name_from_master')
    min_order_amount = models.DecimalField(blank=True, max_digits=20, decimal_places=2)
    max_outstanding_threshold = models.DecimalField(blank=True, max_digits=20, decimal_places=2)
    credit_days = models.IntegerField(blank=True, null=True)
    is_active = models.BooleanField(default=True)
    created_on = models.DateTimeField(auto_now_add=True)
    created_by = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.SET_NULL, null=True,
                                   related_name='dealer_business_master_created_by_dealer_master')
    modified_on = models.DateTimeField(auto_now=True)
    modified_by = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.SET_NULL, null=True,
                                    related_name='dealer_business_master_modified_by_dealer_master')
    slab = models.ForeignKey(CashDiscountSlabsMaster, on_delete=models.SET_NULL, null=True,
                             related_name='slab_by_DB')
    is_deleted = models.BooleanField(default=False, null=True, blank=True)

    # def __str__(self):
    #     return self.dealer


class DealerSalesTarget(models.Model):
    dealer = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.SET_NULL, null=True,
                               related_name='dealer_sales_modified_by_dealer')
    brand = models.ForeignKey(BrandMaster, null=True, on_delete=models.SET_NULL, related_name='brand_name_by_dealer')
    monthly_target = models.DecimalField(null=True, max_digits=20, decimal_places=2)
    category = models.ForeignKey(ProductCategoryMaster, null=True, on_delete=models.SET_NULL,
                                 related_name='category_name_by_dealer')
    is_active = models.BooleanField(default=True, null=True, blank=True)
    is_deleted = models.BooleanField(default=False, null=True, blank=True)
    created_on = models.DateTimeField(auto_now_add=True)
    modified_on = models.DateTimeField(auto_now=True)

    # def __str__(self):
    #     return self.dealer


class SchemeMaster(models.Model):
    scheme_master_id = models.BigAutoField(primary_key=True)
    # state = models.ForeignKey(StateMaster, on_delete=models.SET_NULL, null=True, related_name='state_by_Scheme_master')
    # city = models.ForeignKey(CityMaster, on_delete=models.SET_NULL, null=True, related_name='city_by_Scheme_master')
    state = models.CharField(null=True, blank=True, max_length=40)
    city = models.CharField(null=True, blank=True, max_length=40)
    product = models.CharField(max_length=10)
    discount_percent = models.DecimalField(blank=True, max_digits=4, decimal_places=2)
    discount_amount = models.IntegerField(default=0, null=True, blank=True)
    scheme_start_date = models.DateTimeField()
    scheme_end_date = models.DateTimeField()
    scheme_name = models.CharField(max_length=250)
    scheme_code = models.CharField(max_length=250)
    is_active = models.BooleanField(default=True, null=True, blank=True)
    is_deleted = models.BooleanField(default=False, null=True, blank=True)

    def __str__(self):
        return self.scheme_name

    def __str__(self):
        return self.scheme_code


class StockRegister(models.Model):
    stock_register_id = models.BigAutoField(primary_key=True)
    warehouse = models.ForeignKey(WarehouseMaster, on_delete=models.SET_NULL, null=True,
                                  related_name='warehouse_from_stock')
    product = models.ForeignKey(ProductMaster, on_delete=models.SET_NULL, null=True,
                                related_name='product_name_from_stock')
    stock_in = models.DecimalField(blank=True, max_digits=15, decimal_places=2)
    stock_out = models.DecimalField(blank=True, max_digits=4, decimal_places=2)
    reference = models.CharField(max_length=100)
    # order_id = models.ForeignKey(null=True)
    stock_type = models.CharField(max_length=100)
    remarks = models.CharField(max_length=500)
    created_on = models.DateTimeField(auto_now_add=True)
    created_by = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.SET_NULL, null=True,
                                   related_name='stock_register_created_from_stock')
    modified_on = models.DateTimeField(auto_now=True)
    modified_by = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.SET_NULL, null=True,
                                    related_name='stock_register_modified_from_stock')
    is_active = models.BooleanField(default=True, null=True, blank=True)
    is_deleted = models.BooleanField(default=False, null=True, blank=True)

    # def __str__(self):
    #     return self.stock_type

# Scheme Category M, B, Val, Vol


class SchemeCategoryMaster(models.Model):
    scheme_category_code = models.CharField(max_length=10, primary_key=True)
    scheme_category_name = models.CharField(max_length=150)
    is_active = models.BooleanField(default=True, null=True, blank=True)
    is_deleted = models.BooleanField(default=False, null=True, blank=True)

    def __str__(self):
        return self.scheme_category_name


class AllowanceMaster(models.Model):
    allowance_master_id = models.BigAutoField(primary_key=True)
    dealer = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.SET_NULL, null=True,
                               related_name='stock_register_modified_by_allowance')
    brand = models.ForeignKey(BrandMaster, on_delete=models.PROTECT)
    scheme_category_code = models.ForeignKey(SchemeCategoryMaster, on_delete=models.SET_NULL, null=True,
                                             related_name='sch_category_by_allowance')
    discount_percent = models.DecimalField(blank=True, max_digits=4, decimal_places=2, default=0, null=True)
    discount_amount = models.IntegerField(blank=True, default=0, null=True)
    is_active = models.BooleanField(default=True, null=True, blank=True)
    is_deleted = models.BooleanField(default=False, null=True, blank=True)
    created_on = models.DateTimeField(auto_now_add=True)
    modified_on = models.DateTimeField(auto_now=True)

    def __str__(self):
        return str(self.dealer.user_code)


class TieUpMaster(models.Model):
    tie_up_master_id = models.BigAutoField(primary_key=True)
    dealer = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.SET_NULL, null=True,
                               related_name='stock_register_modified_by_tie_up')
    brand = models.ForeignKey(BrandMaster, on_delete=models.PROTECT)
    scheme_category_code = models.ForeignKey(SchemeCategoryMaster, on_delete=models.SET_NULL, null=True,
                                             related_name='sch_category_by_tie_up')
    discount_percent = models.DecimalField(blank=True, max_digits=10, decimal_places=2, default=0, null=True)
    discount_amount = models.IntegerField(default=0, null=True, blank=True)
    is_active = models.BooleanField(default=True, null=True, blank=True)
    is_deleted = models.BooleanField(default=False, null=True, blank=True)
    created_on = models.DateTimeField(auto_now_add=True)
    modified_on = models.DateTimeField(auto_now=True)

    def __str__(self):
        if self.brand:
            return self.brand.brand_name
        else:
            return ''


class SchemeProductMaster(models.Model):
    scheme_product_master_id = models.AutoField(primary_key=True)
    brand = models.ForeignKey(BrandMaster, on_delete=models.SET_NULL, null=True)
    product = models.ForeignKey(ProductMaster, on_delete=models.SET_NULL, null=True,
                                related_name='product_short_name_by_scheme')
    scheme_category_code = models.CharField(max_length=30, default=None)
    is_active = models.BooleanField(default=True, null=True, blank=True)
    is_deleted = models.BooleanField(default=False, null=True, blank=True)

    class Meta:
        unique_together = ('brand', 'product', 'scheme_category_code')

    def __str__(self):
        return self.scheme_category_code


class CashDiscountSchemeMaster(models.Model):
    slab = models.ForeignKey(CashDiscountSlabsMaster, on_delete=models.SET_NULL, null=True,
                             related_name='slab_by_master')
    dealer = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.SET_NULL, null=True,
                               related_name='dealer_by_cash_discount')
    is_active = models.BooleanField(default=True, null=True, blank=True)
    is_deleted = models.BooleanField(default=False, null=True, blank=True)

    # def __str__(self):
    #     return self.slab


class CreditNoteTypeMaster(models.Model):
    note_type_code = models.CharField(max_length=10)
    note_type_name = models.CharField(max_length=60)
    description = models.CharField(max_length=1000)
    is_active = models.BooleanField(default=True)
    is_deleted = models.BooleanField(default=False, null=True, blank=True)

    def __str__(self):
        return self.note_type_name


class DocumentTypeMaster(models.Model):
    document_type_id = models.BigAutoField(primary_key=True)
    document_name = models.CharField(max_length=50)
    is_active = models.BooleanField(default=True, null=True, blank=True)
    is_deleted = models.BooleanField(default=False, null=True, blank=True)

    def __str__(self):
        return self.document_name


class CityStateRegionMaster(models.Model):
    region_id = models.BigAutoField(primary_key=True)
    name = models.CharField(max_length=150, null=True, blank=True)
    parent = models.IntegerField(null=True, blank=True)
    created_on = models.DateTimeField(auto_now_add=True)
    created_by = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.SET_NULL, null=True,
                                   related_name='region_created_by')
    modified_on = models.DateTimeField(auto_now=True)
    modified_by = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.SET_NULL, null=True,
                                    related_name='region_modified_by')
    is_active = models.BooleanField(default=True, null=True, blank=True)
    is_deleted = models.BooleanField(default=False, null=True, blank=True)

    def __str__(self):
        return self.name

    @property
    def get_parent_obj(self):
        if self.parent is not None:
            obj = CityStateRegionMaster.objects.get(region_id=self.parent)
            return obj
        return None


product = models.ForeignKey(ProductMaster, on_delete=models.SET_NULL, null=True,
                            related_name='product_id_from_product_master')

# class InventoryMaster(models.Model):
#     warehouse_code = models.ForeignKey(WarehouseMaster, on_delete=models.SET_NULL, null=True,
#  related_name='w_code_from_inv')
#     city =
#     brand = models.ForeignKey(BrandMaster, on_delete=models.SET_NULL, null=True, related_name='brand_from_inv')
#     sku_code = models.ForeignKey(ProductMaster, on_delete=models.SET_NULL, null=True, related_name='sku_from_inv')
#     saleable_stock =
#     non_saleable_stock =
#     min_stock_level =


class SchemeSellThroughMaster(models.Model):
    master_id = models.BigAutoField(primary_key=True)
    # dealer = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.SET_NULL, null=True,
    #                            related_name='dealer_by_sell_through')
    brand = models.ForeignKey(BrandMaster, on_delete=models.PROTECT)
    product = models.ForeignKey(ProductMaster, on_delete=models.SET_NULL, null=True,
                                related_name='product_id_from_product_master')
    discount_percent = models.DecimalField(blank=True, max_digits=4, decimal_places=2)
    discount_amount = models.IntegerField(default=0, null=True, blank=True)
    scheme_start_date = models.DateTimeField()
    scheme_end_date = models.DateTimeField()
    scheme_name = models.CharField(max_length=250, null=True)
    scheme_code = models.CharField(max_length=100, null=True)
    state_code = models.ForeignKey(StateMaster, on_delete=models.SET_NULL, null=True, related_name='state_by_sell_thru')
    city_code = models.ForeignKey(CityMaster, on_delete=models.SET_NULL, null=True, related_name='city_by_sell_thru')
    is_active = models.BooleanField(default=True, null=True, blank=True)
    is_deleted = models.BooleanField(default=False, null=True, blank=True)
    created_on = models.DateTimeField(auto_now_add=True)
    modified_on = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.scheme_code
