
$(document).ready(function() {

//    $("#myForm").submit(function(event) {
//
//        var one = $("#ddlSelect_1").val();
//        var two = $("#ddlSelect_2").val()
//        var three = $("#ddlSelect_3").val()
//        if(one != undefined && one != null && one != "")
//            $.session.set('first', one)
//        else {
//            $.session.remove('first');
//            $.session.remove('second');
//            $.session.remove('third');
//        }
//        if(two != undefined && two != null && two != "")
//            $.session.set('second', two)
//        else {
//            $.session.remove('second');
//            $.session.remove('third');
//        }
//        if(three != undefined && three != null && three != "")
//            $.session.set('third', three)
//        else {
//            $.session.remove('third');
//        }
//    });

//    if($.session.get('third') != undefined) {
//        // one, two, three
//        var first = $.session.get('first');
//        var second = $.session.get('second');
//        var third = $.session.get('third');
//
//        $('#ddlSelect_1 option[value="' + first + '"]').attr("selected", true);
//        $('#ddlSelect_2 option[value="' + second + '"]').attr("selected", true);
//        $('#ddlSelect_3 option[value="' + third + '"]').attr("selected", true);
//
//        $("#btn-product-search").triggerevent(function(){
//            $("#btn-product-search").click();
//        });
//    }
//    else if($.session.get('second') != undefined) {
//        // one, two
//        var first = $.session.get('first');
//        var second = $.session.get('second');
//
//        $('#ddlSelect_1 option[value="' + first + '"]').attr("selected", true);
//        $('#ddlSelect_2 option[value="' + second + '"]').attr("selected", true);
//
//        $("#btn-product-search").triggerevent(function(){
//            $("#btn-product-search").click();
//        });
//    }
//    else if($.session.get('first') != undefined) {
//        // one
//        var first = $.session.get('first');
//        $("#ddlSelect_1 select").val(first);
//        $('#ddlSelect_1 option[value="' + first + '"]').attr("selected", true);
//
//        $("#btn-product-search").triggerevent(function(){
//            $("#btn-product-search").click();
//        });
//    }

    $('[id^="ddlSelect_"]').change(function() {

        var index = this.id.split('_')[1];
        var value = $(this).val();

        if(index == '2' && value == '') {
            index = '1';
            value = $('#ddlSelect_1').val();
        }
        else if(index == '3' && value == '') {
            index = '2';
            value = $('#ddlSelect_2').val();
        }
        else if(index == '3' && value != '') {
            index = '3'
        }
        else if(index == '2' && value != '') {
            index = '2'
        }
        else if(index == '1' && value != '') {
            index = '1'
        }
        else {
            index = '1'
            value = 0
        }

        $.ajax({
            type:'get',
            url:'/user/product_category?index='+index+'&value='+value,
            success: function(res){
                if(index == '1'){
                    var optionlist1 = "<option value=''>All</option>";
                    var optionlist2 = "<option value=''>All</option>";
                    $.each(res['list1'], function (index, d) {
                        optionlist1 += "<option value='" + d.name + "'>" + d.name + "</option>";
                    });
                    $('#ddlSelect_2').html(optionlist1);

                    $.each(res['list2'], function (index, d) {
                        optionlist2 += "<option value='" + d.name + "'>" + d.name + "</option>";
                    });
                    $('#ddlSelect_3').html(optionlist2);
                }
                else if (index == '2'){
                    var optionlist = "<option value=''>All</option>";
                    $.each(res['list1'], function (index, d) {
                        optionlist += "<option value='" + d.name + "'>" + d.name + "</option>";
                    });
                    $('#ddlSelect_3').html(optionlist);
                }
                else{
                    $.each(res['list1'], function (index, d) {
                        $('#ddlSelect_2 option[value="' + d.name + '"]').attr("selected", true);
                    });
                }
            }
        });
    });
});

function minus(num) {
    var data=$('#ordervalue'+num.toString()).text();
    var minusdata=parseInt(data)-1;
    if (minusdata<=1){
        minusdata=1;
    }
    $('#ordervalue'+num.toString()).text(minusdata);
    $('#hdnorder'+num.toString()).val(minusdata);
}

function plus(num) {
    var data=$('#ordervalue'+num.toString()).text();
    var minusdata=parseInt(data)+1;
    if (minusdata<=1){
        minusdata=1;
    }
    $('#ordervalue'+num.toString()).text(minusdata);
    $('#hdnorder'+num.toString()).val(minusdata);
}
