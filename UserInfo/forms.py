from django import forms
from django.core.exceptions import ValidationError
from django.core.validators import RegexValidator
import string
from UserInfo import models
from IndospiritTransactions.models import Payment, SpecialCDType, DebitNote
from django.contrib.auth import get_user_model
User = get_user_model()


class CreateSalesRep(forms.ModelForm):
    # user_code = forms.CharField(max_length=20, label='SR Code')
    user_code = forms.CharField(help_text='ex: Should be alphanumeric', validators=[RegexValidator('^(\w+\d+|\d+\w+)+$',
                                message="Sr Code should be a combination of Alphabets and Numbers")], label='SR ID')
    first_name = forms.CharField(max_length=50)
    last_name = forms.CharField(max_length=50)
    mobile = forms.IntegerField(help_text='ex: 5555555555', widget=forms.TextInput
                                (attrs={'max_length': 13, 'required': True, }), label='Primary Contact')
    phone = forms.IntegerField(required=False, help_text='ex: 5555555555', widget=forms.TextInput
                               (attrs={'max_length': 13}), label='Alternate contact')

    class Meta:
        model = User
        fields = ('email', 'user_code', 'first_name', 'last_name', 'mobile', 'phone', )

    def clean_user_code(self):
        user_code = self.cleaned_data.get('user_code', None)
        if user_code is not None:
            User = get_user_model()
            try:
                User.objects.get(user_code=user_code)
                raise ValidationError("SR Code should be unique")

            except User.DoesNotExist:
                pass
        else:
            raise ValidationError('This field is required')

    def validate(user_code):
        letters = set(string.ascii_letters)
        digits = set(string.digits)
        pwd = set(user_code)
        return not (pwd.isdisjoint(letters) or pwd.isdisjoint(digits))


class DealerProfileForm(forms.ModelForm):
    state_code = forms.ModelChoiceField(queryset=models.StateMaster.objects.all())
    city_code = forms.ModelChoiceField(queryset=models.CityMaster.objects.all())
    email = forms.EmailField(max_length=100)
    pincode = forms.IntegerField(min_value=0)
    primary_contact = forms.IntegerField(min_value=0)
    # alternate_contact = forms.IntegerField(min_value=0)
    # values_list('user_code', flat=True).
    sr_code = forms.ModelChoiceField(queryset=User.objects.filter(
        groups__name='SR', is_active=True).order_by('-pk'))
    cd_slab_code = forms.ModelChoiceField(queryset=models.CashDiscountSlabsMaster.objects.all())

    # def clean_city_code(self):
    #     if self.cleaned_data.get("city_code", None) is not None:
    #         return self.cleaned_data["city_code"].pk
    #     else:
    #         raise ValidationError("This field is required!")

    class Meta:
        model = models.DealerProfile
        fields = ('dealer_code', 'brand', 'company_address', 'owner_name', 'region_id', 'shipping_address', 'pincode',
                  'company_name', 'email', 'primary_contact', 'alternate_contact', 'gst_no', 'pan_no', 'Aadhaar_No',
                  'cd_slab_code', 'state_code', 'city_code', 'sr_code')
                  
    def clean_email(self):
        if self.instance.pk is None:
            email = self.cleaned_data.get('email', None)
            if email is not None:
                User = get_user_model()
                try:
                    User.objects.get(email=email)
                    raise ValidationError("Email already exists")
                except User.DoesNotExist:
                    return email
            else:
                raise ValidationError('This field is required')
        else:
            email = self.cleaned_data.get('email', None)
            return email

    def __init__(self, *args, **kwargs):
        super(DealerProfileForm, self).__init__(*args, **kwargs)
        self.fields['shipping_address'].label = "Shipping address*"
    
    # class DealerAddressForm(forms.ModelForm):
    #     class Meta:
    #         model = models.DealerAddress
    #
    #         fields = ('dealer', 'address_type')
    #


class SRDealerForm(forms.ModelForm):
    class Meta:
        model = models.SRDealer
        fields = ('sr_id', 'dealer_id')


class DealerDocumentsForm(forms.ModelForm):
    class Meta:
        model = models.UserDocuments
        fields = ('user', 'document_type', 'document_number', 'document')


class PaymentForm(forms.ModelForm):
    class Meta:
        model = Payment
        fields = ('dealer', 'payment_type', 'reference', 'amount', 'payment_date')


class SpecialTypeForm(forms.ModelForm):
    class Meta:
        model = SpecialCDType
        fields = ('dealer', 'discount_type', 'remarks', 'amount')


class DebitNoteForm(forms.ModelForm):
    class Meta:
        model = DebitNote
        fields = ('dealer', 'type', 'reference', 'amount')
