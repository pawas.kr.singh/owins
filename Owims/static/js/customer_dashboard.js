// $(document).ready(function(){
//     $(".minus").click(function(){
//         var data=$('#ordervalue').text();
//         var minusdata=parseInt(data)-1;
//         if (minusdata<=0){
//             minusdata=0;
//         }
//         $('#ordervalue').text(minusdata);
//     });
//     $(".plus").click(function(){
//         var data=$('#ordervalue').text();
//         var minusdata=parseInt(data)+1;
//         if (minusdata<=0){
//             minusdata=0;
//         }
//         $('#ordervalue').text(minusdata); 
//     });
// });
function minus(num){
    var data=$('#ordervalue'+num.toString()).text();
    var minusdata=parseInt(data)-1;
    if (minusdata<=0){
        minusdata=0;
    }
    $('#ordervalue'+num.toString()).text(minusdata);
    $('#hdnorder'+num.toString()).val(minusdata);
}

function plus(num){
    var data=$('#ordervalue'+num.toString()).text();
    var minusdata=parseInt(data)+1;
    if (minusdata<=0){
        minusdata=0;
    }
    $('#ordervalue'+num.toString()).text(minusdata);
    $('#hdnorder'+num.toString()).val(minusdata);
}

$(document).ready(function(){
    $('.order-view').on('click', function () {
       var data=$(this).val();
       data=data.split('_')
       data=$('#hdnorder'+data[0].toString()).val();
        $.toast({
            heading: data+'New Order',
            text: 'order has been added to your cart',
            icon: 'info',
            loader: true,        // Change it to false to disable loader
            loaderBg: '#9EC600',  // To change the background
            position: 'top-right',
            stack: false
        })   
});
$('.deleteord').on('click', function () {
    var addressValue = $(this).attr("href");
    data=addressValue.split('&&')[1].split('=')[1]
     $.toast({
         heading: data+'Delete Order',
         text: 'order has been deleted to your cart',
         icon: 'warning',
         loader: true,        // Change it to false to disable loader
         loaderBg: '#9EC600',  // To change the background
         position: 'top-right',
         stack: false
     })   
});
})
    
   
