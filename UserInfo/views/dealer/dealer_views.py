import csv
import datetime
import json
import os
from _testcapi import exception_print
from calendar import month
from datetime import timezone
from itertools import count
from urllib.parse import urljoin, urlparse
from urllib.request import urlretrieve
from django.core.mail import EmailMultiAlternatives
from ClassicUserAccounts.models import User
from IndospiritTransactions.models import Payment, CreditNotes, Ledger, \
    OrderAllowanceDetails, OrderDetails, OrderDispatchDetail, \
    OrderDispatchHead, OrderEventHistory, OrderHead, \
    OrderSchemeDetailsSellThrough, OrderTieUpDetails, SpacialDiscount, SpecialCDType, DebitNote
from IndospritCore.models import AllowanceMaster, BrandMaster, \
    CashDiscountSchemeMaster, CashDiscountSlabsMaster, CityMaster, \
    CreditNoteTypeMaster, DealerBusinessMaster, DealerSalesTarget, \
    DealersBrand, DocumentTypeMaster, ProductCategoryMaster, ProductMaster, \
    SchemeCategoryMaster, SchemeProductMaster, SchemeSellThroughMaster, \
    StateMaster, StockLevelMaster, StockRegister, TaxSlabMaster, TieUpMaster, \
    WarehouseAdmin, WarehouseMaster
from Owims.settings import import_settings
from UserInfo import forms, models, status_in
from UserInfo.models import Action, Alerts, DealerProfile, Notification_role, \
    OrderNotes, SRDealer, UserDocuments, EmailTemplateMaster
from UserInfo.tasks import send_email_or_sms
from django.conf import settings
from django.contrib import messages
from django.contrib.auth import get_user_model
from django.contrib.auth.models import Group
from django.core.files.storage import FileSystemStorage
from django.core.paginator import EmptyPage, PageNotAnInteger, Paginator
from django.db.models import Count, Max, Q, Sum
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
from django.shortcuts import get_object_or_404, redirect, render
from odf import style
from odf.number import Year
#-------------Shivendra--------------
from UserInfo.tokens import account_activation_token
from django.contrib.sites.shortcuts import get_current_site
from django.template.loader import render_to_string
from django.utils.http import urlsafe_base64_decode, urlsafe_base64_encode
from django.utils.encoding import force_bytes, force_text 
from django.contrib.auth import logout
#-------------------------------------------------------------
from django.template.loader import get_template
import pdfkit
import requests
from django.utils.dateparse import parse_date
from itertools import chain
from UserInfo.utills import *
from UserInfo.ledger import *
# today = datetime.date.today()
# yesterday = today - datetime.timedelta(days=1)
# tomorrow = today + datetime.timedelta(days=1)


User = get_user_model()


def load_city(request,cid):
    if request.is_ajax():
        street = CityMaster.objects.filter(state_id_id=int(cid),is_active=True,is_deleted=False)
        streetlist = []
        for getstreet in street:
            street = {}
            street['cityid'] = getstreet.city_id
            street['cityname'] = getstreet.city_name
            street['citycode'] = str(getstreet.city_code)
            streetlist.append(street)
        data = json.dumps(streetlist)
    else:
        street = CityMaster.objects.filter(state_id_id=int(cid),is_active=True,is_deleted=False)
        streetlist = []
        for getstreet in street:
            street = {}
            street['cityid'] = getstreet.city_id
            street['cityname'] = getstreet.city_name
            street['citycode'] = str(getstreet.city_code)
            streetlist.append(street)
        data = json.dumps(streetlist)
    mimetype = 'application/json'
    return HttpResponse(data, mimetype)


def create_sr(request):
    form_class = forms.CreateSalesRep
    if request.method == 'GET':
        return render(request, 'sr/create_sr.html', {'form': form_class})
    elif request.method == 'POST':
        form_class = forms.CreateSalesRep(request.POST)
        if form_class.is_valid():
            user = User()
            user.email = request.POST["email"]
            user.set_password(request.POST['mobile'])
            user.first_name = request.POST["first_name"]
            user.last_name = request.POST["last_name"]
            user.mobile = request.POST["mobile"]
            user.phone = request.POST["phone"]
            user.user_code = request.POST['user_code']
            user.save()

            my_group = Group.objects.filter(name='SR').first()
            if my_group:
                my_group.user_set.add(user)
            else:
                Group.objects.get_or_create(name='SR')
                my_group = Group.objects.filter(name='SR').first()
                my_group.user_set.add(user)

            messages.add_message(request, messages.SUCCESS, 'SR created successfully...')
            return redirect('/user/all/?list=SR')
        else:
            return render(request, 'sr/create_sr.html', {'form': form_class})
        return redirect('/user/sr/list')


def sr_list(request):
    searchValue=''
    querycount=''
    page_index = request.GET.get("page", 1)
    sr_user = User.objects.filter(groups__name='SR', is_active=True).order_by('-pk')
    srCount = User.objects.filter(groups__name='SR', is_active=True).count()
    if request.method == "POST":
        if "search" in request.POST:
            searchValue = request.POST.get('search', None)
            sr_user = User.objects.filter(Q(user_code=searchValue)|Q(user_code__endswith=searchValue)|Q(first_name=searchValue)|Q(mobile=searchValue)|Q(email=searchValue),groups__name='SR', is_active=True).order_by('-pk')
            querycount=str(sr_user.count())+' Record found.'
        if "assign_sr" in request.POST:
            srid=request.POST.get('sr')
            try:
                usercode=User.objects.get(user_code=request.POST['user_code'], is_active=True)
            except Exception:
                usercode = ''
            if usercode=='':
                try:
                    useremail=User.objects.get(email=request.POST["email"], is_active=True)
                except Exception:
                    try:
                        useremail=User.objects.get(email=request.POST["email"], is_active=False)
                        useremail.delete()
                        useremail = ''
                    except Exception:
                        useremail = '' 
                    if useremail=='':
                        user=User()
                        user.email = request.POST["email"]
                        user.set_password(request.POST['mobile'])
                        user.first_name = request.POST["first_name"]
                        user.last_name = request.POST["last_name"]
                        user.mobile = request.POST["mobile"]
                        user.phone = request.POST["phone"]
                        user.user_code = request.POST['user_code']
                        user.save()
                        my_group = Group.objects.filter(name='SR').first()
                        if my_group:
                            my_group.user_set.add(user)
                        else:
                            Group.objects.get_or_create(name='SR')
                            my_group = Group.objects.filter(name='SR').first()
                            my_group.user_set.add(user)
                        sr_dealer=SRDealer.objects.filter(sr_id=srid)
                        for sr in sr_dealer:
                            sr.sr_id=User.objects.get(user_code=user.user_code, is_active=True)
                            sr.save()
                        sr_orderhead=OrderHead.objects.filter(sr_id=srid)
                        for sr in sr_orderhead:
                            sr.sr_id=User.objects.get(user_code=user.user_code, is_active=True)
                            sr.save()
                        try:
                            user=User.objects.get(pk=int(srid), is_active=True)
                            user.delete()
                        except Exception:
                            user = ''
                        messages.add_message(request, messages.SUCCESS, 'New SR assigned successfully...')
                        return redirect('/user/sr/list')
                    else:
                        messages.add_message(request, messages.ERROR, 'SR e-mail already exist!!!')
                        return redirect('/user/sr/list')


            else:
                messages.add_message(request, messages.ERROR, 'SR Code already exist!!!')
                return redirect('/user/sr/list')
        if 'export' in request.POST:
            if len(sr_user)>0:
                result=[]
                for list_sr in sr_user:
                    sr_lt={}
                    sr_lt['SR ID']=list_sr.user_code
                    sr_lt['First Name']=list_sr.first_name
                    sr_lt['Last Name']=list_sr.last_name
                    sr_lt['Email']=list_sr.email
                    sr_lt['Mobile']=list_sr.mobile
                    result.append(sr_lt)
                response = HttpResponse(content_type='text/csv')
                response['Content-Disposition'] = 'attachment;filename=SR_list.csv'
                keys = ['SR ID','First Name','Last Name','Email','Mobile']
                writer = csv.DictWriter(response,keys)                                
                writer.writeheader()
                writer.writerows(result)
                return response
    paginator = Paginator(sr_user, settings.PAGE_SIZE)
    try:
        data_list = paginator.page(page_index)
    except PageNotAnInteger:
        data_list = paginator.page(1)
    except EmptyPage:
        data_list = paginator.page(paginator.num_pages)
        

    context = {'data_list': data_list,'srCount':srCount,'search':searchValue,'querycount':querycount}
    return render(request, 'sr/sr_list.html', context)


def sr_delete(request, uid):
    sr = User.objects.get(id=uid)
    sr.is_active = False
    sr.save()
    messages.add_message(request, messages.SUCCESS, 'SR deleted successfully...')
    

def dealer_active(request, Did=None):
    try:
        CUA = User.objects.get(is_active=True, pk=Did)
        CUA.is_active = False
        dp = DealerProfile.objects.get(dealer=CUA)
        dp.is_active = False
    except Exception:
        CUA = User.objects.get(is_active=False, pk=Did)
        CUA.is_active = True
        dp = DealerProfile.objects.get(dealer=CUA)
        dp.is_active = True
    CUA.save()
    dp.save()
    Did = str(Did)
    return redirect('/user/dealer_profile/'+Did)


def dealer_profile(request, Did=None):
    month = datetime.datetime.now().month
    msg = ''
    dealer_profile = None
    price = 0
    sr = ''
    upload_file = ''
    ad = ''
    try:
        CUA = User.objects.get(is_active=True, pk=Did)
    except User.DoesNotExist:
        CUA = ''
    if CUA != '':
        if not CUA.avatar:
            CUA = ''
        else:
            CUA = CUA.avatar
    else:
        CUA = ''

    dealerlist = models.DealerProfile.objects.filter(
        is_active=True, is_deleted=False).values_list(
        'dealer').order_by('-created_on')
    dealerlist = [li[0] for li in dealerlist]

    try:
        fobj = dealerlist.index(int(Did))
        fobj += 1
    except Exception:
        fobj = ''

    totaldealercount = models.DealerProfile.objects.filter(is_active=True, is_deleted=False).count()
    if Did:
        try:
            dealer_profile = models.DealerProfile.objects.get(dealer_id=Did)
        except DealerProfile.DoesNotExist:
            dealer_profile = ''
    else:
        try:
            dealer_profile = models.DealerProfile.objects.get(dealer_id=request.user)
        except DealerProfile.DoesNotExist:
            dealer_profile = ''
    target = DealerSalesTarget.objects.filter(
        created_on__month=month, created_on__year=datetime.datetime.now().year,
        dealer_id=Did, is_active=True, is_deleted=False)
    # .............................
    if len(target) == 0:
        try:
            prevTarget = DealerSalesTarget.objects.filter(
            dealer_id=Did, is_active=True, is_deleted=False).latest('pk')
            prevTarget.pk = None
            prevTarget.created_on = datetime.datetime.now()
            prevTarget.modified_on = datetime.datetime.now()
            newTarget = prevTarget
            newTarget.save()
            target = DealerSalesTarget.objects.filter(
                created_on__month=month, created_on__year=datetime.datetime.now().year,
                dealer_id=Did, is_active=True, is_deleted=False)
        except:
            pass


    # .............................
    get_target = []
    for DST in target:
        category = None
        order = 0
        DST_get = {'category': DST.category.category_name}
        monthly_target = DST.monthly_target
        placed_orders = OrderHead.objects.filter(
            status__in=['OrderPlaced', 'PaymentDone', 'InWarehouse', 'Dispatched'],
            dealer_id=DST.dealer, is_active=True, is_deleted=False)

        category = ProductCategoryMaster.objects.filter(pk=DST.category_id, is_active=True, is_deleted=False)
        get_product = []
        # for PC in category:
        #     p_id = PC.pk
        #     productCategories = ProductCategoryMaster.objects.filter(
        #         parent=p_id, is_active=True, is_deleted=False)
        #     for parentCategory in productCategories:
        #         get_product.append(parentCategory.pk)
        #         productCategories = ProductCategoryMaster.objects.filter(
        #             parent=parentCategory.pk, is_active=True, is_deleted=False)
        #         for subCategory in productCategories:
        #             get_product.append(subCategory.pk)
        #             productCategories = ProductCategoryMaster.objects.filter(
        #                 parent=subCategory.pk, is_active=True, is_deleted=False)
        #             for subCategoryOne in productCategories:
        #                 get_product.append(subCategoryOne.pk)
        #     get_product.reverse()
        for PC in category:
            p_id = PC.pk
            product_category = ProductCategoryMaster.objects.filter(parent=p_id, is_active=True, is_deleted=False)
            for product_category in product_category:
                get_product.append(product_category.pk)
                product_category = ProductCategoryMaster.objects.filter(
                    parent=product_category.pk, is_active=True, is_deleted=False)
                for product_category in product_category:
                    get_product.append(product_category.pk)
                    product_category = ProductCategoryMaster.objects.filter(
                        parent=product_category.pk, is_active=True, is_deleted=False)
                    for product_category in product_category:
                        get_product.append(product_category.pk)
                        product_category = ProductCategoryMaster.objects.filter(
                            parent=product_category.pk, is_active=True, is_deleted=False)
                        for product_category in product_category:
                            get_product.append(product_category.pk)
        get_product.reverse()
        if len(placed_orders) > 0:
            for i in get_product:
                for placed_order in placed_orders:
                    order_detail = OrderDetails.objects.filter(
                        order_id_id=placed_order.pk, created_on__month=DST.created_on.month,
                        created_on__year=DST.created_on.year, product_id__product_category_id=i)
                    for order_detail in order_detail:
                        if order_detail.product_id:
                            category = ProductCategoryMaster.objects.filter(
                                is_active=True, is_deleted=False).values_list('category_name')
                            category = [li[0] for li in category]
                            ctaxdata = order_detail.cgst_rate
                            staxdata = order_detail.sgst_rate
                            total_tax_percent = ctaxdata + staxdata
                            c_percent = 100 * (float(ctaxdata)/float(total_tax_percent))
                            s_percent = 100 * (float(staxdata)/float(total_tax_percent))
                            prg = int(order_detail.total_amount) / (1 + (float(ctaxdata) / 100) + (float(staxdata) / 100))
                            total_tax = int(order_detail.total_amount) - prg
                            cgstobj = (c_percent / 100) * total_tax
                            cgst = float("{:.2f}".format(cgstobj))
                            sgstobj = (s_percent / 100) * total_tax
                            sgst = float("{:.2f}".format(sgstobj))
                            totalbill = total_tax+cgst+sgst
                            dealeramt = int(order_detail.total_amount)-int(totalbill)
                            # order += dealeramt
                            order += prg
        if category is not None and DST.category.category_name in category:
            complete_target = order
        else:
            complete_target = 0
        DST_get['monthly_target'] = monthly_target
        if order != 0:
            DST_get['complete_target'] = complete_target
        else:
            DST_get['complete_target'] = 0
        if DST_get['complete_target'] > 0:
            Remain = int(DST_get['monthly_target'])-int(DST_get['complete_target'])
            DST_get['percent'] = str(Remain)
        else:
            DST_get['percent'] = str(int((int(DST_get['complete_target'])*100)/int(DST_get['monthly_target'])))
        if '-' in DST_get['percent']:
                DST_get['percent'] = '100'
        get_target.append(DST_get)
    pay = OrderHead.objects.filter(
        status__in=['OrderPlaced', 'InWarehouse', 'Dispatched'],
        dealer_id=Did).aggregate(Sum('payable_amount'))
    objdst = DealerSalesTarget.objects.filter(dealer_id=Did).aggregate(Sum('monthly_target'))

    total_outstanding_amount = OrderHead.objects.filter(
        dealer_id=Did, status__in=status_in.OutstandingStatus(),
        is_active=True, is_deleted=False).aggregate(Sum('payable_amount'))

    if total_outstanding_amount['payable_amount__sum'] is None:
        total_outstanding_amount['payable_amount__sum'] = 0.00
    if pay['payable_amount__sum'] is None:
        pay['payable_amount__sum'] = 0
    if objdst['monthly_target__sum'] is None:
        objdst['monthly_target__sum'] = 0
    # outstanding = str(int(objdst['monthly_target__sum']) - int(pay['payable_amount__sum']))
    outstanding = str(total_outstanding_amount['payable_amount__sum'])
    if '-' in outstanding:
        outstanding = 0
    if dealer_profile != '':
        if dealer_profile.Aadhaar_No!='' and dealer_profile.Aadhaar_No!=None:
            ad='Aadhaar Card'
            DealerDoc=UserDocuments.objects.filter(user_id=Did,document=ad)
            if not DealerDoc:
                try:
                    DTM=DocumentTypeMaster.objects.get(document_name=ad)
                except Exception:
                    DTM=DocumentTypeMaster()
                DTM.document_name=ad
                DTM.save()
                doc=UserDocuments()
                doc.user=User.objects.get(pk=Did)
                doc.document_type=DocumentTypeMaster.objects.get(document_type_id=DTM.pk)
                doc.document_number=dealer_profile.Aadhaar_No
                doc.document=ad
                doc.document_file='Document not uploaded'
                doc.save()
        if dealer_profile.gst_no!='' and dealer_profile.gst_no!=None:
            ad='GST'
            DealerDoc=UserDocuments.objects.filter(user_id=Did,document=ad)
            if not DealerDoc:
                try:
                    DTM=DocumentTypeMaster.objects.get(document_name=ad)
                except Exception:
                    DTM=DocumentTypeMaster()
                DTM.document_name=ad
                DTM.save()
                doc=UserDocuments()
                doc.user=User.objects.get( pk=Did)
                doc.document_type=DocumentTypeMaster.objects.get(document_type_id=DTM.pk)
                doc.document_number=dealer_profile.gst_no
                doc.document=ad
                doc.document_file='Document not uploaded'
                doc.save()
        if dealer_profile.pan_no!='' and dealer_profile.pan_no!=None:
            ad='Pan Card'
            DealerDoc=UserDocuments.objects.filter(user_id=Did,document=ad)
            if not DealerDoc:
                try:
                    DTM=DocumentTypeMaster.objects.get(document_name=ad)
                except Exception:
                    DTM=DocumentTypeMaster()
                DTM.document_name=ad
                DTM.save()
                doc=UserDocuments()
                doc.user=User.objects.get( pk=Did)
                doc.document_type=DocumentTypeMaster.objects.get(document_type_id=DTM.pk)
                doc.document_number=dealer_profile.pan_no
                doc.document=ad
                doc.document_file='Document not uploaded'
                doc.save()
    totalOrderActive = OrderHead.objects.filter(
        dealer_id=Did, status__in=['Processing', 'SchemeApplied', 'OrderPlaced']).count()
    OrderActive = OrderHead.objects.filter(
        dealer_id=Did, status__in=status_in.OutstandingStatus())
    for rs in OrderActive.filter(status__in=status_in.OutstandingStatus()):
        price += rs.payable_amount
    price = float(price)
    if price < 0:
        price = 0.0
    # OrderHistory=OrderHead.objects.filter(Q(status='Delivered') | Q(status='Dispatched'),dealer_id=Did)
    OrderHistory = OrderHead.objects.filter(
        dealer_id=Did, status__in=[
             'OnHold', 'Deleted', 'PaymentDone'])
    DBM = ''
    objDealerSalesTarget = ''
    if dealer_profile != '':
        try:
            DBM = DealerBusinessMaster.objects.get(is_active=True, dealer_id=dealer_profile.dealer_id)
        except Exception:
            DBM = ''
        try:
            sr = SRDealer.objects.get(dealer_id_id=dealer_profile.pk)
        except Exception:
            sr = ''
        objDealerSalesTarget = DealerSalesTarget.objects.filter(dealer_id=dealer_profile.dealer_id).order_by('-pk')
        
    if request.method == 'POST':
        if 'Image' in request.POST:
            doctype=request.POST.get('doctype')
            try:
                userImg = request.FILES['img']
            except Exception:
                userImg=''
            if userImg != '':
                fs = FileSystemStorage(location=os.path.join(settings.MEDIA_ROOT, 'avatars/'), base_url=urljoin(settings.MEDIA_URL,'avatars/'))
                filename = fs.save(userImg.name, userImg)
                uploaded_file_url = ('avatars/'+filename)
                imguser=User.objects.get(pk=Did)
                imguser.avatar=uploaded_file_url
                imguser.save()
        if 'upload' in request.POST:
            doctype=request.POST.get('doctype')
            if dealer_profile != '':
                if doctype=='Aadhaar':
                    try:
                        panfile = request.FILES['document']
                    except Exception:
                        panfile=''
                    if panfile != '':
                        fs = FileSystemStorage(location=os.path.join(settings.DOCUMENT_ROOT, ''), base_url=urljoin(settings.DOCUMENT_URL,''))
                        filename = fs.save(panfile.name, panfile)
                        uploaded_file_url = fs.url(filename)
                    ad='Aadhaar Card'
                    try:
                        DTM=DocumentTypeMaster.objects.get(document_name=ad)
                    except Exception:
                        DTM=DocumentTypeMaster()
                    DTM.document_name=ad
                    DTM.save()
                    try:
                        DD=UserDocuments.objects.get(document_type_id=DTM.pk,user_id=Did)
                    except Exception:
                        DD=UserDocuments()
                    DD.user=User.objects.get( pk=Did)
                    DD.document_type=DocumentTypeMaster.objects.get(document_type_id=DTM.pk)
                    DD.document_number=request.POST.get('txtdoc')
                    DD.document=ad
                    DD.document_file=uploaded_file_url
                    DD.save()
                    dealer_profile.Aadhaar_No=request.POST.get('txtdoc')
                    dealer_profile.save()
                if dealer_profile.Aadhaar_No=='' and dealer_profile.Aadhaar_No==None:
                    dealer_profile.pan_no='Not Uploaded'
                    dealer_profile.save()
                if doctype=='Pan':
                    try:
                        adhaarfile = request.FILES['document']
                    except Exception:
                        adhaarfile=''
                    if adhaarfile != '':
                        fs = FileSystemStorage(location=os.path.join(settings.DOCUMENT_ROOT, ''), base_url=urljoin(settings.DOCUMENT_URL,''))
                        filename = fs.save(adhaarfile.name, adhaarfile)
                        uploaded_file_url = fs.url(filename)
                    ad='Pan Card'
                    try:
                        DTM=DocumentTypeMaster.objects.get(document_name=ad)
                    except Exception:
                        DTM=DocumentTypeMaster()
                    DTM.document_name=ad
                    DTM.save()
                    try:
                        DD=UserDocuments.objects.get(document_type_id=DTM.pk,user_id=Did)
                    except Exception:
                        DD=UserDocuments()
                    DD.user=User.objects.get(pk=Did)
                    DD.document_type=DocumentTypeMaster.objects.get(document_type_id=DTM.pk)
                    DD.document_number=request.POST.get('txtdoc')
                    DD.document=ad
                    DD.document_file=uploaded_file_url
                    DD.save()
                    dealer_profile.pan_no=request.POST.get('txtdoc')
                    dealer_profile.save()
                if dealer_profile.pan_no=='' and dealer_profile.pan_no==None:
                    dealer_profile.pan_no='Not Uploaded'
                    dealer_profile.save()
                if doctype=='GST':
                    try:
                        gstfile = request.FILES['document']
                    except Exception:
                        gstfile=''
                    if gstfile != '':
                        fs = FileSystemStorage(location=os.path.join(settings.DOCUMENT_ROOT, ''), base_url=urljoin(settings.DOCUMENT_URL,''))
                        filename = fs.save(gstfile.name, gstfile)
                        uploaded_file_url = fs.url(filename)
                    ad='GST'
                    try:
                        DTM=DocumentTypeMaster.objects.get(document_name=ad)
                    except Exception:
                        DTM=DocumentTypeMaster()
                    DTM.document_name=ad
                    DTM.save()
                    try:
                        DD=UserDocuments.objects.get(document_type_id=DTM.pk,user_id=Did)
                    except Exception:
                        DD=UserDocuments()
                    DD.user=User.objects.get(pk=Did)
                    DD.document_type=DocumentTypeMaster.objects.get(document_type_id=DTM.pk)
                    DD.document_number=request.POST.get('txtdoc')
                    DD.document=ad
                    DD.document_file=uploaded_file_url
                    DD.save()
                    dealer_profile.gst_no=request.POST.get('txtdoc')
                    dealer_profile.save()
                if dealer_profile.gst_no=='' and dealer_profile.gst_no==None:
                    dealer_profile.gst_no='Not Uploaded'
                    dealer_profile.save()
                msg="<div class='wright_msg'  style='max-width: 665px;margin: 0 auto;'>" \
                    "<ul><li style='color: #5cb85c;font-size: 21px;font-weight: 700;> <span class='glyphicon glyphicon-ok-sign'></span> File  Upload Successfully .</li></ul>" \
                    "</div>"
    DealerDoc = UserDocuments.objects.filter(user_id=Did)
    for ab in DealerDoc:
        if ab.document_file.name.endswith('.pdf'):
            upload_file = '<img style=" height:30px" src="/static/img/PDF_download.png" class="status-image">'
        if ab.document_file.name.endswith('.gif'):
            upload_file = '<img style=" height:30px" src="/static/img/image-file.png" class="status-image">'
        if ab.document_file.name.endswith('.jpg'):
            upload_file = '<img style=" height:30px" src="/static/img/image-file.png" class="status-image">'
        if ab.document_file.name.endswith('.png'):
            upload_file = '<img style=" height:30px" src="/static/img/image-file.png" class="status-image">'
        if ab.document_file.name.endswith('.jpeg'):
            upload_file = '<img style="height:30px" src="/static/img/image-file.png" class="status-image">'

    cd_slab = CashDiscountSchemeMaster.objects.filter(dealer=dealer_profile.dealer).first()
    slab = CashDiscountSlabsMaster.objects.get(pk=cd_slab.slab.pk)
    user_role = ""
    user = request.user
    # for user in allUsers:
    groups = user.groups.all()
    groups = [group.name for group in groups]
    if 'SR' in groups:
        user.__dict__["role"] = "SR"
        user.__dict__["userRole"] = "SR"
        user_role = "SR"
    elif 'WHS' in groups:
        user.__dict__["role"] = "Warehouse Admin"
        user.__dict__["userRole"] = "WHS"
        user_role = "WHS"
    elif 'DEL' in groups:
        user.__dict__["role"] = "Dealer"
        user.__dict__["userRole"] = "DEL"
        user_role = "DEL"
    elif 'WHSUSER' in groups:
        user.__dict__["role"] = "Warehouse Manager"
        user.__dict__["userRole"] = "WHSUSER"
        user_role = "WHSUSER"
    elif 'Finance' in groups:
        user.__dict__["role"] = "Finance"
        user.__dict__["userRole"] = "Finance"
        user_role = "Finance"
    elif 'BusinessHead' in groups:
        user.__dict__["role"] = "Business Head"
        user.__dict__["userRole"] = "BusinessHead"
        user_role = "BusinessHead"
    else:
        user.__dict__["role"] = "Admin"
        user.__dict__["userRole"] = "Admin"
        user_role = "Admin"

    try:
        maxoutstanding = DealerBusinessMaster.objects.get(dealer_id=Did, is_active=True)
    except Exception:
        maxoutstanding = None
    if maxoutstanding:
        orderheadamt = OrderHead.objects.filter(
            dealer_id=Did,status__in=status_in.OutstandingStatus(), is_active=True).aggregate(Sum('payable_amount'))
        if orderheadamt['payable_amount__sum'] is None:
            orderheadamt['payable_amount__sum'] = 0
        else:
            orderheadamt['payable_amount__sum'] = float(orderheadamt['payable_amount__sum'])
        max_outstanding_threshold = float(maxoutstanding.max_outstanding_threshold)

    OrderActive = OrderHead.objects.filter(
        dealer_id=Did, status__in=status_in.DELProfileActiveOrder())
    """ IF outstanding is negative """

    try:
        prevDealerLedger = Ledger.objects.filter(
            ledger_type="Unadjusted", dealer=Did,
            is_active=True, is_deleted=False).latest('pk')
        balance = float(prevDealerLedger.balance)
    except:
        balance = float(0)
    overdue = 0
    if balance < 0:
        overdue = balance
    orderheadblockamt = OrderHead.objects.filter(
        dealer=Did, status__in=['Processing', 'OrderPlaced', 'SchemeApplied'], is_active=True).aggregate(
        Sum('payable_amount'))
    if orderheadblockamt['payable_amount__sum'] is None:
        orderheadblockamt['payable_amount__sum'] = 0
    else:
        orderheadblockamt['payable_amount__sum'] = float(orderheadblockamt['payable_amount__sum'])
    out_remain = float(orderheadamt["payable_amount__sum"])
    if out_remain < 0:
        out_remain = 0
    target_remain = max_outstanding_threshold - out_remain - \
                    orderheadblockamt['payable_amount__sum'] - float(overdue)
    context = {
        'CUA': CUA,
        'dealer_profile': dealer_profile,
        'DBM': DBM,
        'did': Did,
        'totaldealercount': totaldealercount,
        'objDealerSalesTarget': objDealerSalesTarget,
        'dealerlist': dealerlist,
        'fobj': fobj,
        'sobj': fobj,
        'msg': msg,
        'DealerDoc': DealerDoc,
        'OrderActive': OrderActive,
        'OrderHistory': OrderHistory,
        'totalOrderActive': totalOrderActive,
        'outstanding': price,
        'sr': sr,
        'upload_file': upload_file,
        'target': get_target,
        'month': month,
        'objdst': objdst,
        'pay': pay['payable_amount__sum'],
        'cd_slab':slab,
        'user_role':user_role,
        'target_acheived':orderheadamt['payable_amount__sum'],
        'target_remain':target_remain,
        'credit_limit':max_outstanding_threshold
    }
    return render(request, 'dealer/dealer_profile.html', context)


import urllib.parse


def document_delete(request, Did):
    dealer=request.GET.get('dealer_id')
    document = UserDocuments.objects.get(pk=Did)
    if document.document_file.name!='Document not uploaded':
        obj=os.path.join(settings.DOCUMENT_ROOT).rstrip('/document')+urllib.parse.unquote(document.document_file.name)
        os.remove(obj)
        document.document_file='Document not uploaded'
        document.save()
        messages.add_message(request, messages.SUCCESS, 'document deleted successfully...')
    else:
        messages.add_message(request, messages.ERROR, 'Cannot delete file. Document Not Found!!!')
    return redirect('/user/dealer_profile/'+dealer)

# def document_edit(request, Did):
#     instance = get_object_or_404(UserDocuments, pk=Did)
#     form_class = forms.DealerDocumentsForm(request.POST or None, instance=instance)
#     if request.method == 'GET':
#         return render(request, 'dealer_documents/dealer_documents_add.html', {'form': form_class})
#     elif request.method == 'POST':
#         #dealer=request.GET.get('deale_id')
#         instance = get_object_or_404(UserDocuments, pk=Did)
#         form_class = forms.DealerDocumentsForm(request.POST or None, instance=instance)
#         if form_class.is_valid():
#             dealer_doc = form_class.save(commit=False)
#             dealer_doc.created_by = request.user
#             dealer_doc.save()
#         return redirect('/user/dealer_profile/'+dealer)


def dealer_list(request):
    urlobj = request.GET.get('url')
    dealer_url = request.GET.get('url')
    if 'selected_brand' in request.session:
        groups = request.user.groups.all()
        groups = [group.name for group in groups]
        if request.user.is_superuser or 'Finance' in groups or 'BusinessHead' in groups:
            dealerProfile = models.DealerProfile.objects.filter(
                brand=request.session["selected_brand"], is_deleted=False).order_by(
                '-created_on').order_by('-modified_on')
        elif request.user.id:
            try:
                srUser = User.objects.get(pk=request.user.id, groups__name='SR', is_active=True)
            except Exception:
                srUser = None
            if srUser is not None:
                SRDl = SRDealer.objects.filter(sr_id_id=srUser.pk).values_list('dealer_id_id')
                dealerProfile = models.DealerProfile.objects.filter(
                    pk__in=SRDl, brand=request.session["selected_brand"],
                    is_deleted=False).order_by('-created_on').order_by('-modified_on')
                dealerCount = models.DealerProfile.objects.filter(
                    brand=request.session["selected_brand"],
                    pk__in=SRDl, is_deleted=False).count()
    else:
        groups = request.user.groups.all()
        groups = [group.name for group in groups]
        if request.user.is_superuser or 'Finance' in groups or 'BusinessHead' in groups:
            dealerProfile = models.DealerProfile.objects.filter(
                is_deleted=False).order_by('-created_on').order_by('-modified_on')
        elif request.user.id:
            try:
                srUser = User.objects.get(pk=request.user.id, groups__name='SR', is_active=True)
            except Exception:
                srUser = None
            if srUser is not None:
                SRDl = SRDealer.objects.filter(sr_id_id=srUser.pk).values_list('dealer_id_id')
                dealerProfile = models.DealerProfile.objects.filter(
                    pk__in=SRDl, is_deleted=False).order_by('-created_on').order_by('-modified_on')
                
    # add sr code of dealer..............................
    searchValue = ""
    querycount = ''
    for dealer in dealerProfile:
        sr_code = SRDealer.objects.filter(dealer_id=dealer).first()
        try:
            dealer.__dict__["sr_code"] = sr_code.sr_id.user_code
        except:
            dealer.__dict__["sr_code"] = '---'

        # sr_list = SRDealer.objects.filter(dealer_id=dealer)
        # try:
        #     sr = [{'id': sr.id, 'code': sr.sr_id.user_code} for sr in sr_list]
        #     dealer.__dict__["sr_code"] = sr
        # except User.DoesNotExist:
        #     pass
    # end................................................

    if request.method == 'POST':
        if "searchbtn" in request.POST:
            searchValue = request.POST.get('search', None)
            if searchValue is not None:
                # dealerProfile = models.DealerProfile.objects.filter(
                #     Q(company_name__startswith=searchValue)|
                #     Q(company_address__startswith=searchValue),
                #     is_deleted=False)

                dealerProfile = dealerProfile.filter(
                    Q(dealer__user_code__icontains=searchValue) |
                    Q(company_name__icontains=searchValue) |
                    Q(shipping_address__icontains=searchValue) |
                    Q(dealer__state__icontains=searchValue) |
                    Q(region_id__icontains=searchValue) |
                    Q(dealer__city__icontains=searchValue) |
                    # Q(sr_code__icontains=searchValue) |
                    Q(email__icontains=searchValue) |
                    Q(owner_name__icontains=searchValue) |
                    Q(primary_contact__icontains=searchValue))
                for dealer in dealerProfile:
                    sr_code = SRDealer.objects.filter(dealer_id=dealer).first()
                    try:
                        dealer.__dict__["sr_code"] = sr_code.sr_id.user_code
                    except:
                        dealer.__dict__["sr_code"] = '---'
                querycount = str(len(dealerProfile)) + ' Record found.'
                # if not dealerProfile:
                #     try:
                #         user = User.objects.filter(Q(user_code=searchValue)|Q(user_code__endswith=searchValue)|Q(city__endswith=searchValue)|Q(city__startswith=searchValue)|Q(state__endswith=searchValue)|Q(state__startswith=searchValue)|Q(first_name=searchValue)|Q(mobile=searchValue)|Q(email=searchValue), is_active=True).values_list('user_code')
                #         querycount=User.objects.filter(Q(user_code=searchValue)|Q(user_code__endswith=searchValue)|Q(city__endswith=searchValue)|Q(city__startswith=searchValue)|Q(state__endswith=searchValue)|Q(state__startswith=searchValue)|Q(first_name=searchValue)|Q(mobile=searchValue)|Q(email=searchValue), is_active=True).count()
                #         querycount=str(querycount)+' Record found.'
                #         dealerProfile = models.DealerProfile.objects.filter(dealer_code__in=user, is_deleted=False).order_by('-created_on')
                #     except User.DoesNotExist:
                #         pass
                # else:
                #     querycount=str(len(dealerProfile))+' Record found.'
                # for dealer in dealerProfile:
                #     sr_code = SRDealer.objects.filter(dealer_id=dealer).first()
                #     if sr_code:
                #         dealer.__dict__["sr_code"] = sr_code.sr_id.user_code
                #     else:
                #         dealer.__dict__["sr_code"] = 'NA'

        if 'export' in request.POST:
            dealer = dealer_export(request)
            file_path = os.getcwd()
            paths = file_path + '/upload/tempexcelfile/dealer.csv'
            if os.path.exists(paths):
                with open(paths, "rb") as output:
                    response = HttpResponse(output.read(), content_type="application/vnd.ms-excel")
                    response['Content-Disposition'] = 'attachment; filename="dealer.csv"'
                    return response

    page_style = request.GET.get("page_style", settings.DEFAULT_PAGING_VIEW)
    # paginator = Paginator(dealer_profile, settings.PAGE_SIZE)
    paginator = Paginator(dealerProfile, 10)
    try:
        page_index = request.GET.get("page", 1)
        data_list = paginator.page(page_index)
    except PageNotAnInteger:
        data_list = paginator.page(1)
    except EmptyPage:
        data_list = paginator.page(paginator.num_pages)

    context = {
        'data_list': data_list,
        'dealer_url': dealer_url,
        'dealerCount': len(data_list),
        'searchValue': searchValue,
        'querycount': querycount,
        'urlobj': urlobj
    }
    if request.GET.get('list', None):
        context['flagList'] = True
    else:
        context['flagList'] = False
    if page_style == 'grid':
        return render(request, 'dealer/dealer_grid_view.html', context)
    else:
        return render(request, 'dealer/dealer_list_view.html', context)


def import_dealer_profile(request):
    invalid_rows = []
    index = 0
    Inserted = 0
    set_exist = 0
    not_insert = 0
    process = 0
    Update = 0
    not_exist = []
    filexlsx = False
    lblMsg = ''
    errormsg = ''
    Msg = ''
    result = []
    User = get_user_model()
    if request.method == 'POST':
        if 'Save' in request.POST:
            # brand_name = request.POST.get('brand')
            getSl_num = {}
            file = request.FILES.get('dealer')
            if file is not None and file != '':
                filexlsx = str(file._name).endswith('csv')
            if filexlsx == True:
                decoded_file = request.FILES['dealer'].read().decode('utf-8').splitlines()
                reader = csv.DictReader(decoded_file)
                for row in reader:
                    process += 1
                    if index == 0:
                        flag = True
                        for x in import_settings.DEALER_DATA_IMPORT_KEY:
                            if x not in row.keys():
                                not_exist.append(x)
                            for tie_up_code in row.keys():
                                if 'tie_up_' in tie_up_code:
                                    tie_up_code = tie_up_code.replace('tie_up_', '').upper()
                                    scheme_catgs = SchemeCategoryMaster.objects.filter(scheme_category_code=tie_up_code)
                                    if len(scheme_catgs) <= 0:
                                        SchemeCategoryMaster.objects.create(scheme_category_code=tie_up_code,
                                                                            scheme_category_name=tie_up_code)
                                        index = index + 1
                    set_exist = len(not_exist)
                    count = 0
                    if set_exist == 0:
                        if row['dealer_code'] != '' and row['brand_name'] != '' and row['company_name'] != '' \
                                and row['owner_name'] != '' and row['primary_contact'] != '' and row['sr_id'] != '' \
                                and row['email'] != '' :
                            if len(row['aadhaar_no'])>=0 and len(row['PAN_no'])>=0 and len(row['GST_no'])>=0 and \
                                    len(row['primary_contact']) == 10:
                                #srid=int(row['sr_id'])
                                #sales = SRDealer.objects.filter(pk=srid)
                                state=''
                                city = ''
                                try:
                                    SR = User.objects.get(user_code=row['sr_id'], groups__name="SR")
                                except Exception:
                                    SR=''
                                if SR != '':
                                    try:
                                        CDS = CashDiscountSlabsMaster.objects.get(slab_code=row['CDslab_code'])
                                    except Exception:
                                        CDS = ''
                                    if CDS != '':
                                        allowance_b=row['allowance_b']
                                        if allowance_b == '':
                                            allowance_b = 0
                                        else:
                                            allowance_b = float(allowance_b.rstrip('%'))
                                        allowance_m = row['allowance_m']
                                        if allowance_m == '':
                                            allowance_m = 0
                                        else:
                                            allowance_m = float(allowance_m.rstrip('%'))
                                        allowance_val = row['allowance_val']
                                        if allowance_val == '':
                                            allowance_val = 0
                                        else:
                                            allowance_val = float(allowance_val.rstrip('%'))
                                        allowance_vol = row['allowance_vol']
                                        if allowance_vol == '':
                                            allowance_vol = 0
                                        else:
                                            allowance_vol=float(allowance_vol.rstrip('%'))
                                        tie_up_b = row['tie_up_b']
                                        if tie_up_b == '':
                                            tie_up_b = 0
                                        else:
                                            tie_up_b = float(tie_up_b.rstrip('%'))
                                        tie_up_m = row['tie_up_m']
                                        if tie_up_m == '':
                                            tie_up_m = 0
                                        else:
                                            tie_up_m = float(tie_up_m.rstrip('%'))
                                        tie_up_vol = row['tie_up_vol']
                                        if tie_up_vol == '':
                                            tie_up_vol = 0
                                        else:
                                            tie_up_vol = float(tie_up_vol.rstrip('%'))
                                        tie_up_val = row['tie_up_val']
                                        if tie_up_val == '':
                                            tie_up_val = 0
                                        else:
                                            tie_up_val = float(tie_up_val.rstrip('%'))
                                        min_order_amount = row['min_order_amount']
                                        if min_order_amount == '':
                                            min_order_amount = 0
                                        else:
                                            min_order_amount = float(min_order_amount)
                                        max_outstanding_threshold = row['max_outstanding_threshold']
                                        if max_outstanding_threshold == '':
                                            max_outstanding_threshold = 0
                                        else:
                                            max_outstanding_threshold=float(max_outstanding_threshold)
                                        if min_order_amount>=0 and max_outstanding_threshold>=0 and allowance_b>=0 and allowance_m>=0 \
                                            and allowance_val>=0 and allowance_vol>=0 and tie_up_b>=0 and tie_up_m>=0\
                                            and tie_up_vol>=0 and tie_up_val>=0:
                                            users=User.objects.filter(user_code=row['dealer_code'])
                                            if len(users)>1:
                                                error_user = {}
                                                error_user['error_message'] = 'Multiple user_code in user table ' + str(
                                                    row['dealer_code'])
                                                invalid_rows.append(error_user)
                                                not_insert += 1
                                            else:
                                                try:
                                                    state = StateMaster.objects.get(state_code=row['state'])
                                                except Exception:
                                                    state = {}
                                                    state['error_message'] = 'Multiple user_code in user table ' + str(
                                                    row['state'])
                                                    invalid_rows.append(state)
                                                    #state = ''
                                                try:
                                                    city = CityMaster.objects.get(city_code=row['city'])
                                                except Exception:
                                                    state = {}
                                                    state['error_message'] = 'Multiple user_code in user table ' + str(
                                                    row['city'])
                                                    invalid_rows.append(state)
                                                try:
                                                    users = User.objects.get(user_code=row['dealer_code'])
                                                    Update+=1
                                                except User.DoesNotExist:
                                                    users = User()
                                                    Inserted += 1
                                                users.email = row['email']
                                                users.first_name = row['owner_name']
                                                users.mobile = row['primary_contact']
                                                users.set_password(row['primary_contact'])
                                                users.date_joined = datetime.datetime.now()
                                                if state!='':
                                                    users.state = row['state']
                                                users.business_email = row['email']
                                                if city!='':
                                                    users.city = row['city']
                                                users.user_code = row['dealer_code']
                                                User.is_staff = True
                                                # User.superuser=True
                                                try:
                                                    users.save()
                                                    my_group = Group.objects.filter(name='DEL').first()
                                                    if my_group:
                                                        my_group.user_set.add(users)
                                                    else:
                                                        Group.objects.get_or_create(name='DEL')
                                                        my_group = Group.objects.filter(name='DEL').first()
                                                        my_group.user_set.add(users)
                                                    try:
                                                        DP = DealerProfile.objects.get(dealer_code=row['dealer_code'])
                                                    except DealerProfile.DoesNotExist:
                                                        DP = DP = DealerProfile()
                                                    DP.dealer = User.objects.get(id=users.pk)
                                                    DP.dealer_code = row['dealer_code']
                                                    DP.owner_name = row['owner_name']
                                                    DP.company_name = row['company_name']
                                                    DP.region_id = row['region_id']
                                                    DP.shipping_address = row['shipping_address']

                                                    DP.company_address = row['company_address']
                                                    pincode = row['pincode']
                                                    if pincode != '' and pincode != None:
                                                        DP.pincode = int(row['pincode'])
                                                    else:
                                                        DP.pincode = 0
                                                    DP.email = row['email']
                                                    DP.primary_contact = int(row['primary_contact'])
                                                    alternate_contact = row['alternate_contact']
                                                    if alternate_contact == '' or alternate_contact == None:
                                                        DP.alternate_contact = 0
                                                    else:
                                                        DP.alternate_contact = int(alternate_contact)
                                                    DP.cd_slab_code=CashDiscountSlabsMaster.objects.get(pk=CDS.pk)
                                                    DP.gst_no = row['GST_no']
                                                    DP.pan_no = row['PAN_no']
                                                    DP.Aadhaar_No = row['aadhaar_no']
                                                    DP.created_by = User.objects.get(id=users.id, is_active=True)
                                                    DP.created_on = datetime.datetime.now()
                                                    DP.modified_by = User.objects.get(id=users.id, is_active=True)
                                                    DP.modified_on = datetime.datetime.now()

                                                    code = row['brand_name'].upper()[0:3]
                                                    # count += 1
                                                    # code = code + '10' + str(count)
                                                    try:
                                                        BM = BrandMaster.objects.get(
                                                            brand_name__icontains=row['brand_name'], is_active=True)
                                                        code = BM.brand_code
                                                    except BrandMaster.DoesNotExist:
                                                        BM = BrandMaster()
                                                    BM.brand_name = row['brand_name']
                                                    BM.brand_code = code
                                                    BM.created_by = User.objects.get(id=users.id, is_active=True)
                                                    BM.created_on = datetime.datetime.now()
                                                    BM.modified_by = User.objects.get(id=users.id, is_active=True)
                                                    BM.modified_on = datetime.datetime.now()
                                                    BM.save()
                                                    DP.brand = BM
                                                    DP.save()
                                                    try:
                                                        # SRD = SRDealer.objects.get(dealer_id=DP.pk, sr_id=SR.pk)
                                                        SRD = SRDealer.objects.get(dealer_id=DP.pk)
                                                    except SRDealer.DoesNotExist:
                                                        SRD = SRDealer()
                                                    SRD.sr_id = User.objects.get(id=SR.pk)
                                                    SRD.dealer_id = DealerProfile.objects.get(pk=DP.pk)
                                                    SRD.save()

                                                    try:
                                                        DB = DealersBrand.objects.get(brand_id=BM.brand_code, user_id=users.id,is_active=True)
                                                        #DB=[0]
                                                    except DealersBrand.DoesNotExist:
                                                        DB = DealersBrand()
                                                    DB.user = User.objects.get(id=users.id, is_active=True)
                                                    DB.brand = BrandMaster.objects.get(brand_code=BM.brand_code, is_active=True)
                                                    DB.save()
                                                    try:
                                                        DBM = DealerBusinessMaster.objects.get(brand_id=BM.brand_code, dealer_id=DP.dealer_id,
                                                                                           is_active=True)
                                                    except DealerBusinessMaster.DoesNotExist:
                                                        DBM = DealerBusinessMaster()
                                                    DBM.dealer = User.objects.get(id=users.id, is_active=True)
                                                    DBM.brand = BrandMaster.objects.get(brand_code=BM.brand_code, is_active=True)
                                                    if row['min_order_amount']=='':
                                                        DBM.min_order_amount = 0
                                                    else:
                                                        DBM.min_order_amount = row['min_order_amount']
                                                    if row['max_outstanding_threshold']=='':
                                                        DBM.max_outstanding_threshold = 0
                                                    else:
                                                        DBM.max_outstanding_threshold = row['max_outstanding_threshold']
                                                    credit_days = row['credit_days']
                                                    if credit_days != '' and credit_days != None and credit_days != 'None' :
                                                        DBM.credit_days = int(credit_days)
                                                    else:
                                                        credit_days = int(0)
                                                    DBM.created_by = User.objects.get(id=users.id, is_active=True)
                                                    DBM.created_on = datetime.datetime.now()
                                                    DBM.modified_by = User.objects.get(id=users.id, is_active=True)
                                                    DBM.modified_on = datetime.datetime.now()
                                                    DBM.save()
                                                    for column in row.keys():
                                                        if 'tie_up_' in column:
                                                            if row[column]!='':
                                                                scheme_code = column.replace('tie_up_', '')
                                                                try:
                                                                    TUM = TieUpMaster.objects.get(dealer_id=users.id, brand_id=BM.brand_code,
                                                                                          scheme_category_code=scheme_code.upper())
                                                                except TieUpMaster.DoesNotExist:
                                                                    TUM = TieUpMaster()
                                                                TUM.dealer = User.objects.get(id=users.id, is_active=True)
                                                                TUM.brand = BrandMaster.objects.get(pk=BM.pk)
                                                                TUM.scheme_category_code = SchemeCategoryMaster.objects.get(scheme_category_code=scheme_code.upper())
                                                                TUM.discount_percent = float(row[column].rstrip('%'))
                                                                TUM.discount_amount = float(0)
                                                                TUM.save()
                                                    for column in row.keys():
                                                        if 'allowance_' in column:
                                                            if row[column]!='':
                                                                scheme_code = column.replace('allowance_', '')
                                                                try:
                                                                    obj_allowance = AllowanceMaster.objects.get(scheme_category_code=scheme_code.upper(),
                                                                                                        brand_id=BM.brand_code,
                                                                                                        dealer_id=users.id)
                                                                except AllowanceMaster.DoesNotExist:
                                                                    obj_allowance = AllowanceMaster()
                                                                obj_allowance.dealer = User.objects.get(id=users.id, is_active=True)
                                                                obj_allowance.brand = BrandMaster.objects.get(pk=BM.pk)
                                                                obj_allowance.scheme_category_code = SchemeCategoryMaster.objects.get(scheme_category_code=scheme_code.upper())
                                                                obj_allowance.discount_percent = float(row[column].rstrip('%'))
                                                                obj_allowance.discount_amount = 0
                                                                obj_allowance.save()
                                                    try:
                                                        CDSM = CashDiscountSchemeMaster.objects.get(slab_id=CDS.pk, dealer_id=users.id)
                                                    except Exception:
                                                        CDSM = CashDiscountSchemeMaster()
                                                    CDSM.dealer = User.objects.get(id=users.id, is_active=True)
                                                    CDSM.slab = CashDiscountSlabsMaster.objects.get(pk=CDS.pk)
                                                    CDSM.save()
                                                except Exception as error:
                                                    error_email = {'error_message': 'Column <email> at row ' + str(
                                                        process) + ' ' + str(error)}
                                                    invalid_rows.append(error_email)
                                                    not_insert += 1
                                        else:
                                            error_b={}
                                            if  tie_up_b<=0:
                                                error_b['error_message']='Column <tie_up_b> at row '+str(process)+' contain negative values'
                                                invalid_rows.append(error_b)
                                            error_m={}
                                            if tie_up_m<=0:
                                                error_m['error_message']='Column <tie_up_m> at row '+str(process)+' contain negative values'
                                                invalid_rows.append(error_m)
                                            error_val={}
                                            if tie_up_val<=0:
                                                error_val['error_message']='Column <tie_up_val> at row '+str(process)+' contain negative values'
                                                invalid_rows.append(error_val)
                                            error_vol={}
                                            if tie_up_vol<=0:
                                                error_vol['error_message']='Column <tie_up_vol> at row '+str(process)+' contain negative values'
                                                invalid_rows.append(error_vol)
                                            error_b1 = {}
                                            if allowance_b <= 0:
                                                error_b1['error_message'] = 'Column <allowance_b> at row ' + str(
                                                process) + ' contain negative values'
                                                invalid_rows.append(error_b1)
                                            error_m1 = {}
                                            if allowance_m <= 0:
                                                error_m1['error_message'] = 'Column <allowance_m> at row ' + str(
                                                    process) + ' contain negative values'
                                                invalid_rows.append(error_m1)
                                            error_val1 = {}
                                            if allowance_val <= 0:
                                                error_val1['error_message'] = 'Column <allowance_val> at row ' + str(
                                                    process) + ' contain negative values'
                                                invalid_rows.append(error_val1)
                                            error_vol1 = {}
                                            if allowance_vol <= 0:
                                                error_vol1['error_message'] = 'Column <allowance_vol> at row ' + str(
                                                    process) + ' contain negative values'
                                                invalid_rows.append(error_vol1)
                                            error_min={}
                                            if min_order_amount <= 0:
                                                error_min['error_message'] = 'Column <min_order_amount> at row ' + str(
                                                    process) + ' contain negative values'
                                                invalid_rows.append(error_min)
                                            error_max = {}
                                            if max_outstanding_threshold <= 0:
                                                error_max['error_message'] = 'Column <max_outstanding_threshold> at row ' + str(
                                                    process) + ' contain negative values'
                                                invalid_rows.append(error_max)
                                            not_insert+=1
                                    else:
                                        error_slab = {}
                                        error_slab['error_message'] = 'Column <CDslab_code> at row ' + str(
                                            process) + ' does not exist'
                                        invalid_rows.append(error_slab)
                                        not_insert+=1
                                else:
                                    error_sr = {}
                                    error_sr['error_message'] = 'Column <sr_id> at row ' + str(
                                        process) + ' does not exist'
                                    invalid_rows.append(error_sr)
                                    not_insert+=1
                            else:
                                # if len(row['PAN_no'])!=10:
                                #     error_sr = {}
                                #     error_sr['error_message'] = 'Column <PAN_no> at row ' + str(
                                #         process) + ' Must be 10 digit Number '
                                #     invalid_rows.append(error_sr)
                                # if len(row['GST_no'])!=15:
                                #     error_sr = {}
                                #     error_sr['error_message'] = 'Column <GST_no> at row ' + str(
                                #         process) + ' Must be 15 digit Number '
                                #     invalid_rows.append(error_sr)
                                # if len(row['aadhaar_no'])!=12:
                                #     error_sr = {}
                                #     error_sr['error_message'] = 'Column <aadhaar_no> at row ' + str(
                                #         process) + ' Must be 12 digit Number '
                                #     invalid_rows.append(error_sr)
                                if len(row['primary_contact'])!=10:
                                    error_sr = {}
                                    error_sr['error_message'] = 'Column <primary_contact> at row ' + str(
                                        process) + ' Must be 10 digit Number '
                                    invalid_rows.append(error_sr)
                                not_insert+=1
                        else:
                            error_code = {}
                            if row['dealer_code'] == '':
                                error_code['error_message'] = 'Column <dealer_code> at row ' + str(
                                    process) + ' contain null values'
                                invalid_rows.append(error_code)
                            error_ab = {}
                            if row['brand_name'] == '':
                                error_ab['error_message'] = 'Column <brand_name> at row ' + str(
                                    process) + ' contain null values'
                                invalid_rows.append(error_ab)
                            error_am = {}
                            if row['company_name'] == '':
                                error_am['error_message'] = 'Column <company_name> at row ' + str(
                                    process) + ' contain null values'
                                invalid_rows.append(error_am)
                            error_aval = {}
                            if row['owner_name'] == '':
                                error_aval['error_message'] = 'Column <owner_name> at row ' + str(
                                    process) + ' contain null values'
                                invalid_rows.append(error_aval)
                            error_avol = {}
                            if row['primary_contact'] == '':
                                error_avol['error_message'] = 'Column <primary_contact> at row ' + str(
                                    process) + ' contain null values'
                                invalid_rows.append(error_avol)
                            error_avl = {}
                            if row['sr_id'] == '':
                                error_avl['error_message'] = 'Column <sr_id> at row ' + str(
                                    process) + ' contain null values'
                                invalid_rows.append(error_avl)

                            if row['email'] == '':
                                error_email = {'error_message': 'Column <email> at row ' + str(process) + ' contain null values'}
                                invalid_rows.append(error_email)
                            # if row['aadhaar_no']=='':
                            #     error_aadhaar = {'error_message': 'Column <aadhaar_no> at row ' + str(process) + ' contain null values'}
                            #     invalid_rows.append(error_aadhaar)

                            not_insert+=1
                        if len(invalid_rows) >= 0:
                            #not_insert = len(invalid_rows)
                            filespath = os.getcwd()
                            paths = filespath + '/upload/tempexcelfile/'
                            if not os.path.exists(paths):
                                os.makedirs(paths)
                            file_path = filespath + '/upload/tempexcelfile/dealer_not_inserted.csv'
                            keys = ['error_message']
                            with open(file_path, "w") as output:
                                dict_writer = csv.DictWriter(output, keys)
                                dict_writer.writeheader()
                                if len(invalid_rows) > 0:
                                    dict_writer.writerows(invalid_rows)
                    else:
                        errormsg =  "<div class='wrong_msg ' style='max-width: 665px;margin: 0 auto;'>" \
                                 "<ul>"\
                                 "<li style='text-align:center'><span class='glyphicon glyphicon-remove-sign'></span>Error! </li>"\
                                 "<li><span class='glyphicon glyphicon-arrow-right'></span>  Sheet must have these columns !!</li>" \
                                 "<li> <span class='glyphicon glyphicon-arrow-right'></span>  dealer_code, brand_name, company_name, owner_name, primary_contact, sr_id, CDslab_code</li>" \
                                 "</ul>" \

                    if Inserted != 0 or not_insert != 0 or Update != 0:
                        lblMsg ="<div class='wright_msg'  style='max-width: 665px;margin: 0 auto;'>" \
                                     "<ul>" \
                                     "<li style='color: #5cb85c;font-size: 21px;font-weight: 700; text-align:center'> <span class='glyphicon glyphicon-ok-sign'></span> File  Processed Successfully .</li>" \
                                     "<li style='color: #5cb85c;font-size: 21px;font-weight: 700;'> <span class='glyphicon glyphicon-arrow-right'></span> Total Record(s) Processed =" + str(
                                process) + \
                                     "<li  style='color: #5cb85c;font-size: 21px;font-weight: 700;' > <span class='glyphicon glyphicon-arrow-right'></span> Total Record(s) Inserted =" + str(
                                Inserted) + \
                                     "</li ><li  style='color: #5cb85c;font-size: 21px;font-weight: 700;'> <span class='glyphicon glyphicon-arrow-right'></span> Total Record(s) Updated =" + str(
                                Update) + \
                                     "</li ><li  style='color: #5cb85c;font-size: 21px;font-weight: 700;'> <span class='glyphicon glyphicon-arrow-right'></span> Total Record(s) Not Inserted =" + str(
                                not_insert) + \
                                     "</li ></ul>" \
                                     "For Errors, <input value='Click to download' type='submit' " \
                                        "name='download' style=' background-color: #ecf0f5; padding: 5px 15px; text-align: center;font-size: 15px; cursor: pointer;display: inline-block; border-radius: 4px;font-weight: 600;'></div>"
                if lblMsg!='':
                    messages.add_message(request, messages.SUCCESS, 'File Processed Successfully...')
            else:
                lblMsg = "<div class='wrong_msg' style='max-width: 665px;margin: 0 auto;'>" \
                                 "<ul>"\
                                 "<li style='text-align:center'><span class='glyphicon glyphicon-remove-sign'></span>Error! </li>"\
                                 "<li><span class='glyphicon glyphicon-arrow-right'></span>  Must contain .csv file!!</li>" \
                                 "</ul>" \
                                 "</div>"

        if 'download' in request.POST:
            file_path = os.getcwd()
            paths = file_path + '/upload/tempexcelfile/dealer_not_inserted.csv'
            if os.path.exists(paths):
                with open(paths, 'rb') as fh:
                    response = HttpResponse(fh.read(), content_type="application/vnd.ms-excel")
                    response['Content-Disposition'] = 'inline; filename=' + os.path.basename(paths)
                    return response

    context = {
        'lblMsg': lblMsg,
        'Msg': Msg,
        'set_exist': set_exist,
        'not_exist': not_exist,
        'errormsg': errormsg,
    }
    return render(request, 'dealer/import_dealer_data.html', context)


def handle_uploaded_file(request, imgfile, paths):
    # pic_path='upload/'
    if not os.path.exists(paths):
        os.makedirs(paths)
    if imgfile != '':
        if os.listdir(paths) != []:
            j = os.listdir(paths)
            os.remove(paths + j[0])
            picfolder = str(os.listdir(paths))
            picfolder = picfolder.replace('[', '').replace(']', '').replace("'", '')
            os.remove(paths + picfolder)
        with open(paths + imgfile.name, 'wb+') as destination:
            for chunk in imgfile.chunks():
                destination.write(chunk)


def dealer_export(request):
    # dealerinfo = DealerProfile.objects.filter(is_active=True,is_deleted=False)
    dealerinfo = DealerProfile.objects.filter(is_active=True).order_by('-created_on')
    result = []
    for dealer in dealerinfo:
        try:
            get_dealer = {}
            get_dealer['dealer_code'] = str(dealer.dealer_code)
            get_dealer['company_name'] = str(dealer.company_name)
            try:
                # getbrand = DealersBrand.objects.get(user_id=dealer.dealer_id)
                getbrand = dealer.brand
            except Exception:
                getbrand = ''

            if getbrand != '' and getbrand is not None:
                get_dealer['brand_name'] = str(getbrand.brand_name)
                # get_dealer['brand_name']=str(getbrand.brand.brand_name)
            else:
                get_dealer['brand_name']='Not Found'
            get_dealer['shipping_address'] = str(dealer.shipping_address)
            get_dealer['company_address'] = str(dealer.company_address)

            try:
                getUser = User.objects.get(user_code=dealer.dealer_code)
            except Exception:
                getUser = ''

            if getUser != '' and getUser is not None:
                get_dealer['state'] = str(getUser.state)
                get_dealer['city'] = str(getUser.city)
            else:
                get_dealer['state'] = 'Not Found'
                get_dealer['city'] = 'Not Found'
            get_dealer['owner_name'] = str(dealer.owner_name)
            get_dealer['region_id'] = str(dealer.region_id)
            get_dealer['pincode'] = __str__(dealer.pincode)

            get_dealer['email'] = str(dealer.email)
            get_dealer['primary_contact'] = __str__(dealer.primary_contact)
            get_dealer['alternate_contact']=__str__(dealer.alternate_contact)

            try:
                getsr_id = SRDealer.objects.get(dealer_id_id=dealer.pk)
            except Exception:
                getsr_id = ''

            if getsr_id != '' and getsr_id is not None:
                # get_dealer['sr_id'] = str(getsr_id.sr_id_id)
                get_dealer['sr_id'] = str(getsr_id.sr_id.user_code)
            else:
                get_dealer['sr_id'] = 'Not Found'
            try:
                ger_order = DealerBusinessMaster.objects.get(dealer_id=dealer.dealer_id)
            except Exception:
                ger_order = ''

            get_dealer['min_order_amount'] = __str__(ger_order.min_order_amount)
            get_dealer['max_outstanding_threshold'] = __str__(ger_order.max_outstanding_threshold)
            get_dealer['credit_days'] = __str__(ger_order.credit_days)
            get_dealer['GST_no'] = str(dealer.gst_no)
            get_dealer['PAN_no'] = str(dealer.pan_no)
            get_dealer['aadhaar_no'] = str(dealer.Aadhaar_No)

            try:
                # slabcode = CashDiscountSchemeMaster.objects.get(dealer_id=dealer.dealer_id)
                slabcode = dealer.cd_slab_code
            except Exception:
                slabcode = ''

            if slabcode != '' and slabcode is not None:
                # get_dealer['CDslab_code'] = str(slabcode.slab)
                get_dealer['CDslab_code'] = str(slabcode.slab_code)
            else:
                get_dealer['CDslab_code'] = 'Not Found'
            try:
                allowance_in = AllowanceMaster.objects.filter(dealer_id=dealer.dealer_id)
            except Exception:
                allowance_in = ''

            if allowance_in != '' and allowance_in is not None:
                for allowance in allowance_in:
                    code=str(allowance.scheme_category_code.scheme_category_code)
                    if code=='B':
                        get_dealer['allowance_b'] = __str__(allowance.discount_percent)
                    if code=='M':
                        get_dealer['allowance_m'] = __str__(allowance.discount_percent)
                    if code=='VOL':
                        get_dealer['allowance_vol'] = __str__(allowance.discount_percent)
                    if code=='VAL':
                        get_dealer['allowance_val'] = __str__(allowance.discount_percent)
            else:
                get_dealer['allowance_b'] =0
                get_dealer['allowance_m'] =0
                get_dealer['allowance_vol'] =0
                get_dealer['allowance_val'] =0
            try:
                tie_up_in = TieUpMaster.objects.filter(dealer_id=dealer.dealer_id)
            except Exception:
                tie_up_in = ''

            if tie_up_in != '' and tie_up_in is not None:
                for tie_up in tie_up_in:
                    code=str(tie_up.scheme_category_code.scheme_category_code)
                    if code == 'B':
                        get_dealer['tie_up_b'] = __str__(tie_up.discount_percent)
                    if code== 'M':
                        get_dealer['tie_up_m'] = __str__(tie_up.discount_percent)
                    if code == 'VOL':
                        get_dealer['tie_up_vol'] = __str__(tie_up.discount_percent)
                    if code == 'VAL':
                        get_dealer['tie_up_val'] = __str__(tie_up.discount_percent)
            else:
                get_dealer['tie_up_b'] =0
                get_dealer['tie_up_m'] =0
                get_dealer['tie_up_vol'] =0
                get_dealer['tie_up_val'] =0
        except:
            continue
        result.append(get_dealer)
    filespath = os.getcwd()
    paths = filespath + '/upload/tempexcelfile/'
    if not os.path.exists(paths):
        os.makedirs(paths)
    file_path = filespath + '/upload/tempexcelfile/dealer.csv'
    keys = ['dealer_code','brand_name','company_name','company_address','shipping_address','state','region_id','city','pincode','email','owner_name','primary_contact','alternate_contact','sr_id','GST_no','PAN_no','aadhaar_no','min_order_amount','max_outstanding_threshold','credit_days','allowance_b','allowance_m','allowance_vol','allowance_val','tie_up_b','tie_up_m','tie_up_vol','tie_up_val','CDslab_code']
    with open(file_path, "w") as output:
        dict_writer = csv.DictWriter(output, keys)
        dict_writer.writeheader()
        if len(result)>0:
            dict_writer.writerows(result)
    return result


def __str__(self):
    if self=='' or self=='None' or self==None:
        self=0
    return self


def edit_dealer(request, Did):
    if request.method == 'GET':
        instance = get_object_or_404(DealerProfile, profile_id=Did)
        form_class = forms.DealerProfileForm(request.POST or None, instance=instance)
        business = DealerBusinessMaster.objects.filter(dealer=instance.dealer).last()
        srUser = SRDealer.objects.filter(dealer_id=instance).first()

        if business:
            min_order_amount = business.min_order_amount
            max_outstanding_threshold = business.max_outstanding_threshold
            credit_days = business.credit_days
        else:
            min_order_amount = max_outstanding_threshold = credit_days = ''

        update = {
            'state': instance.dealer.state,
            'city': instance.dealer.city,
            'gst_no': instance.gst_no,
            'pan_no': instance.pan_no,
            'Aadhaar_No': instance.Aadhaar_No,
            'state_list': models.StateMaster.objects.filter(is_active=True, is_deleted=False),
            'city_list': models.CityMaster.objects.filter(
                state_id__state_code=instance.dealer.state,
                is_active=True, is_deleted=False),
            'min_order_amount': min_order_amount,
            'max_outstanding_threshold': max_outstanding_threshold,
            'credit_days': credit_days,
        }
        context = {
            'form': form_class,
            'dealer': instance,
            'update': update,
            'sr_list': User.objects.filter(groups__name='SR', is_active=True).order_by('-pk'),
            'sr': srUser.sr_id if srUser else None
            # 'sr': SRDealer.objects.get(id=request.GET.get('sr')).sr_id
        }
        return render(request, 'dealer/dealer_create.html', context=context)

    elif request.method == 'POST':
        instance = DealerProfile.objects.get(profile_id=Did)
        form_class = forms.DealerProfileForm(request.POST, instance=instance)

        if form_class.is_valid():
            # form_class.save()

            dealer = form_class.save(commit=False)
            dealer.modified_by = request.user
            dealer.save()

            city = models.CityMaster.objects.get(city_id=request.POST['city_code'])
            user = User.objects.get(id=dealer.dealer.id)

            if len(request.POST["owner_name"].split(' ')) > 1:
                first_name = request.POST["owner_name"].split(' ')[0]
                last_name = request.POST["owner_name"].split(' ')[1]
            else:
                first_name = request.POST["owner_name"].split(' ')[0]
                last_name = ''
            user.first_name = first_name
            user.last_name = last_name
            user.mobile = request.POST["primary_contact"]
            # user.set_password(form_class.cleaned_data['primary_contact'])
            user.email = request.POST["email"]
            user.city = city.city_code
            user.state = city.state_id.state_code
            user.zip_code = request.POST["pincode"]
            user.address_line1 = request.POST["company_address"]
            user.address_line2 = request.POST["shipping_address"]
            user.save()

            brand = BrandMaster.objects.get(brand_code=request.POST.get('brand'), is_active=True)

            try:
                dbm = DealerBusinessMaster.objects.get(brand=brand, dealer=dealer.dealer, is_active=True)
                dbm.modified_by = request.user
            except DealerBusinessMaster.DoesNotExist:
                dbm = DealerBusinessMaster(created_by=request.user)
            dbm.dealer = dealer.dealer
            dbm.brand = brand
            if request.POST['min_order_amount'] is None or request.POST['min_order_amount'] == '':
                dbm.min_order_amount = float(0)
            else:
                dbm.min_order_amount = float(request.POST.get('min_order_amount'))

            if request.POST['max_outstanding_threshold'] is None or request.POST['max_outstanding_threshold'] == '':
                dbm.max_outstanding_threshold = float(0)
            else:
                dbm.max_outstanding_threshold = float(request.POST.get('max_outstanding_threshold'))

            if request.POST['credit_days'] is None or request.POST['credit_days'] == '':
                dbm.credit_days = int(0)
            else:
                dbm.credit_days = int(request.POST.get('credit_days'))
            dbm.save()

            sr_user = User.objects.get(id=request.POST['sr_code'])
            try:
                SRD = SRDealer.objects.get(dealer_id=dealer)
                # SRD = SRDealer.objects.get(id=request.GET.get('sr'))
            except SRDealer.DoesNotExist:
                SRD = SRDealer()
            SRD.dealer_id = dealer
            SRD.sr_id = sr_user
            SRD.save()

            try:
                DB = DealersBrand.objects.get(brand=brand, user=user, is_active=True)
            except DealersBrand.DoesNotExist:
                DB = DealersBrand()
            DB.user = user
            DB.brand = brand
            DB.save()

            slab = CashDiscountSlabsMaster.objects.get(pk=request.POST['cd_slab_code'])
            try:
                scheme = CashDiscountSchemeMaster.objects.get(slab=slab, dealer=dealer.dealer)
            except CashDiscountSchemeMaster.DoesNotExist:
                scheme = CashDiscountSchemeMaster()
            scheme.dealer = dealer.dealer
            scheme.slab = slab
            scheme.save()

            messages.add_message(request, messages.SUCCESS, 'Dealer updated successfully...')
        else:
            update = {
                'state': instance.dealer.state,
                'city': instance.dealer.city,
                'gst_no': instance.gst_no,
                'pan_no': instance.pan_no,
                'Aadhaar_No': instance.Aadhaar_No,
                'state_list': models.StateMaster.objects.filter(is_active=True, is_deleted=False),
                'city_list': models.CityMaster.objects.filter(
                    state_id__state_code=instance.dealer.state,
                    is_active=True, is_deleted=False),
                'min_order_amount': request.POST['min_order_amount'],
                'max_outstanding_threshold': request.POST['max_outstanding_threshold'],
                'credit_days': request.POST['credit_days'],
            }
            context = {
                'form': form_class,
                'dealer': instance,
                'update': update,
                'sr_list': User.objects.filter(groups__name='SR', is_active=True).order_by('-pk'),
                'sr': SRDealer.objects.get(dealer_id=instance).sr_id
                # 'sr': SRDealer.objects.get(id=request.GET.get('sr')).sr_id
            }
            return render(request, 'dealer/dealer_create.html', context=context)
        return redirect('/user?page_style=list')


def delete_dealer(request, Did):
    dealer = DealerProfile.objects.get(profile_id=Did, is_active=True, is_deleted=False)

    if OrderHead.objects.filter(dealer=dealer.dealer).exists():
        messages.add_message(request, messages.ERROR, 'Dealer cannot be deleted!!!')
    else:
        # user
        dealerUser = User.objects.get(pk=dealer.dealer.pk)
        dealerUser.is_active = False
        dealerUser.save()

        # dealer Profile
        dealer.is_active = False
        dealer.is_deleted = True
        dealer.save()

        # dealer business master
        dealer_business_master = DealerBusinessMaster.objects.filter(
            dealer=dealer.dealer, is_active=True, is_deleted=False)
        for dbm in dealer_business_master:
            dbm.is_active = False
            dbm.is_deleted = True
            dbm.save()

        # Dealer Brand
        dealerBrands = DealersBrand.objects.filter(
            user=dealerUser, is_active=True, is_deleted=False)
        for dealer_brand in dealerBrands:
            dealer_brand.is_active = False
            dealer_brand.is_deleted = True
            dealer_brand.save()

        # cash discount scheme
        cash_discount_scheme = CashDiscountSchemeMaster.objects.filter(
            dealer=dealer.dealer, is_active=True, is_deleted=False)
        for scheme in cash_discount_scheme:
            scheme.is_active = False
            scheme.is_deleted = True
            scheme.save()

        messages.add_message(request, messages.SUCCESS, 'Dealer deleted successfully...')
    return redirect('/user/?page_style=list')


def create_dealer_profile(request):
    form_class = forms.DealerProfileForm
    sr_list = User.objects.filter(groups__name='SR', is_active=True).order_by('-pk')
    if request.method == 'GET':
        context = {'form': form_class, 'sr_list': sr_list}
        return render(request, 'dealer/dealer_create.html', context=context)
    elif request.method == 'POST':
        try:
            userobj=User.objects.get(user_code=request.POST.get('dealer_code'),is_active=True)
        except Exception:
            userobj=''
        if userobj=='':
            if request.POST['city_code']:
                city = models.CityMaster.objects.get(city_id=request.POST['city_code'])
            form_class = forms.DealerProfileForm(request.POST)
            if form_class.is_valid():
                try:
                    user = User()
                    if len(request.POST["owner_name"].split(' ')) > 1:
                        first_name = request.POST["owner_name"].split(' ')[0]
                        last_name = request.POST["owner_name"].split(' ')[1]
                    else:
                        first_name = request.POST["owner_name"].split(' ')[0]
                        last_name = ''
                    user.first_name = first_name
                    user.last_name = last_name
                    user.mobile = request.POST["primary_contact"]
                    user.email = request.POST["email"]
                    user.set_password(form_class.cleaned_data['primary_contact'])
                    user.user_code = request.POST['dealer_code']
                    user.city = city.city_code
                    user.state = city.state_id.state_code
                    user.zip_code = request.POST["pincode"]
                    user.address_line1 = request.POST["company_address"]
                    user.address_line2 = request.POST["shipping_address"]
                    user.save()

                    my_group = Group.objects.filter(name='DEL').first()
                    if my_group:
                        my_group.user_set.add(user)
                    else:
                        Group.objects.get_or_create(name='DEL')
                        my_group = Group.objects.filter(name='DEL').first()
                        my_group.user_set.add(user)

                    dealer = form_class.save(commit=False)
                    dealer.dealer = user
                    dealer.dealer_code = user.user_code
                    dealer.email = request.POST.get('email')
                    dealer.created_by = request.user
                    dealer.save()

                    brand = BrandMaster.objects.get(brand_code=request.POST.get('brand'), is_active=True)

                    try:
                        dbm = DealerBusinessMaster.objects.get(brand=brand, dealer=dealer.dealer, is_active=True)
                    except DealerBusinessMaster.DoesNotExist:
                        dbm = DealerBusinessMaster()
                    dbm.dealer = dealer.dealer
                    dbm.brand = brand
                    dbm.created_by = request.user
                    if request.POST['min_order_amount'] is None or request.POST['min_order_amount'] == '':
                        dbm.min_order_amount = float(0)
                    else:
                        dbm.min_order_amount = float(request.POST.get('min_order_amount'))

                    if request.POST['max_outstanding_threshold'] is None or request.POST['max_outstanding_threshold'] == '':
                        dbm.max_outstanding_threshold = float(0)
                    else:
                        dbm.max_outstanding_threshold = float(request.POST.get('max_outstanding_threshold'))

                    if request.POST['credit_days'] is None or request.POST['credit_days'] == '':
                        dbm.credit_days = int(0)
                    else:
                        dbm.credit_days = int(request.POST.get('credit_days'))
                    dbm.save()

                    sr_user = User.objects.get(id=request.POST['sr_code'])
                    try:
                        # SRD = SRDealer.objects.get(dealer_id=dealer, sr_id=sr_user)
                        SRD = SRDealer.objects.get(dealer_id=dealer)
                    except SRDealer.DoesNotExist:
                        SRD = SRDealer()
                    SRD.dealer_id = dealer
                    SRD.sr_id = sr_user
                    SRD.save()

                    try:
                        DB = DealersBrand.objects.get(brand=brand, user=user, is_active=True)
                    except DealersBrand.DoesNotExist:
                        DB = DealersBrand()
                    DB.user = user
                    DB.brand = brand
                    DB.save()

                    try:
                        slab = CashDiscountSlabsMaster.objects.get(pk=request.POST['cd_slab_code'])
                        scheme = CashDiscountSchemeMaster.objects.get(slab=slab, dealer=dealer.dealer)
                    except CashDiscountSchemeMaster.DoesNotExist:
                        scheme = CashDiscountSchemeMaster()
                    scheme.dealer = dealer.dealer
                    scheme.slab = slab
                    scheme.save()

                    messages.add_message(request, messages.SUCCESS, 'Dealer created successfully...')
                except:
                    if request.POST.get('sr_code'):
                        sr = User.objects.get(id=request.POST.get('sr_code', None))
                    else:
                        sr = ''
                    context = {'form': form_class, 'sr_list': sr_list, 'sr': sr}
                    return render(request, 'dealer/dealer_create.html', context=context)
            else:
                if request.POST.get('sr_code'):
                    sr = User.objects.get(id=request.POST.get('sr_code', None))
                else:
                    sr = ''
                context = {'form': form_class, 'sr_list': sr_list, 'sr': sr}
                return render(request, 'dealer/dealer_create.html', context=context)
            return redirect('/user/?page_style=list')
        else:
            messages.add_message(request, messages.ERROR, 'Dealer Code already Exists!!!')
            context = {'form': form_class, 'sr_list': sr_list}
            return render(request, 'dealer/dealer_create.html', context=context)

# def dealer_address(request):
#     dealer_address = models.DealerAddress.objects.filter(is_active=True,is_deleted=False)
#     return render(request, 'dealer_address.html', {'dealer_address': dealer_address})
#
#
# def create_dealer_address(request):
#     form_class = forms.DealerAddressForm
#     if request.method == 'GET':
#         return render(request, 'dealer_address_create.html', {'form': form_class})
#     elif request.method == 'POST':
#         form_class = forms.DealerAddressForm(request.POST)
#         if form_class.is_valid():
#             dealer_address = form_class.save(commit=False)
#             dealer_address.created_by = request.user
#             dealer_address.save()
#         return render(request, 'dealer_address_create.html', {'form': form_class})
#     elif request.method == 'PUT':
#         form_class = forms.DealerAddressForm(request.POST)
#         if form_class.is_valid():
#             dealer_address = form_class.save(commit=False)
#             dealer_address.created_by = request.user
#             dealer_address.save()
#         return render(request, 'dealer_address_create.html', {'form': form_class})
#


def sr_dealer(request):
    sr_dealer = models.SRDealer.objects.filter(is_active=True,is_deleted=False)
    return render(request, 'sr_dealer/sr_dealer.html', {'sr_dealer': sr_dealer})


def create_sr_dealer(request):
    form_class = forms.SRDealerForm
    if request.method == 'GET':
        return render(request, 'sr_dealer/sr_dealer_create.html', {'form': form_class})
    elif request.method == 'POST':
        form_class = forms.SRDealerForm(request.POST)
        if form_class.is_valid():
            sr_dealer = form_class.save(commit=False)
            sr_dealer.created_by = request.user
            sr_dealer.save()
        return render(request, 'sr_dealer/sr_dealer_create.html', {'form': form_class})
    elif request.method == 'PUT':
        form_class = forms.SRDealerForm(request.POST)
        if form_class.is_valid():
            sr_dealer = form_class.save(commit=False)
            sr_dealer.created_by = request.user
            sr_dealer.save()
        return render(request, 'sr_dealer/sr_dealer_create.html', {'form': form_class})


def edit_sr_dealer(request, srid):
    if request.method == 'GET':
        instance = get_object_or_404(SRDealer, pk=srid)
        form = forms.SRDealerForm(request.POST or None, instance=instance)
        return render(request, 'sr_dealer/sr_dealer_create.html', {'form': form})


def dealer_documents(request):
    dealer_documents = models.UserDocuments.objects.filter(is_active=True,is_deleted=False)
    return render(request, 'dealer_documents/dealer_documents.html', {'dealer_documents': dealer_documents})


def create_dealer_documents(request):
    form_class = forms.DealerDocumentsForm
    if request.method == 'GET':
        return render(request, 'dealer_documents/dealer_documents_add.html', {'form': form_class})
    elif request.method == 'POST':
        form_class = forms.DealerDocumentsForm(request.POST)
        if form_class.is_valid():
            dealer_doc = form_class.save(commit=False)
            dealer_doc.created_by = request.user
            dealer_doc.save()
        return render(request, 'dealer_documents/dealer_documents_add.html', {'form': form_class})
    elif request.method == 'PUT':
        form_class = forms.DealerDocumentsForm(request.POST)
        if form_class.is_valid():
            dealer_doc = form_class.save(commit=False)
            dealer_doc.created_by = request.user
            dealer_doc.save()
        return render(request, 'dealer_documents/dealer_documents_add.html', {'form': form_class})


def addcart(request):
    try:
        orderheadobj=OrderHead.objects.get(dealer_id=request.user.id,status='Draft')
        orderheadobj=orderheadobj.pk
    except Exception:
        orderheadobj=''
    result=[]
    ocount=0
    PCM_id=0
    totalmrp=0
    orderdetailobj=''
    if orderheadobj!='':
        orderdetailobj=OrderDetails.objects.filter(order_id=orderheadobj)

    for obj in orderdetailobj:
        try:
            objpm=ProductMaster.objects.get(sku_code=obj.sku_code)
        except Exception:
            objpm=''
        jsondata={}
        no=int(objpm.product_category_id)-5
        if no<0:
            no=0
        count=5
        datas=''
        category = ProductCategoryMaster.objects.filter(is_active=True,is_deleted=False)
        get_product=[]
        for PC in category:
            #get_category={}
            CID=objpm.product_category_id
            PCM=ProductCategoryMaster.objects.get(pk=CID)
            get_product.append(PCM.category_name)
            PID=PCM.parent
            try:
                PCM=ProductCategoryMaster.objects.get(pk=PID)
            except Exception:
                PCM=''
            if PCM!='':
                get_product.append(PCM.category_name)
                PID=PCM.parent
            try:
                PCM=ProductCategoryMaster.objects.get(pk=PID)
            except Exception:
                PCM=''
            if PCM!='':
                get_product.append(PCM.category_name)
                PID=PCM.parent
            try:
                PCM=ProductCategoryMaster.objects.get(pk=PID)
            except Exception:
                PCM=''
            if PCM!='':
                get_product.append(PCM.category_name)
                PID=PCM.parent
            try:
                PCM1=ProductCategoryMaster.objects.get(pk=PID)
            except Exception:
                PCM1=''
            if PCM1!='':
                get_product.append(PCM1.category_name)
                PID=PCM1.parent
            break
        get_product.reverse()
        for i in get_product:
            datas+=str(i)+','
        jsondata['name']=datas.rstrip(',')
        jsondata['skucode']=obj.sku_code
        jsondata['orderobjid']=obj.pk
        jsondata['totalorder']=obj.quantity
        jsondata['mrp']=float(objpm.max_retail_price)*float(obj.quantity)
        jsondata['totalcount']=ocount
        totalmrp+=float("{:.2f}".format(jsondata['mrp']))
        ocount+=1
        result.append(jsondata)

    return [result, float("{:.2f}".format(totalmrp)), ocount]


def customer_dashboard(request):
    listdata = addcart(request)
    if not listdata:
        totalamt=0
        totalcount=0
        listobj=''
    else:
        totalamt=listdata[1]
        totalcount=listdata[2]
        listobj=listdata[0]
    dealer = User.objects.filter(pk=request.user.id,groups__name="DEL",is_active=True).values_list('pk')
    total_products_count = ProductMaster.objects.filter(is_active=True, is_deleted=False).count()
    order_head=OrderHead.objects.filter(
            dealer_id__in=dealer,
            status__in=status_in.AdminStatus(),is_active=True,is_deleted=False).order_by('-order_date').exclude(order_date__exact=None)
    orderheadnull = OrderHead.objects.filter(
            dealer_id__in=dealer,order_date=None, status__in=status_in.SRHeadNullStatus(),is_active=True,is_deleted=False).order_by('-order_date')
       
    order_head=list(chain(order_head,orderheadnull))
    for obj in order_head:
        dealerProfile = DealerProfile.objects.filter(
            dealer=obj.dealer, is_deleted=False).first()
        dealerBusiness = DealerBusinessMaster.objects.filter(
            brand=dealerProfile.brand, dealer=dealerProfile.dealer,
            is_active=True, is_deleted=False).first()
        obj.__dict__['creditdays']= dealerBusiness
        # ........................
        try:
            orderPlaced = OrderEventHistory.objects.get(order_id=obj, order_status='Dispatched')
            today = datetime.date.today()
            order_placed_day = orderPlaced.created_on
            diff = today - order_placed_day.date()
            days = diff.days

            credit_days = dealerBusiness.credit_days
            remaining_days = credit_days - days
            if remaining_days < 0:
                remaining_days = 0
            dealerBusiness.credit_days = remaining_days
            obj.__dict__['dispatch_date'] = orderPlaced.created_on
        except Exception:
            pass
    total_outstanding_amount = OrderHead.objects.filter(
            dealer_id__in=dealer,
            status__in=['Dispatched','Delivered','PartialDispatched'],
        is_active=True,is_deleted=False).aggregate(Sum('payable_amount'))
    if total_outstanding_amount['payable_amount__sum'] is None:
        total_outstanding_amount['payable_amount__sum']=0.00
    total_order_count = OrderHead.objects.filter(
            dealer_id__in=dealer,
            status__in=status_in.SRHeadActiveStatus(),is_active=True, is_deleted=False).count()
    querysetcount = order_head
    if len(order_head)>10:
        order_head=order_head[:10]
    try:
        objdbm = DealerBusinessMaster.objects.get(dealer_id__in=dealer, is_active=True)
    except Exception as e:
        objdbm = 0

    target = DealerSalesTarget.objects.filter(dealer_id__in=dealer, is_active=True, is_deleted=False)
    get_target = []
    for DST in target:
        category = None
        order_price = 0
        monthly_target = DST.monthly_target
        placed_orders = OrderHead.objects.filter(status__in=['OrderPlaced','PaymentDone','InWarehouse','Dispatched'], dealer_id__in=dealer)
        if len(placed_orders)>0:
            for placed_order in placed_orders:
                order_detail = OrderDetails.objects.filter(order_id_id=placed_order.pk,created_on__month=DST.created_on.month,created_on__year=DST.created_on.year)
                for order_detail in order_detail:
                    if order_detail.product_id:
                        category = ProductCategoryMaster.objects.filter(is_active=True,is_deleted=False).values_list('category_name')
                        category = [li[0] for li in category]
                        order_price += placed_order.total_amount

        if category is not None and DST.category.category_name in category:
            complete_target=order_price
            percent=(order_price*100)/monthly_target
        else:
            percent=0
            complete_target=0
            try:
                target=DealerSalesTarget.objects.get(dealer_id=DST.dealer_id,category_id=DST.category_id,created_on__month=datetime.datetime.now().month,created_on__year=datetime.datetime.now().year,is_active=True, is_deleted=False)
            except Exception:
                target=DealerSalesTarget()
                target.monthly_target=float(DST.monthly_target)
                target.brand=DST.brand
                target.category=DST.category
                target.dealer=DST.dealer
                target.created_on=datetime.datetime.now()
                target.save()
    target_mo=DealerSalesTarget.objects.filter(dealer_id__in=dealer,created_on__month=datetime.datetime.now().month,created_on__year=datetime.datetime.now().year,is_active=True, is_deleted=False)
    for DST in target_mo:
        DST_gt={}
        category = None
        order=0
        order_price = 0
        monthly_target = DST.monthly_target
        placed_orders = OrderHead.objects.filter(status__in=['OrderPlaced','PaymentDone','InWarehouse','Dispatched'], dealer_id__in=dealer,order_date__month=DST.created_on.month,order_date__year=DST.created_on.year)
        category = ProductCategoryMaster.objects.filter(pk=DST.category_id,is_active=True,is_deleted=False)
        get_product=[]
        for PC in category:
            p_id=PC.pk
            product_category=ProductCategoryMaster.objects.filter(parent=p_id,is_active=True,is_deleted=False)
            for product_category in product_category:
                get_product.append(product_category.pk)
                product_category=ProductCategoryMaster.objects.filter(parent=product_category.pk,is_active=True,is_deleted=False)
                for product_category in product_category:
                    get_product.append(product_category.pk)
                    product_category=ProductCategoryMaster.objects.filter(parent=product_category.pk,is_active=True,is_deleted=False)
                    for product_category in product_category:
                        get_product.append(product_category.pk)
                        product_category=ProductCategoryMaster.objects.filter(parent=product_category.pk,is_active=True,is_deleted=False)
                        for product_category in product_category:
                            get_product.append(product_category.pk)
            get_product.reverse()
        if len(placed_orders)>0:
            for i in get_product:
                for placed_order in placed_orders:
                    order_detail = OrderDetails.objects.filter(order_id_id=placed_order.pk,created_on__month=DST.created_on.month,created_on__year=DST.created_on.year,product_id__product_category_id=i)
                    for order_detail in order_detail:
                        if order_detail.product_id:
                            category = ProductCategoryMaster.objects.filter(is_active=True,is_deleted=False).values_list('category_name')
                            category = [li[0] for li in category]
                            ctaxdata=order_detail.cgst_rate
                            staxdata=order_detail.sgst_rate
                            total_tax_percent = ctaxdata + staxdata
                            c_percent = 100 * (float(ctaxdata)/float(total_tax_percent))
                            s_percent = 100 * (float(staxdata)/float(total_tax_percent))
                            prg = int(order_detail.total_amount) / (1 + (float(ctaxdata) / 100) + (float(staxdata) / 100))
                            total_tax = int(order_detail.total_amount) - prg
                            cgstobj = (c_percent / 100) * total_tax
                            cgst = float("{:.2f}".format(cgstobj))
                            sgstobj = (s_percent / 100) * total_tax
                            sgst = float("{:.2f}".format(sgstobj))
                            #pregst = float("{:.2f}".format(prg))
                            totalbill=total_tax
                            dealeramt=int(order_detail.total_amount)-int(totalbill)
                            order+=dealeramt
            if category is not None and DST.category.category_name in category:
                complete_target=order
                percent=(complete_target*100)/monthly_target
            else:
                percent=0
                complete_target=0
        DST_gt['monthly_target']=monthly_target
        DST_gt['created_on']=DST.created_on
        if order!=0:
            DST_gt['complete_target']=complete_target
            DST_gt['percent']=percent
        else:
            DST_gt['complete_target']=0
            DST_gt['percent']=0
        DST_gt['category'] = DST.category.category_name
        if int(percent)<10:
            DST_gt['color']='red'
        elif int(percent)<50 and int(percent)>=10:
            DST_gt['color']='blue'
        elif int(percent)>50:
            DST_gt['color']='green'
        get_target.append(DST_gt)
    pay=OrderHead.objects.filter(Q(status='Confirm')|Q(status='Delivered'),dealer_id__in=dealer).aggregate(Sum('total_amount'))
    objdst=DealerSalesTarget.objects.filter(dealer_id__in=dealer,created_on__month=datetime.datetime.now().month,created_on__year=datetime.datetime.now().year,is_active=True, is_deleted=False).aggregate(Sum('monthly_target'))
    if pay['total_amount__sum']==None:
        pay['total_amount__sum']=0
    if objdst['monthly_target__sum']==None:
        objdst['monthly_target__sum']=0
    if total_outstanding_amount["payable_amount__sum"] < 0:
        total_outstanding_amount["payable_amount__sum"] = 0.0

    try:
        maxoutstanding = DealerBusinessMaster.objects.get(dealer_id__in=dealer, is_active=True)
    except Exception:
        maxoutstanding = None
    if maxoutstanding:
        orderheadamt = OrderHead.objects.filter(
            dealer_id__in=dealer, status__in=status_in.OutstandingStatus(), is_active=True).aggregate(Sum('payable_amount'))
        if orderheadamt['payable_amount__sum'] is None:
            orderheadamt['payable_amount__sum'] = 0
        else:
            orderheadamt['payable_amount__sum'] = float(orderheadamt['payable_amount__sum'])
        max_outstanding_threshold = float(maxoutstanding.max_outstanding_threshold)
    else:
        max_outstanding_threshold = 0
    """ IF outstanding is negative """

    try:
        prevDealerLedger = Ledger.objects.filter(
            ledger_type="Unadjusted", dealer_id__in=dealer,
            is_active=True, is_deleted=False).latest('pk')
        balance = float(prevDealerLedger.balance)
    except:
        balance = float(0)
    overdue = 0
    if balance < 0:
        overdue = balance

    remain_percent = 0
    acheived_percent = 0
    hold_percent =0

    acheived_percent = (orderheadamt['payable_amount__sum'] / max_outstanding_threshold) * 100
    orderheadblockamt = OrderHead.objects.filter(
        dealer_id__in=dealer, status__in=['Processing', 'OrderPlaced','SchemeApplied','InWarehouse'], is_active=True).aggregate(
        Sum('payable_amount'))
    if orderheadblockamt['payable_amount__sum'] is None:
        orderheadblockamt['payable_amount__sum'] = 0
    else:
        orderheadblockamt['payable_amount__sum'] = float(orderheadblockamt['payable_amount__sum'])
    hold_percent = (orderheadblockamt['payable_amount__sum'] / max_outstanding_threshold) * 100
    out_remain = float(total_outstanding_amount["payable_amount__sum"])
    if out_remain < 0:
        out_remain = 0
    target_remain = float(max_outstanding_threshold) - out_remain - \
                    orderheadblockamt['payable_amount__sum']  - float(overdue)
    remain_percent = (target_remain / max_outstanding_threshold) * 100
    context={
        'objdst': objdst,
        'objdbm': objdbm,
        'orderhead': order_head,
        'get_target': get_target,
        'orderheadactive': total_order_count,
        'outstanding': total_outstanding_amount["payable_amount__sum"],
        'totalamt': totalamt,
        'totalcount': totalcount,
        'listdata': listobj,
        'Product': total_products_count,
        'querysetcount':querysetcount,
        'target_acheived': out_remain,
        'target_remain': target_remain,
        'credit_limit': max_outstanding_threshold,
        'remain_percent': remain_percent,
        'acheived_percent':acheived_percent,
        'hold_percent': hold_percent,
        'target_block': orderheadblockamt['payable_amount__sum']
    }
    return render(request,'dealer/customer_dashboard.html',context)


def customer_order_api(request,getskucode,gettxtvalue):
    msg=''
    try:
        userobj=User.objects.get(pk=request.user.id,groups__name='DEL')
    except Exception:
        userobj=''
    try:
        dealerobj=DealerProfile.objects.get(dealer_id=userobj.pk,is_active=True)
    except Exception:
        dealerobj=''
    try:
        srdealerobj=SRDealer.objects.get(dealer_id_id=dealerobj.pk)
    except Exception:
        srdealerobj=''
    if userobj!='' and dealerobj!='' and srdealerobj!='':
        result=[]

        try:
            DB=DealersBrand.objects.get(is_active=True,user_id=request.user.id)
        except Exception:
            DB=''
        try:
            objpm=ProductMaster.objects.get(sku_code=getskucode)
        except Exception:
            pass
        try:
            taxdata=TaxSlabMaster.objects.get(tax_slab_id=objpm.TaxSlabCode_id)
        except Exception:
            taxdata=''
        if taxdata!='':
            ctaxdata=taxdata.cgst_rate
            staxdata=taxdata.sgst_rate
            otaxdata=taxdata.other_tax_rate
            gtaxdata=taxdata.gst_rate
        else:
            ctaxdata=0
            staxdata=0
            otaxdata=0
            gtaxdata=0
        brand=str(objpm.brand)
        skucode=objpm.sku_code
        quantity=gettxtvalue
        mrp=float(objpm.max_retail_price)*float(gettxtvalue)
        dp=float(objpm.dealer_price)*float(gettxtvalue)
        try:
            scheme=SchemeProductMaster.objects.get(product=objpm.product_id)
        except Exception:
            pass
        try:
            allowanceobj=AllowanceMaster.objects.get(dealer=request.user.id,scheme_category_code=scheme.scheme_category_code)
            allowance=allowanceobj.discount_percent
        except Exception:
            allowance=0
        allowanceobj=float(dp)/100*float(allowance)
        valueafterallowance=float(dp)-float(allowanceobj)
        total_tax_percent = ctaxdata + staxdata
        c_percent = 100 * (float(ctaxdata)/float(total_tax_percent))
        s_percent = 100 * (float(staxdata)/float(total_tax_percent))
        prg = valueafterallowance / (1 + (float(ctaxdata) / 100) + (float(staxdata) / 100))
        total_tax = valueafterallowance - prg
        cgstobj = (c_percent / 100) * total_tax
        cgst = float("{:.2f}".format(cgstobj))
        sgstobj = (s_percent / 100) * total_tax
        sgst = float("{:.2f}".format(sgstobj))
        pregst = float("{:.2f}".format(prg))
        totalbill=pregst+cgst+sgst
        #pregst=float("{:.2f}".format(prg))
        #valueafterallowance=pregst/ (1 + sgst/100 + cgst/100)
        try:

            maxoutstanding=DealerBusinessMaster.objects.filter(dealer_id=request.user.id, is_active=True).last()

        except Exception:
            maxoutstanding=''
        if maxoutstanding != '':
            orderheadamt = OrderHead.objects.filter(dealer_id=request.user.id,is_active=True,
                                            status__in=status_in.SRHeadActiveStatus()).aggregate(Sum('payable_amount'))
            if orderheadamt['payable_amount__sum'] is None:
                orderheadamt['payable_amount__sum']=0+totalbill
            else:
                orderheadamt['payable_amount__sum']=float(orderheadamt['payable_amount__sum'])+totalbill
            max_outstanding_threshold = float(maxoutstanding.max_outstanding_threshold)
            if max_outstanding_threshold >= orderheadamt['payable_amount__sum']:
                try:
                    OHobj=OrderHead.objects.get(dealer_id=request.user.id,status='Draft')
                except Exception:
                    OHobj=''
                if OHobj!='':
                    OHobj=OrderHead.objects.get(dealer_id=request.user.id,status='Draft')
                    OHobj.dealer=request.user
                    if DB!='':
                        OHobj.brand=BrandMaster.objects.get(brand_code=DB.brand_id,is_active=True)
                    item = OrderDetails.objects.get(order_id=OHobj.order_id, sku_code=getskucode)
                    OHobj.sr_id=srdealerobj.sr_id
                    OHobj.status='Draft'
                    # OHobj.discount_amount=float(OHobj.discount_amount)+valueafterallowance
                    # OHobj.total_amount=float(OHobj.total_amount)+valueafterallowance
                    # OHobj.payable_amount=float(OHobj.payable_amount)+valueafterallowance
                    OHobj.discount_amount=0
                    OHobj.total_amount = float(OHobj.total_amount) + totalbill - float(item.total_amount)
                    OHobj.payable_amount = float(OHobj.payable_amount) + totalbill - float(item.total_amount)
                    OHobj.save()
                try:
                    orderdetail=OrderDetails.objects.get(sku_code=getskucode,order_id=OHobj.pk)
                except Exception:
                    orderdetail=''
                if orderdetail!='':
                    orderdetail.sku_code=getskucode
                    product=ProductMaster.objects.get(sku_code=getskucode)
                    orderdetail.size_capacity=product.size_capacity
                    orderdetail.size_capacity_unit=product.size_capacity_unit
                    # orderdetail.dealer_price=float(orderdetail.dealer_price)+dp
                    orderdetail.dealer_price=dp
                    orderdetail.gst_rate=float(gtaxdata)
                    orderdetail.sgst_rate=float(staxdata)
                    orderdetail.cgst_rate=float(ctaxdata)
                    orderdetail.other_tax_rate=float(otaxdata)
                    if product.pattern_color!='' and product.pattern_color!=None:
                        orderdetail.pattern_color=product.pattern_color
                    if product.product_description_feature!='' and product.product_description_feature!=None:
                        orderdetail.product_description_feature=product.product_description_feature
                    orderdetail.best_buying_price=float(product.best_buying_price)
                    orderdetail.product_id=product
                    #orderdetail.max_retail_price=float(orderdetail.max_retail_price)+mrp
                    orderdetail.max_retail_price=mrp
                    #orderdetail.quantity=int(orderdetail.quantity)+int(gettxtvalue)
                    orderdetail.quantity=int(gettxtvalue)
                    orderdetail.discount_percent_per_unit=0.0
                    #orderdetail.total_amount=float(orderdetail.total_amount)+valueafterallowance
                    orderdetail.total_amount=totalbill
                    orderdetail.order_id=OHobj
                    orderdetail.save()
                if orderdetail!='':
                    orderdetailid=orderdetail.pk
                    queryorder=orderdetail
                try:
                    OrderAllowance=OrderAllowanceDetails.objects.get(sku_code=getskucode,order_detail_id=orderdetailid,order_id=OHobj.pk)
                except Exception:
                    OrderAllowance=''
                if OrderAllowance!='':
                    OrderAllowance.order_detail_id=queryorder
                    OrderAllowance.product_id=product
                    OrderAllowance.sku_code=getskucode
                    OrderAllowance.order_id=OHobj
                try:
                    schemeobj=SchemeProductMaster.objects.get(product_id=product.pk)
                except Exception:
                    schemeobj=''
                if schemeobj!='':
                    try:
                        allo=AllowanceMaster.objects.get(scheme_category_code_id=schemeobj.scheme_category_code,dealer_id=request.user.id)
                    except Exception:
                        allo=''
                    if allo!='':
                        if allo.discount_percent!=None and allo.discount_percent!='' and allo.discount_percent!=0:
                            OrderAllowance.allowance_percent=allo.discount_percent
                        elif allo.discount_amount!=None and allo.discount_amount!='' and allo.discount_amount!=0:
                            OrderAllowance.allowance_amount=allo.discount_amount
                            OrderAllowance.allowance_percent=0
                        else:
                            OrderAllowance.allowance_amount=0
                            OrderAllowance.allowance_percent=0
                    else:
                        OrderAllowance.allowance_amount=0
                        OrderAllowance.allowance_percent=0
                else:
                    OrderAllowance.allowance_amount=0
                    OrderAllowance.allowance_percent=0
                OrderAllowance.save()
            else:
                msg='You have exceeded  your max credit limit ('+str(maxoutstanding.max_outstanding_threshold)+'), Kindly clear previous outstanding to Process this order.Contact your SR ('+srdealerobj.sr_id.first_name+' '+srdealerobj.sr_id.last_name +' and '+str(srdealerobj.sr_id.mobile)+') for further clarification'
    return msg


import datetime
import pytz


def customer_order_detail(request):
    listdata=addcart(request)
    if not listdata:
        totalamt=0
        totalcount=0
        listobj=''
    else:
        totalamt=listdata[1]
        totalcount=listdata[2]
        listobj=listdata[0]
    orderno=request.GET.get('orderno')
    dealerid=request.GET.get('dealerid')
    getdealerid=0
    msg=''
    if orderno!='' and orderno!=None and dealerid!='' and dealerid!=None:
        getdealerid=dealerid
        try:

            orderhead=OrderHead.objects.get(
                dealer_id=dealerid,order_number=int(orderno),
                status__in=['Draft', 'Dispatched', 'InWarehouse', 'Processing', 'SchemeApplied', 'OrderPlaced',
                            'Deleted', 'PaymentDone','OnHold'])
        except Exception:
            orderhead=''
            orderno=''
    else:
        getdealerid=request.user.id
        orderno=''
        try:
            orderhead=OrderHead.objects.get(dealer_id=request.user.id,status='Draft')
        except Exception:
            orderhead=''
    if orderno=='':
        dealer=request.GET.get('dealer_id')
        try:
            orderheadlist=OrderHead.objects.filter(
                dealer_id=int(dealer),status__in=['Processing', 'SchemeApplied', 'OrderPlaced', 'Deleted']).values_list('order_id')
            #orderhead.order_id=
        except Exception:
            orderheadlist=''
            dealer=''
    result=[]
    count=0
    totaldp=0
    totalafterallowance=0
    totalpregst=0
    totalsgst=0
    totalcgst=0
    totalbill=0
    totalbill1=0
    pre_gst1=0
    ordernote=''
    orderdetail=''
    comment=[]
    if orderhead!='':
        orderdetail=OrderDetails.objects.filter(order_id=orderhead.order_id)
        ordernote=OrderNotes.objects.filter(order_id=int(orderhead.order_id),is_active=True).order_by('-id')
        comment=[]
    else:
        orderdetail=OrderDetails.objects.filter(order_id__in=orderheadlist)
        ordernote=OrderNotes.objects.filter(order_id__in=orderheadlist,is_active=True).order_by('-id')
        comment=[]
    if orderhead!='':
        for obj in ordernote:
            try:
                userobj=User.objects.get(pk=obj.created_by,is_active=True)
            except Exception:
                userobj=''
            js={}
            js['id']=obj.id

            try:
                noteobj=OrderNotes.objects.filter(order_id=int(obj.id),is_active=False)
            except Exception:
                noteobj=''
            if noteobj!='':
                notelist=[]
                for a in noteobj:
                    li={}
                    li['queryset']=a
                    pst = pytz.timezone('Asia/Kolkata')
                    abc=a.created_on.replace(tzinfo=pytz.utc).astimezone(pst)
                    aobj=datetime.datetime.strptime(abc.strftime('%Y-%m-%d %I:%M:%S %p'), '%Y-%m-%d %I:%M:%S %p')
                    li['childdate']=aobj#datetime.datetime.strptime(str(abc), '%b,%d,%y %I:%M:%S')
                    try:
                        userobject=User.objects.get(pk=a.created_by,is_active=True)
                    except Exception:
                        userobject=''
                    if userobject!='':
                        li['createbyname']=str(str(userobject.first_name)+' '+str(userobject.last_name))
                    else:
                        li['createbyname']=''
                    if userobject != '':
                        if userobject.avatar!='' and userobject.avatar!=None:
                            li['image']=userobject.avatar
                        else:
                            li['image']=''
                    else:
                        li['image']=''
                    notelist.append(li)
                js['childnote']=notelist
            else:
                js['childnote']=noteobj
            js['createby']=int(obj.created_by)
            if userobj!='':
                js['name']=str(str(userobj.first_name)+' '+str(userobj.last_name))
            else:
                js['name']=''
            pst = pytz.timezone('Asia/Kolkata')
            ab=obj.created_on.replace(tzinfo=pytz.utc).astimezone(pst)
            aaa=datetime.datetime.strptime(ab.strftime('%Y-%m-%d %I:%M:%S %p'), '%Y-%m-%d %I:%M:%S %p')
            js['date']=aaa
            if obj.description!='' and obj.description!=None:
                js['description']=obj.description
            else:
                js['description']=''
            if userobj.avatar!='' and userobj.avatar!=None:
                js['image']=userobj.avatar
            else:
                js['image']=''
            comment.append(js)
    for obj in orderdetail:
        objpm = ProductMaster.objects.get(sku_code=obj.sku_code)


        ctaxdata = obj.cgst_rate
        staxdata = obj.sgst_rate
        otaxdata = obj.other_tax_rate
        gtaxdata = obj.gst_rate

        tie_upobj = 0

        jsondata = {
            'pk': objpm.pk,
            'status': orderhead.status,
            'brand': str(obj.order_id.brand),
            'skucode': obj.sku_code,
            'quantity': obj.quantity,
            'mrp': float(obj.max_retail_price)/float(obj.quantity),
            'bbp': float(obj.best_buying_price),
            'dp': float(obj.dealer_price)/float(obj.quantity)
        }
        dpobj = float(obj.dealer_price) / float(obj.quantity)
        totaldp += float(dpobj) * float(obj.quantity)

        try:

            allowanceobj = OrderAllowanceDetails.objects.get(
                order_detail_id=obj, product_id=obj.product_id,
                order_id=obj.order_id, is_active=True, is_deleted=False)
            jsondata['allowance'] = allowanceobj.allowance_percent

        except Exception:
            jsondata['allowance'] = 0
        allowance = float(dpobj)/100*float(jsondata['allowance'])
        jsondata['valueafterallowance'] = (float(dpobj)-float(allowance))
        # valueafterallowance = (float(dpobj) - float(allowance)) * float(obj.quantity)
        afterallowance = float(dpobj)-float(allowance)
        totalafterallowance += float(jsondata['valueafterallowance'])
        total_tax_percent = ctaxdata + staxdata
        c_percent = 100 * (float(ctaxdata)/float(total_tax_percent))
        s_percent = 100 * (float(staxdata)/float(total_tax_percent))

        prg = afterallowance / (1 + (float(ctaxdata) / 100) + (float(staxdata) / 100))
        total_tax = afterallowance - prg

        cgstobj = (c_percent / 100) * total_tax
        cgst = float("{:.2f}".format(cgstobj))
        sgstobj = (s_percent / 100) * total_tax
        sgst = float("{:.2f}".format(sgstobj))
        pregst = float("{:.2f}".format(prg))
        totalamt = pregst+sgst+cgst
        totalamount=pregst+sgst+cgst

        pre_gst_tieup = pregst * float(tie_upobj)/100
        pre_gst = pregst - pre_gst_tieup
        totalb = totalamount - pre_gst_tieup
        totalbill1 += float("{:.2f}".format(totalb))
        pre_gst1 += float("{:.2f}".format(pre_gst))

        jsondata['cgst'] = cgst  #round(float(float(jsondata['valueafterallowance'])/float(avc)))
        totalcgst += float(jsondata['cgst']) * float(obj.quantity)
        jsondata['sgst'] = sgst  #round(float(float(jsondata['valueafterallowance'])/float(av)))
        totalsgst += float(jsondata['sgst']) * float(obj.quantity)
        jsondata['pregst'] = pregst    #float(tax)-float(jsondata['valueafterallowance'])
        totalpregst += float(jsondata['pregst']) * float(obj.quantity)
        jsondata['totalbill'] = float("{:.2f}".format(float(jsondata['pregst'])+float(
            jsondata['cgst'])+float(jsondata['sgst']))) * float(obj.quantity)
        totalbill += float(jsondata['totalbill'])
        jsondata["orderobjid"] = obj.pk
        category = ProductCategoryMaster.objects.filter(is_active=True, is_deleted=False)
        get_product = []
        datas = ''
        for PC in category:
            if objpm.product_category is not None:
                pass
            else:
                continue
            CID = objpm.product_category_id
            PCM = ProductCategoryMaster.objects.get(pk=CID)
            get_product.append(PCM.category_name)
            PID=PCM.parent
            try:
                PCM=ProductCategoryMaster.objects.get(pk=PID)
            except Exception:
                PCM=''
            if PCM!='':
                get_product.append(PCM.category_name)
                PID=PCM.parent
            try:
                PCM=ProductCategoryMaster.objects.get(pk=PID)
            except Exception:
                PCM=''
            if PCM!='':
                get_product.append(PCM.category_name)
                PID=PCM.parent
            try:
                PCM=ProductCategoryMaster.objects.get(pk=PID)
            except Exception:
                PCM=''
            if PCM!='':
                get_product.append(PCM.category_name)
                PID=PCM.parent
            try:
                PCM1=ProductCategoryMaster.objects.get(pk=PID)
            except Exception:
                PCM1=''
            if PCM1!='':
                get_product.append(PCM1.category_name)
                PID=PCM1.parent
            break
        if len(get_product)==4 or len(get_product)==5:
            get_product.reverse()
            count=0
            for a in get_product:
                count+=1
                if count==3 or count==4:
                    datas+=str(a)+','
        jsondata['name']=datas.rstrip(',')
        result.append(jsondata)
        count += 1
    if request.method == 'POST':
        context = {}
        if "on_hold" in request.POST:
            try:
                orderno = request.GET.get('orderno')
                dealerid = request.GET.get('dealerid')
                orderHead = OrderHead.objects.get(order_number=orderno, is_active=True, is_deleted=False)
                orderHead.remarks = request.POST["remarks"]
                orderHead.status = "OnHold"
                orderHead.save()

                # ......... Order History .........
                orderHistory = OrderEventHistory(
                    order_id=orderHead,
                    order_status=orderHead.status,
                    created_by=request.user
                )
                orderHistory.save()
                try:
                    send_sms_or_email(request, orderHead)
                except Exception:
                    pass
                # .................................
                messages.add_message(request, messages.SUCCESS, 'Order Successfully On Hold!')
                return redirect('/user/Customer-Order-Detail?orderno=' + orderno + '&&dealerid=' + str(dealerid))
            except Exception:
                pass

        if 'processorder' in request.POST:
            try:
                orderheadobj = OrderHead.objects.get(dealer_id=request.user.id, status='Draft')
            except Exception:
                orderheadobj = ''
            if orderheadobj != '':
                if orderheadobj.status != 'Processing':
                    orderheadobj.status = 'Processing'
                    orderheadobj.order_date = datetime.datetime.now()
                    orderheadobj.save()

                    # ......... Order History .........
                    orderHistory = OrderEventHistory(
                        order_id=orderheadobj,
                        order_status=orderheadobj.status,
                        created_by=request.user
                    )
                    orderHistory.save()
                    # .................................
                    # send email...
                    try:
                        send_sms_or_email(request, orderheadobj)
                    except Exception:
                        pass
                    # .................................

                    messages.add_message(request, messages.SUCCESS, 'Order Submitted For Scheme')
            return HttpResponseRedirect('/user/Customer-Dashboard')
        if 'commentbtn' in request.POST:
            try:
                orderheadobj=OrderHead.objects.get(
                    dealer_id=request.user.id,order_number=int(orderno),
                    status__in=['Processing', 'SchemeApplied', 'OrderPlaced', 'Deleted','Draft','Dispatched','InWarehouse'])
            except Exception:
                orderheadobj=''
            if orderheadobj!='':
                ordernoteonj=OrderNotes()
                ordernoteonj.order_id=int(orderheadobj.pk)
                ordernoteonj.created_by=request.user.id
                ordernoteonj.description=request.POST.get('ordernote')
                ordernoteonj.save()
            return HttpResponseRedirect('/user/Customer-Order-Detail?orderno='+orderno+'&&dealerid='+str(request.user.id))
        if 'replybtn' in request.POST:
            from django.utils import timezone

            now = timezone.now()
            try:
                orderheadobj=OrderHead.objects.get(
                    dealer_id=request.user.id,order_number=int(orderno),
                    status__in=['Processing', 'SchemeApplied', 'OrderPlaced', 'Deleted','Draft','Dispatched','InWarehouse'])
            except Exception:
                orderheadobj=''
            if orderheadobj!='':
                try:
                    userobj=User.objects.get(pk=request.user.id,groups__name='DEL')
                except Exception:
                    userobj=''
                try:
                    dealerobj=DealerProfile.objects.get(dealer_id=userobj.pk,is_active=True)
                except Exception:
                    dealerobj=''
                try:
                    srdealerobj=SRDealer.objects.get(dealer_id_id=dealerobj.pk)
                except Exception:
                    srdealerobj=''
                if userobj!='' and dealerobj!='' and srdealerobj!='':
                    ordernoteonj=OrderNotes()
                    ordernoteonj.order_id=int(request.POST.get('replybtn'))
                    ordernoteonj.created_by=request.user.id
                    ordernoteonj.dealer_id=request.user.id
                    ordernoteonj.sr_id=srdealerobj.sr_id_id
                    ordernoteonj.description=request.POST.get('ordernote'+request.POST.get('replybtn'))
                    ordernoteonj.is_active=False
                    ordernoteonj.save()
            return HttpResponseRedirect('/user/Customer-Order-Detail?orderno='+orderno+'&&dealerid='+str(request.user.id))
        if 'orderupdate' in request.POST:
            sku_code = request.POST.get('orderupdate')
            quantity = request.POST.get(str(sku_code))
            msg = customer_order_api(request, sku_code, quantity)
            if msg == '':
                if orderno != '':
                    return HttpResponseRedirect('/user/Customer-Order-Detail?orderno='+orderno+'&&dealerid='+str(request.user.id))
                else:
                    return HttpResponseRedirect('/user/Customer-Order-Detail')
            # else:
            #     context["max_order_error"] = True
    context={
        'objpm':result,
        'listdata':listobj,
        'totalamt':totalamt   ,
        'totalcount':totalcount,
        'totaldp':float("{:.2f}".format(totaldp)),
        'totalafterallowance':float("{:.2f}".format(totalafterallowance)),
        'totalpregst':float("{:.2f}".format(totalpregst)),
        'totalsgst':float("{:.2f}".format(totalsgst)),
        'totalcgst':float("{:.2f}".format(totalcgst)),
        'totalbill':float("{:.2f}".format(totalbill)),
        'orderno':orderno,
        'dealerid':dealerid,
        'comment':comment,
        'orderhead':orderhead,
        'msg':msg,
        'totalbill1':totalbill1,
        'pre_gst1':pre_gst1,
        'outstanding_flag': False,
        'min_order_error': False,
        'max_order_error': False,
        'credit_day_expire': False,
        'on_hold_flag': False,
        'on_hold_date': "",
        'edit_flag': False,
    }
    if msg:
        context["edit_flag"] = True
    # ................... Minimum order validation ......................

    groups = [group.name for group in request.user.groups.all()]
    if 'DEL' in groups:
        try:
            userobj = User.objects.get(pk=request.user.id, groups__name='DEL')
        except Exception:
            userobj = ''
        try:
            dealerobj = DealerProfile.objects.get(dealer_id=userobj.pk, is_active=True)
        except Exception:
            dealerobj = ''
        try:
            srdealerobj = SRDealer.objects.get(dealer_id_id=dealerobj.pk)
        except Exception:
            srdealerobj = ''
        try:
            dealerOrderLimit = DealerBusinessMaster.objects.filter(
                dealer_id=request.user.id, is_active=True).first()
        except DealerBusinessMaster.DoesNotExist:
            dealerOrderLimit = None

        if dealerOrderLimit:
            orderAmount = OrderHead.objects.filter(
                dealer_id=request.user.id, is_active=True).aggregate(Sum('total_amount'))
            if orderAmount['total_amount__sum'] is None:
                orderAmount['total_amount__sum'] = float(0)
            else:
                orderAmount['total_amount__sum'] = float(orderAmount['total_amount__sum'])
            minOrderAmount = float(dealerOrderLimit.min_order_amount)
            maxOrderAmount = float(dealerOrderLimit.max_outstanding_threshold)

            """ find the payable of all processing orders"""
            orderheadblockamt = OrderHead.objects.filter(
                dealer_id= request.user.pk, status__in=['Processing', 'SchemeApplied', 'OrderPlaced',
                                                        'InWarehouse'], is_active=True).aggregate(
                Sum('payable_amount'))
            if orderheadblockamt['payable_amount__sum'] is None:
                orderheadblockamt['payable_amount__sum'] = 0
            else:
                orderheadblockamt['payable_amount__sum'] = float(orderheadblockamt['payable_amount__sum'])


            # minimum order condition
            # if minOrderAmount > orderAmount['total_amount__sum']:
            if minOrderAmount > totalbill:
                context["min_amount"] = minOrderAmount
                # context["order_amount"] = orderAmount['total_amount__sum']
                context["order_amount"] = totalbill
                context["min_order_error"] = True
            
            if orderno:
                order_type = OrderHead.objects.get(order_id=orderno)
                if order_type.status != 'Draft':
                    totalbill = 0
            if maxOrderAmount < totalbill + orderheadblockamt['payable_amount__sum']:
                context["max_amount"] = maxOrderAmount
                # context["order_amount"] = orderAmount['total_amount__sum']
                context['sr_name'] = srdealerobj.sr_id.first_name + ' ' + srdealerobj.sr_id.last_name
                context['mobile'] = srdealerobj.sr_id.mobile
                context["order_amount"] = totalbill
                context["max_order_error"] = True

        """======= Dealer Credit Days Expire ========"""
        dealerProfile = DealerProfile.objects.filter(
            dealer=orderhead.dealer, is_deleted=False).first()
        dealerBusiness = DealerBusinessMaster.objects.filter(
            brand=dealerProfile.brand, dealer=dealerProfile.dealer,
            is_active=True, is_deleted=False).first()
        credit_days = dealerBusiness.credit_days
        dispatchedOrder = OrderHead.objects.filter(
            dealer=request.user, status='Dispatched',
            is_active=True, is_deleted=False)
        for dispatch in dispatchedOrder:
            try:
                placedOrder = OrderEventHistory.objects.get(order_id=dispatch, order_status='Dispatched')

                today = datetime.date.today()
                order_dispatch_day = placedOrder.created_on
                diff_of_date = today - order_dispatch_day.date()
                diff_of_days = diff_of_date.days

                remaining_days = credit_days - diff_of_days
                if remaining_days <= 0:
                    context["credit_day_expire"] = True
                    context["expire_outstanding_amount"] = dispatch.payable_amount
            except Exception:
                continue
    # .........................................

    # for details in draft state
    if orderhead != '':
        dealerProfile = DealerProfile.objects.filter(
            dealer=orderhead.dealer, is_deleted=False).first()
        dealerBusiness = DealerBusinessMaster.objects.filter(
            brand=dealerProfile.brand, dealer=dealerProfile.dealer,
            is_active=True, is_deleted=False).first()
        context["dbm"] = dealerBusiness
        # ........................
        try:
            orderPlaced = OrderEventHistory.objects.get(order_id=orderhead, order_status='Dispatched')
            today = datetime.date.today()
            order_placed_day = orderPlaced.created_on
            diff = today - order_placed_day.date()
            days = diff.days

            credit_days = dealerBusiness.credit_days
            remaining_days = credit_days - days
            if remaining_days < 0:
                remaining_days = 0
            dealerBusiness.credit_days = remaining_days
        except Exception:
            pass
        # ........................
        if orderno != "":
            context['status_image'] = orderhead.status
            if orderhead.status in status_in.OutstandingFlagStatus():
                context['outstanding_flag'] = True
        # ........................
        # dealerProfile = DealerProfile.objects.filter(
        #     dealer=orderhead.dealer, is_active=True, is_deleted=False).first()
        # dealerBusiness = DealerBusinessMaster.objects.filter(
        #     brand=dealerProfile.brand, dealer=dealerProfile.dealer,
        #     is_active=True, is_deleted=False).first()
        # context["dbm"] = dealerBusiness
        context['outstanding'] = orderhead.payable_amount
        context['order_date'] = orderhead.order_date
        context['dms_number'] = orderhead.DMS_order_number
        context['cc_status'] = False
        context["order_id"] = orderhead.order_number
        context["dealer"] = dealerProfile
        context["history"] = False

        try:
            order_status_detail = OrderEventHistory.objects.filter(
                order_id=orderhead, order_status=orderhead.status).last()

            """ On Hold Order details"""
            if orderhead.status == "OnHold":
                context["on_hold_flag"] = True
                context["on_hold_date"] = order_status_detail.created_on

            if orderhead.status in status_in.SRStatus():
                status_detail = {
                    "head_name": "SR Name",
                    "head_number": "SR Number",
                    "date": order_status_detail.created_on,
                    "name": "", "number": ""
                }
                dealerProfile = DealerProfile.objects.filter(dealer=orderhead.dealer).first()
                srDealer = SRDealer.objects.filter(dealer_id=dealerProfile).first()
                status_detail["name"] = "{0} {1}".format(srDealer.sr_id.first_name, srDealer.sr_id.last_name)
                status_detail["number"] = srDealer.sr_id.mobile
            elif orderhead.status in status_in.ManagerStatus():
                status_detail = {
                    "head_name": "WH User Name", "head_number": "WH User Number",
                    "date": order_status_detail.created_on,
                    "name": "{0} {1}".format(orderhead.warehouse_manager.first_name,
                                             orderhead.warehouse_manager.last_name),
                    "number": orderhead.warehouse_manager.mobile}
            context["status_detail"] = status_detail
        except Exception:
            pass
        # ........................

        # groups = [group.name for group in request.user.groups.all()]
        # if 'DEL' in groups or 'SR' in groups:

        orderno = orderhead.order_number
        after_scheme, total = get_scheme(request, orderno, flag=True)
        context["total"] = total
        context["details"] = after_scheme

        if orderhead.status in status_in.DetailStatus():
            credit_notes = get_credit_notes(orderno, request)
            if credit_notes['grand_total'] > 0:
                context["credit_notes"] = credit_notes
                context['cc_status'] = True

        if orderhead.status in status_in.ShowHistoryStatus():
            history = OrderEventHistory.objects.filter(order_id=orderhead).order_by('pk')
            context["orderHistory"] = history
            warehouse = orderhead.warehouse_code if orderhead.warehouse_code else ""

            if orderhead.warehouse_manager:
                warehouse_manager = "{0} {1}".format(
                    orderhead.warehouse_manager.first_name, orderhead.warehouse_manager.last_name)
            else:
                warehouse_manager = ""
            context["warehouse"] = warehouse
            context["manager"] = warehouse_manager
            context["history"] = True
    return render(request, 'dealer/custom_order_detail.html', context)
    # return render(request, 'dealer/customer_order_detail.html', context)


def order_delete(request):
    code=request.GET.get('skucode')
    orderdetailid=request.GET.get('orderid')
    url=request.GET.get('url')
    orderno=request.GET.get('orderno')
    dealerid=request.GET.get('dealerid')
    try:
        userobj=User.objects.get(pk=request.user.id,groups__name='DEL')

    except Exception:
        userobj=''
    try:
        dealerobj=DealerProfile.objects.get(dealer_id=userobj.pk,is_active=True)
    except Exception:
        dealerobj=''
    try:
        srdealerobj=SRDealer.objects.get(dealer_id_id=dealerobj.pk)
    except Exception:
        srdealerobj=''
    if userobj!='' and dealerobj!='' and srdealerobj!='':
        try:
            dealer=OrderDetails.objects.get(order_detail_id=int(orderdetailid))
        except Exception:
            dealer=''
        if dealer!='':
            try:
                orderallo=OrderAllowanceDetails.objects.get(order_detail_id=dealer.pk)
            except Exception:
                orderallo=''
            if orderallo!='':
                orderallo.delete()

            if orderno!='' and orderno!=None and dealerid!='' and dealerid!=None:
                try:
                    orderhead=OrderHead.objects.get(dealer_id=request.user.id,order_number=int(orderno),status='Processing')
                except Exception:
                    orderhead=''
            else:
                try:
                    orderhead=OrderHead.objects.get(dealer_id=request.user.id,status='Draft')
                except Exception:
                    orderhead=''
            if orderhead!='':
                #orderhead=OrderHead.objects.get(dealer_id=request.user.id,status='Draft')
                orderhead.dealer=request.user
                orderhead.brand=BrandMaster.objects.get(brand_code=orderhead.brand_id,is_active=True)
                orderhead.sr_id=srdealerobj.sr_id
                orderhead.discount_amount=float(0)
                orderhead.total_amount=float(orderhead.total_amount)-float(dealer.total_amount)
                orderhead.payable_amount=float(orderhead.payable_amount)-float(dealer.total_amount)
               
                if orderhead.total_amount<=0:
                    dealer.delete()
                    ordercount=OrderDetails.objects.filter(order_id=orderhead.pk).count()
                    if ordercount<=1:
                        try:
                            orderde=OrderDetails.objects.get(order_id_id=orderhead.pk)
                        except Exception:
                            orderhead.delete()
                            messages.add_message(request, messages.SUCCESS, 'item removed from cart')
                            return HttpResponseRedirect('/user/Customer-Dashboard')
                else:
                    dealer.delete()
                    if orderhead.status=='Processing':
                        orderhead.save()
                        ordercount=OrderDetails.objects.filter(order_id=orderhead.pk).count()
                        if ordercount<=1:
                            try:
                                orderde=OrderDetails.objects.get(order_id=orderhead.pk)
                            except Exception:
                                orderhead.delete()
                                messages.add_message(request, messages.SUCCESS, 'item removed from cart')
                                return HttpResponseRedirect('/user/Customer-Dashboard')
                        messages.add_message(request, messages.SUCCESS, 'item removed from cart')
                        return HttpResponseRedirect('/user/Customer-Order-Detail?orderno='+orderno+'&&dealerid='+dealerid+'')
                    else:
                        orderhead.save()
                        ordercount=OrderDetails.objects.filter(order_id=orderhead.pk).count()
                        if ordercount <= 1:
                            try:
                                orderde = OrderDetails.objects.get(order_id_id=orderhead.pk)
                            except Exception:
                                orderhead.delete()
                                messages.add_message(request, messages.SUCCESS, 'item removed from cart')
                                return HttpResponseRedirect('/user/Customer-Dashboard')
                        messages.add_message(request, messages.SUCCESS, 'item removed from cart')
                        return HttpResponseRedirect('/user/Customer-Order-Detail')
                messages.add_message(request, messages.SUCCESS, 'item removed from cart')
    if int(url)!=1:
        return HttpResponseRedirect('/user/Customer-Dashboard')
    else:
        return HttpResponseRedirect('/user/Product-List')


def product_search_API(request):

    if request.is_ajax():
        ddl=int(request.GET.get('ddlindex'))
        txtvalue=request.GET.get('txtvalue').rstrip(' ')
        firstlist=[]
        secondlist=[]
        thirdlist=[]
        fourthlist=[]
        fivelist=[]
        revddl=ddl
        if revddl==4:
            try:
                obj=ProductCategoryMaster.objects.get(category_name__startswith=txtvalue)
            except Exception:
                obj=''
            if obj!='':
                ddl-=1
                try:
                    obj1=ProductCategoryMaster.objects.get(category_id=obj.parent)
                except Exception:
                    obj1=''
                if obj1!='':
                    txtvalue=str(obj1.category_name)


        for obj in ProductCategoryMaster.objects.filter(category_name__startswith=txtvalue):
            cid=0
            if int(ddl)==1:
                firstlist.append(obj.category_name)
            elif int(ddl)==2:
                secondlist.append(obj.category_name)
            elif int(ddl)==3:
                thirdlist.append(obj.category_name)
            elif int(ddl)==4:
                fourthlist.append(obj.category_name)
            elif int(ddl)==5:
                fivelist.append(obj.category_name)

            if obj.parent!='' and obj.parent!=None:
                if revddl!=5:
                    cid=int(obj.category_id)
                else:
                    cid=int(obj.parent)
            else:
                cid=int(obj.category_id)
            if revddl!=5:
                ddl+=1
                PCM = ProductCategoryMaster.objects.filter(parent=cid,is_active=True)
            else:
                ddl-=1
                PCM = ProductCategoryMaster.objects.filter(category_id=cid,is_active=True)
            second=[]
            if not PCM:
                PCM=''
            if PCM!='':
                for a in PCM:
                    if int(ddl)==1:
                        firstlist.append(a.category_name)
                    elif int(ddl)==2:
                        secondlist.append(a.category_name)
                    elif int(ddl)==3:
                        thirdlist.append(a.category_name)
                    elif int(ddl)==4:
                        fourthlist.append(a.category_name)
                    elif int(ddl)==5:
                        fivelist.append(a.category_name)
                    if revddl!=5:
                        second.append(int(a.category_id))
                    else:
                        second.append(int(a.parent))

                if revddl!=5:
                    ddl+=1
                else:
                    ddl-=1
            third=[]
            if revddl!=5:
                PCM1= ProductCategoryMaster.objects.filter(parent__in=second,is_active=True)
            else:
                PCM1= ProductCategoryMaster.objects.filter(category_id__in=second,is_active=True)
            if not PCM1:
                PCM1=''
            if PCM1!='':
                for b in PCM1:
                    if int(ddl)==1:
                        firstlist.append(b.category_name)
                    elif int(ddl)==2:
                        secondlist.append(b.category_name)
                    elif int(ddl)==3:
                        thirdlist.append(b.category_name)
                    elif int(ddl)==4:
                        fourthlist.append(b.category_name)
                    elif int(ddl)==5:
                        fivelist.append(b.category_name)
                    if revddl!=5:
                        third.append(int(b.category_id))
                    else:
                        third.append(int(b.parent))
                if revddl!=5:
                    ddl+=1
                else:
                    ddl-=1
            four=[]
            if revddl!=5:
                PCM2=ProductCategoryMaster.objects.filter(parent__in=third,is_active=True)
            else:
                PCM2=ProductCategoryMaster.objects.filter(category_id__in=third,is_active=True)
            if not PCM2:
                PCM2=''
            if PCM2!='':
                for c in PCM2:
                    if int(ddl)==1:
                        firstlist.append(c.category_name)
                    elif int(ddl)==2:
                        secondlist.append(c.category_name)
                    elif int(ddl)==3:
                        thirdlist.append(c.category_name)
                    elif int(ddl)==4:
                        fourthlist.append(c.category_name)
                    elif int(ddl)==5:
                        fivelist.append(c.category_name)
                    if revddl!=5:
                        four.append(int(c.category_id))
                    else:
                        four.append(int(c.parent))
                if revddl!=5:
                    ddl+=1
                else:
                    ddl-=1
            if revddl!=5:
                PCM3=ProductCategoryMaster.objects.filter(parent__in=four,is_active=True)
            else:
                PCM3=ProductCategoryMaster.objects.filter(category_id__in=four,is_active=True)
            if not PCM3:
                PCM3=''
            if PCM3!='':
                for d in PCM3:
                    if int(ddl)==1:
                        firstlist.append(d.category_name)
                    elif int(ddl)==2:
                        secondlist.append(d.category_name)
                    elif int(ddl)==3:
                        thirdlist.append(d.category_name)
                    elif int(ddl)==4:
                        fourthlist.append(d.category_name)
                    elif int(ddl)==5:
                        fivelist.append(d.category_name)
                if revddl!=5:
                    ddl+=1
                else:
                    ddl-=1
            break
        allist=[firstlist,secondlist,thirdlist,fourthlist,fivelist]
        data=json.dumps(allist)
        mimetype='application/json'
    else:
        ddl=int(request.GET.get('ddlindex'))
        txtvalue=request.GET.get('txtvalue')
        firstlist=[]
        secondlist=[]
        thirdlist=[]
        fourthlist=[]
        fivelist=[]
        for obj in ProductCategoryMaster.objects.filter(category_name=txtvalue):
            cid=0
            if int(ddl)==1:
                firstlist.append(obj.category_name)
            elif int(ddl)==2:
                secondlist.append(obj.category_name)
            elif int(ddl)==3:
                thirdlist.append(obj.category_name)
            elif int(ddl)==4:
                fourthlist.append(obj.category_name)
            elif int(ddl)==5:
                fivelist.append(obj.category_name)

            if obj.parent!='' and obj.parent!=None:
                cid=int(obj.category_id)
            else:
                cid=int(obj.category_id)
            ddl+=1
            second=[]
            PCM = ProductCategoryMaster.objects.filter(parent=cid,is_active=True)
            if not PCM:
                PCM=''
            if PCM!='':
                for a in PCM:
                    if int(ddl)==1:
                        firstlist.append(a.category_name)
                    elif int(ddl)==2:
                        secondlist.append(a.category_name)
                    elif int(ddl)==3:
                        thirdlist.append(a.category_name)
                    elif int(ddl)==4:
                        fourthlist.append(a.category_name)
                    elif int(ddl)==5:
                        fivelist.append(a.category_name)
                    second.append(int(a.category_id))
                ddl+=1
            third=[]
            PCM1= ProductCategoryMaster.objects.filter(parent__in=second,is_active=True)
            if not PCM1:
                PCM1=''
            if PCM1!='':
                for b in PCM1:
                    if int(ddl)==1:
                        firstlist.append(b.category_name)
                    elif int(ddl)==2:
                        secondlist.append(b.category_name)
                    elif int(ddl)==3:
                        thirdlist.append(b.category_name)
                    elif int(ddl)==4:
                        fourthlist.append(b.category_name)
                    elif int(ddl)==5:
                        fivelist.append(b.category_name)
                    third.append(int(b.category_id))
                ddl+=1
            four=[]
            PCM2=ProductCategoryMaster.objects.filter(parent__in=third,is_active=True)
            if not PCM2:
                PCM2=''
            if PCM2!='':
                for c in PCM2:
                    if int(ddl)==1:
                        firstlist.append(c.category_name)
                    elif int(ddl)==2:
                        secondlist.append(c.category_name)
                    elif int(ddl)==3:
                        thirdlist.append(c.category_name)
                    elif int(ddl)==4:
                        fourthlist.append(c.category_name)
                    elif int(ddl)==5:
                        fivelist.append(c.category_name)
                    four.append(int(c.category_id))
                ddl+=1
            PCM3=ProductCategoryMaster.objects.filter(parent__in=four,is_active=True)
            if not PCM3:
                PCM3=''
            if PCM3!='':
                for d in PCM3:
                    if int(ddl)==1:
                        firstlist.append(d.category_name)
                    elif int(ddl)==2:
                        secondlist.append(d.category_name)
                    elif int(ddl)==3:
                        thirdlist.append(d.category_name)
                    elif int(ddl)==4:
                        fourthlist.append(d.category_name)
                    elif int(ddl)==5:
                        fivelist.append(d.category_name)
                ddl+=1
            break
        allist=[firstlist,secondlist,thirdlist,fourthlist,fivelist]
        data=json.dumps(allist)
        mimetype='application/json'
    return HttpResponse(data,mimetype)


def product_list(request):

    search_record_flag = False
    varfirst = ''
    varsecond = ''
    varthird = ''
    varfour = ''
    varfive = ''
    msg = ''
    listdata = addcart(request)
    if not listdata:
        totalamt1 = 0
        totalcount = 0
        listobj = ''
    else:
        totalamt1 = listdata[1]
        totalcount = listdata[2]
        listobj = listdata[0]
    # .................................
    # one, noUse, noNeed = get_product_category()
    # allCategory = ProductCategoryMaster.objects.filter(
    #     category_name=one[0]["name"], is_active=True, is_deleted=False).first()
    # if allCategory:
    #     categoryList = ProductCategoryMaster.objects.filter(
    #         parent=allCategory.pk, is_active=True, is_deleted=False)
    #     category = [item.category_name for item in categoryList]
    #     category.append(one[0]["name"])
    # result = productsearch(request, category)
    # .................................
    htmlformat = ''
    # result = productsearch(request)
    # htmlformat=result[0]
    # objpm=result[1]
    # listobj=result[2]
    # totalcount=result[3]
    # totalamt=result[4]
    objpm = []
    try:
        objdbm=DealerBusinessMaster.objects.filter(dealer_id=request.user.id,is_active=True).last()
    except Exception as e:
        objdbm=0
    objdst=DealerSalesTarget.objects.filter(dealer_id=request.user.id).aggregate(Sum('monthly_target'))

    firstlist=[]
    secondlist=[]
    thirdlist=[]
    fourthlist=[]
    fivelist=[]
    for obj in ProductCategoryMaster.objects.filter(is_active=True,is_deleted=False):
        cid=0
        if str(obj.category_name)=='CE':
            firstlist.append(obj.category_name)
            cid=int(obj.category_id)
            second=[]
            for a in ProductCategoryMaster.objects.filter(parent=cid,is_active=True):
                secondlist.append(a.category_name)
                second.append(int(a.category_id))
            third=[]
            for b in ProductCategoryMaster.objects.filter(parent__in=second,is_active=True):
                thirdlist.append(b.category_name)
                third.append(int(b.category_id))
            four=[]
            for c in ProductCategoryMaster.objects.filter(parent__in=third,is_active=True):
                fourthlist.append(c.category_name)
                four.append(int(c.category_id))
            for d in ProductCategoryMaster.objects.filter(parent__in=four,is_active=True):
                fivelist.append(d.category_name)
            break
    if request.method == 'POST':
        varthird = request.POST.get('third')
        varfour = request.POST.get('four')
        varfive = request.POST.get('five')

        if 'btnaddorder' in request.POST:

            searchlist = get_product_search_new(request)

            result = productsearch(request, searchlist)
            htmlformat = result[0]
            objpm = result[1]
            listobj = result[2]
            totalcount = result[3]
            totalamt = result[4]
            # ..........................................
            try:
                userobj=User.objects.get(pk=request.user.id,groups__name='DEL')
            except Exception:
                userobj=''
            try:
                dealerobj=DealerProfile.objects.get(dealer_id=userobj.pk, is_active=True)
            except Exception:
                dealerobj=''
            try:
                srdealerobj=SRDealer.objects.get(dealer_id_id=dealerobj.pk)
            except Exception:
                srdealerobj=''
            if userobj != '' and dealerobj!='' and srdealerobj != '':
                data=request.POST.get('btnaddorder')
                data=data.split('_')
                totalno=request.POST.get('hdnorder'+data[0])
                try:
                    DB=DealersBrand.objects.filter(is_active=True,user_id=request.user.id).first()
                except Exception:
                    DB=''
                try:
                    objpm=ProductMaster.objects.get(sku_code=data[1])
                except Exception:
                    pass
                try:
                    taxdata=TaxSlabMaster.objects.get(tax_slab_id=objpm.TaxSlabCode_id)
                except Exception:
                    taxdata=''
                if taxdata!='':
                    ctaxdata=taxdata.cgst_rate
                    staxdata=taxdata.sgst_rate
                    otaxdata=taxdata.other_tax_rate
                    gtaxdata=taxdata.gst_rate
                else:
                    ctaxdata=0
                    staxdata=0
                    otaxdata=0
                    gtaxdata=0
                brand=str(objpm.brand)
                skucode=objpm.sku_code
                quantity=totalno
                mrp=float(objpm.max_retail_price)*float(totalno)
                dp=float(objpm.dealer_price)*float(totalno)
                try:
                    scheme=SchemeProductMaster.objects.get(product=objpm.product_id)
                except Exception:
                    pass
                try:
                    allowanceobj=AllowanceMaster.objects.get(dealer=request.user.id,scheme_category_code=scheme.scheme_category_code)
                    allowance=allowanceobj.discount_percent
                except Exception:
                    allowance=0
                allowanceobj = float(dp)/100*float(allowance)
                valueafterallowance = float(dp)-float(allowanceobj)
                total_tax_percent = ctaxdata + staxdata
                if total_tax_percent > 0:
                    c_percent = 100 * (float(ctaxdata)/float(total_tax_percent))
                    s_percent = 100 * (float(staxdata)/float(total_tax_percent))
                else:
                    c_percent = 0
                    s_percent = 0
                prg = valueafterallowance / (1 + (float(ctaxdata) / 100) + (float(staxdata) / 100))
                total_tax = valueafterallowance - prg
                cgstobj = (c_percent / 100) * total_tax
                cgst = float("{:.2f}".format(cgstobj))
                sgstobj = (s_percent / 100) * total_tax
                sgst = float("{:.2f}".format(sgstobj))
                pregst = float("{:.2f}".format(prg))
                totalbill=pregst+cgst+sgst
                try:
                    maxoutstanding=DealerBusinessMaster.objects.filter(dealer_id=request.user.id, is_active=True).last()
                except Exception:
                    maxoutstanding = None
                if maxoutstanding:
                    orderheadamt = OrderHead.objects.filter(
                        dealer_id=request.user.id,status__in=status_in.OutstandingStatus(), is_active=True).aggregate(Sum('payable_amount'))
                    if orderheadamt['payable_amount__sum'] is None:
                        orderheadamt['payable_amount__sum'] = 0 + totalbill
                    else:
                        orderheadamt['payable_amount__sum'] = float(orderheadamt['payable_amount__sum']) + totalbill
                    max_outstanding_threshold = float(maxoutstanding.max_outstanding_threshold)
                    # min_order_amount = float(maxoutstanding.min_order_amount)

                    # if max_outstanding_threshold >= orderheadamt['total_amount__sum'] >= min_order_amount:
                    pendingorderheadamt = OrderHead.objects.filter(
                        dealer_id=request.user.id, status__in=['Draft', 'Processing', 'SchemeApplied', 'OrderPlaced', 
                                                               'InWarehouse'], is_active=True)\
                        .aggregate( Sum('payable_amount'))
                    if pendingorderheadamt['payable_amount__sum'] is None:
                        pending = 0
                    else:
                        pending = float(pendingorderheadamt['payable_amount__sum'])

                    orderheadamt['payable_amount__sum'] = float(orderheadamt['payable_amount__sum']) + pending
                    if max_outstanding_threshold >= orderheadamt['payable_amount__sum']:
                        OHobj=''
                        OHobj1=OrderHead.objects.filter(dealer_id=request.user.id,status='Processing')
                        if not OHobj1:
                            OHobj1=''
                        try:
                            OHobj=OrderHead.objects.get(dealer_id=request.user.id,status='Draft')
                        except Exception:
                            OHobj=''
                            OHobj1=OrderHead.objects.filter(dealer_id=request.user.id,status='Processing')
                            if not OHobj1:
                                OHobj1=''
                        if OHobj=='' and (OHobj1!='' or OHobj1==''):
                            OH=OrderHead()
                            OH.dealer=request.user
                            if DB!='':
                                OH.brand=BrandMaster.objects.get(brand_code=DB.brand_id,is_active=True)
                            OH.sr_id=srdealerobj.sr_id
                            OH.order_number='0'
                            OH.status='Draft'
                            OH.discount_amount=float(0)
                            OH.total_amount=totalbill
                            OH.payable_amount=totalbill
                            OH.save()

                            # ......... Order History .........
                            # orderHistory = OrderEventHistory(
                            #     order_id=OH,
                            #     order_status='Draft',
                            #     created_by=request.user
                            # )
                            # orderHistory.save()
                            # .................................

                            OHobj=OrderHead.objects.get(dealer_id=request.user.id, status='Draft')
                            OHobj.order_number=int('00'+str(OH.pk))
                            OHobj.save()
                        else:
                            OHobj=OrderHead.objects.get(dealer_id=request.user.id,status='Draft')
                            OHobj.dealer=request.user
                            OHobj.brand=BrandMaster.objects.get(brand_code=DB.brand_id,is_active=True)
                            OHobj.sr_id=srdealerobj.sr_id
                            OHobj.status='Draft'
                            OHobj.discount_amount=float(0)
                            OHobj.total_amount=float(OHobj.total_amount)+totalbill
                            OHobj.payable_amount=float(OHobj.payable_amount)+totalbill
                            OHobj.save()
                        try:
                            orderdetail=OrderDetails.objects.get(sku_code=data[1],order_id=OHobj.pk)
                        except Exception:
                            orderdetail=''
                        if orderdetail=='':
                            OD=OrderDetails()
                            OD.sku_code=data[1]
                            product=ProductMaster.objects.get(sku_code=data[1])
                            if product.size_capacity != '' and product.size_capacity != None:
                                OD.size_capacity=product.size_capacity
                            if product.size_capacity_unit != '' and product.size_capacity_unit != None:
                                OD.size_capacity_unit=product.size_capacity_unit
                            OD.dealer_price=dp
                            OD.gst_rate=float(gtaxdata)
                            OD.sgst_rate=float(staxdata)
                            OD.cgst_rate=float(ctaxdata)
                            OD.other_tax_rate=float(otaxdata)
                            if product.pattern_color!='' and product.pattern_color!=None:
                                OD.pattern_color=product.pattern_color
                            if product.product_description_feature!='' and product.product_description_feature!=None:
                                OD.product_description_feature=product.product_description_feature
                            if product.best_buying_price != '' and product.best_buying_price != None:
                                OD.best_buying_price=float(product.best_buying_price)
                            OD.product_id=product
                            OD.max_retail_price=mrp
                            OD.quantity=int(totalno)
                            OD.discount_percent_per_unit=0.0
                            OD.total_amount=totalbill
                            OD.order_id=OHobj
                            OD.save()
                        else:
                            orderdetail.sku_code=data[1]
                            product=ProductMaster.objects.get(sku_code=data[1])
                            orderdetail.size_capacity=product.size_capacity
                            orderdetail.size_capacity_unit=product.size_capacity_unit
                            orderdetail.dealer_price=float(orderdetail.dealer_price)+dp
                            orderdetail.gst_rate=float(gtaxdata)
                            orderdetail.sgst_rate=float(staxdata)
                            orderdetail.cgst_rate=float(ctaxdata)
                            orderdetail.other_tax_rate=float(otaxdata)
                            if product.pattern_color!='' and product.pattern_color!=None:
                                orderdetail.pattern_color=product.pattern_color
                            if product.product_description_feature!='' and product.product_description_feature!=None:
                                orderdetail.product_description_feature=product.product_description_feature
                            #orderdetail.pattern_color=product.pattern_color
                            #orderdetail.product_description_feature=product.product_description_feature
                            orderdetail.best_buying_price=float(product.best_buying_price)
                            orderdetail.product_id=product
                            orderdetail.max_retail_price=float(orderdetail.max_retail_price)+mrp
                            orderdetail.quantity=int(orderdetail.quantity)+int(totalno)
                            orderdetail.discount_percent_per_unit=0.0
                            orderdetail.total_amount=float(orderdetail.total_amount)+totalbill
                            orderdetail.order_id=OHobj
                            orderdetail.save()
                        if orderdetail=='':
                            orderdetailid=OD.pk
                            queryorder=OD
                        else:
                            orderdetailid=orderdetail.pk
                            queryorder=orderdetail
                        try:
                            OrderAllowance=OrderAllowanceDetails.objects.get(sku_code=data[1],order_detail_id=orderdetailid,order_id=OHobj.pk)
                        except Exception:
                            OrderAllowance=''
                        if OrderAllowance=='':
                            OAD=OrderAllowanceDetails()
                        else:
                            OAD=OrderAllowanceDetails.objects.get(sku_code=data[1],order_detail_id=orderdetailid,order_id=OHobj.pk)
                        OAD.order_detail_id=queryorder
                        OAD.product_id=product
                        OAD.sku_code=data[1]
                        OAD.order_id=OHobj
                        try:
                            schemeobj=SchemeProductMaster.objects.get(product_id=product.pk)
                        except Exception:
                            schemeobj=''
                        if schemeobj!='':
                            try:
                                allo=AllowanceMaster.objects.get(scheme_category_code_id=schemeobj.scheme_category_code,dealer_id=request.user.id)
                            except Exception:
                                allo=''
                            if allo!='':
                                if allo.discount_percent!=None and allo.discount_percent!='' and allo.discount_percent!=0:
                                    OAD.allowance_percent=allo.discount_percent
                                elif allo.discount_amount!=None and allo.discount_amount!='' and allo.discount_amount!=0:
                                    OAD.allowance_amount=allo.discount_amount
                                    OAD.allowance_percent=0
                                else:
                                    OAD.allowance_amount=0
                                    OAD.allowance_percent=0
                            else:
                                OAD.allowance_amount=0
                                OAD.allowance_percent=0
                        else:
                            OAD.allowance_amount=0
                            OAD.allowance_percent=0
                        OAD.save()
                        messages.add_message(request, messages.SUCCESS, 'Item added successfully')
                        return HttpResponseRedirect('/user/Product-List')
                    else:
                        msg = 'You have exceeded your max credit limit ('+str(maxoutstanding.max_outstanding_threshold)+'), Contact your SR ('+srdealerobj.sr_id.first_name+' '+srdealerobj.sr_id.last_name +' and '+str(srdealerobj.sr_id.mobile)+') for further clarification'
                    # else:
                    #     msg = 'Your minimum order limit is (' + str(maxoutstanding.min_order_amount) + '), you can not order less then (' + str(maxoutstanding.min_order_amount) + ')'

        if 'search' in request.POST:
            # import pdb
            # pdb.set_trace()

            searchlist = get_product_search_new(request)

            result = productsearch(request, searchlist)
            htmlformat = result[0]
            objpm = result[1]
            listobj = result[2]
            totalcount = result[3]
            totalamt = result[4]
            search_record_flag = True

    context = {
        'objpm':objpm,
        'objdst':objdst,
        'objdbm':objdbm,
        'htmlformat':htmlformat,
        'listdata':listobj,
        'totalcount':totalcount,
        'totalamt':totalamt1,
        'firstlist':firstlist,
        'secondlist':secondlist,
        'thirdlist':thirdlist,
        'fourthlist':fourthlist,
        'fivelist':fivelist,
        'varfirst':varfirst,
        'varsecond':varsecond,
        'varthird':varthird,
        'varfour':varfour,
        'varfive':varfive,
        'msg':msg,
        'search_record': search_record_flag,
        'total_search_record': "No records found"
    }

    if varthird == '' or varthird is None:
        # print("start-1")
        one, two, three = get_product_category()
        two, three = [], []
    # ...........
    else:
        # print("start-2")
        one, noUse, noNeed = get_product_category()
        two, three = product_category(request, '1', varthird)
    # ...........
    # ...........................................
    if htmlformat:
        """Length of Search Products"""
        totalItem = htmlformat.split('class="col-xs-12 col-sm-3 col-md-3"')
        # print(len(totalItem))
        context["total_search_record"] = "{0} records found".format(len(totalItem) - 1)

    # ...........................................

    context["one"] = one
    context["two"] = two
    context["three"] = three
    context["varOne"] = varthird
    context["varTwo"] = varfour
    context["varThree"] = varfive
    # return render(request, 'dealer/productlist.html',  context)
    return render(request, 'dealer/product_list.html', context)


def get_product_search_new(request):
    searchlist = []
    if request.POST.get('third') != '' and request.POST.get('third') is not None:
        varthird = request.POST.get('third')
        searchlist.append(request.POST.get('third'))

        # .........................
        if request.POST.get('four') != '' and request.POST.get('four') is not None:
            pass
        else:
            category = ProductCategoryMaster.objects.filter(
                category_name__icontains=varthird, is_active=True, is_deleted=False).first()
            if category:
                categoryList = ProductCategoryMaster.objects.filter(
                    parent=category.pk, is_active=True, is_deleted=False)
                category = [item.category_name for item in categoryList]
                searchlist += category
        # .........................

    if request.POST.get('four') != '' and request.POST.get('four') is not None:
        varfour = request.POST.get('four')
        searchlist.append(request.POST.get('four'))

        # .........................
        if request.POST.get('five') != '' and request.POST.get('five') is not None:
            pass
        else:
            category = ProductCategoryMaster.objects.filter(
                category_name__icontains=varfour, is_active=True, is_deleted=False).first()
            if category:
                categoryList = ProductCategoryMaster.objects.filter(
                    parent=category.pk, is_active=True, is_deleted=False)
                category = [item.category_name for item in categoryList]
                searchlist += category
        # .........................

    if request.POST.get('five') != '' and request.POST.get('five') is not None:
        varfive = request.POST.get('five')
        searchlist.append(request.POST.get('five'))
    return searchlist


def productsearch(request, prosearch=None):
    # import pdb
    # pdb.set_trace()
    listdata=''
    listdata=addcart(request)
    if not listdata:
        totalamt=0
        totalcount=0
        listobj=''
    else:
        totalamt=listdata[1]
        totalcount=listdata[2]
        listobj=listdata[0]

    result = []
    objpm = ProductMaster.objects.filter(is_active=True, is_deleted=False)
    PCM_id=0
    incount=''
    count=0
    abc=''
    htmlformat=''
    htmlformat1=''
    btncount=0
    try:
        dealers=DealersBrand.objects.filter(user_id=request.user.id, is_active=True).first()
    except Exception:
        dealers=''
    if dealers!='':
        for data in objpm:
            count=0
            jsondata={}
            try:
                stockobj = StockRegister.objects.get(product=data.product_id, is_active=True)
            except Exception:
                stockobj = 0
            if stockobj != 0:
                if int(stockobj.stock_in) <= int(stockobj.stock_out):
                    jsondata['stock'] = '<a href="#" class="out-stock">Out of Stock</a>'
                    incount = 'out'
                else:
                    jsondata['stock'] = '<a href="#" class="in-stock">In Stock</a>'
                    incount = 'in'
            else:
                jsondata['stock'] = '<a href="#" class="out-stock">Out of Stock</a>'
                incount = 'out'
                # incount = 'in'
            if data.sku_code != '' and data.sku_code is not None:
                jsondata['sku_code'] = str(data.sku_code)
            else:
                jsondata['sku_code'] = ''

            if data.pattern_color != '' and data.pattern_color is not None:
                jsondata['color'] = str(data.pattern_color)
            else:
                jsondata['color'] = ''

            if data.size_capacity != '' and data.size_capacity is not None:
                jsondata['size_capacity'] = str(data.size_capacity)
            else:
                jsondata['size_capacity'] = ''
            if data.size_capacity_unit != '' and data.size_capacity_unit is not None:
                jsondata['size_capacity_unit'] = str(data.size_capacity_unit)
            else:
                jsondata['size_capacity_unit'] = ''
            if data.dealer_price != '' and data.dealer_price is not None:
                jsondata['dealer_price'] = str(data.dealer_price)
            else:
                jsondata['dealer_price'] = ''
            if data.max_retail_price != '' and data.max_retail_price is not None:
                jsondata['max_retail_price'] = str(data.max_retail_price)
            else:
                jsondata['max_retail_price'] = ''
            if data.product_description_feature != '' and data.product_description_feature is not None:
                jsondata['product_description_feature'] = str(data.product_description_feature)
            else:
                jsondata['product_description_feature'] = ''

            datas = ''
            category = ProductCategoryMaster.objects.filter(is_active=True, is_deleted=False)
            get_product = []
            for PC in category:
                if data.product_category is not None:
                    pass
                else:
                    continue
                CID = data.product_category_id
                PCM = ProductCategoryMaster.objects.get(pk=CID)
                get_product.append(PCM.category_name)
                PID = PCM.parent
                try:
                    PCM = ProductCategoryMaster.objects.get(pk=PID)
                except Exception:
                    PCM = ''
                if PCM != '':
                    get_product.append(PCM.category_name)
                    PID = PCM.parent
                try:
                    PCM = ProductCategoryMaster.objects.get(pk=PID)
                except Exception:
                    PCM = ''
                if PCM != '':
                    get_product.append(PCM.category_name)
                    PID = PCM.parent
                try:
                    PCM = ProductCategoryMaster.objects.get(pk=PID)
                except Exception:
                    PCM = ''
                if PCM != '':
                    get_product.append(PCM.category_name)
                    PID = PCM.parent
                try:
                    PCM1 = ProductCategoryMaster.objects.get(pk=PID)
                except Exception:
                    PCM1 = ''
                if PCM1 != '':
                    get_product.append(PCM1.category_name)
                    PID = PCM1.parent
                break
            get_product.reverse()

            # import pdb
            # pdb.set_trace()

            if prosearch is not None:
                count = 0
                for category in get_product:
                    if category in prosearch:
                        count += 1
                # if count >= 3:
                if count > 0:
                    for i in get_product:
                        datas += str(i) +', '
                    jsondata['name'] = datas.rstrip(',')
                    #if incount=='in':
                    result.append(jsondata)
            else:
                for i in get_product:
                    datas += str(i) + ', '
                jsondata['name'] = datas.rstrip(',')
                #if incount=='in':
                result.append(jsondata)
        count=0
        abc=''
        htmlformat=''
        htmlformat1=''
        btncount=0

        # import pdb
        # pdb.set_trace()

        for obj in result:
            count+=1
            btncount+=1
            groups = [group.name for group in request.user.groups.all()]
            if 'DEL' in groups:
                abc+='<div class="col-xs-12 col-sm-3 col-md-3">'\
                '<div class="order-figger">'\
                '<figure class="samsung-figger samsung-text" style="font-size:24px;">'+obj['sku_code']+''\
                '</figure><div class="order-detail"><h5>'+obj['name']+'</h5>'\
                '<h5>Color: '+obj['color']+', Size/Unit: '+obj['size_capacity']+' '+obj['size_capacity_unit']+' </h5>'\
                '<h5>MRP: <i class="fa fa-inr" aria-hidden="true"></i><span class="price">'\
                ''+obj['max_retail_price']+'</span>, DP: <i class="fa fa-inr" aria-hidden="true">'\
                '</i> <span class="dp-price">'+obj['dealer_price']+'</span></h5>'\
                '<div class="pagination-detail"><div class="row"><div class="col-xs-6 col-sm-6 col-md-7">'\
                '<ul class="pagination pagination-text"><li onclick="return minus('+str(btncount)+');"><a>-</a></li><li><a id="ordervalue'+str(btncount)+'">1</a>'\
                '<input type="hidden" name="hdnorder'+str(btncount)+'" id="hdnorder'+str(btncount)+'" value="1"></li>'\
                '<li class="active" onclick="return plus('+str(btncount)+');"><a >+</a></li></ul></div><div class="col-xs-6 col-sm-6 col-md-5 text-center">'\
                '<button type="submit" class="order-view" name="btnaddorder" value="'+str(btncount)+'_'+obj['sku_code']+'">Add Order</button></div></div></div>'\
                '<a class="in-stock">Features<div class="feature" id="over_'+str(btncount)+'"><p>'+obj['product_description_feature']+'</p></div></a>'\
                '</div></div></div>'
            #query=obj['query']
            else:
                abc+='<div class="col-xs-12 col-sm-6 col-md-4" style="width:20%;">'\
                '<div class="order-figger">'\
                '<figure class="samsung-figger samsung-text">'\
                '<!--<img src="/static/img/samsung-order-img.png" alt="samsung" class="samsung_brand">-->Samsung'\
                '</figure><div class="order-detail"><h5>'+obj['name']+'</h5>'\
                '<h5>'+obj['sku_code']+', '+obj['size_capacity']+' '+obj['size_capacity_unit']+' </h5>'\
                '<h5>MRP: <i class="fa fa-inr" aria-hidden="true"></i><span class="price">'\
                ''+obj['max_retail_price']+'</span>, DP: <i class="fa fa-inr" aria-hidden="true">'\
                '</i> <span class="dp-price">'+obj['dealer_price']+'</span></h5>'\
                '<div class="pagination-detail"><div class="row"><div class="col-xs-6 col-sm-6 col-md-7">'\
                '<ul class="pagination pagination-text"><li onclick="return minus('+str(btncount)+');"><a>-</a></li><li><a id="ordervalue'+str(btncount)+'">1</a>'\
                '<input type="hidden" name="hdnorder'+str(btncount)+'" id="hdnorder'+str(btncount)+'" value="1"></li>'\
                '<li class="active" onclick="return plus('+str(btncount)+');"><a >+</a></li></ul></div><div class="col-xs-6 col-sm-6 col-md-5 text-center">'\
                '<button type="submit" class="order-view" name="btnaddorder" value="'+str(btncount)+'_'+obj['sku_code']+'">Add Order</button></div></div></div><!--<p>'+obj['product_description_feature']+'</p>-->'\
                ''+obj['stock']+'</div></div></div>'
                if count==5:
                    count=0
                    htmlformat+='<div style="margin-bottom:10%" class="row order-row">'+abc+'</div>'
                    abc=''
        if count!=5:
            count=0
            htmlformat+='<div class="row order-row">'+abc+'</div>'
            abc=''
    return [htmlformat,objpm,listobj,totalcount,totalamt]


def latest_sr_orders(request):
    ordpaid=0
    try:
        SR = User.objects.get(pk=request.user.id, groups__name="SR",is_active=True)
    except Exception:
        SR = ''
    if SR != '':
        sr_dealer = SRDealer.objects.filter(sr_id_id=SR.pk).values_list('dealer_id_id')
        dealer = DealerProfile.objects.filter(
            pk__in=sr_dealer, is_active=True, is_deleted=False).values_list('dealer_id')
        userobj=User.objects.filter(is_active=True,pk__in=dealer,groups__name="DEL").values_list('pk')
        orderhead = OrderHead.objects.filter(                                                                                                                                                                             
            dealer_id__in=userobj, status__in=status_in.AdminStatus(),is_active=True,is_deleted=False).order_by('-order_date').exclude(order_date__exact=None)
        orderheadnull = OrderHead.objects.filter(
            dealer_id__in=userobj,order_date=None, status__in=['SchemeApplied','OrderPlaced','InWarehouse','OnHold','Dispatched','Delivered','PartialDispatched','Processing','Deleted'],is_active=True,is_deleted=False).order_by('-order_date')
       
        orderhead=list(chain(orderhead,orderheadnull))
        for obj in orderhead:
            dealer_company = DealerProfile.objects.get(dealer=obj.dealer,is_active=True,is_deleted=False)
            obj.__dict__['company'] = dealer_company.company_name
    for order in orderhead:
        ordpaid+=float(order.payable_amount)

    # ......................
    for order in orderhead:
        try:
            order.__dict__["dispatched_date"] = ""
            dealerBusiness = DealerBusinessMaster.objects.get(
                brand=order.brand, dealer=order.dealer,
                is_active=True, is_deleted=False)

            orderPlaced = OrderEventHistory.objects.get(
                order_id=order, order_status='Dispatched')

            today = datetime.date.today()
            order_placed_day = orderPlaced.created_on
            diff = today - order_placed_day.date()
            days = diff.days

            credit_days = dealerBusiness.credit_days
            remaining_days = credit_days - days
            if remaining_days < 0:
                remaining_days = 0
            order.__dict__["credit_days"] = remaining_days
            order.__dict__["dispatched_date"] = orderPlaced.created_on
        except DealerBusinessMaster.DoesNotExist:
            order.__dict__["credit_days"] = 0
        except OrderEventHistory.DoesNotExist:
            order.__dict__["credit_days"] = 0
        except Exception:
            order.__dict__["credit_days"] = 0
    # ......................


    if request.method=='POST':
        if "export" in request.POST:
            result=[]
            if len(orderhead)>0:
                for order in orderhead:
                    # dealer_company = DealerProfile.objects.get(
                    #     dealer=obj.dealer, is_active=True, is_deleted=False)
                    credit_days_left = order.credit_days if order.credit_days > 0 else ""
                    get_order={}
                    get_order['Order No']=order.order_number
                    get_order['DMS No'] = order.DMS_order_number
                    get_order['Dispatch Date'] = order.dispatched_date.date() if order.dispatched_date else ""
                    get_order['Discount'] = order.discount_amount
                    get_order['Payment due'] = order.payable_amount
                    get_order['Order Date']=order.order_date.date()
                    get_order['Invoice Value']=order.total_amount
                    get_order['Status']=order.status
                    get_order['Credit days left'] = credit_days_left
                    get_order['Dealer'] = "{0}.{1}".format(order.dealer.user_code, order.company)
                    result.append(get_order)
                response = HttpResponse(content_type='text/csv')
                response['Content-Disposition'] = 'attachment;filename=order_list.csv'
                keys = ['Order No', 'DMS No', 'Order Date', 'Dispatch Date', 'Invoice Value',
                        'Discount', 'Payment due', 'Status', 'Credit days left', 'Dealer']
                writer = csv.DictWriter(response, keys)
                writer.writeheader()
                writer.writerows(result)
                return response
    context={  
        'orderhead':orderhead, 
        'orderlen':len(orderhead),
        'orderheadsum':ordpaid
    }
    return render(request,'sr/latest_orders.html',context)


def SR_dashboard(request):
    sr_id = request.user.id
    Totalscheme = 0
    # count=0
    querysetcount = ''
    try:
        SR = User.objects.get(pk=sr_id, groups__name="SR")
    except Exception:
        SR = ''
    if SR != '':
        # totaldealer = SRDealer.objects.filter(sr_id_id=SR.pk).values_list('sr_id_id').count()
        srDealers = SRDealer.objects.filter(sr_id_id=SR.pk).values_list('dealer_id_id')
        dealer = DealerProfile.objects.filter(
            pk__in=srDealers, is_active=True, is_deleted=False).values_list('dealer_id')
        userobj = User.objects.filter(is_active=True, pk__in=dealer, groups__name="DEL").values_list('pk')
        totaldealer = SRDealer.objects.filter(sr_id_id=SR.pk).values_list('sr_id_id').count()

        # dealers = DealerProfile.objects.filter(is_active=True)
        # dealers = [dealer.dealer for dealer in dealers]
        # orderheadactive = OrderHead.objects.filter(dealer__in=dealers, status='Processing').count()

        orderheadactive = OrderHead.objects.filter(
            dealer_id__in=userobj, status__in=status_in.SRHeadActiveStatus(),
            is_active=True, is_deleted=False).count()
        orderhead = OrderHead.objects.filter(
            dealer_id__in=userobj, status__in=status_in.AdminStatus(),
            is_active=True, is_deleted=False).order_by(
            '-order_date').exclude(order_date__exact=None)
        orderHeadNull = OrderHead.objects.filter(
            dealer_id__in=userobj, order_date=None,
            status__in=status_in.SRHeadNullStatus(),
            is_active=True, is_deleted=False).order_by('-order_date')
       
        orderhead = list(chain(orderhead, orderHeadNull))
        for obj in orderhead:
            dealer_company = DealerProfile.objects.get(dealer=obj.dealer)
            obj.__dict__['company'] = dealer_company.company_name
        querysetcount = orderhead
        # print(querysetcount.count())
        if len(orderhead) > 10:
            orderhead = orderhead[:10]
        try:
            objdbm = DealerBusinessMaster.objects.get(dealer_id__in=dealer, is_active=True)
        except Exception as e:
            objdbm = 0
        scheme = SchemeSellThroughMaster.objects.filter(
            scheme_end_date__date__gte=datetime.datetime.today().date(),
            is_active=True, is_deleted=False).order_by('-pk')
        # Totalscheme=0
        schemesell=[]
        totalschemesell=[]
        breakcount=0
        for SSTM in scheme:
            breakcount+=1
            count=0
            SSTM_get={}
            try:
                product=ProductMaster.objects.get(pk=SSTM.product_id)
            except Exception:
                product=''
            if product!='':
                Totalscheme+=1
                try:
                    PCM=ProductCategoryMaster.objects.get(pk=product.product_category_id)
                except Exception:
                    PCM=''
                if PCM!='':
                    get_product={}
                    pc1=PCM.category_name
                    count+=1
                    PID=PCM.parent
                    try:
                        PCM=ProductCategoryMaster.objects.get(pk=PID)
                    except Exception:
                        PCM=''
                    if PCM!='':
                        pc2=PCM.category_name
                        count+=1
                        PID=PCM.parent
                    else:
                        pc2 = ''

                    try:
                        PCM=ProductCategoryMaster.objects.get(pk=PID)
                    except Exception:
                        PCM=''
                    if PCM!='':
                        pc3=PCM.category_name
                        count+=1
                        PID=PCM.parent
                    else:
                        pc3 = ''

                    try:
                        PCM=ProductCategoryMaster.objects.get(pk=PID)
                    except Exception:
                        PCM=''
                    if PCM!='':
                        pc4=PCM.category_name
                        count+=1
                        PID=PCM.parent
                    try:
                        PCM1=ProductCategoryMaster.objects.get(pk=PID)
                    except Exception:
                        PCM1=''
                    if PCM1!='':
                        pc5=PCM1.category_name
                        count+=1
                        PID=PCM1.parent
                    if count==5:
                        SSTM_get['product'] = pc3
                    else:
                        SSTM_get['product'] = pc2
            SSTM_get['discount_percent'] = SSTM.discount_percent
            SSTM_get['discount_amount'] = SSTM.discount_amount
            SSTM_get['scheme_end_date'] = SSTM.scheme_end_date
            SSTM_get['scheme_start_date'] = SSTM.scheme_start_date
            SSTM_get['scheme_name'] = SSTM.scheme_name
            SSTM_get['scheme_code'] = SSTM.scheme_code
            SSTM_get['sku'] = product.sku_code
            totalschemesell.append(SSTM_get)
            if breakcount <= 10:
                schemesell.append(SSTM_get)
        target = DealerSalesTarget.objects.filter(dealer_id__in=dealer)
        get_target = []
        for DST in target:
            order_price=0
            DST_get={}
            DST_get['category']=DST.category.category_name
            monthly_target=DST.monthly_target
            order=OrderHead.objects.filter(Q(status='Confirm')|Q(status='Delivered'),dealer_id__in=dealer)
            get_product=[]
            if order!='':
                for amt in order:
                    get_cat={}
                    try:
                        OD=OrderDetails.objects.get(order_id_id=amt.pk)
                    except Exception:
                        OD=''
                    try:
                        PM=ProductMaster.objects.get(pk=OD.product_id_id)
                    except Exception:
                        PM=''
                    if PM!='':
                        category = ProductCategoryMaster.objects.filter(is_active=True,is_deleted=False)

                        for PC in category:
                            #get_category={}
                            CID=PM.product_category_id
                            PCM=ProductCategoryMaster.objects.get(pk=CID)
                            get_product.append(PCM.category_name)
                            PID=PCM.parent
                            try:
                                PCM=ProductCategoryMaster.objects.get(pk=PID)
                            except Exception:
                                PCM=''
                            if PCM!='':
                                get_product.append(PCM.category_name)
                                PID=PCM.parent
                            try:
                                PCM=ProductCategoryMaster.objects.get(pk=PID)
                            except Exception:
                                PCM=''
                            if PCM!='':
                                get_product.append(PCM.category_name)
                                PID=PCM.parent
                            try:
                                PCM=ProductCategoryMaster.objects.get(pk=PID)
                            except Exception:
                                PCM=''
                            if PCM!='':
                                get_product.append(PCM.category_name)
                                PID=PCM.parent
                            try:
                                PCM1=ProductCategoryMaster.objects.get(pk=PID)
                            except Exception:
                                PCM1=''
                            if PCM1!='':
                                get_product.append(PCM1.category_name)
                                PID=PCM1.parent
                            break
                        get_product.reverse()
                        order_price+=amt.total_amount
            if DST.category.category_name in get_product:
                DST_get['complete_target']=order_price
                DST_get['percent']=(order_price*100)/monthly_target
            else:
                DST_get['percent']=0
                DST_get['complete_target']=0
            DST_get['monthly_target']=monthly_target

            get_target.append(DST_get)
        pay = OrderHead.objects.filter(
            Q(status='Confirm') | Q(status='Delivered'),
            dealer_id__in=dealer).aggregate(Sum('total_amount'))
        objdst = DealerSalesTarget.objects.filter(
            dealer_id__in=dealer).aggregate(Sum('monthly_target'))
        if pay['total_amount__sum'] is None:
            pay['total_amount__sum'] = 0
        if objdst['monthly_target__sum'] is None:
            objdst['monthly_target__sum'] = 0
        # outstanding=int(objdst['monthly_target__sum'])-int(pay['total_amount__sum'])
        # outstanding = orderhead.aggregate(Sum('total_amount'))
        # if outstanding['total_amount__sum'] is None:
        #     outstanding['total_amount__sum'] = 0

        # ..........................
        total_outstanding_amount = OrderHead.objects.filter(
            dealer_id__in=userobj, status__in=['Dispatched','Delivered','PartialDispatched', ],
            is_active=True, is_deleted=False).aggregate(Sum('payable_amount'))
        if total_outstanding_amount['payable_amount__sum'] is None:
            total_outstanding_amount['payable_amount__sum'] = 0.00

        # ......................
        for order in orderhead:
            try:
                dealerBusiness = DealerBusinessMaster.objects.get(
                    brand=order.brand, dealer=order.dealer,
                    is_active=True, is_deleted=False)

                orderPlaced = OrderEventHistory.objects.get(
                    order_id=order, order_status='Dispatched')

                today = datetime.date.today()
                order_placed_day = orderPlaced.created_on
                diff = today - order_placed_day.date()
                days = diff.days

                credit_days = dealerBusiness.credit_days
                remaining_days = credit_days - days
                if remaining_days < 0:
                    remaining_days = 0
                order.__dict__["credit_days"] = remaining_days
                order.__dict__["dispatched_date"] = orderPlaced.created_on
            except DealerBusinessMaster.DoesNotExist:
                order.__dict__["credit_days"] = None
            except OrderEventHistory.DoesNotExist:
                order.__dict__["credit_days"] = None
            except Exception:
                order.__dict__["credit_days"] = None
        # ......................

        context={
            'objdst':objdst,
            'objdbm':objdbm,
            'orderhead':orderhead,
            'get_target':get_target,
            'orderheadactive': orderheadactive,
            'outstanding': total_outstanding_amount["payable_amount__sum"],
            'totaldealer': totaldealer,
            'scheme':schemesell,
            'Totalscheme':Totalscheme,
            'querysetcount':querysetcount,
            'totalschemesell':totalschemesell,
        }

        return render(request, 'sr/sr_dashboard.html',  context)
    else:
        return render(request, 'sr/sr_dashboard.html')


def new_schemes(request):
    scheme=SchemeSellThroughMaster.objects.filter(is_active=True,is_deleted=False).order_by('-pk')
    #Totalscheme=0
    schemesell=[]
    for SSTM in scheme:
        count=0
        SSTM_get={}
        try:
            product=ProductMaster.objects.get(pk=SSTM.product_id)
        except Exception:
            product=''
        if product!='':
            try:
                PCM=ProductCategoryMaster.objects.get(pk=product.product_category_id)
            except Exception:
                PCM=''
            if PCM!='':
                get_product={}
                pc1=PCM.category_name
                count+=1
                PID=PCM.parent
                try:
                    PCM=ProductCategoryMaster.objects.get(pk=PID)
                except Exception:
                    PCM=''
                if PCM!='':
                    pc2=PCM.category_name
                    count+=1
                    PID=PCM.parent
                else:
                    pc2 = ''
                try:
                    PCM=ProductCategoryMaster.objects.get(pk=PID)
                except Exception:
                    PCM=''
                if PCM!='':
                    pc3=PCM.category_name
                    count+=1
                    PID=PCM.parent
                else:
                    pc3 = ''

                try:
                    PCM=ProductCategoryMaster.objects.get(pk=PID)
                except Exception:
                    PCM=''
                if PCM!='':
                    pc4=PCM.category_name
                    count+=1
                    PID=PCM.parent
                try:
                    PCM1=ProductCategoryMaster.objects.get(pk=PID)
                except Exception:
                    PCM1=''
                if PCM1!='':
                    pc5=PCM1.category_name
                    count+=1
                    PID=PCM1.parent
                if count==5:
                    SSTM_get['product']=pc3
                else:
                    SSTM_get['product']=pc2
        SSTM_get['discount_percent']=SSTM.discount_percent
        SSTM_get['scheme_end_date']=SSTM.scheme_end_date
        SSTM_get['scheme_start_date']=SSTM.scheme_start_date
        SSTM_get['scheme_name']=SSTM.scheme_name
        SSTM_get['scheme_code']=SSTM.scheme_code
        schemesell.append(SSTM_get)
    context={
        'schemesell':schemesell,
    }
    return render(request,'sr/new_scheme_list.html',context)


def Admin_dashboard(request):
    querysetcount=''
    month=datetime.datetime.now().month
    year=datetime.datetime.now().year
    if request.user.is_superuser:
        price=0
        dealer=User.objects.filter(is_active=True,groups__name="DEL").values_list('pk')
        product=ProductMaster.objects.filter(is_active=True,is_deleted=False).count()
        orderheadactive=OrderHead.objects.filter(
            dealer_id__in=dealer,
            status__in=status_in.admin_active()).count()
        orderhead=OrderHead.objects.filter(
            dealer_id__in=dealer,
            status__in=status_in.AdminStatus(),is_active=True,is_deleted=False).\
            order_by('-order_date').exclude(order_date__exact=None)
        orderheadnull = OrderHead.objects.filter(
            dealer_id__in=dealer,order_date=None,
            status__in=status_in.admin_null_status(), is_active=True,
            is_deleted=False).order_by('-order_date')
       
        orderhead=list(chain(orderhead,orderheadnull))
        for order in orderhead:
            try:
                status=OrderEventHistory.objects.get(order_id_id=order.pk,order_status='Dispatched')
                order.__dict__['Dispatched_Date']=status.created_on
            except Exception:
                status=''

            try:
                _x_dealer_profile = DealerProfile.objects.get(
                    dealer_id=order.dealer, is_active=True, is_deleted=False)
                order.__dict__["company"] = _x_dealer_profile.company_name
            except DealerProfile.DoesNotExist:
                order.__dict__["company"] = ""

        for rs in orderhead:
            price+=rs.total_amount
        querysetcount=orderhead
        if len(orderhead)>10:
            orderhead=orderhead[:10]
        try:
            objdbm=DealerBusinessMaster.objects.get(dealer_id__in=dealer,is_active=True)
        except Exception as e:
            objdbm=0
        target=DealerSalesTarget.objects.filter(dealer_id__in=dealer,is_active=True, is_deleted=False)
        get_target=[]
        for DST in target:
            category = None
            DST_get={}
            DST_get['category'] = DST.category.category_name
            monthly_target = DST.monthly_target
            placed_orders = OrderHead.objects.filter(status__in=['OrderPlaced','PaymentDone','InWarehouse','Dispatched'], dealer_id=DST.dealer)
            if len(placed_orders)>0:
                for placed_order in placed_orders:
                    order_detail = OrderDetails.objects.filter(order_id_id=placed_order.pk,created_on__month=DST.created_on.month)
                    for order_detail in order_detail:
                        if order_detail.product_id:
                            category = ProductCategoryMaster.objects.filter(is_active=True,is_deleted=False).values_list('category_name')
                            category = [li[0] for li in category]

            if category is not None and DST.category.category_name in category:
                complete_target=placed_order.total_amount
            
            else:
                complete_target=0
            if complete_target==0:
                try:
                    target=DealerSalesTarget.objects.get(dealer_id=DST.dealer_id,category_id=DST.category_id,created_on__month=datetime.datetime.now().month,created_on__year=datetime.datetime.now().year,is_active=True, is_deleted=False)
                except Exception:
                    target=DealerSalesTarget()
                    target.monthly_target=float(DST.monthly_target)
                    target.brand=DST.brand
                    target.category=DST.category
                    target.dealer=DST.dealer
                    target.created_on=datetime.datetime.now()
                    target.save()
        target_mo=DealerSalesTarget.objects.filter(dealer_id__in=dealer,created_on__month=month,created_on__year=datetime.datetime.now().year,is_active=True, is_deleted=False)
        for DST in target_mo:
            DST_gt={}
            category = None
            order=0
            monthly_target = DST.monthly_target
            placed_orders = OrderHead.objects.filter(status__in=['OrderPlaced','PaymentDone','InWarehouse','Dispatched'], dealer_id=DST.dealer,order_date__month=DST.created_on.month,order_date__year=DST.created_on.year)
            category = ProductCategoryMaster.objects.filter(pk=DST.category_id,is_active=True,is_deleted=False)
            get_product=[]
            for PC in category:
                p_id=PC.pk
                product_category=ProductCategoryMaster.objects.filter(parent=p_id,is_active=True,is_deleted=False)
                for product_category in product_category:
                    get_product.append(product_category.pk)
                    product_category=ProductCategoryMaster.objects.filter(parent=product_category.pk,is_active=True,is_deleted=False)
                    for product_category in product_category:
                        get_product.append(product_category.pk)
                        product_category=ProductCategoryMaster.objects.filter(parent=product_category.pk,is_active=True,is_deleted=False)
                        for product_category in product_category:
                            get_product.append(product_category.pk)
                            product_category=ProductCategoryMaster.objects.filter(parent=product_category.pk,is_active=True,is_deleted=False)
                            for product_category in product_category:
                                get_product.append(product_category.pk)
            get_product.reverse()
            if len(placed_orders)>0:
                for i in get_product:
                    for placed_order in placed_orders:
                        order_detail = OrderDetails.objects.filter(order_id_id=placed_order.pk,created_on__month=DST.created_on.month,created_on__year=DST.created_on.year,product_id__product_category_id=i)
                        for order_detail in order_detail:
                            if order_detail.product_id:
                                category = ProductCategoryMaster.objects.filter(is_active=True,is_deleted=False).values_list('category_name')
                                category = [li[0] for li in category]
                                ctaxdata=order_detail.cgst_rate
                                staxdata=order_detail.sgst_rate
                                total_tax_percent = ctaxdata + staxdata
                                c_percent = 100 * (float(ctaxdata)/float(total_tax_percent))
                                s_percent = 100 * (float(staxdata)/float(total_tax_percent))
                                prg = int(order_detail.total_amount) / (1 + (float(ctaxdata) / 100) + (float(staxdata) / 100))
                                total_tax = int(order_detail.total_amount) - prg
                                cgstobj = (c_percent / 100) * total_tax
                                cgst = float("{:.2f}".format(cgstobj))
                                sgstobj = (s_percent / 100) * total_tax
                                sgst = float("{:.2f}".format(sgstobj))
                                #pregst = float("{:.2f}".format(prg))
                                totalbill=total_tax+cgst+sgst
                                dealeramt=int(order_detail.total_amount)-int(totalbill)
                                # order+=dealeramt
                                order += prg
            if category is not None and DST.category.category_name in category:
                complete_target=order
            else:
                complete_target=0
            DST_gt['category'] = DST.category.category_name
            DST_gt['monthly_target']=monthly_target
            DST_gt['dealer']=DST.dealer.user_code
            DST_gt['fname']=DST.dealer.first_name
            DST_gt['lname']=DST.dealer.last_name
            DST_gt['did']=DST.dealer.pk
            DST_gt['created_on']=DST.created_on
            if order!=0:
                DST_gt['complete_target']=complete_target
            else:
                DST_gt['complete_target']=0
            if DST_gt['complete_target']>0:
                Remain=int(DST_gt['monthly_target'])-int(DST_gt['complete_target'])
                DST_gt['Remain']=str(Remain)
            else:
                DST_gt['Remain']=str(int(DST_gt['monthly_target'])-int(DST_gt['complete_target']))
            if '-' in DST_gt['Remain']:
                    DST_gt['Remain']='0'

            try:
                _x_dealer_profile = DealerProfile.objects.get(
                    dealer_id=DST.dealer, is_active=True, is_deleted=False)
                DST_gt["company"] = _x_dealer_profile.company_name
            except DealerProfile.DoesNotExist:
                DST_gt["company"] = ""

            get_target.append(DST_gt)
        objdst=DealerSalesTarget.objects.filter(dealer_id__in=dealer).aggregate(Sum('monthly_target'))
        target_total=DealerSalesTarget.objects.filter(dealer_id__in=dealer,created_on__month=month,created_on__year=datetime.datetime.now().year,is_active=True, is_deleted=False).aggregate(Sum('monthly_target'))
        # .................
        total_outstanding_amount = OrderHead.objects.filter(dealer_id__in=dealer,
            status__in=['Dispatched','Delivered','PartialDispatched', ],
                        is_active=True,is_deleted=False).aggregate(Sum('payable_amount'))
        # .................
        if total_outstanding_amount['payable_amount__sum'] is None:
            total_outstanding_amount['payable_amount__sum'] = 0.00
        if target_total['monthly_target__sum'] is None:
            target_total['monthly_target__sum'] = 0
        if objdst['monthly_target__sum'] is None:
            objdst['monthly_target__sum'] = 0
        #outstanding=int(objdst['monthly_target__sum'])-int(pay['total_amount__sum'])

    context={
        'objdst':target_total,
        'objdbm':objdbm,
        'orderhead':orderhead,
        'get_target':get_target,
        'orderheadactive':orderheadactive,
        'outstanding':total_outstanding_amount["payable_amount__sum"],
        'product':product,
        'month':month,
        'year':year,
        'Dealer_Month':datetime.datetime.now(),
        'querysetcount':querysetcount,
    }
    return render(request,'admin/Admin_dashboard.html',context)


def admin_orders(request):
    orderhead=''
    searchValue=''
    querycount=''
    od_status=request.GET.get('status')
    from_date = request.POST.get('from_date', None)
    to_date = request.POST.get('to_date', None)
    if request.user.is_superuser:
        if od_status=='All':
            dealer=User.objects.filter(is_active=True).values_list('pk')
            orderhead=OrderHead.objects.filter(
                dealer_id__in=dealer,
                status__in=status_in.AdminStatus(),is_active=True,is_deleted=False).order_by('-order_date').exclude(order_date__exact=None)
            orderheadnull = OrderHead.objects.filter(
                dealer_id__in=dealer,order_date=None, status__in=['SchemeApplied','OrderPlaced','InWarehouse','OnHold','Dispatched','Delivered','PartialDispatched','Processing','Deleted'],is_active=True,is_deleted=False).order_by('-order_date')
            orderheadcount=OrderHead.objects.filter(
                dealer_id__in=dealer,
                status__in=['SchemeApplied','OrderPlaced','InWarehouse','OnHold','Dispatched','Delivered','PartialDispatched','Processing','Deleted'],is_active=True,is_deleted=False).count()
            orderheadsum=OrderHead.objects.filter(
                dealer_id__in=dealer,
                status__in=['SchemeApplied','OrderPlaced','InWarehouse','OnHold','Dispatched','Delivered','PartialDispatched','Processing','Deleted'],is_active=True,is_deleted=False).aggregate(Sum('total_amount'))
            ohc=OrderHead.objects.filter(
                dealer_id__in=dealer,
                status__in=['SchemeApplied','OrderPlaced','InWarehouse','OnHold','Dispatched','Delivered','PartialDispatched','Processing','Deleted'],is_active=True,is_deleted=False).values_list('dealer_id')        
            dealerC = User.objects.filter(pk__in=ohc,groups__name="DEL",is_active=True).count()
            if orderheadsum['total_amount__sum'] is None:
                orderheadsum['total_amount__sum'] = 0
        else:
            dealer=User.objects.filter(is_active=True).values_list('pk')
            orderhead=OrderHead.objects.filter(
                dealer_id__in=dealer,
                status=od_status,is_active=True,is_deleted=False).order_by('-order_date').exclude(order_date__exact=None)
            orderheadnull = OrderHead.objects.filter(
                dealer_id__in=dealer,order_date=None, status=od_status,is_active=True,is_deleted=False).order_by('-order_date')
            orderheadcount=OrderHead.objects.filter(
                dealer_id__in=dealer,
                status=od_status,is_active=True,is_deleted=False).count()
            orderheadsum=OrderHead.objects.filter(
                dealer_id__in=dealer,
                status=od_status,is_active=True,is_deleted=False).aggregate(Sum('total_amount'))
            ohc=OrderHead.objects.filter(
                dealer_id__in=dealer,
                status__in=od_status,is_active=True,is_deleted=False).values_list('dealer_id')        
            dealerC = User.objects.filter(pk__in=ohc,groups__name="DEL",is_active=True).count()
            if orderheadsum['total_amount__sum'] is None:
                orderheadsum['total_amount__sum'] = 0
        # if request.method=='POST':
        #     if "search" in request.POST:
        #         searchValue = request.POST.get('search', None)
        #         orderhead=OrderHead.objects.filter(
        #     dealer_id__in=dealer,
        #     status=searchValue,is_active=True,is_deleted=False).order_by('-order_date').exclude(order_date__exact=None)
        #     querycount=str(orderhead.count())+' Record found.'
        if from_date is not None and from_date != "":
            if to_date is None or to_date == "":
                to_date =  datetime.date.today().strftime("%Y-%m-%d")
            orderhead = orderhead.filter(order_date__date__gte=from_date, order_date__date__lte=to_date)
        orderhead=list(chain(orderhead,orderheadnull))
    else:
        if od_status=='All':
            dealer = User.objects.filter(pk=request.user.id,groups__name="DEL",is_active=True).values_list('pk')
            #orderhead = OrderHead.objects.filter(dealer_id__in=dealer,status__in=['SchemeApplied','OrderPlaced','InWarehouse','OnHold','Dispatched','Delivered','PartialDispatched','Processing','Deleted'],is_active=True,is_deleted=False).order_by('-order_date')
            orderhead=OrderHead.objects.filter(
                dealer_id__in=dealer,
                status__in=status_in.AdminStatus(),is_active=True,is_deleted=False).order_by('-order_date').exclude(order_date__exact=None)
            orderheadnull = OrderHead.objects.filter(
                dealer_id__in=dealer,order_date=None, status__in=['SchemeApplied','OrderPlaced','InWarehouse','OnHold','Dispatched','Delivered','PartialDispatched','Processing','Deleted'],is_active=True,is_deleted=False).order_by('-order_date')
            orderheadcount=OrderHead.objects.filter(
                dealer_id__in=dealer,
            status__in=['SchemeApplied','OrderPlaced','InWarehouse','OnHold','Dispatched','Delivered','PartialDispatched','Processing','Deleted'],is_active=True,is_deleted=False).count()
            ohc=OrderHead.objects.filter(
                dealer_id__in=dealer,
                status__in=['SchemeApplied','OrderPlaced','InWarehouse','OnHold','Dispatched','Delivered','PartialDispatched','Processing','Deleted'],is_active=True,is_deleted=False).values_list('dealer_id')        
            dealerC = User.objects.filter(pk__in=ohc,groups__name="DEL",is_active=True).count()
            orderheadsum=OrderHead.objects.filter(
                dealer_id__in=dealer,
                status__in=['SchemeApplied','OrderPlaced','InWarehouse','OnHold','Dispatched','Delivered','PartialDispatched','Processing','Deleted'],is_active=True,is_deleted=False).aggregate(Sum('total_amount'))
            if orderheadsum['total_amount__sum'] is None:
                orderheadsum['total_amount__sum'] = 0
        else:
            dealer=User.objects.filter(pk=request.user.id,groups__name="DEL",is_active=True).values_list('pk')
            orderhead=OrderHead.objects.filter(
                dealer_id__in=dealer,
                status=od_status,is_active=True,is_deleted=False).order_by('-order_date').exclude(order_date__exact=None)
            orderheadnull = OrderHead.objects.filter(
                dealer_id__in=dealer,order_date=None, status=od_status,is_active=True,is_deleted=False).order_by('-order_date')
            orderheadcount=OrderHead.objects.filter(
                dealer_id__in=dealer,
                status=od_status,is_active=True,is_deleted=False).count()
            orderheadsum=OrderHead.objects.filter(
                dealer_id__in=dealer,
                status=od_status,is_active=True,is_deleted=False).aggregate(Sum('total_amount'))
            ohc=OrderHead.objects.filter(
                dealer_id__in=dealer,
                status=od_status,is_active=True,is_deleted=False).values_list('dealer_id')
            dealerC = User.objects.filter(pk__in=ohc,groups__name="DEL",is_active=True).count()
            if orderheadsum['total_amount__sum'] is None:
                orderheadsum['total_amount__sum'] = 0
        orderhead = orderhead.filter(order_date__date__gte=from_date, order_date__date__lte=to_date)
        orderhead=list(chain(orderhead,orderheadnull))
    if request.user.is_superuser==False:
        for obj in orderhead:
            dealerProfile = DealerProfile.objects.filter(
                dealer=obj.dealer, is_deleted=False).first()
            obj.__dict__["company"] = dealerProfile.company_name
            dealerBusiness = DealerBusinessMaster.objects.filter(
                brand=dealerProfile.brand, dealer=dealerProfile.dealer,
                is_active=True, is_deleted=False).first()
            obj.__dict__['creditdays']= dealerBusiness
            # ........................
            try:
                orderPlaced = OrderEventHistory.objects.get(order_id=obj, order_status='Dispatched')
                today = datetime.date.today()
                order_placed_day = orderPlaced.created_on
                diff = today - order_placed_day.date()
                days = diff.days

                credit_days = dealerBusiness.credit_days
                remaining_days = credit_days - days
                if remaining_days < 0:
                    remaining_days = 0
                dealerBusiness.credit_days = remaining_days
                obj.__dict__['Dispatched_Date'] = order_placed_day
            except Exception:
                pass
    else:
        for obj in orderhead:
            try:
                orderPlaced = OrderEventHistory.objects.get(order_id=obj, order_status='Dispatched')
                today = datetime.date.today()
                order_placed_day = orderPlaced.created_on

                obj.__dict__['Dispatched_Date'] = order_placed_day
            except Exception:
                pass

            try:
                _x_dealer_profile = DealerProfile.objects.get(
                    dealer_id=obj.dealer, is_active=True, is_deleted=False)
                obj.__dict__["company"] = _x_dealer_profile.company_name
            except DealerProfile.DoesNotExist:
                obj.__dict__["company"] = ""
    if request.method == 'POST':
        if 'btnsearch' in request.POST:
            if "search" in request.POST:
                searchValue = request.POST.get('search', None)
                if searchValue!='':
                    orderhead=OrderHead.objects.filter(
                    dealer_id__in=dealer,
                    order_number=searchValue,is_active=True,is_deleted=False).order_by('-order_date').exclude(order_date__exact=None)
                    querycount=str(orderhead.count())+' Record found.'

                    if request.user.is_superuser == False:
                        for obj in orderhead:
                            dealerProfile = DealerProfile.objects.filter(
                                dealer=obj.dealer, is_deleted=False).first()
                            obj.__dict__["company"] = dealerProfile.company_name
                            dealerBusiness = DealerBusinessMaster.objects.filter(
                                brand=dealerProfile.brand, dealer=dealerProfile.dealer,
                                is_active=True, is_deleted=False).first()
                            obj.__dict__['creditdays'] = dealerBusiness
                            # ........................
                            try:
                                orderPlaced = OrderEventHistory.objects.get(order_id=obj, order_status='Dispatched')
                                today = datetime.date.today()
                                order_placed_day = orderPlaced.created_on
                                diff = today - order_placed_day.date()
                                days = diff.days

                                credit_days = dealerBusiness.credit_days
                                remaining_days = credit_days - days
                                if remaining_days < 0:
                                    remaining_days = 0
                                dealerBusiness.credit_days = remaining_days
                                obj.__dict__['Dispatched_Date'] = order_placed_day
                            except Exception:
                                pass
                    else:
                        for obj in orderhead:
                            try:
                                orderPlaced = OrderEventHistory.objects.get(order_id=obj, order_status='Dispatched')
                                today = datetime.date.today()
                                order_placed_day = orderPlaced.created_on

                                obj.__dict__['Dispatched_Date'] = order_placed_day
                            except Exception:
                                pass

                            try:
                                _x_dealer_profile = DealerProfile.objects.get(
                                    dealer_id=obj.dealer, is_active=True, is_deleted=False)
                                obj.__dict__["company"] = _x_dealer_profile.company_name
                            except DealerProfile.DoesNotExist:
                                obj.__dict__["company"] = ""

        if "export" in request.POST:
            result = []
            if len(orderhead) > 0:
                for order in orderhead:
                    try:
                        order_history = OrderEventHistory.objects.filter(
                            order_id=order, order_status="Dispatched").first()
                        dispatched_date = order_history.created_on.date()
                    except Exception:
                        dispatched_date = ""

                    order_date = order.order_date.date() if order.order_date else ""
                    order_dict = {
                        "Order No": order.order_number, "DMS No": order.DMS_order_number,
                        "Order Date": order_date, "Dispatched Date": dispatched_date,
                        "Invoice Value": order.total_amount, "Discount": order.discount_amount,
                        "Payable": order.payable_amount, "Status": order.get_status_display(),
                        "SR": "{0} {1}".format(order.sr_id.first_name, order.sr_id.last_name),
                        "Dealer": "{0}.{1}".format(order.dealer.user_code, order.company)
                    }

                    # get_order['SR Contact'] = order.sr_id.mobile
                    # get_order['Dealer Contact'] = order.dealer.mobile
                    result.append(order_dict)
                response = HttpResponse(content_type='text/csv')
                response['Content-Disposition'] = 'attachment;filename=order_list.csv'
                keys = ['Order No', 'DMS No', 'Order Date', 'Dispatched Date', 'Invoice Value',
                        'Discount', 'Payable', 'Status', 'SR', 'Dealer']
                writer = csv.DictWriter(response, keys)
                writer.writeheader()
                writer.writerows(result)
                return response

    # .............................
    my_head_count = len(orderhead)

    _x = [a.dealer for a in orderhead]
    _x = list(set(_x))
    my_dealer_count = len(_x)
    my_order_head_sum = 0
    for item in orderhead:
        my_order_head_sum = float(my_order_head_sum) + float(item.total_amount)
    # my_order_head_sum = len(orderhead)
    # ..............................
    page = request.GET.get('page', 1)
    paginator = Paginator(orderhead, 10)
    try:
        orderhead = paginator.page(page)
    except PageNotAnInteger:
        orderhead = paginator.page(1)
    except EmptyPage:
        orderhead = paginator.page(paginator.num_pages)

    context = {
        'orderhead':orderhead,
        # 'orderheadcount':orderheadcount,
        'orderheadcount': my_head_count,
        # 'orderheadsum':orderheadsum['total_amount__sum'],
        'orderheadsum': my_order_head_sum,
        'searchValue':searchValue,
        'querycount':querycount,
        # 'dealercount':dealerC,
        'dealercount': my_dealer_count,
        'od_status':od_status
    }
    return render(request, 'admin/orders.html', context)


def monthly_target_deatils(request):
    month = request.GET.get('month')
    year=request.GET.get('year')
    category_url=request.GET.get('category')
    complete=0
    searchValue = ''
    order=0
    get_year=[]
    target_year=DealerSalesTarget.objects.filter(is_active=True, is_deleted=False).order_by('-created_on')
    for years in target_year:
        if years.created_on.year not in get_year:
            get_year.append(years.created_on.year)
    category_master=ProductCategoryMaster.objects.filter(is_active=True, is_deleted=False)
    groups = request.user.groups.all()
    groups = [group.name for group in groups]
    if request.user.is_superuser :
        dealer=User.objects.filter(is_active=True,groups__name="DEL").values_list('pk')
    elif 'SR' in groups :
        SR = User.objects.get(id=request.user.id, groups__name="SR", is_active=True)
        sr_dealer = SRDealer.objects.filter(sr_id_id=SR.pk).values_list('dealer_id_id')
        dealer_profile = DealerProfile.objects.filter(
            pk__in=sr_dealer, is_active=True, is_deleted=False).values_list('dealer_id')
        dealer = User.objects.filter(is_active=True, pk__in=dealer_profile, groups__name="DEL").values_list('pk')


    target, objdst = get_monthly_target_detail(category_url, year, month, dealer)
    Remainamt = 0

    # for DST in target:
    #     category = None
    #     DST_get={}
    #     DST_get['category'] = DST.category.category_name
    #     monthly_target = DST.monthly_target
    #     placed_orders = OrderHead.objects.filter(status__in=['OrderPlaced','PaymentDone','InWarehouse','Dispatched'], dealer_id=DST.dealer,order_date__month=DST.created_on.month,order_date__year=DST.created_on.year)
    #     category = ProductCategoryMaster.objects.filter(pk=DST.category_id,is_active=True,is_deleted=False)
    #     get_product=[]
    #     for PC in category:
    #         p_id=PC.pk
    #         product_category=ProductCategoryMaster.objects.filter(parent=p_id,is_active=True,is_deleted=False)
    #         for product_category in product_category:
    #             get_product.append(product_category.pk)
    #             product_category=ProductCategoryMaster.objects.filter(parent=product_category.pk,is_active=True,is_deleted=False)
    #             for product_category in product_category:
    #                 get_product.append(product_category.pk)
    #                 product_category=ProductCategoryMaster.objects.filter(parent=product_category.pk,is_active=True,is_deleted=False)
    #                 for product_category in product_category:
    #                     get_product.append(product_category.pk)
    #                     product_category=ProductCategoryMaster.objects.filter(parent=product_category.pk,is_active=True,is_deleted=False)
    #                     for product_category in product_category:
    #                         get_product.append(product_category.pk)
    #     get_product.reverse()
    #     if len(placed_orders)>0:
    #         for i in get_product:
    #             for placed_order in placed_orders:
    #                 order_detail = OrderDetails.objects.filter(order_id_id=placed_order.pk,created_on__month=DST.created_on.month,created_on__year=DST.created_on.year,product_id__product_category_id=i)
    #                 for order_detail in order_detail:
    #                     if order_detail.product_id:
    #                         category = ProductCategoryMaster.objects.filter(is_active=True,is_deleted=False).values_list('category_name')
    #                         category = [li[0] for li in category]
    #                         ctaxdata=order_detail.cgst_rate
    #                         staxdata=order_detail.sgst_rate
    #                         total_tax_percent = ctaxdata + staxdata
    #                         c_percent = 100 * (float(ctaxdata)/float(total_tax_percent))
    #                         s_percent = 100 * (float(staxdata)/float(total_tax_percent))
    #                         prg = int(order_detail.total_amount) / (1 + (float(ctaxdata) / 100) + (float(staxdata) / 100))
    #                         total_tax = int(order_detail.total_amount) - prg
    #                         cgstobj = (c_percent / 100) * total_tax
    #                         cgst = float("{:.2f}".format(cgstobj))
    #                         sgstobj = (s_percent / 100) * total_tax
    #                         sgst = float("{:.2f}".format(sgstobj))
    #                         #pregst = float("{:.2f}".format(prg))
    #                         totalbill=total_tax+cgst+sgst
    #                         dealeramt=int(order_detail.total_amount)-int(totalbill)
    #                         order+=dealeramt
    #     if category is not None and DST.category.category_name in category:
    #         DST_get['complete_target']=order
    #         complete+=order
    #     else:
    #         DST_get['complete_target']=0
    #     DST_get['monthly_target']=monthly_target
    #     DST_get['dealer']=DST.dealer.user_code
    #     DST_get['created_on']=DST.created_on
    #     DST_get['did']=DST.dealer.pk
    #     if order>0:
    #         DST_get['Remain']=str(int(DST_get['monthly_target'])-int(DST_get['complete_target']))
    #     else:
    #         DST_get['complete_target']=0
    #         DST_get['Remain']=str(int(DST_get['monthly_target'])-int(DST_get['complete_target']))
    #     if '-' in DST_get['Remain']:
    #             DST_get['Remain']='0'
    #     Remainamt+=int(DST_get['Remain'])
    #     get_target.append(DST_get)

    if objdst['monthly_target__sum'] is None:
        objdst['monthly_target__sum'] = 0

    if request.method == 'POST':

        if 'export' in request.POST:
            result = []
            searchValue = request.POST.get('export', None)
            if searchValue:
                target = target.filter(
                    Q(dealer__user_code__icontains=searchValue) |
                    Q(dealer__first_name__icontains=searchValue) |
                    Q(dealer__last_name__icontains=searchValue) |
                    Q(dealer__email__icontains=searchValue) |
                    Q(dealer__mobile__icontains=searchValue))
            get_target, Remainamt, complete = get_monthly_target_export(target, order, Remainamt, complete)
            for targets in get_target:
                month={}
                if targets['created_on'].month == 1:
                    month['Month'] = 'January'
                elif targets['created_on'].month == 2:
                    month['Month'] = 'February'
                elif targets['created_on'].month == 3:
                    month['Month'] = 'March'
                elif targets['created_on'].month == 4:
                    month['Month'] = 'April'
                elif targets['created_on'].month == 5:
                    month['Month'] = 'May'
                elif targets['created_on'].month == 6:
                    month['Month'] = 'June'
                elif targets['created_on'].month == 7:
                    month['Month'] = 'July'
                elif targets['created_on'].month == 8:
                    month['Month'] = 'August'
                elif targets['created_on'].month == 9:
                    month['Month'] = 'September'
                elif targets['created_on'].month == 10:
                    month['Month'] = 'October'
                elif targets['created_on'].month == 11:
                    month['Month'] = 'November'
                elif targets['created_on'].month == 12:
                    month['Month'] = 'December'
                month['Category ID'] = targets['category']
                month['Dealer'] = "{}.{}".format(targets['dealer'], targets["company"])
                month['Monthly Target'] = targets['monthly_target']
                month['Target Achieved'] = targets['complete_target']
                month['Remaining Target'] = targets['Remain']
                # month["Company"] = targets["company"]
                result.append(month)
            response = HttpResponse(content_type='text/csv')
            response['Content-Disposition'] = 'attachment;filename=monthly_target.csv'
            keys = ['Category ID', 'Month', 'Dealer', 'Monthly Target',
                    'Target Achieved', 'Remaining Target']
            writer = csv.DictWriter(response, keys)
            writer.writeheader()
            writer.writerows(result)
            return response

        if 'search' in request.POST:
            searchValue = request.POST.get('search', None)
            if searchValue:
                target = target.filter(
                    Q(dealer__user_code__icontains=searchValue) |
                    Q(dealer__first_name__icontains=searchValue) |
                    Q(dealer__last_name__icontains=searchValue) |
                    Q(dealer__email__icontains=searchValue) |
                    Q(dealer__mobile__icontains=searchValue))

    get_target, Remainamt, complete = get_monthly_target_export(target, order, Remainamt, complete)

    # for item in get_target:
    #     item.dealer
    dealers = [item['dealer'] for item in get_target]
    dealers = list(set(dealers))
    context={
        'target':get_target,
        'dealers': dealers,
        'searchValue': searchValue,
        'month':month,
        'year':year,
        'objdst':objdst,
        'category_master':category_master,
        'outstanding':Remainamt,
        'category_url':category_url,
        'target_year':get_year,
        'pay':complete,}
    return render(request,'admin/monthly_target_details.html',context)


def get_monthly_target_detail(category_url, year, month, dealer):
    if year == 'All' and month == 'All':
        target = DealerSalesTarget.objects.all().order_by('-created_on')
        objdst = DealerSalesTarget.objects.filter(
            dealer_id__in=dealer).aggregate(Sum('monthly_target'))
    elif year == 'All':
        target = DealerSalesTarget.objects.filter(
            created_on__month=month, is_active=True,
            is_deleted=False).order_by('-created_on')
        objdst = DealerSalesTarget.objects.filter(
            created_on__month=month, dealer_id__in=dealer).aggregate(
            Sum('monthly_target'))
    elif month == 'All':
        target = DealerSalesTarget.objects.filter(
            created_on__year=year, is_active=True,
            is_deleted=False).order_by('-created_on')
        objdst = DealerSalesTarget.objects.filter(
            created_on__year=year, dealer_id__in=dealer).aggregate(
            Sum('monthly_target'))
    else:
        target = DealerSalesTarget.objects.filter(
            created_on__month=month, created_on__year=year,
            is_active=True, is_deleted=False,dealer_id__in=dealer).order_by('-created_on')
        objdst = DealerSalesTarget.objects.filter(
            created_on__month=month, created_on__year=year,
            dealer_id__in=dealer).aggregate(Sum('monthly_target'))
    if category_url != 'All':
        if year == 'All' and month == 'All':
            target = DealerSalesTarget.objects.filter(
                category_id=category_url, is_active=True,
                is_deleted=False).order_by('-created_on')
        elif month == 'All':
            target = DealerSalesTarget.objects.filter(
                category_id=category_url, created_on__year=year,
                is_active=True, is_deleted=False).order_by('-created_on')
        elif year == 'All':
            target = DealerSalesTarget.objects.filter(
                category_id=category_url, created_on__year=year,
                created_on__month=month, is_active=True,
                is_deleted=False).order_by('-created_on')
        else:
            target = DealerSalesTarget.objects.filter(
                category_id=category_url, created_on__month=month,
                is_active=True, is_deleted=False).order_by('-created_on')
    return target, objdst


def get_monthly_target_export(target, order, Remainamt, complete):
    get_target = []
    for DST in target:
        category = None
        DST_get = {'category': DST.category.category_name}
        monthly_target = DST.monthly_target
        placed_orders = OrderHead.objects.filter(
            status__in=['OrderPlaced', 'PaymentDone', 'InWarehouse', 'Dispatched'],
            dealer_id=DST.dealer, order_date__month=DST.created_on.month,
            order_date__year=DST.created_on.year, is_active=True, is_deleted=False)
        category = ProductCategoryMaster.objects.filter(pk=DST.category_id, is_active=True, is_deleted=False)
        get_product = []
        for PC in category:
            p_id = PC.pk
            product_category = ProductCategoryMaster.objects.filter(parent=p_id,is_active=True,is_deleted=False)
            for product_category in product_category:
                get_product.append(product_category.pk)
                product_category=ProductCategoryMaster.objects.filter(
                    parent=product_category.pk,is_active=True,is_deleted=False)
                for product_category in product_category:
                    get_product.append(product_category.pk)
                    product_category=ProductCategoryMaster.objects.filter(
                        parent=product_category.pk,is_active=True,is_deleted=False)
                    for product_category in product_category:
                        get_product.append(product_category.pk)
                        product_category=ProductCategoryMaster.objects.filter(
                            parent=product_category.pk,is_active=True,is_deleted=False)
                        for product_category in product_category:
                            get_product.append(product_category.pk)
        get_product.reverse()
        if len(placed_orders) > 0:
            for i in get_product:
                for placed_order in placed_orders:
                    order_detail = OrderDetails.objects.filter(
                        order_id_id=placed_order.pk, created_on__month=DST.created_on.month,
                        created_on__year=DST.created_on.year, product_id__product_category_id=i)
                    for order_detail in order_detail:
                        if order_detail.product_id:
                            category = ProductCategoryMaster.objects.filter(
                                is_active=True, is_deleted=False).values_list('category_name')
                            category = [li[0] for li in category]
                            ctaxdata = order_detail.cgst_rate
                            staxdata = order_detail.sgst_rate
                            total_tax_percent = ctaxdata + staxdata
                            c_percent = 100 * (float(ctaxdata)/float(total_tax_percent))
                            s_percent = 100 * (float(staxdata)/float(total_tax_percent))
                            prg = int(order_detail.total_amount) / (1 + (float(ctaxdata) / 100) + (float(staxdata) / 100))
                            total_tax = int(order_detail.total_amount) - prg
                            cgstobj = (c_percent / 100) * total_tax
                            cgst = float("{:.2f}".format(cgstobj))
                            sgstobj = (s_percent / 100) * total_tax
                            sgst = float("{:.2f}".format(sgstobj))
                            totalbill = total_tax + cgst + sgst
                            dealeramt = int(order_detail.total_amount) - int(totalbill)
                            # order += dealeramt
                            order += prg
        if category is not None and DST.category.category_name in category:
            DST_get['complete_target'] = order
            complete += order
        else:
            DST_get['complete_target'] = 0
        dealer_company = DealerProfile.objects.filter(dealer=DST.dealer)
        if len(dealer_company) >0:
            DST_get['company'] = dealer_company[0].company_name
        else:
            DST_get['company'] = '---'
        DST_get['monthly_target'] = monthly_target
        DST_get['dealer'] = DST.dealer.user_code
        DST_get['created_on'] = DST.created_on
        DST_get['did'] = DST.dealer.pk
        if order > 0:
            DST_get['Remain'] = str(int(DST_get['monthly_target']) - int(DST_get['complete_target']))
        else:
            DST_get['complete_target'] = 0
            DST_get['Remain'] = str(int(DST_get['monthly_target']) - int(DST_get['complete_target']))
        if '-' in DST_get['Remain']:
                DST_get['Remain'] = '0'
        Remainamt += int(DST_get['Remain'])
        get_target.append(DST_get)
    return get_target, Remainamt, complete


def sr_customer_order(request):
    orderno=request.GET.get('orderno')
    dealerid=request.GET.get('dealerid')
    jsdata={}
    if orderno!='' and orderno!=None and dealerid!='' and dealerid!=None:
        try:
            orderhead=OrderHead.objects.get(
                dealer_id=dealerid, order_number=int(orderno),
                status__in=['Processing', 'SchemeApplied', 'OrderPlaced', 'Deleted'])
        except Exception:
            orderhead=''
            orderno=''
        try:
            dealerpro=DealerProfile.objects.get(dealer_id=dealerid,is_active=True)
        except Exception:
            dealerpro=''
        if dealerpro!='':
            jsdata['company_name']=dealerpro.company_name
            jsdata['company_address']=dealerpro.company_address
            jsdata['email']=dealerpro.email
            jsdata['gst_no']=dealerpro.gst_no
            jsdata['pan_no']=dealerpro.pan_no
            jsdata['primary_contact']=dealerpro.primary_contact
            try:
                dealerbusiness=DealerBusinessMaster.objects.get(dealer_id=dealerid,is_active=True)
            except Exception:
                dealerbusiness=''
            if dealerbusiness!='':
                jsdata['credit_days']=dealerbusiness.credit_days
                jsdata['max_outstanding_threshold']=dealerbusiness.max_outstanding_threshold
            else:
                jsdata['credit_days']=''
                jsdata['max_outstanding_threshold']=''
            if orderhead!='':
                jsdata['total_amount']=float("{:.2f}".format(orderhead.total_amount))
            else:
                jsdata['total_amount']=''

    count=0
    totaldp=0
    totalafterallowance=0
    totalpregst=0
    totalsgst=0
    totalcgst=0
    totalbill=0
    ordernote=''
    orderdetail=''
    if orderhead!='':
        orderdetail=OrderDetails.objects.filter(order_id=orderhead.order_id)
        ordernote=OrderNotes.objects.filter(order_id=int(orderhead.order_id)).order_by('-id')

    for obj in orderdetail:
        objpm=ProductMaster.objects.get(sku_code=obj.sku_code)
        try:
            taxdata=TaxSlabMaster.objects.get(tax_slab_id=objpm.TaxSlabCode_id)
        except Exception:
            taxdata=''
        if taxdata!='':
            ctaxdata=taxdata.cgst_rate
            staxdata=taxdata.sgst_rate
            otaxdata=taxdata.other_tax_rate
            gtaxdata=taxdata.gst_rate
        else:
            ctaxdata=0
            staxdata=0
            otaxdata=0
            gtaxdata=0
        dpobj=float(objpm.dealer_price)*float(obj.quantity)
        totaldp+=float(dpobj)
        try:
            scheme=SchemeProductMaster.objects.get(product=objpm.product_id)
        except Exception:
            pass
        try:
            allowanceobj=AllowanceMaster.objects.get(dealer=dealerid,scheme_category_code=scheme.scheme_category_code)
            allowance=allowanceobj.discount_percent
        except Exception:
            allowance=0
        allowance=float(dpobj)/100*float(allowance)
        valueafterallowance=float(dpobj)-float(allowance)
        totalafterallowance+=float(valueafterallowance)
        total_tax_percent = ctaxdata + staxdata
        c_percent = 100 * (float(ctaxdata)/float(total_tax_percent))
        s_percent = 100 * (float(staxdata)/float(total_tax_percent))
        prg = valueafterallowance / (1 + (float(ctaxdata) / 100) + (float(staxdata) / 100))
        total_tax = valueafterallowance - prg
        cgstobj = (c_percent / 100) * total_tax
        cgst = float("{:.2f}".format(cgstobj))
        sgstobj = (s_percent / 100) * total_tax
        sgst = float("{:.2f}".format(sgstobj))
        pregst = float("{:.2f}".format(prg))
        totalbillobj=float("{:.2f}".format(pregst+cgst+sgst))
        totalcgst+=float(cgst)
        totalsgst+=float(sgst)
        totalpregst+=float(pregst)
        totalbill+=float(totalbillobj)
    context={
        'totaldp':totaldp,
        'totalafterallowance':totalafterallowance,
        'totalcgst':totalcgst,
        'totalsgst':totalsgst,
        'totalpregst':totalpregst,
        'totalbill':totalbill,
        'jsdata':jsdata,
    }
    return render(request,'dealer/sr_customer_order_detail.html',context)


# ===============================================================


def SROrderDetail(request, orderID=None):
    orderHead = OrderHead.objects.get(order_number=orderID, is_active=True, is_deleted=False)
    # orderDetails = OrderDetails.objects.filter(order_id=orderHead, is_active=True, is_deleted=False)
    dealerProfile = DealerProfile.objects.get(dealer=orderHead.dealer, is_active=True, is_deleted=False)
    dealerBusiness = DealerBusinessMaster.objects.get(
        brand=dealerProfile.brand, dealer=dealerProfile.dealer, is_active=True, is_deleted=False)
    context = get_order(request, orderID)
    context['dealer'] = dealerProfile
    context['dbm'] = dealerBusiness
    context['cc_status'] = False
    context["outstanding"] = orderHead.payable_amount
    context["order_id"] = orderHead.order_number
    context["order_date"] = orderHead.order_date
    context['dms_number'] = orderHead.DMS_order_number
    context["history"] = False
    context['on_hold_flag'] = False
    context['on_hold_date'] = ""

    # ........................
    try:
        orderPlaced = OrderEventHistory.objects.get(order_id=orderHead, order_status='Dispatched')
        today = datetime.date.today()
        order_placed_day = orderPlaced.created_on
        diff = today - order_placed_day.date()
        days = diff.days

        credit_days = dealerBusiness.credit_days
        remaining_days = credit_days - days
        if remaining_days < 0:
            remaining_days = 0
        dealerBusiness.credit_days = remaining_days
    except Exception:
        pass
    # ........................
    try:
        order_status_detail = OrderEventHistory.objects.filter(
            order_id=orderHead, order_status=orderHead.status).last()

        """ On Hold Order details"""
        if orderHead.status == "OnHold":
            context["on_hold_flag"] = True
            context["on_hold_date"] = order_status_detail.created_on

        if orderHead.status in status_in.SRStatus():
            status_detail = {
                "head_name": "SR Name", "head_number": "SR Number",
                "date": order_status_detail.created_on,
                "name": "", "number": ""
            }
            dealerProfile = DealerProfile.objects.filter(dealer=orderHead.dealer).first()
            srDealer = SRDealer.objects.filter(dealer_id=dealerProfile).first()
            status_detail["name"] = "{0} {1}".format(srDealer.sr_id.first_name, srDealer.sr_id.last_name)
            status_detail["number"] = srDealer.sr_id.mobile
        elif orderHead.status in status_in.ManagerStatus():
            status_detail = {
                "head_name": "WH User Name", "head_number": "WH User Number",
                "date": order_status_detail.created_on,
                "name": "{0} {1}".format(orderHead.warehouse_manager.first_name,
                                         orderHead.warehouse_manager.last_name),
                "number": orderHead.warehouse_manager.mobile}
        context["status_detail"] = status_detail
    except Exception:
        pass
    # ........................

    if orderHead.status in status_in.DetailStatus():
        after_scheme, total = get_scheme(request, orderID)
        context["total"] = total
        context["details"] = after_scheme

        credit_notes = get_credit_notes(orderID, request)
        if credit_notes['grand_total'] > 0:
            context["credit_notes"] = credit_notes
            context['cc_status'] = True

    if orderHead.status in status_in.ShowHistoryStatus():
        history = OrderEventHistory.objects.filter(order_id=orderHead).order_by('pk')
        context["orderHistory"] = history
        warehouse = orderHead.warehouse_code if orderHead.warehouse_code else ""

        if orderHead.warehouse_manager:
            warehouse_manager = "{0} {1}".format(
                orderHead.warehouse_manager.first_name, orderHead.warehouse_manager.last_name)
        else:
            warehouse_manager = ""
        context["warehouse"] = warehouse
        context["manager"] = warehouse_manager
        context["history"] = True
#----shivendra------------------------------------
    if orderID!='' and orderID!=None: 
        try:
            orderhead=OrderHead.objects.get(
                order_number=int(orderID),
                status__in=['Draft', 'Dispatched', 'InWarehouse', 'Processing', 'SchemeApplied', 'OrderPlaced', 'Deleted'])
        except Exception:
            orderhead=''
    ordernote=''
    orderdetail=''
    comment=[]
    if orderhead!='':
        #orderdetail=OrderDetails.objects.filter(order_id=orderhead.order_id)
        ordernote=OrderNotes.objects.filter(order_id=int(orderhead.order_id),is_active=True).order_by('-id')
        comment=[]
    if orderhead!='':
        for obj in ordernote:
            try:
                userobj=User.objects.get(pk=obj.created_by,is_active=True)
            except Exception:
                userobj=''
            js={}
            js['id']=obj.id

            try:
                noteobj=OrderNotes.objects.filter(order_id=int(obj.id),is_active=False)
            except Exception:
                noteobj=''
            if noteobj!='':
                notelist=[]
                for a in noteobj:
                    li={}
                    li['queryset']=a
                    pst = pytz.timezone('Asia/Kolkata')
                    abc=a.created_on.replace(tzinfo=pytz.utc).astimezone(pst)
                    aobj=datetime.datetime.strptime(abc.strftime('%Y-%m-%d %I:%M:%S %p'), '%Y-%m-%d %I:%M:%S %p')
                    li['childdate']=aobj#datetime.datetime.strptime(str(abc), '%b,%d,%y %I:%M:%S')
                    try:
                        userobject=User.objects.get(pk=a.created_by,is_active=True)
                    except Exception:
                        userobject=''
                    if userobject!='':
                        li['createbyname']=str(str(userobject.first_name)+' '+str(userobject.last_name))
                    else:
                        li['createbyname']=''
                    if userobject != '':
                        if userobject.avatar!='' and userobject.avatar!=None:
                            li['image']=userobject.avatar
                        else:
                            li['image']=''
                    else:
                        li['image']=''
                    notelist.append(li)
                js['childnote']=notelist
            else:
                js['childnote']=noteobj
            js['createby']=int(obj.created_by)
            if userobj!='':
                js['name']=str(str(userobj.first_name)+' '+str(userobj.last_name))
            else:
                js['name']=''
            pst = pytz.timezone('Asia/Kolkata')
            ab=obj.created_on.replace(tzinfo=pytz.utc).astimezone(pst)
            aaa=datetime.datetime.strptime(ab.strftime('%Y-%m-%d %I:%M:%S %p'), '%Y-%m-%d %I:%M:%S %p')
            js['date']=aaa
            if obj.description!='' and obj.description!=None:
                js['description']=obj.description
            else:
                js['description']=''
            if userobj.avatar!='' and userobj.avatar!=None:
                js['image']=userobj.avatar
            else:
                js['image']=''
            comment.append(js)
    if request.method=='POST':
        if 'commentbtn' in request.POST:
            try:
                userobj=User.objects.get(pk=request.user.id,groups__name='SR')
            except Exception:
                userobj=''
            if userobj!='':
                try:
                    orderheadobj=OrderHead.objects.get(
                        sr_id_id=request.user.id,order_number=int(orderID),
                        status__in=['Draft','Processing','Dispatched', 'InWarehouse','SchemeApplied', 'OrderPlaced', 'Deleted'])
                except Exception:
                    orderheadobj=''
            else:
                try:
                    orderheadobj=OrderHead.objects.get(
                        dealer_id=request.user.id,order_number=int(orderID),
                        status__in=['Draft','Processing','Dispatched', 'InWarehouse','SchemeApplied', 'OrderPlaced', 'Deleted'])
                except Exception:
                    orderheadobj=''
            if orderheadobj!='':
                ordernoteonj=OrderNotes()
                ordernoteonj.order_id=int(orderheadobj.pk)
                ordernoteonj.created_by=request.user.id
                ordernoteonj.description=request.POST.get('ordernote')
                ordernoteonj.save()
            return HttpResponseRedirect('/user/sr-order-detail/'+str(orderID))
        if 'replybtn' in request.POST:
            from django.utils import timezone
            now = timezone.now()
            try:
                userobj=User.objects.get(pk=request.user.id,groups__name='SR')
            except Exception:
                userobj=''
            if userobj!='':
                try:
                    orderheadobj=OrderHead.objects.get(
                        sr_id_id=request.user.id,order_number=int(orderID),
                        status__in=['Draft','Processing','Dispatched', 'InWarehouse','SchemeApplied', 'OrderPlaced', 'Deleted'])
                except Exception:
                    orderheadobj=''
            else:
                try:
                    orderheadobj=OrderHead.objects.get(
                        dealer_id=request.user.id,order_number=int(orderID),
                        status__in=['Draft','Processing','Dispatched', 'InWarehouse','SchemeApplied', 'OrderPlaced', 'Deleted'])
                except Exception:
                    orderheadobj=''
            if orderheadobj!='':
                try:
                    obj=OrderHead.objects.get(is_active=True,is_deleted=False,order_number=orderID)
                except Exception:
                    obj=''
                if obj!='':
                    ordernoteonj=OrderNotes()
                    ordernoteonj.order_id=int(request.POST.get('replybtn'))
                    ordernoteonj.created_by=request.user.id
                    ordernoteonj.dealer_id=obj.dealer_id
                    ordernoteonj.sr_id=obj.sr_id_id
                    ordernoteonj.description=request.POST.get('ordernote'+request.POST.get('replybtn'))
                    ordernoteonj.is_active=False
                    ordernoteonj.save()
            return HttpResponseRedirect('/user/sr-order-detail/'+str(orderID))
    context['orderID']=orderID
    context['comment']=comment
    return render(request, 'sr/custom_order_detail.html', context=context)
    # return render(request, 'sr/order_detail.html', context=context)


# ====================


def apply_tieup(request, tieupWithScheme=None):
    orderID = request.GET.get('order')
    tieup = request.GET.get('tieup')

    for item in tieup.split('|'):
        productID = item.split('_')[0]
        sr_tie_up = item.split('_')[1]

        orderDetail = OrderDetails.objects.get(pk=productID, is_active=True, is_deleted=False)
        try:
            scheme = SchemeProductMaster.objects.get(
                product=orderDetail.product_id, is_active=True, is_deleted=False)
        except SchemeProductMaster.DoesNotExist:
            continue

        try:
            scheme_category = SchemeCategoryMaster.objects.get(
                scheme_category_code__icontains=scheme.scheme_category_code,
                is_active=True, is_deleted=False)
        except SchemeCategoryMaster.DoesNotExist:
            continue

        try:
            tieup = TieUpMaster.objects.get(
                dealer=orderDetail.order_id.dealer, brand=orderDetail.order_id.brand,
                scheme_category_code=scheme_category, is_active=True, is_deleted=False)
        except TieUpMaster.DoesNotExist:
            continue

        try:
            order_tieup = OrderTieUpDetails.objects.get(
                order_detail_id=orderDetail, product_id=orderDetail.order_id.brand,
                sku_code=orderDetail.sku_code, order_id=orderDetail.order_id, is_active=True, is_deleted=False)
        except OrderTieUpDetails.DoesNotExist:
            order_tieup = OrderTieUpDetails(
                order_detail_id=orderDetail,
                product_id=orderDetail.order_id.brand,
                sku_code=orderDetail.sku_code,
                order_id=orderDetail.order_id,
                max_tie_up_percent=tieup.discount_percent,
                max_tie_up_amount=tieup.discount_amount
            )
        if tieup.discount_percent:
            order_tieup.tie_up_percent = float(sr_tie_up)
        elif tieup.discount_amount:
            order_tieup.tie_up_amount = int(sr_tie_up)
        order_tieup.save()

        context, total = get_scheme(request, orderID)
        """ Minus discount from total amount """
        try:
            amount = float(total["sr_total"])
            discount = float(total["less_tieup"]) + float(total["less_sell"])

            orderHead = orderDetail.order_id
            orderHead.total_amount = amount
            orderHead.discount_amount = discount
            orderHead.payable_amount = amount - discount
            orderHead.save()
        except Exception:
            pass
    # ..............................

    if tieupWithScheme is not None:
        return True

    context, total = get_scheme(request, orderID)
    return JsonResponse({"data": context, "total": total})


def send_to_dealer(request):
    orderID = request.GET.get('order')
    pre_gst = request.GET.get('dealer')

    orderHead = OrderHead.objects.get(order_number=orderID)
    orderDetail = OrderDetails.objects.filter(order_id=orderHead)

    for order in orderDetail:
        try:
            sell_credit_master = CreditNoteTypeMaster.objects.get(
                note_type_code__icontains='Sell', is_active=True, is_deleted=False)
        except CreditNoteTypeMaster.DoesNotExist:
            sell_credit_master = CreditNoteTypeMaster(
                note_type_code="Sell",
                note_type_name="Sell",
                description="Sell"
            )
            sell_credit_master.save()

        try:
            tieup_credit_master = CreditNoteTypeMaster.objects.get(
                note_type_code__icontains='Tieup', is_active=True, is_deleted=False)
        except CreditNoteTypeMaster.DoesNotExist:
            tieup_credit_master = CreditNoteTypeMaster(
                note_type_code="Tieup",
                note_type_name="Tieup",
                description="Tieup"
            )
            tieup_credit_master.save()

        pre_amount = 0
        pre_gst_tieup = 0
        for pre in pre_gst.split('|'):
            orderid = pre.split('_')[0]
            amount = pre.split('_')[1]
            if order.pk == int(orderid):
                pre_amount = amount

        try:
            order_tieup = OrderTieUpDetails.objects.get(
                order_detail_id=order, product_id=orderHead.brand,
                sku_code=order.sku_code, order_id=orderHead, is_active=True, is_deleted=False)
            if order_tieup.tie_up_percent:
                sr_tie_up = order_tieup.tie_up_percent
                pre_gst_tieup = float(pre_amount) * float(sr_tie_up) / 100

            elif order_tieup.tie_up_amount:
                sr_tie_up = order_tieup.tie_up_amount
                pre_gst_tieup = sr_tie_up

            try:
                tieup_credit = CreditNotes.objects.get(
                    dealer=orderHead.dealer, note_type=tieup_credit_master,
                    tieup_sell_through=order_tieup.pk, order_id=orderHead, order_detail_id=order,
                    is_active=True, is_deleted=False)
            except CreditNotes.DoesNotExist:
                tieup_credit = CreditNotes(
                    dealer=orderHead.dealer,
                    note_type=tieup_credit_master,
                    order_id=orderHead,
                    order_detail_id=order,
                    tieup_sell_through=order_tieup.pk
                )

            tieup_credit.credit_amount = pre_gst_tieup
            # if pre_gst_tieup > 0:
            tieup_credit.save()
        except OrderTieUpDetails.DoesNotExist:
            pass
        except Exception:
            pass

        try:
            order_sell_through = OrderSchemeDetailsSellThrough.objects.filter(
                order_detail_id=order, product_id=order.order_id.brand,
                order_id=order.order_id, is_active=True, is_deleted=False)
            for sell in order_sell_through:
                # sr_sell_through = 0
                percent = sell.scheme_percent
                per_amt = float(pre_amount) * float(percent) / 100
                amount = sell.scheme_amount

                sr_sell_through = per_amt + amount

                try:
                    sell_through = CreditNotes.objects.get(
                        dealer=orderHead.dealer, note_type=sell_credit_master,
                        tieup_sell_through=sell.pk, order_id=orderHead, order_detail_id=order,
                        is_active=True, is_deleted=False)
                except CreditNotes.DoesNotExist:
                    sell_through = CreditNotes(
                        dealer=orderHead.dealer,
                        note_type=sell_credit_master,
                        order_id=orderHead,
                        order_detail_id=order,
                        tieup_sell_through=sell.pk
                    )

                sell_through.credit_amount = sr_sell_through
                # if sr_sell_through > 0:
                sell_through.save()
        except:
            pass

    orderHead.status = 'SchemeApplied'
    orderHead.save()

    # ......... Order History .........
    orderHistory = OrderEventHistory(
        order_id=orderHead,
        order_status=orderHead.status,
        created_by=request.user
    )
    orderHistory.save()
    # .................................email send---------------
    try:
        send_sms_or_email(request, orderHead)
    except Exception:
        pass

    return JsonResponse({"msg": "done"})


def apply_scheme(request):
    product = request.GET.get('order')
    schemes = request.GET.get('scheme')
    sr_tie_up = request.GET.get('tieup')

    tieup = sr_tie_up.split('_')[1]
    if float(tieup) >= 0:
        apply_tieup(request, sr_tie_up)

    orderDetail = OrderDetails.objects.get(pk=product, is_active=True, is_deleted=False)
    for item in schemes.split('|'):
        sellID = item.split('_')[1]
        discount_percent = item.split('_')[2]
        discount_amount = item.split('_')[3]

        try:
            sell_through = SchemeSellThroughMaster.objects.get(pk=sellID, is_active=True, is_deleted=False)
        except SchemeSellThroughMaster.DoesNotExist:
            continue

        try:
            order_scheme = OrderSchemeDetailsSellThrough.objects.get(
                scheme_code=sell_through.scheme_code,
                order_detail_id=orderDetail, order_id=orderDetail.order_id,
                product_id=orderDetail.order_id.brand,
                sku_code=orderDetail.sku_code, is_active=True, is_deleted=False)
        except OrderSchemeDetailsSellThrough.DoesNotExist:
            order_scheme = OrderSchemeDetailsSellThrough(
                order_detail_id=orderDetail,
                product_id=orderDetail.order_id.brand,
                sku_code=orderDetail.sku_code,
                order_id=orderDetail.order_id,
                scheme_code=sell_through.scheme_code,
                scheme_name=sell_through.scheme_name,
                max_scheme_percent=sell_through.discount_percent,
                max_scheme_amount=sell_through.discount_amount
            )
        order_scheme.scheme_percent = float(discount_percent)
        order_scheme.scheme_amount = int(discount_amount)
        order_scheme.save()

    context, total = get_scheme(request, orderDetail.order_id.order_number)

    try:
        amount = float(total["sr_total"])
        discount = float(total["less_tieup"]) + float(total["less_sell"])

        orderHead = orderDetail.order_id
        orderHead.total_amount = amount
        orderHead.discount_amount = discount
        orderHead.payable_amount = amount - discount
        orderHead.save()
    except Exception:
        pass
    return JsonResponse({"data": context, "total": total})


def applyed_scheme(request):
    applyed = OrderSchemeDetailsSellThrough.objects.filter(
        pk=request.GET['id'], is_active=True, is_deleted=False)
    applyedSchemes = []
    for scheme in applyed:
        scheme_master = SchemeSellThroughMaster.objects.filter(
            brand=scheme.order_id.brand, product=scheme.order_detail_id.product_id,
            scheme_code=scheme.scheme_code, is_active=True, is_deleted=False).first()
        applyedSchemes.append({
            "order_id": scheme.order_detail_id.pk,
            "scheme_id": scheme_master.pk,
            "scheme_name": scheme.scheme_name,
            "scheme_code": scheme.scheme_code,
            "discount_percent": str(scheme_master.discount_percent),
            "discount_amount": scheme_master.discount_amount,
            "percent": str(scheme.scheme_percent),
            "amount": scheme.scheme_amount,
            "scheme_start_date": scheme_master.scheme_start_date.strftime('%d-%B-%Y'),
            "scheme_end_date": scheme_master.scheme_end_date.strftime('%d-%B-%Y'),
        })
    return JsonResponse({"data": applyedSchemes})


# **********************************************************************


def get_order(request, orderno):
    groups = [group.name for group in request.user.groups.all()]
    if 'SR' in groups:
        srDealers = SRDealer.objects.filter(sr_id=request.user)
        listDealer = []
        for srdealer in srDealers:
            if srdealer.dealer_id:
                if srdealer.dealer_id.dealer:
                    listDealer.append(srdealer.dealer_id.dealer)
        srDealers = listDealer
    elif 'DEL' in groups:
        srDealers = [request.user]
    else:
        orderHead = OrderHead.objects.filter(order_number=orderno, is_active=True, is_deleted=False).first()
        srDealers = [orderHead.dealer]
    # ....................................................
    # srDealers = SRDealer.objects.filter(sr_id=request.user)
    # listDealer = []
    # for srdealer in srDealers:
    #     if srdealer.dealer_id:
    #         if srdealer.dealer_id.dealer:
    #             listDealer.append(srdealer.dealer_id.dealer)
    # srDealers = listDealer

    # status__in = ['Processing', 'SchemeApplied', 'OrderPlaced', 'Deleted', 'InWarehouse'],
    orderHead = OrderHead.objects.filter(
        dealer__in=srDealers, order_number=orderno,
        is_active=True, is_deleted=False).first()
    orderDetails = OrderDetails.objects.filter(order_id=orderHead, is_active=True, is_deleted=False)

    totaldp = 0
    totalafterallowance = 0
    totalpregst = 0
    totalsgst = 0
    totalcgst = 0
    totalbill = 0
    percent = 0
    for order in orderDetails:
        try:
            objpm = ProductMaster.objects.get(sku_code=order.sku_code, is_active=True, is_deleted=False)
            # taxdata = TaxSlabMaster.objects.get(tax_slab_id=objpm.TaxSlabCode_id, is_active=True, is_deleted=False)
        except:
            pass
            # taxdata = None

        # if taxdata:
        #     ctaxdata = taxdata.cgst_rate
        #     staxdata = taxdata.sgst_rate
        # else:
        #     ctaxdata = 0
        #     staxdata = 0

        ctaxdata = order.cgst_rate
        staxdata = order.sgst_rate
        otaxdata = order.other_tax_rate
        gtaxdata = order.gst_rate

        # dpobj = float(objpm.dealer_price) * float(order.quantity)
        # dpobj = float(order.dealer_price) * float(order.quantity)
        # totaldp += float(dpobj)

        dpobj = float(order.dealer_price) / float(order.quantity)
        totaldp += float(dpobj) * float(order.quantity)

        try:
            scheme = SchemeProductMaster.objects.get(
                brand=orderHead.brand, product=order.product_id, is_active=True, is_deleted=False)
        except:
            pass

        try:
            scheme_category = SchemeCategoryMaster.objects.get(
                scheme_category_code__icontains=scheme.scheme_category_code, is_active=True, is_deleted=False)

            # allowanceobj = AllowanceMaster.objects.get(
            #     brand=orderHead.brand, dealer=orderHead.dealer,
            #     scheme_category_code=scheme_category, is_active=True, is_deleted=False)
            #
            # if allowanceobj.discount_percent:
            #     allowanceValue = str(allowanceobj.discount_percent)
            # elif allowanceobj.discount_amount:
            #     allowanceValue = str(allowanceobj.discount_amount)
            # else:
            #     allowanceValue = str(0)

            allowanceobj = OrderAllowanceDetails.objects.get(
                order_detail_id=order, product_id=order.product_id,
                order_id=order.order_id, is_active=True, is_deleted=False)

            if allowanceobj.allowance_percent:
                allowanceValue = str(allowanceobj.allowance_percent)
            elif allowanceobj.allowance_amount:
                allowanceValue = str(allowanceobj.allowance_amount)
            else:
                allowanceValue = str(0)

            jsonallowance = allowanceValue
            percent = allowanceValue
        except Exception:
            jsonallowance = 0
            percent = 0

        # =================================================

        allowance = float(dpobj) / 100 * float(jsonallowance)
        valueafterallowance = (float(dpobj) - float(allowance)) * float(order.quantity)
        afterallowance = float(dpobj) - float(allowance)
        totalafterallowance += float(valueafterallowance)
        total_tax_percent = ctaxdata + staxdata
        c_percent = 100 * (float(ctaxdata) / float(total_tax_percent))
        s_percent = 100 * (float(staxdata) / float(total_tax_percent))

        prg = afterallowance / (1 + (float(ctaxdata) / 100) + (float(staxdata) / 100))
        total_tax = afterallowance - prg

        cgstobj = (c_percent / 100) * total_tax
        cgst = float("{:.2f}".format(cgstobj))
        sgstobj = (s_percent / 100) * total_tax
        sgst = float("{:.2f}".format(sgstobj))
        pregst = float("{:.2f}".format(prg)) / float(order.quantity)

        json_cgst = cgst
        totalcgst += float(json_cgst) * float(order.quantity)
        json_sgst = sgst
        totalsgst += float(json_sgst) * float(order.quantity)
        json_pregst = pregst * float(order.quantity)
        totalpregst += float(json_pregst) * float(order.quantity)
        json_totalbill = float("{:.2f}".format(float(json_pregst) + float(
            json_cgst) + float(json_sgst))) * float(order.quantity)
        totalbill += float(json_totalbill)

    context = {
        'totalafterallowance': totalafterallowance,
        'totaldp': float("{:.2f}".format(totaldp)),
        'totalsgst': float("{:.2f}".format(totalsgst)),
        'totalcgst': float("{:.2f}".format(totalcgst)),
        'totalbill': float("{:.2f}".format(totalbill)),
        'percent': str(percent),
        'totalpregst': float("{:.2f}".format(totalpregst)),
        'status_image': orderHead.status,
        'outstanding_flag': False
    }
    if orderHead.status in status_in.ShowOutstandingStatus():
        context['outstanding_flag'] = True
    return context


def get_scheme(request, orderno, flag=None):
    groups = [group.name for group in request.user.groups.all()]
    # if flag is None:
    if 'SR' in groups:
        srDealers = SRDealer.objects.filter(sr_id=request.user)
        listDealer = []
        for srdealer in srDealers:
            if srdealer.dealer_id:
                if srdealer.dealer_id.dealer:
                    listDealer.append(srdealer.dealer_id.dealer)
        srDealers = listDealer
    elif 'DEL' in groups:
        srDealers = [request.user]
    else:
        orderHead = OrderHead.objects.filter(
            order_number=orderno, is_active=True,
            is_deleted=False).first()
        srDealers = [orderHead.dealer]

    # orderHead = OrderHead.objects.filter(
    #     dealer__in=srDealers, order_number=orderno,
    #     status__in=['Draft', 'Processing', 'SchemeApplied', 'OrderPlaced', 'Deleted', 'InWarehouse'],
    #     is_active=True, is_deleted=False).first()
    orderHead = OrderHead.objects.filter(
        dealer__in=srDealers, order_number=orderno,
        is_active=True, is_deleted=False).first()
    orderDetails = OrderDetails.objects.filter(order_id=orderHead, is_active=True, is_deleted=False)

    totaldp, totalcgst, totalsgst, totalpregst, totalbill, count, result, totalafterallowance = 0, 0, 0, 0, 0, 0, [], 0
    dealer_price = 0
    after_allowance = 0
    count = 0
    totaldp = 0
    totalafterallowance = 0
    totalpregst = 0
    totalsgst = 0
    totalcgst = 0
    totalbill = 0
    totalbill1 = 0
    pre_gst1 = 0
    pre_gst = 0
    totalVal = 0
    pre_gst_tieup = 0
    bill_total = 0
    total_sell = 0
    final_total_sell = 0
    total_tie = 0
    final_total_tie = 0
    tieup_flag = False
    scheme_flag = False
    allowance_percent = 0
    allFinalTotal = 0
    x_all_invoice_value = 0
    for order in orderDetails:
        try:
            objpm = ProductMaster.objects.get(sku_code=order.sku_code, is_active=True, is_deleted=False)
            # taxdata = TaxSlabMaster.objects.get(tax_slab_id=objpm.TaxSlabCode_id, is_active=True, is_deleted=False)
        except:
            pass
            # taxdata = None

        # if taxdata:
        #     ctaxdata = taxdata.cgst_rate
        #     staxdata = taxdata.sgst_rate
        #     otaxdata = taxdata.other_tax_rate
        #     gtaxdata = taxdata.gst_rate
        # else:
        #     ctaxdata = 0
        #     staxdata = 0
        #     otaxdata = 0
        #     gtaxdata = 0

        ctaxdata = order.cgst_rate
        staxdata = order.sgst_rate
        otaxdata = order.other_tax_rate
        gtaxdata = order.gst_rate

        tie_upobj = 0
        # jsondata = {
        #     'pk': order.pk, 'brand': objpm.brand.brand_name,
        #     'skucode': objpm.sku_code, 'quantity': order.quantity,
        #     'mrp': "{0} INR".format(float(objpm.max_retail_price)),
        #     'bbp': float(objpm.best_buying_price),
        #     'dp': float(objpm.dealer_price)
        # }
        jsondata = {
            'pk': order.pk, 'brand': orderHead.brand.brand_name,
            'skucode': order.sku_code, 'quantity': order.quantity,
            'mrp': "{0} INR".format(float(order.max_retail_price)/float(order.quantity)),
            'bbp': float(order.best_buying_price),
            'dp': float(order.dealer_price)/float(order.quantity)
        }

        # ===== warehouse detail =====
        try:
            stockLevel = StockLevelMaster.objects.get(product=order.product_id, is_active=True, is_deleted=False)
            jsondata["warehouse_code"] = stockLevel.warehouse.warehouse_code
            jsondata["warehouse_name"] = stockLevel.warehouse.warehouse_name
        except StockLevelMaster.DoesNotExist:
            pass
        except Exception:
            pass
        # ............................

        # dpobj = float(objpm.dealer_price) * float(order.quantity)
        # dpobj = float(order.dealer_price) * float(order.quantity)
        # totaldp += float(dpobj)
        dpobj = float(order.dealer_price) / float(order.quantity)
        totaldp += float(dpobj) * float(order.quantity)
        dealer_price += float(dpobj) * float(order.quantity)

        try:
            scheme = SchemeProductMaster.objects.get(product=order.product_id, is_active=True, is_deleted=False)
            # need to change
            # scheme = SchemeProductMaster.objects.get(
            #     brand=orderHead.brand, product=order.product_id.product_id,
            #     is_active=True, is_deleted=False)
        except Exception:
            scheme = None

        try:
            scheme_category = SchemeCategoryMaster.objects.get(
                scheme_category_code__icontains=scheme.scheme_category_code, is_active=True, is_deleted=False)

            tieup = TieUpMaster.objects.get(
                dealer=orderHead.dealer, brand=orderHead.brand,
                scheme_category_code=scheme_category, is_active=True, is_deleted=False)

            if tieup.discount_percent:
                jsondata['tie_up'] = "{0} %".format(tieup.discount_percent)
                tie_upobj = tieup.discount_percent
            elif tieup.discount_amount:
                jsondata['tie_up'] = "{0} INR".format(tieup.discount_amount)
                tie_upobj = tieup.discount_amount
            else:
                jsondata['tie_up'] = 0
                tie_upobj = 0

            # tieup = OrderTieUpDetails.objects.get(
            #     order_detail_id=order, product_id=order.order_id.brand,
            #     order_id=order.order_id, is_active=True, is_deleted=False)
            #
            # if tieup.tie_up_percent:
            #     jsondata['tie_up'] = "{0} %".format(tieup.tie_up_percent)
            #     tie_upobj = tieup.tie_up_percent
            # elif tieup.tie_up_amount:
            #     jsondata['tie_up'] = "{0} INR".format(tieup.tie_up_amount)
            #     tie_upobj = tieup.tie_up_amount
            # else:
            #     jsondata['tie_up'] = 0
            #     tie_upobj = 0

            # allowanceobj = AllowanceMaster.objects.get(
            #     brand=orderHead.brand, dealer=orderHead.dealer,
            #     scheme_category_code=scheme_category, is_active=True, is_deleted=False)
            #
            # if allowanceobj.discount_percent:
            #     allowanceValue = str(allowanceobj.discount_percent)
            # elif allowanceobj.discount_amount:
            #     allowanceValue = str(allowanceobj.discount_amount)
            # else:
            #     allowanceValue = str(0)
        except Exception:
            jsondata['tie_up'] = "{0} %".format(0)

        try:
            # .........................
            allowanceobj = OrderAllowanceDetails.objects.get(
                order_detail_id=order, product_id=order.product_id,
                order_id=order.order_id, is_active=True, is_deleted=False)

            if allowanceobj.allowance_percent:
                allowanceValue = str(allowanceobj.allowance_percent)
            elif allowanceobj.allowance_amount:
                allowanceValue = str(allowanceobj.allowance_amount)
            else:
                allowanceValue = str(0)

            jsondata['allowance'] = allowanceValue
            allowance = allowanceValue
            allowance_percent = allowanceValue
            # .........................
        except Exception:
            jsondata['allowance'] = 0
            allowance = 0
            allowance_percent = 0

        # =================================================
        allowance = float(dpobj) / 100 * float(jsondata['allowance'])
        jsondata["allowance_value"] = allowance
        # jsondata["allowance_value"] = allowance / float(order.quantity)
        jsondata['valueafterallowance'] = float(dpobj) - float(allowance)
        afterallowance = float(dpobj) - float(allowance)
        totalafterallowance += float(jsondata['valueafterallowance'])
        total_tax_percent = ctaxdata + staxdata
        c_percent = 100 * (float(ctaxdata) / float(total_tax_percent))
        s_percent = 100 * (float(staxdata) / float(total_tax_percent))

        prg = afterallowance / (1 + (float(ctaxdata) / 100) + (float(staxdata) / 100))
        total_tax = afterallowance - prg

        cgstobj = (c_percent / 100) * total_tax
        cgst = float("{:.2f}".format(cgstobj))
        sgstobj = (s_percent / 100) * total_tax
        sgst = float("{:.2f}".format(sgstobj))
        totalVal = float("{:.2f}".format(prg))
        bill_total += totalVal
        jsondata['total'] = totalVal

        # pregst = float("{:.2f}".format(prg)) / float(order.quantity)
        pregst = float("{:.2f}".format(prg))
        totalamt = pregst + sgst + cgst

        # ...........................
        applyed_tieup = 0

        try:
            order_tieup = OrderTieUpDetails.objects.get(
                order_detail_id=order, product_id=order.order_id.brand,
                order_id=order.order_id, is_active=True, is_deleted=False)
            if order_tieup.tie_up_percent:
                sr_tie_up = order_tieup.tie_up_percent
                pre_gst_tieup = pregst * float(sr_tie_up) / 100
                final_total_tie = final_total_tie + (pre_gst_tieup * order.quantity)
                total_tie += pre_gst_tieup
                applyed_tieup = sr_tie_up
            elif order_tieup.tie_up_amount:
                sr_tie_up = order_tieup.tie_up_amount
                pre_gst_tieup = sr_tie_up
                final_total_tie = final_total_tie + (pre_gst_tieup * order.quantity)
                total_tie += pre_gst_tieup
                applyed_tieup = sr_tie_up
            else:
                sr_tie_up = 0
                pre_gst_tieup = 0
                applyed_tieup = sr_tie_up
        except Exception:
            sr_tie_up = 0
            pre_gst_tieup = 0
            applyed_tieup = sr_tie_up
        jsondata["apply_tieup"] = applyed_tieup

        # ............................
        # ...........................

        sr_sell_through = 0
        applyed_schemes = []
        try:
            order_sell_through = OrderSchemeDetailsSellThrough.objects.filter(
                order_detail_id=order, product_id=order.order_id.brand,
                order_id=order.order_id, is_active=True, is_deleted=False)
            final_total_sell_prev = 0
            for sell in order_sell_through:
                percent = sell.scheme_percent
                per_amt = pregst * float(percent)/100
                amount = sell.scheme_amount
                sr_sell_through += per_amt + amount
                total_sell += sr_sell_through

                applyed_schemes.append({
                    "order_id": order.pk,
                    "scheme_id": sell.pk,
                    "scheme_name": sell.scheme_name,
                })
                final_total_sell_prev = sr_sell_through * order.quantity
            final_total_sell = final_total_sell + final_total_sell_prev
        except Exception:
            sr_sell_through = 0
        jsondata["apply_schemes"] = applyed_schemes

        # ............................

        pre_gst = pregst - pre_gst_tieup - sr_sell_through
        jsondata["tieup_value"] = pre_gst_tieup
        jsondata["sell_through_value"] = sr_sell_through

        jsondata['pre_gst'] = pre_gst

        totalb = totalamt - pre_gst_tieup
        totalbill1 += float("{:.2f}".format(totalb))
        pre_gst1 += float("{:.2f}".format(pre_gst))

        jsondata['cgst'] = cgst  # round(float(float(jsondata['valueafterallowance'])/float(avc)))
        totalcgst += float(jsondata['cgst'])
        jsondata['sgst'] = sgst  # round(float(float(jsondata['valueafterallowance'])/float(av)))
        totalsgst += float(jsondata['sgst'])
        jsondata['pregst'] = pregst  # float(tax)-float(jsondata['valueafterallowance'])
        # json_pregst = float(jsondata['pregst']) * float(order.quantity)
        # json_cgst = cgst * float(order.quantity)
        # json_sgst = sgst * float(order.quantity)
        # totalpregst += float(json_pregst)

        # jsondata['totalbill'] = float(
        #     "{:.2f}".format(float(json_pregst) + float(json_cgst) + float(jsondata['sgst'])))
        jsondata['totalbill'] = float(
            "{:.2f}".format(float(jsondata['pregst']) + float(jsondata['cgst']) + float(jsondata['sgst'])))
        totalbill += float(jsondata['totalbill']) * float(order.quantity)
        jsondata['total_pre'] = float(
            "{:.2f}".format(float(jsondata['valueafterallowance'])))
        jsondata['total_pre_gst'] = float(
            "{:.2f}".format(float(jsondata['total_pre']) - (pre_gst_tieup + sr_sell_through)))
        jsondata['final_total'] = float(jsondata['total_pre_gst']) * order.quantity
        # =================================================
        allFinalTotal += jsondata['final_total']

        jsondata["dealer_price"] = "{0} INR".format(jsondata["dp"] * jsondata["quantity"])

        """ =========== ADD TWO NEW FIELD FOR WAREHOUSE FROM ============ """
        jsondata["x_invoice_value"] = float(jsondata["totalbill"]) * float(jsondata["quantity"])
        x_all_invoice_value += float(jsondata["x_invoice_value"])
        """ =========== ADD TWO NEW FIELD FOR WAREHOUSE TO ============ """

        jsondata["totalbill"] = "{0} INR".format(jsondata["totalbill"])

        # Applyed schemes...

        # skuCodes = [order.sku_code for order in orderDetails]
        applyed = OrderSchemeDetailsSellThrough.objects.filter(
            order_detail_id=order, product_id=orderHead.brand,
            sku_code=order.sku_code, order_id=orderHead, is_active=True, is_deleted=False)
        applyedSchemes = []
        for apply in applyed:
            applyedSchemes.append({
                'order_id': order.pk,
                "scheme_id": apply.pk,
                "scheme_name": apply.scheme_name
            })
        jsondata["applyed_schemes"] = applyedSchemes
        # ..........................................
        alreadyApplyed = []
        if len(applyed) > 0:
            alreadyApplyed = [item.scheme_code for item in applyed]
        # ..........................................

        # Schemes available or not...

        jsondata["available_scheme"] = False
        availableSchemes = SchemeSellThroughMaster.objects.filter(
            brand=order.order_id.brand, product=order.product_id, is_active=True, is_deleted=False)
        for myScheme in availableSchemes:
            if myScheme.scheme_code in alreadyApplyed:
                continue
            else:
                if (utc_to_local(myScheme.scheme_end_date)).date() >= datetime.datetime.today().date():
                    jsondata["available_scheme"] = True
                    break
                else:
                    continue

        # ..........................................

        result.append(jsondata)
        count += 1
    bill_total = totalbill - (final_total_tie + final_total_sell)

    if final_total_tie > 0:
        tieup_flag = True
    if final_total_sell > 0:
        scheme_flag = True
    date = ""
    orderNo = ""
    dms_no = ""
    if orderHead is not None:
        date = orderHead.order_date
        orderNo = orderHead.order_number
        dms_no = orderHead.DMS_order_number
    after_data = {
        "sr_total": totalbill,
        "less_tieup": final_total_tie,
        "less_sell": final_total_sell,
        "net_bill": bill_total,
        "allowance": allowance_percent,
        "tieup_flag": tieup_flag,
        "scheme_flag": scheme_flag,
        "order_date": date,
        "order_id": orderNo,
        "dms_number": dms_no,
        "allFinalTotal": allFinalTotal,
        "x_all_invoice_total": x_all_invoice_value
    }
    # ==================================================
    if orderHead:
        dealerProfile = DealerProfile.objects.filter(dealer=orderHead.dealer).first()
        srDealer = SRDealer.objects.filter(dealer_id=dealerProfile).first()
        after_data["sr_name"] = "{0} {1}".format(srDealer.sr_id.first_name, srDealer.sr_id.last_name)
        after_data["sr_contact"] = srDealer.sr_id.mobile
    # ==================================================
    return result, after_data


# =================================


def utc_to_local(utc_dt):
    pst = pytz.timezone('Asia/Kolkata')
    ab = utc_dt.replace(tzinfo=pytz.utc).astimezone(pst)
    current_date = datetime.datetime.strptime(ab.strftime('%Y-%m-%d %I:%M:%S'), '%Y-%m-%d %I:%M:%S')
    return current_date


def get_order_schemes(request):
    product = request.GET.get('product')
    orderDetail = OrderDetails.objects.get(pk=product, is_active=True, is_deleted=False)

    applyed = OrderSchemeDetailsSellThrough.objects.filter(
        order_detail_id=orderDetail, product_id=orderDetail.order_id.brand,
        sku_code=orderDetail.sku_code, order_id=orderDetail.order_id,
        is_active=True, is_deleted=False)
    applyedSchemes = []
    if len(applyed) > 0:
        applyedSchemes = [item.scheme_code for item in applyed]

    schemes = SchemeSellThroughMaster.objects.filter(
        brand=orderDetail.order_id.brand, product=orderDetail.product_id,
        is_active=True, is_deleted=False)
    allSchemes = []
    for scheme in schemes:
        if scheme.scheme_code in applyedSchemes:
            continue
        else:
            if (utc_to_local(scheme.scheme_end_date)).date() >= datetime.datetime.today().date():
                allSchemes.append({
                    "order_id": orderDetail.pk,
                    "scheme_id": scheme.pk,
                    "scheme_name": scheme.scheme_name,
                    "scheme_code": scheme.scheme_code,
                    "discount_percent": str(scheme.discount_percent),
                    "discount_amount": scheme.discount_amount,
                    "scheme_start_date": scheme.scheme_start_date.strftime('%d-%B-%Y'),
                    "scheme_end_date": utc_to_local(scheme.scheme_end_date).strftime('%d-%B-%Y'),
                })

    return JsonResponse({"data": allSchemes})


def get_products(request):
    context, total = get_scheme(request, request.GET.get('order'))
    return JsonResponse({"data": context, "total": total})


def delete_scheme(request):
    sell_through = OrderSchemeDetailsSellThrough.objects.get(
        pk=request.GET.get('scheme'), is_active=True, is_deleted=False)
    sell_through.is_active = False
    sell_through.is_deleted = True
    sell_through.save()
    context, total = get_scheme(request, request.GET.get('order'))
    return JsonResponse({"data": context, "total": total})


# -----------------------------------------------


def get_credit_notes(orderID, request):
    statusList = status_in.CreditNoteStatus()
    orderHead = OrderHead.objects.filter(order_number=orderID, status__in=statusList).first()
    orderDetail = OrderDetails.objects.filter(order_id=orderHead)

    tieup, sell_through = [], []
    total_tieup_discount = 0
    total_tieup_amount = 0

    total_sell_percent = 0
    total_sell_percent_amount = 0
    total_sell_amount = 0
    for order in orderDetail:
        sell_credit_master = CreditNoteTypeMaster.objects.filter(
            note_type_code__icontains='Sell', is_active=True, is_deleted=False).first()
        tieup_credit_master = CreditNoteTypeMaster.objects.filter(
            note_type_code__icontains='Tieup', is_active=True, is_deleted=False).first()

        try:
            order_tieup = OrderTieUpDetails.objects.get(
                order_detail_id=order, product_id=orderHead.brand,
                sku_code=order.sku_code, order_id=orderHead, is_active=True, is_deleted=False)

            tieup_credit = CreditNotes.objects.get(
                tieup_sell_through=order_tieup.pk, dealer=orderHead.dealer, note_type=tieup_credit_master,
                order_id=orderHead, order_detail_id=order, is_active=True, is_deleted=False)

            total_sr_tie_up, sr_tie_up, sr_tie_up_max = 0, 0, 0
            if order_tieup.tie_up_percent:
                sr_tie_up = "{0} %".format(order_tieup.tie_up_percent)
                sr_tie_up_max = "{0} %".format(order_tieup.max_tie_up_percent)
                total_sr_tie_up = order_tieup.tie_up_percent
            elif order_tieup.tie_up_amount:
                sr_tie_up = order_tieup.tie_up_amount
                sr_tie_up_max = order_tieup.max_tie_up_amount
                total_sr_tie_up = order_tieup.tie_up_amount

            total_tieup_discount += total_sr_tie_up
            total_tieup_amount += tieup_credit.credit_amount * order.quantity

            tieup_json = {
                "tieup_date": tieup_credit.created_on,
                "applyed_discount": sr_tie_up,
                "applyed_discount_amount": tieup_credit.credit_amount * order.quantity,
                "max_discount": sr_tie_up_max
            }
            tieup.append(tieup_json)
        except Exception:
            pass
        # ............................................................

        order_sell_through = OrderSchemeDetailsSellThrough.objects.filter(
            order_detail_id=order, product_id=order.order_id.brand,
            order_id=order.order_id, is_active=True, is_deleted=False)
        for sell in order_sell_through:
            try:
                sell_through_credit = CreditNotes.objects.get(
                    tieup_sell_through=sell.pk, dealer=orderHead.dealer, note_type=sell_credit_master,
                    order_id=orderHead, order_detail_id=order, is_active=True, is_deleted=False)

                sell_percent = sell.scheme_percent
                sell_percent_amount = sell_through_credit.credit_amount - sell.scheme_amount
                sell_max_percent = "{0} %".format(sell.max_scheme_percent)

                sell_amount = sell.scheme_amount
                sell_max_amount = sell.max_scheme_amount

                total_sell_percent += sell_percent
                total_sell_percent_amount += sell_percent_amount * order.quantity
                total_sell_amount += sell_amount * order.quantity

                sellThrough_json = {
                    "sell_through_date": sell_through_credit.created_on,
                    "applyed_percent_amount": sell_percent_amount,
                    "max_percent": sell_max_percent, "applyed_percent": "{0} %".format(sell_percent),
                    "applyed_amount": sell_amount, "max_amount": sell_max_amount,
                    "total_sell_amount": (sell_percent_amount + sell_amount) * order.quantity
                }
                sell_through.append(sellThrough_json)
            except Exception:
                pass

    dealerProfile = DealerProfile.objects.filter(
        dealer=orderHead.dealer, is_deleted=False).first()
    dealerSR = SRDealer.objects.filter(dealer_id=dealerProfile).first()

    context = {
        "sr_name": "{0} {1}".format(dealerSR.sr_id.first_name, dealerSR.sr_id.last_name),
        "sr_contact_no": "{0}".format(dealerSR.sr_id.mobile),
        "order_no": orderHead.order_number,
        "tieup": tieup,
        "sell_through": sell_through,

        "total_sell_percent": "{0} %".format(total_sell_percent),
        "total_sell_percent_amount": total_sell_percent_amount,
        "total_sell": total_sell_percent_amount + total_sell_amount,
        "total_sell_amount": total_sell_amount,
        "total_tieup_discount": "{0} %".format(total_tieup_discount),
        "total_tieup_amount": total_tieup_amount,
        "grand_total": total_tieup_amount + total_sell_percent_amount + total_sell_amount
    }
    return context


def confirm_order_cancel(request, status, orderID):
    setstatus, message_text = "", ""
    if status == 1:
        setstatus = 'Deleted'
        message_text = 'Order Successfully Canceled!'
    elif status == 2:
        setstatus = 'OrderPlaced'
        message_text = 'Order Successfully Confirmed!'

    try:
        orderHead = OrderHead.objects.get(
            order_number=orderID, is_active=True, is_deleted=False)

        """ IF outstanding is negative """
        # if setstatus == 'OrderPlaced':
            # activeOrder = OrderHead.objects.filter(
            #     dealer=orderHead.dealer, status__in=status_in.AdvanceOutstandingStatus(),
            #     is_active=True, is_deleted=False).aggregate(total=Sum('payable_amount'))
            # outstanding = activeOrder['total']
            # if outstanding is None:
            #     outstanding = 0

        if status == 1:
            orderHead.payable_amount = float(0)
        orderHead.status = setstatus
        orderHead.save()

        # ......... Order History .........
        try:
            orderHistory = OrderEventHistory(
                order_id=orderHead,
                order_status=orderHead.status,
                created_by=request.user
            )
            orderHistory.save()
        except Exception:
            pass
        # .................................
        if orderHead.status == 'OrderPlaced':
            amount = float(request.GET.get('price', 0))
            discount = float(request.GET.get('discount', 0))

            orderHead.total_amount = amount
            orderHead.discount_amount = discount
            orderHead.payable_amount = amount - discount
            orderHead.save()

            # create_ledger(request, orderHead, amount, discount)
            #
            # """ IF outstanding is negative """
            # if outstanding < 0:
            #     activeOrder = OrderHead.objects.filter(
            #         payable_amount__lt=0, dealer=orderHead.dealer,
            #         status__in=status_in.AdvanceOutstandingStatus(),
            #         is_active=True, is_deleted=False)
            #     advancePayment = activeOrder.payable_amount
            #     activeOrder.payable_amount = float(0)
            #     activeOrder.status = 'PaymentDone'
            #     activeOrder.save()
            #
            #     """ payable = payable + (-advance) """
            #     orderHead.payable_amount = float(orderHead.payable_amount) + float(advancePayment)
            #     orderHead.save()

        # ---------Email send-------------------
        try:
            send_sms_or_email(request, orderHead)
            # send_email("Test", ["kailash.c@skysoft.net.in", "vikram.r@skysoft.net.in"], "Hello...")
        except Exception:
            pass

        messages.add_message(request, messages.SUCCESS, message_text)
    except Exception as error:
        messages.add_message(request, messages.ERROR, str(error))
    return redirect('/user/Customer-Dashboard')


# ==================== Bind Product Categories ========================


def get_sub_category(parent):
    categoryList = ProductCategoryMaster.objects.filter(
        parent__in=parent, is_active=True, is_deleted=False)
    categoryList = [{"id": item.pk, "name": item.category_name} for item in categoryList]
    return categoryList


def product_category(request, index=None, value=None):
    if request.is_ajax():
        index = request.GET.get('index')
        value = request.GET.get('value').rstrip(' ')

        if value != '0':
            category = ProductCategoryMaster.objects.filter(
                category_name__istartswith=value, is_active=True, is_deleted=False).first()
            value = category.pk
        # if index == '1':
        #     sub_category = get_sub_category([value])
        #     parents = [item["id"] for item in sub_category]
        #     sub_category_one = get_sub_category(parents)
        #     parents = [item["id"] for item in sub_category_one]
        #     sub_category_two = get_sub_category(parents)
        #     data = {"list": sub_category, "list1": sub_category_one, "list2": sub_category_two}
        #     return JsonResponse(data)
        if index == '1' and value == '0':
            return JsonResponse({"list1": [], "list2": []})
        elif index == '1':
            sub_category_one = get_sub_category([value])
            parents = [item["id"] for item in sub_category_one]
            sub_category_two = get_sub_category(parents)
            return JsonResponse({"list1": sub_category_one, "list2": sub_category_two})
        elif index == '2':
            sub_category = get_sub_category([value])
            return JsonResponse({"list1": sub_category})
        elif index == '3':
            parent = ProductCategoryMaster.objects.filter(
                pk=category.parent, is_active=True, is_deleted=False)
            parent = [{"id": item.pk, "name": item.category_name} for item in parent]
            return JsonResponse({"list1": parent})
    else:
        category = ProductCategoryMaster.objects.filter(
            category_name__istartswith=value, is_active=True, is_deleted=False).first()
        value = category.pk
        if index == '1':
            sub_category_one = get_sub_category([value])
            parents = [item["id"] for item in sub_category_one]
            sub_category_two = get_sub_category(parents)
            return sub_category_one, sub_category_two
        elif index == '2':
            sub_category = get_sub_category([value])
            return sub_category
        elif index == '3':
            parent = ProductCategoryMaster.objects.filter(
                pk=category.parent, is_active=True, is_deleted=False)
            parent = [{"id": item.pk, "name": item.category_name} for item in parent]
            return parent


def get_product_category():
    main_category = ProductCategoryMaster.objects.filter(
        category_name='CE', is_active=True, is_deleted=False).first()
    sub_category = ProductCategoryMaster.objects.filter(
        parent=main_category.category_id, is_active=True, is_deleted=False)
    sub_category = [item.category_id for item in sub_category]
    sub_category_one = get_sub_category(sub_category)  # third categories
    parents = []
    for sub_one in sub_category_one:
        # parents = [item["id"]]
        parents.append(sub_one["id"])
        # break
    sub_category_two = get_sub_category(parents)  # fourth categories
    parents = [sub_two["id"] for sub_two in sub_category_two]
    sub_category_three = get_sub_category(parents)  # fifth categories
    return sub_category_one, sub_category_two, sub_category_three


# ============== Warehouse Management System ===================


def warehouse_admin_dashboard(request):
    querysetcount=None
    #warehouseAdmin = WarehouseAdmin.objects.filter(is_active=True, is_deleted=False, user_id=request.user.id).values_list('warehouse_id')
    #orderhead = OrderHead.objects.filter(
    #    status__in=['Dispatched', 'OrderPlaced', 'Confirm', 'InWarehouse'], is_active=True, is_deleted=False)
    orderhead = OrderHead.objects.filter(
        status__in=status_in.WarehouseAdminStatus(),
        is_active=True, is_deleted=False).exclude(order_date__exact=None).order_by('-order_date')
    orderheadnull = OrderHead.objects.filter(
        status__in=status_in.WarehouseAdminStatus(),
        is_active=True,order_date=None, is_deleted=False)
    dispatch = []
    for order in orderhead:
        try:
            _x_dealer_profile = DealerProfile.objects.get(
                dealer_id=order.dealer, is_active=True, is_deleted=False)
            order.__dict__["company"] = _x_dealer_profile.company_name
        except DealerProfile.DoesNotExist:
            order.__dict__["company"] = ""

        try:
            status = OrderEventHistory.objects.get(order_id_id=order.pk, order_status='Dispatched')
            order.__dict__['Dispatched_Date'] = status.created_on
        except:
            continue
    pending = orderhead.filter(status='OrderPlaced')
    dispatched = orderhead.filter(status__in=['InWarehouse'])
    orderhead=list(chain(orderhead,orderheadnull))
    querysetcount=orderhead
    if len(orderhead) >10:
        orderhead=orderhead
    orderdispatch = OrderHead.objects.filter(status__in=['Dispatched','PaymentDone'], is_active=True, is_deleted=False).count()

    for order in dispatched:
        try:
            _x_dealer_profile = DealerProfile.objects.get(
                dealer_id=order.dealer, is_active=True, is_deleted=False)
            order.__dict__["company"] = _x_dealer_profile.company_name
        except DealerProfile.DoesNotExist:
            order.__dict__["company"] = ""

        try:
            status = OrderEventHistory.objects.get(order_id_id=order.pk, order_status='Dispatched')
            order.__dict__['Dispatched_Date'] = status.created_on
        except:
            continue

    for order in pending:
        try:
            _x_dealer_profile = DealerProfile.objects.get(
                dealer_id=order.dealer, is_active=True, is_deleted=False)
            order.__dict__["company"] = _x_dealer_profile.company_name
        except DealerProfile.DoesNotExist:
            order.__dict__["company"] = ""

        try:
            status = OrderEventHistory.objects.get(order_id_id=order.pk, order_status='Dispatched')
            order.__dict__['Dispatched_Date'] = status.created_on
        except:
            continue
    context = {"orderhead": orderhead,'pending':pending ,'dispatched': dispatched, 'orderdispatch': orderdispatch,'querysetcount':querysetcount}
    stock = StockRegister.objects.filter(
        is_active=True, is_deleted=False)
    for stocks in stock:
        stocklevel=''
        remain_stock=int(int(stocks.stock_in)-int(stocks.stock_out))
        try:
            stocklevel = StockLevelMaster.objects.get(is_active=True, is_deleted=False, warehouse_id=stocks.warehouse_id)
        except Exception:
            stocklevel=''
        if stocklevel!='':
            min_stock=int(remain_stock)<=int(stocklevel.min_stock_level)
            if min_stock==True:
                wadmin=User.objects.filter(is_active=True,groups__name="WHS")
                wuser=User.objects.filter(is_active=True,groups__name="WHSUSER")
                WHS_all=list(chain(wadmin,wuser))
                html_content=''
                if WHS_all:
                    emails=[]
                    for user in WHS_all:
                        emails.append(user.email)
                    html_content+= "Stocks of pruduct "+str(stocks.product.sku_code)+", "+str(remain_stock)+" has remains."
                    subject="Minimum Stocks"
                    email = EmailMultiAlternatives(subject, html_content, settings.DEFAULT_FROM_EMAIL, to=emails)
                    # email.send()
                    """ comment """
    # orderHead = OrderHead.objects.filter(status__in=['Dispatched', 'Delivered'], is_active=True, is_deleted=False)
    # dataAll = orderHead.values("status").annotate(total=Count('order_id'))
    # header = {}
    # for item in dataAll:
    #     if item["status"] == "Dispatched":
    #         header["dispatched"] = item["total"]
    #     elif item["status"] == "Delivered":
    #         header["delivered"] = item["total"]
    #     else:
    #         header = {"delivered": 0, "dispatched": 0}
    #warehouse_name= WarehouseMaster.objects.filter(warehouse_code=)
    warehouses = WarehouseMaster.objects.filter(is_active=True, is_deleted=False).count()
    # inprocess = OrderHead.objects.filter(status='Processing').aggregate(amount=Sum('total_amount'))
    inprocess = OrderHead.objects.filter(status__in=['OrderPlaced'], is_active=True, is_deleted=False).count()
    context["warehouses"] = warehouses
    context["confirm"] = inprocess
    try:
        total_outstock_sku = get_length_of_outstock(request)
    except:
        total_outstock_sku = 0
    # context["header"] = header
    context["out_stock"] = total_outstock_sku
    return render(request, 'warehouse/warehouse_dashboard.html', context=context)


def warehouseadmin_all_order(request,statusobj=None):
    if statusobj == 'OrderPlaced':
        orderhead = OrderHead.objects.filter(
            status=statusobj,
            is_active=True, is_deleted=False).exclude(order_date__exact=None)
        orderheadnull = OrderHead.objects.filter(
            status=statusobj,
            is_active=True,order_date=None, is_deleted=False)
    elif statusobj == 'InWarehouse':
        orderhead = OrderHead.objects.filter(
            status=statusobj,
            is_active=True, is_deleted=False).exclude(order_date__exact=None)
        orderheadnull = OrderHead.objects.filter(
            status=statusobj,
            is_active=True,order_date=None, is_deleted=False)
    else:
        orderhead = OrderHead.objects.filter(
            status__in=['Dispatched', 'PaymentDone'],
            is_active=True, is_deleted=False).exclude(order_date__exact=None)
        orderheadnull = OrderHead.objects.filter(
            status__in=['Dispatched', 'PaymentDone'],
            is_active=True, order_date=None, is_deleted=False)
    orderhead=list(chain(orderhead,orderheadnull))
    if orderhead:
        for order in orderhead:
            try:
                status = OrderEventHistory.objects.get(order_id_id=order.pk, order_status='Dispatched')
                order.__dict__['Dispatched_Date'] = status.created_on
            except:
                order.__dict__['Dispatched_Date'] = '---'

            try:
                x_dealer = DealerProfile.objects.get(
                    dealer=order.dealer, is_active=True, is_deleted=False)
                order.__dict__['company'] = x_dealer.company_name
            except:
                order.__dict__['company'] = ""

    if request.method=='POST':
        if 'export' in request.POST:
            result=[]

            for order in orderhead:
                try:
                    get_order={}
                    get_order['Order No']=order.order_number
                    if isinstance(order.order_date, datetime.date):
                        get_order['Order Date']=order.order_date.date()
                    else:
                        get_order['Order Date'] = order.order_date
                    if isinstance(order.Dispatched_Date, datetime.date):
                        get_order['Dispatched Date'] = order.Dispatched_Date.date()
                    else:
                        get_order['Dispatched Date'] = order.Dispatched_Date
                    get_order['Invoice Value']=order.total_amount
                    get_order['Status']=order.status
                    get_order['Warehouse']=order.warehouse_code
                    if order.warehouse_manager is not None:
                        get_order['Manager']=order.warehouse_manager.first_name
                    else:
                        get_order['Manager'] = '---'
                    get_order['SR'] = str(order.sr_id.first_name + ' ' + order.sr_id.last_name)
                    get_order['Dealer']=str(order.dealer.user_code+'.'+order.company)

                except Exception as error:
                    continue
                result.append(get_order)
            response = HttpResponse(content_type='text/csv')
            response['Content-Disposition'] = 'attachment;filename=warehouse_user_order_list.csv'
            keys = ['Order No','Order Date', 'Dispatched Date', 'Invoice Value','Status','Warehouse','Manager','SR',
                    'Dealer']
            writer = csv.DictWriter(response,keys)
            writer.writeheader()
            writer.writerows(result)
            return response
    context = {"orderhead": orderhead}
    return render(request, 'warehouse/all_orders.html', context=context)


def WarehouseOrderDetail(request, orderID):
    orderHead = OrderHead.objects.get(order_number=orderID, is_active=True, is_deleted=False)
    orderDetail = OrderDetails.objects.filter(order_id=orderHead, is_active=True, is_deleted=False)
    try:
        dealerProfile = DealerProfile.objects.get(dealer=orderHead.dealer, is_active=True, is_deleted=False)
    except Exception:
        dealerProfile = ''
    if dealerProfile != '':
        dealerBusiness = DealerBusinessMaster.objects.get(
            brand=dealerProfile.brand, dealer=dealerProfile.dealer, is_active=True, is_deleted=False)
        context = {"dealer": dealerProfile, "dbm": dealerBusiness}
        after_scheme, total = get_scheme(request, orderID)
        context['total'] = total
        context['details'] = after_scheme
        # ..................................
        warehouseList = WarehouseMaster.objects.filter(is_active=True, is_deleted=False)
        context["warehouses"] = warehouseList
    
        if request.method == 'POST':
            if orderHead.status != 'Dispatched':
                if 'btnsave' in request.POST:

                    orderHead.DMS_order_number = request.POST.get('dmsorderno')
                    orderHead.remarks = request.POST.get('remarks')
                    orderHead.status = 'Dispatched'
                    orderHead.save()
                    # ......... Order History .........
                    orderHistory = OrderEventHistory(
                        order_id=orderHead,
                        order_status=orderHead.status,
                        created_by=request.user
                    )
                    orderHistory.save()
                    try:
                        send_sms_or_email(request, orderHead)
                    except Exception:
                        pass
                    # ........... Ledger ................
                    advance_amount, balance, previous_order = 0, 0, 0
                    remarks = ''
                    head_type = ''
                    try:
                        """ IF outstanding is negative """
                        try:
                            prevDealerLedger = Ledger.objects.filter(
                                ledger_type="Unadjusted", dealer=orderHead.dealer,
                                is_active=True, is_deleted=False).latest('pk')
                            balance = float(prevDealerLedger.balance)
                        except:
                            balance = float(0)

                        discount = float(orderHead.discount_amount)
                        amount = float(orderHead.total_amount)
                        head_type = 'Sales'
                        create_ledger(request, orderHead, amount, discount, head_type)


                    except:
                        pass
                    # ------------------Ledger-----------------

                    ####################################
                    """check if any advance payment received"""
                    if balance < 0:
                        advance_amount = balance
                        no_of_days = 0

                        cdSlab = CashDiscountSlabsMaster.objects.get(
                            pk=dealerProfile.cd_slab_code.pk,
                            is_active=True, is_deleted=False)
                        if no_of_days <= cdSlab.days_1:
                            slabDiscount = cdSlab.cd1
                        elif no_of_days <= cdSlab.days_2:
                            slabDiscount = cdSlab.cd2
                        elif no_of_days <= cdSlab.days_3:
                            slabDiscount = cdSlab.cd3
                        elif no_of_days <= cdSlab.days_4:
                            slabDiscount = cdSlab.cd4
                        else:
                            slabDiscount = float(0)
                        if orderHead.payable_amount < -1 * float(advance_amount):
                            advance_amount = orderHead.payable_amount
                            orderHead.status = 'PaymentDone'
                            # ......... Order History .........
                            orderHistory = OrderEventHistory(
                                order_id=orderHead,
                                order_status='PaymentDone',
                                created_by=request.user
                            )
                            orderHistory.save()
                        discount = float(advance_amount) * float(slabDiscount) / 100
                        if discount < 0:
                            discount = -1 * float(discount)
                        if advance_amount < 0:
                            advance_amount = -1 * float(advance_amount)
                        orderHead.payable_amount = float(orderHead.payable_amount) - float(discount)
                        orderHead.payable_amount = float(orderHead.payable_amount) - float(advance_amount)
                        orderHead.save()
                        # """ ========= SEND EMAIL OR SMS ========== """
                        try:
                            send_sms_or_email(request, orderHead)
                        except Exception:
                            pass
                        amount = 0
                        comment = 'Credit Note Type 2'
                        head_type = 'CreditNote'
                        create_ledger(request, orderHead, amount, discount, head_type, comment)



                    """ CREATE STOCK IN STOCK-REGISTER """
                    stock_out(request, orderHead)
                    # .................................
                    try:
                        orderDispatchHead = OrderDispatchHead(
                            order_id=orderHead,
                            dispatched_by=request.user,
                            dispatched_on=datetime.datetime.now()
                        )
                        orderDispatchHead.save()
                        # .................................
                        warehouse = warehouseList.filter(warehouse_code=orderHead.warehouse_code).first()
                        for order in orderDetail:
                            orderDispatchDetail = OrderDispatchDetail(
                                dispatch_id=orderDispatchHead,
                                sku_code=order.sku_code,
                                quantity=order.quantity,
                                warehouse_id=warehouse
                            )
                            orderDispatchDetail.save()
                    except Exception:
                        pass
                    # .................................
                    try:
                        # send_email_or_sms_both("Dispatch_Delivered", orderHead)
                        send_sms_or_email(request, orderHead)
                    except Exception:
                        pass
                    # .................................

                    messages.add_message(request, messages.SUCCESS, 'order dispatched successfully!!!')
                    orderID = str(orderID)
                    return redirect('/user/warehouse-order-detail/'+orderID)
            else:
                messages.add_message(request, messages.ERROR, 'Order already dispatched')
    # ..................................
        if orderHead.status == "Dispatched":
            context["disable"] = True

        if orderHead.warehouse_manager:
            editFlag = True
            warehouse_manager_id = orderHead.warehouse_manager.pk
            warehouse_manager_name = orderHead.warehouse_manager.first_name
        else:
            editFlag = False
            warehouse_manager_id = ""
            warehouse_manager_name = ""

        warehouseUsers = WarehouseAdmin.objects.filter(
            warehouse=orderHead.warehouse_code, is_active=True, is_deleted=False)
        # warehouseUsers = [{"id": item.user.pk, "name": item.user.first_name} for item in warehouseUsers]
        warehouseUserList = []
        for item in warehouseUsers:
            if item.user:
                full_name = "{0} {1}".format(item.user.first_name, item.user.last_name)
                warehouseUserList.append({"id": item.user.pk, "name": full_name})
        context["warehouse_code"] = orderHead.warehouse_code
        context["user"] = warehouse_manager_id
        context["manager"] = warehouse_manager_name
        context["users"] = warehouseUserList
        context["edit"] = editFlag
        context['status_image'] = orderHead.status
        context["history"] = False
    # ........................
        try:
            order_status_detail = OrderEventHistory.objects.filter(
                order_id=orderHead, order_status=orderHead.status).first()
            if orderHead.status in status_in.SRStatus():
                status_detail = {
                    "head_name": "SR Name", "head_number": "SR Number",
                    "date": order_status_detail.created_on,
                    "name": "", "number": ""
                }
                dealerProfile = DealerProfile.objects.filter(dealer=orderHead.dealer).first()
                srDealer = SRDealer.objects.filter(dealer_id=dealerProfile).first()
                status_detail["name"] = "{0} {1}".format(srDealer.sr_id.first_name, srDealer.sr_id.last_name)
                status_detail["number"] = srDealer.sr_id.mobile
            elif orderHead.status in status_in.WarehouseStatus():
                status_detail = {
                    "head_name": "WH User Name", "head_number": "WH User Number",
                    "date": order_status_detail.created_on,
                    "name": "{0} {1}".format(
                        orderHead.warehouse_manager.first_name,
                        orderHead.warehouse_manager.last_name),
                    "number": orderHead.warehouse_manager.mobile
                }
            context["status_detail"] = status_detail
        except Exception:
            pass
    # ........................
        whsAdmin = User.objects.filter(groups__name='WHS', is_active=True)
        if len(whsAdmin) > 0:
            whsAdmin = whsAdmin.first()
            context["whs_admin_name"] = "{0} {1}".format(whsAdmin.first_name, whsAdmin.last_name)
            context["whs_admin_number"] = whsAdmin.mobile
        # ........................

        if orderHead.status in status_in.HistoryStatus():
            history = OrderEventHistory.objects.filter(order_id=orderHead).order_by('pk')
            context["orderHistory"] = history
            context["history"] = True
    # return render(request, 'warehouse/order_detail.html', context=context)
        return render(request, 'warehouse/custom_order_detail.html', context=context)
    else:
        messages.add_message(request, messages.ERROR, 'Inactive Dealer!!!')
        return redirect('/user/warehouse-admin-dashboard')


def Warehouse_orders(request, statusobj):
    orderhead = ''
    urlobj = request.path.split('/')[2]
    if urlobj == 'warehouse-manager-dashboard':
        # wareadmin = WarehouseAdmin.objects.filter(is_active=True,is_deleted=False,user_id=request.user.id).values_list('warehouse_id')
        # stock=StockRegister.objects.filter(is_active=True,is_deleted=False,warehouse_id__in=wareadmin).values_list('product_id')
        # orderdetail=OrderDetails.objects.filter(is_active=True,is_deleted=False,product_id_id__in=stock).values_list('order_id_id')
        if statusobj == 'orders':
            orderhead = OrderHead.objects.filter(
                status__in=['InWarehouse'],
                warehouse_manager=request.user,
                is_active=True, is_deleted=False).order_by('-pk')
        if statusobj == 'dispatched':
            orderhead = OrderHead.objects.filter(
                status__in=['Dispatched', "PaymentDone"],
                warehouse_manager=request.user,
                is_active=True, is_deleted=False).order_by('-pk')
    if urlobj == 'warehouse-admin-dashboard':
        if statusobj == 'orders':
            orderhead = OrderHead.objects.filter(
                status__in=['OrderPlaced'],
                is_active=True, is_deleted=False).order_by('-pk')
        if statusobj == 'dispatched':
            orderhead = OrderHead.objects.filter(
                status__in=['Dispatched', "PaymentDone"],
                is_active=True, is_deleted=False).order_by('-pk')
    if orderhead:
        for order in orderhead:
            try:
                status = OrderEventHistory.objects.get(order_id_id=order.pk, order_status='Dispatched')
                order.__dict__['Dispatched_Date'] = status.created_on
            except:
                order.__dict__['Dispatched_Date'] = '---'

            try:
                _x_dealer = DealerProfile.objects.get(
                    dealer=order.dealer, is_active=True, is_deleted=False)
                order.__dict__['company'] = _x_dealer.company_name
            except:
                order.__dict__['company'] = ""

        if request.method == 'POST':
            if 'export' in request.POST:
                result=[]
                for order in orderhead:
                    get_order={}
                    _x_name = order.warehouse_manager.first_name if order.warehouse_manager else ""
                    get_order['Order No']=order.order_number
                    if isinstance(order.order_date, datetime.date):
                        get_order['Order Date'] = order.order_date.date()
                    else:
                        get_order['Order Date'] = order.order_date
                    if isinstance(order.Dispatched_Date, datetime.date):
                        get_order['Dispatched Date'] = order.Dispatched_Date.date()
                    else:
                        get_order['Dispatched Date'] = order.Dispatched_Date
                    get_order['Invoice Value']=order.total_amount
                    get_order['Status']=order.status
                    get_order['Warehouse']=order.warehouse_code
                    get_order['Warehouse User'] = _x_name
                    get_order['SR'] = str(order.sr_id.first_name + ' ' + order.sr_id.last_name)
                    get_order['Dealer']=str(order.dealer.user_code+'.'+order.company)

                    result.append(get_order)

                response = HttpResponse(content_type='text/csv')
                response['Content-Disposition'] = 'attachment;filename='+str(statusobj)+'_list.csv'
                keys = ['Order No', 'Order Date', 'Dispatched Date', 'Invoice Value',
                        'Status', 'Warehouse', 'Warehouse User', 'SR', 'Dealer']
                writer = csv.DictWriter(response, keys)
                writer.writeheader()
                writer.writerows(result)
                return response
    # context = {'orderhead': orderhead}
    context = {'orderhead': orderhead, 'statusobj': statusobj}
    return render(request, 'warehouse/warehouse_orders.html', context=context)


def WarehouseInventory(request):
    urlobj = request.path.split('/')[2]
    ware = ''
    if urlobj == 'warehouse-inventory':
        ware = WarehouseMaster.objects.filter(is_active=True, is_deleted=False)
    if urlobj == 'warehouse-manager-inventory':
        wareadmin = WarehouseAdmin.objects.filter(
            is_active=True, is_deleted=False, user_id=request.user.id).values_list('warehouse_id')
        ware = WarehouseMaster.objects.filter(is_active=True, is_deleted=False, warehouse_code__in=wareadmin)
    firstlist = []
    secondlist = []
    thirdlist = []
    fourthlist = []
    fivelist = []
    for obj in ProductCategoryMaster.objects.filter(is_active=True, is_deleted=False):
        cid = 0
        if str(obj.category_name) == 'CE':
            firstlist.append(obj.category_name)
            cid = int(obj.category_id)
            second = []
            for a in ProductCategoryMaster.objects.filter(parent=cid, is_active=True):
                secondlist.append(a.category_name)
                second.append(int(a.category_id))
            third = []
            for b in ProductCategoryMaster.objects.filter(parent__in=second, is_active=True):
                thirdlist.append(b.category_name)
                third.append(int(b.category_id))
            four = []
            for c in ProductCategoryMaster.objects.filter(parent__in=third, is_active=True):
                fourthlist.append(c.category_name)
                four.append(int(c.category_id))
            for d in ProductCategoryMaster.objects.filter(parent__in=four, is_active=True):
                fivelist.append(d.category_name)
            break
    # one, two, three = get_product_category()
    # one, two, three, four, five = get_warehouse_product_category()
    # if request.method == 'POST':
    # if 'search' in request.GET:

        # stockregi=StockRegister.objects.filter(is_active=True,is_deleted=False,warehouse_id=request.POST.get('warecode')).values_list('product_id')
        # searchlist=[]
        # warecodeobj=request.POST.get('warecode')
        # makeurl='?warehouse_code='+warecodeobj+' && '
        # if request.POST.get('first')!='' and request.POST.get('first')!=None:
        #     varfirst=request.POST.get('first')
        #     searchlist.append(request.POST.get('first'))
        #     makeurl+='maincategory='+varfirst+' && '
        # if request.POST.get('second')!='' and request.POST.get('second')!=None:
        #     varsecond=request.POST.get('second')
        #     searchlist.append(request.POST.get('second'))
        #     makeurl+='category='+varsecond+' && '
        # if request.POST.get('third')!='' and request.POST.get('third')!=None:
        #     varthird=request.POST.get('third')
        #     searchlist.append(request.POST.get('third'))
        #     makeurl+='subcategory='+varthird+' && '
        # if request.POST.get('four')!='' and request.POST.get('four')!=None:
        #     varfour=request.POST.get('four')
        #     searchlist.append(request.POST.get('four'))
        #     makeurl+='subcategory_2='+varfour+' && '
        # if request.POST.get('five')!='' and request.POST.get('five')!=None:
        #     varfive=request.POST.get('five')
        #     searchlist.append(request.POST.get('five'))
        #     makeurl+='subcategory_3='+varfive+' && '
        # if request.POST.get('Sealable')!='' and request.POST.get('Sealable')!=None:
        #     Sealable=request.POST.get('Sealable')
        #     makeurl+='Sealable='+Sealable+' && '
        # if request.POST.get('nonSealable')!='' and request.POST.get('nonSealable')!=None:
        #     nonSealable=request.POST.get('nonSealable')
        #     makeurl+='nonSealable='+nonSealable+' && '
        # if request.POST.get('inStock')!='' and request.POST.get('inStock')!=None:
        #     instock=request.POST.get('inStock')
        #     makeurl+='inStock='+instock+' && '
        # if request.POST.get('outstock')!='' and request.POST.get('outstock')!=None:
        #     outstock=request.POST.get('outstock')
        #     makeurl+='outstock='+outstock+''
        # result=warehouse_product_search(request,Sealable,nonSealable,instock,outstock,searchlist,stockregi)
        # htmlformat=result[0]

    searchlist = []
    varfirst = request.GET.get('first')
    varsecond = request.GET.get('second')
    varthird = request.GET.get('third')
    varfour = request.GET.get('four')
    varfive = request.GET.get('five')
    warehouse = request.GET.get('warehouse')

    Sealable = request.GET.get('Sealable')
    nonSealable = request.GET.get('nonSealable')
    inStock = request.GET.get('inStock')
    outStock = request.GET.get('outstock')
    mySearch = {
        "varfirst": varfirst,
        "varsecond": varsecond,
        "varthird": varthird,
        "varfour": varfour,
        "varfive": varfive,
        "warehouse": warehouse,
        "Sealable": Sealable,
        "nonSealable": nonSealable,
        "inStock": inStock,
        "outstock": outStock
    }
    if 'warehouse' in request.GET:
        result = get_new_search(request, flag=True, **mySearch)
    else:
        result = get_new_search(request, flag=False, **mySearch)
    htmlformat = result

    context = {
        'ware': ware,
        'htmlformat': htmlformat,
        'Salable': Sealable,
        'nonSalable': nonSealable,
        'inStock': inStock,
        'outStock': outStock
    }

    if varfirst == '' or varfirst == 'all' or varfirst is None:
        one, two, three, four, five = get_warehouse_product_category()
    else:
        one, two, three, four, five = get_product_all_category(request, varfirst)

    # ...........................................
    if htmlformat:
        """Length of Search Products"""
        totalItem = htmlformat.split('class="col-xs-12 col-sm-2 col-md-2"')
        if len(totalItem) - 1 > 0:
            total_record_msg = "{0} records found".format(len(totalItem) - 1)
        else:
            total_record_msg = "No records found"
        context["search_record"] = total_record_msg
    # ...........................................

    context["one"] = one
    context["two"] = two
    context["three"] = three
    context["four"] = four
    context["five"] = five
    context["varOne"] = varfirst
    context["varTwo"] = varsecond
    context["varThree"] = varthird
    context["varFour"] = varfour
    context["varFive"] = varfive
    context["varWare"] = warehouse
    return render(request, 'warehouse/warehouse_inventory.html', context)


def warehouse_product_search(request, Sealable, nonSealable, instock, outstock, prosearch=None, stockregi=None):
    listdata=''
    listdata=addcart(request)
    if not listdata:
        totalamt=0
        totalcount=0
        listobj=''
    else:
        totalamt=listdata[1]
        totalcount=listdata[2]
        listobj=listdata[0]

    result=[]
    objpm=ProductMaster.objects.filter(is_active=True,is_deleted=False,pk__in=stockregi)
    PCM_id=0
    incount=''
    objdata=''
    count=0
    abc=''
    htmlformat=''
    htmlformat1=''
    btncount=0
    for data in objpm:
        count=0
        jsondata={}
        try:
            stockobj=StockRegister.objects.get(product=data.product_id)
        except Exception:
            stockobj=0
        if stockobj!=0:
            if int(stockobj.stock_in)<=int(stockobj.stock_out):
                jsondata['stock']='<a href="#" class="out-stock">Out of Stock</a>'
                incount='Out Stock'
                objdata='non Sealable'
            else:
                jsondata['stock']='<a href="#" class="in-stock">In Stock</a>'
                incount='in Stock'
                objdata='Sealable'
        else:
            jsondata['stock']='<a href="#" class="out-stock">Out of Stock</a>'
            incount='Out Stock'
            objdata='non Sealable'
        if data.sku_code!='' and data.sku_code!=None:
            jsondata['sku_code']=str(data.sku_code)
        else:
            jsondata['skuc_ode']=''
        if data.size_capacity!='' and data.size_capacity!=None:
            jsondata['size_capacity']=str(data.size_capacity)
        else:
            jsondata['size_capacity']=''
        if data.size_capacity_unit!='' and data.size_capacity_unit!=None:
            jsondata['size_capacity_unit']=str(data.size_capacity_unit)
        else:
            jsondata['size_capacity_unit']=''
        if data.dealer_price!='' and data.dealer_price!=None:
            jsondata['dealer_price']=str(data.dealer_price)
        else:
            jsondata['dealer_price']=''
        if data.max_retail_price!='' and data.max_retail_price!=None:
            jsondata['max_retail_price']=str(data.max_retail_price)
        else:
            jsondata['max_retail_price']=''
        if data.product_description_feature!='' and data.product_description_feature!=None:
            jsondata['product_description_feature']=str(data.product_description_feature)
        else:
            jsondata['product_description_feature']=''
        datas=''
        category = ProductCategoryMaster.objects.filter(is_active=True,is_deleted=False)
        get_product=[]
        for PC in category:
            #get_category={}
            CID=data.product_category_id
            PCM=ProductCategoryMaster.objects.get(pk=CID)
            get_product.append(PCM.category_name)
            PID=PCM.parent
            try:
                PCM=ProductCategoryMaster.objects.get(pk=PID)
            except Exception:
                PCM=''
            if PCM!='':
                get_product.append(PCM.category_name)
                PID=PCM.parent
            try:
                PCM=ProductCategoryMaster.objects.get(pk=PID)
            except Exception:
                PCM=''
            if PCM!='':
                get_product.append(PCM.category_name)
                PID=PCM.parent
            try:
                PCM=ProductCategoryMaster.objects.get(pk=PID)
            except Exception:
                PCM=''
            if PCM!='':
                get_product.append(PCM.category_name)
                PID=PCM.parent
            try:
                PCM1=ProductCategoryMaster.objects.get(pk=PID)
            except Exception:
                PCM1=''
            if PCM1!='':
                get_product.append(PCM1.category_name)
                PID=PCM1.parent
            break
        get_product.reverse()
        if prosearch!=None:
            count=0
            for category in  get_product:
                if category in prosearch:
                    count+=1
            if count>=3:
                for i in get_product:
                    datas+=str(i)+','
                jsondata['name']=datas.rstrip(',')
                if Sealable!=''or nonSealable!='' or instock!='' or outstock!='':
                    if Sealable==objdata or nonSealable==objdata or instock==incount or outstock==incount:
                        result.append(jsondata)
                else:
                    if incount=='in Stock':
                        result.append(jsondata)
        else:
            for i in get_product:
                datas+=str(i)+','
            jsondata['name']=datas.rstrip(',')
            if incount=='in Stock':
                result.append(jsondata)
    count=0
    abc=''
    htmlformat=''
    htmlformat1=''
    btncount=0
    for obj in result:
        count+=1
        btncount+=1
        #query=obj['query']
        abc+='<div class="col-xs-12 col-sm-2 col-md-2" style="width:20%;">'\
            '<div class="order-figger">'\
            '<figure class="samsung-figger samsung-text">'\
            '<!--<img src="/static/img/samsung-order-img.png" alt="samsung" class="samsung_brand">-->Samsung'\
            '</figure><div class="order-detail"><h5>'+obj['name']+'</h5>'\
            '<h5>'+obj['sku_code']+', '+obj['size_capacity']+' '+obj['size_capacity_unit']+' </h5>'\
            '<h5>MRP: <i class="fa fa-inr" aria-hidden="true"></i><span class="price">'\
            ''+obj['max_retail_price']+'</span>, DP: <i class="fa fa-inr" aria-hidden="true">'\
            '</i> <span class="dp-price">'+obj['dealer_price']+'</span></h5>'\
            '<div class="pagination-detail"><div class="row"><div class="col-xs-6 col-sm-6 col-md-7">'\
            '<ul class="pagination pagination-text"><li onclick="return minus('+str(btncount)+');"><a>-</a></li><li><a id="ordervalue'+str(btncount)+'">1</a>'\
            '<input type="hidden" name="hdnorder'+str(btncount)+'" id="hdnorder'+str(btncount)+'" value="1"></li>'\
            '<li class="active" onclick="return plus('+str(btncount)+');"><a >+</a></li></ul></div><div class="col-xs-6 col-sm-6 col-md-5 text-center">'\
            '<button type="submit" class="order-view" name="btnaddorder" value="'+str(btncount)+'_'+obj['sku_code']+'">Add Order</button></div></div></div><p>'+obj['product_description_feature']+'</p>'\
            ''+obj['stock']+'</div></div></div>'
        if count==5:
            count=0
            htmlformat+='<div class="row order-row">'+abc+'</div>'
            abc=''
    if count!=5:
        count=0
        htmlformat+='<div class="row order-row">'+abc+'</div>'
        abc=''
    return [htmlformat,objpm,listobj,totalcount,totalamt]


def warehouse_user_dashboard(request):
    querysetcount=None
    stocklevel=''
    warehouseAdmin = WarehouseAdmin.objects.filter(
        is_active=True, is_deleted=False, user_id=request.user.id).values_list('warehouse_id')
    
    # for stockl in stocklevel:
    #     if stockl.min_stock_level>0:
    #         stockl.__dict__['noti']='min_stock'
    stock = StockRegister.objects.filter(
        is_active=True, is_deleted=False, warehouse_id__in=warehouseAdmin)
    for stocks in stock:
        stocklevel=''
        remain_stock=int(int(stocks.stock_in)-int(stocks.stock_out))
        try:
            stocklevel = StockLevelMaster.objects.get(is_active=True, is_deleted=False, warehouse_id=stocks.warehouse_id)
        except Exception:
            stocklevel=''
        if stocklevel!='':
            min_stock=int(remain_stock)<=int(stocklevel.min_stock_level)
            if min_stock==True:
                wadmin=User.objects.filter(is_active=True,groups__name="WHS")
                wuser=User.objects.filter(is_active=True,groups__name="WHSUSER")
                WHS_all=list(chain(wadmin,wuser))
                html_content=''
                if WHS_all:
                    emails=[]
                    for user in WHS_all:
                        emails.append(user.email)
                    html_content+= "Stocks of pruduct "+str(stocks.product.sku_code)+", "+str(remain_stock)+" has remains."
                    subject="Minimum Stocks"
                    email = EmailMultiAlternatives(subject, html_content, settings.DEFAULT_FROM_EMAIL, to=emails)
                    # email.send()
                    """ comments """
    #orderdetail = OrderDetails.objects.filter(is_active=True, is_deleted=False, product_id_id__in=stock).values_list('order_id_id')
    orderhead = OrderHead.objects.filter(
        status__in=['Dispatched', 'InWarehouse'],
        is_active=True, is_deleted=False, warehouse_manager=request.user).exclude(order_date__exact=None).order_by('-order_date')
    orderheadnull = OrderHead.objects.filter(
        status__in=['Dispatched', 'InWarehouse'],
        is_active=True,order_date=None, is_deleted=False, warehouse_manager=request.user)

    for order in orderhead:
        try:
            _x_dealer = DealerProfile.objects.get(
                dealer=order.dealer, is_active=True, is_deleted=False)
            order.__dict__['company'] = _x_dealer.company_name
        except:
            order.__dict__['company'] = ""

        try:
            status = OrderEventHistory.objects.get(order_id_id=order.pk, order_status='Dispatched')
            order.__dict__['Dispatched_Date'] = status.created_on
        except:
            continue

    pending = orderhead.filter(status='InWarehouse')
    dispatched = orderhead.filter(status='Dispatched')
    for order in dispatched:
        try:
            _x_dealer = DealerProfile.objects.get(
                dealer=order.dealer, is_active=True, is_deleted=False)
            order.__dict__['company'] = _x_dealer.company_name
        except:
            order.__dict__['company'] = ""

        try:
            status = OrderEventHistory.objects.get(order_id_id=order.pk, order_status='Dispatched')
            order.__dict__['Dispatched_Date'] = status.created_on
        except:
            continue

    orderhead=list(chain(orderhead,orderheadnull))
    querysetcount=orderhead
    if len(orderhead)>10:
        orderhead=orderhead[:10]
    orderDispatch = OrderHead.objects.filter(
        status__in=['Dispatched','PaymentDone'], is_active=True, is_deleted=False, warehouse_manager=request.user).count()
    context = {"orderhead": orderhead,'pending':pending ,'dispatched': dispatched, 'orderdispatch': orderDispatch}
    warehouses = WarehouseMaster.objects.filter(
        is_active=True, is_deleted=False, pk__in=warehouseAdmin).count()
    inprocess = OrderHead.objects.filter(
        status__in=['OrderPlaced','InWarehouse'], is_active=True, is_deleted=False, warehouse_manager=request.user.id).count()
    context["warehouses"] = warehouses
    context["confirm"] = inprocess
    context['querysetcount']=querysetcount
    context['stocklevel']=stocklevel

    try:
        total_outstock_sku = get_length_of_outstock(request)
    except:
        total_outstock_sku = 0
    context["out_stock"] = total_outstock_sku
    return render(request, 'warehouse/warehouse_user_dashboard.html', context=context)


def warehouse_all_order(request,statusobj=None):
    orderhead = OrderHead.objects.filter(
        status=statusobj,
        is_active=True, is_deleted=False, warehouse_manager=request.user).exclude(order_date__exact=None)
    orderheadnull = OrderHead.objects.filter(
        status=statusobj,
        is_active=True,order_date=None, is_deleted=False, warehouse_manager=request.user)
    orderhead=list(chain(orderhead,orderheadnull))
    if orderhead:
        for order in orderhead:
            try:
                status = OrderEventHistory.objects.get(order_id_id=order.pk, order_status='Dispatched')
                order.__dict__['Dispatched_Date'] = status.created_on
            except:
                order.__dict__['Dispatched_Date'] =  '---'

    if request.method=='POST':
        if 'export' in request.POST:
            result=[]
            for order in orderhead:
                get_order={}
                get_order['Order No']=order.order_number
                if isinstance(order.order_date, datetime.date):
                    get_order['Order Date'] = order.order_date.date()
                else:
                    get_order['Order Date'] = order.order_date
                if isinstance(order.Dispatched_Date, datetime.date):
                    get_order['Dispatched Date'] = order.Dispatched_Date.date()
                else:
                    get_order['Dispatched Date'] = order.Dispatched_Date
                get_order['Invoice Value']=order.total_amount
                get_order['Status']=order.status
                get_order['Warehouse']=order.warehouse_code
                get_order['Manager']=order.warehouse_manager.first_name
                get_order['SR'] = str(order.sr_id.first_name + ' ' + order.sr_id.last_name)
                get_order['Dealer']=str(order.dealer.first_name+' '+order.dealer.last_name)

                result.append(get_order)
            response = HttpResponse(content_type='text/csv')
            response['Content-Disposition'] = 'attachment;filename=warehouse_user_order_list.csv'
            keys = ['Order No','Order Date', 'Dispatched Date', 'Invoice Value','Status','Warehouse','Manager', 'SR','Dealer']
            writer = csv.DictWriter(response,keys)
            writer.writeheader()
            writer.writerows(result)
            return response
    context = {"orderhead": orderhead }
    return render(request,'warehouse/all_orders.html', context=context)


def WarehouseManager(request):
    warehouseID = request.GET.get('warehouse')
    warehouseUsers = WarehouseAdmin.objects.filter(warehouse=warehouseID, is_active=True, is_deleted=False)
    warehouseUsers = [{"id": item.user.pk, "name": "{0} {1}".format(item.user.first_name, item.user.last_name)} for item in warehouseUsers]
    return JsonResponse({"data": warehouseUsers, "msg": "OK"})


def min_stock_of_product(sku_in):
    status_in_stock, sku_code = None, None
    products = ProductMaster.objects.filter(sku_code__in=sku_in, is_active=True, is_deleted=False)
    for product in products:
        sellable = StockRegister.objects.filter(
            product=product, stock_type="Sellable",
            is_active=True, is_deleted=False).aggregate(total=Sum('stock_in'))
        try:
            minStockLevel = StockLevelMaster.objects.filter(
                product=product, is_active=True, is_deleted=False).first()
            minStockLevel = 0 if minStockLevel is None else minStockLevel.min_stock_level
            minStockLevel = int(minStockLevel)
        except Exception:
            minStockLevel = int(0)

        stockOut = StockRegister.objects.filter(
            product=product, stock_type="stock_out",
            is_active=True, is_deleted=False).aggregate(total=Sum('stock_out'))
        if sellable["total"] is not None:
            totalStockOut = 0 if stockOut["total"] is None else stockOut["total"]
            totalStock = int(sellable["total"]) - int(totalStockOut)
            if totalStock > minStockLevel:
                status_in_stock = True
            else:
                status_in_stock = False
                sku_code = product.sku_code
                break
    return status_in_stock, sku_code


def AssignWarehouse(request):
    orderID = request.GET.get('order')
    warehouse_code = request.GET.get('warehouse')
    userID = request.GET.get('user')
    try:
        warehouse = WarehouseMaster.objects.get(warehouse_code=warehouse_code, is_active=True, is_deleted=False)
        orderHead = OrderHead.objects.get(order_number=orderID, is_active=True, is_deleted=False)
        orderDetails = OrderDetails.objects.filter(order_id=orderHead, is_active=True, is_deleted=False)
        # .......................................
        sku_list = [order.sku_code for order in orderDetails]
        # status_of_product_in_stock, sku_code = min_stock_of_product(sku_list)
        status_of_product_in_stock, sku_code = check_stock_out(sku_list, orderDetails)
        if status_of_product_in_stock:
            # print("product available in stock")
            orderHead.status = "InWarehouse"
            orderHead.warehouse_code = warehouse.warehouse_code
            orderHead.warehouse_address = warehouse.address_line1
            orderHead.warehouse_manager_id = userID
            orderHead.save()

            """ ========= ORDER HISTORY ========== """
            orderHistory = OrderEventHistory(
                order_id=orderHead,
                order_status=orderHead.status,
                created_by=request.user
            )
            orderHistory.save()
            # """ ========= SEND EMAIL OR SMS ========== """
            try:
                send_sms_or_email(request, orderHead)
            except Exception:
                pass

            # """ ========= SEND EMAIL OR SMS ========== """
            # try:
            #     send_email_or_sms(orderHead.status, orderHead.order_number, "Dispatch_Delivered-InWarehouse")
            # except Exception:
            #     pass
            message = "success"
        else:
            # print("product not available in stock")
            message = "Product ({0}) Out of Stock.".format(sku_code)
    except WarehouseMaster.DoesNotExist:
        message = "error"
    except OrderHead.DoesNotExist:
        message = "error"
    return JsonResponse({"message": message})


# ---------------------------------------------------


def custom_product_search(request, prosearch=None):
    result = []
    objpm = ProductMaster.objects.filter(is_active=True, is_deleted=False)
    for data in objpm:
        jsondata = {}
        try:
            stockobj = StockRegister.objects.get(product=data.product_id)
        except Exception:
            stockobj = 0
        if stockobj != 0:
            if int(stockobj.stock_in) <= int(stockobj.stock_out):
                jsondata['stock'] = '<a href="#" class="out-stock">Out of Stock</a>'
                incount = 'out'
            else:
                jsondata['stock'] = '<a href="#" class="in-stock">In Stock</a>'
                incount = 'in'
        else:
            jsondata['stock'] = '<a href="#" class="out-stock">Out of Stock</a>'
            incount = 'out'
        if data.sku_code != '' and data.sku_code is not None:
            jsondata['sku_code'] = str(data.sku_code)
        else:
            jsondata['sku_code'] = ''
        if data.size_capacity != '' and data.size_capacity is not None:
            jsondata['size_capacity'] = str(data.size_capacity)
        else:
            jsondata['size_capacity'] = ''
        if data.size_capacity_unit != '' and data.size_capacity_unit is not None:
            jsondata['size_capacity_unit'] = str(data.size_capacity_unit)
        else:
            jsondata['size_capacity_unit'] = ''
        if data.dealer_price != '' and data.dealer_price is not None:
            jsondata['dealer_price'] = str(data.dealer_price)
        else:
            jsondata['dealer_price'] = ''
        if data.max_retail_price != '' and data.max_retail_price is not None:
            jsondata['max_retail_price'] = str(data.max_retail_price)
        else:
            jsondata['max_retail_price'] = ''
        if data.product_description_feature != '' and data.product_description_feature is not None:
            jsondata['product_description_feature'] = str(data.product_description_feature)
        else:
            jsondata['product_description_feature'] = ''

        datas = ''
        category = ProductCategoryMaster.objects.filter(is_active=True, is_deleted=False)
        get_product = []

        for PC in category:
            CID = data.product_category_id
            PCM = ProductCategoryMaster.objects.get(pk=CID)
            get_product.append(PCM.category_name)
            PID = PCM.parent
            try:
                PCM = ProductCategoryMaster.objects.get(pk=PID)
            except Exception:
                PCM = ''
            if PCM != '':
                get_product.append(PCM.category_name)
                PID = PCM.parent
            try:
                PCM = ProductCategoryMaster.objects.get(pk=PID)
            except Exception:
                PCM = ''
            if PCM != '':
                get_product.append(PCM.category_name)
                PID = PCM.parent
            try:
                PCM = ProductCategoryMaster.objects.get(pk=PID)
            except Exception:
                PCM = ''
            if PCM != '':
                get_product.append(PCM.category_name)
                PID = PCM.parent
            try:
                PCM1 = ProductCategoryMaster.objects.get(pk=PID)
            except Exception:
                PCM1 = ''
            if PCM1 != '':
                get_product.append(PCM1.category_name)
                PID = PCM1.parent
            break
        get_product.reverse()

        if prosearch is not None:
            count = 0
            for category in get_product:
                if category in prosearch:
                    count += 1
            if count > 0:
                for i in get_product:
                    datas+=str(i)+','
                jsondata['name']=datas.rstrip(',')
                if incount=='in':
                    result.append(jsondata)
        else:
            for i in get_product:
                datas+=str(i)+','
            jsondata['name']=datas.rstrip(',')
            result.append(jsondata)
    count = 0
    abc = ''
    htmlformat = ''
    btncount = 0
    for obj in result:
        count += 1
        btncount += 1
        abc += '<div class="col-xs-12 col-sm-2 col-md-2" style="width:20%;">'\
            '<div class="order-figger">'\
            '<figure class="samsung-figger samsung-text">'\
            '<!--<img src="/static/img/samsung-order-img.png" alt="samsung" class="samsung_brand">-->Samsung'\
            '</figure><div class="order-detail"><h5>'+obj['name']+'</h5>'\
            '<h5>'+obj['sku_code']+', '+obj['size_capacity']+' '+obj['size_capacity_unit']+' </h5>'\
            '<h5>MRP: <i class="fa fa-inr" aria-hidden="true"></i><span class="price">'\
            ''+obj['max_retail_price']+'</span>, DP: <i class="fa fa-inr" aria-hidden="true">'\
            '</i> <span class="dp-price">'+obj['dealer_price']+'</span></h5>'\
            '<div class="pagination-detail"><div class="row"><div class="col-xs-6 col-sm-6 col-md-7">'\
            '<ul class="pagination pagination-text"><li onclick="return minus('+str(btncount)+');"><a>-</a></li><li><a id="ordervalue'+str(btncount)+'">1</a>'\
            '<input type="hidden" name="hdnorder'+str(btncount)+'" id="hdnorder'+str(btncount)+'" value="1"></li>'\
            '<li class="active" onclick="return plus('+str(btncount)+');"><a >+</a></li></ul></div><div class="col-xs-6 col-sm-6 col-md-5 text-center">'\
            '<button type="submit" class="order-view" name="btnaddorder" value="'+str(btncount)+'_'+obj['sku_code']+'">Add Order</button></div></div></div><p>'+obj['product_description_feature']+'</p>'\
            ''+obj['stock']+'</div></div></div>'
        if count == 5:
            count = 0
            htmlformat += '<div class="row order-row">'+abc+'</div>'
            abc = ''
    if count != 5:
        htmlformat += '<div class="row order-row">'+abc+'</div>'
    return htmlformat


def get_new_search(request, flag=None, **kwargs):
    result = []
    categories = []
    warehouse = None
    outstock = None
    if flag:
        main_category = kwargs.get("varfirst", None)
        category = kwargs.get("varsecond", None)
        sub_category = kwargs.get("varthird", None)
        sub_category_1 = kwargs.get("varfour", None)
        sub_category_2 = kwargs.get("varfive", None)
        warehouse = kwargs.get("warehouse", None)
        Sealable = kwargs.get("Sealable", None)
        nonSealable = kwargs.get("nonSealable", None)
        inStock = kwargs.get("inStock", None)
        outstock = kwargs.get("outstock", None)

        if sub_category_2 != "" and sub_category_2 != "all" and sub_category_2 is not None:
            productCategory = ProductCategoryMaster.objects.get(
                category_name__icontains=sub_category_2, is_active=True, is_deleted=False)
            categories.append(productCategory.pk)
        elif sub_category_1 != "" and sub_category_1 != "all" and sub_category_1 is not None:
            productCategories = get_product_category_list(sub_category_1)
            categories += productCategories
        elif sub_category != "" and sub_category != "all" and sub_category is not None:
            productCategories = get_product_category_list(sub_category)
            categories += productCategories
        elif category != "" and category != "all" and category is not None:
            productCategories = get_product_category_list(category)
            categories += productCategories
        elif main_category != "" and main_category != "all" and main_category is not None:
            productCategories = get_product_category_list(main_category)
            categories += productCategories
    if len(categories) > 0:
        products = ProductMaster.objects.filter(
            product_category_id__in=categories, is_active=True, is_deleted=False)
    else:
        products = ProductMaster.objects.filter(is_active=True, is_deleted=False)

    # ........................
    groups = [group.name for group in request.user.groups.all()]
    if request.user.is_superuser or request.user.is_admin or 'WHS' in groups:
        # all warehouses
        warehouseList = WarehouseMaster.objects.filter(is_active=True, is_deleted=False)
        warehouseList = [item.warehouse_code for item in warehouseList]
    elif 'WHSUSER' in groups:
        # manager warehouses
        warehouseAssigned = WarehouseAdmin.objects.filter(
            is_active=True, is_deleted=False, user_id=request.user.id).values_list('warehouse_id')
        warehouseList = WarehouseMaster.objects.filter(
            is_active=True, is_deleted=False, warehouse_code__in=warehouseAssigned)
        warehouseList = [item.warehouse_code for item in warehouseList]
    else:
        raise PermissionError
    if warehouse != 'all' and warehouse != '' and warehouse is not None:
        warehouseList.append(warehouse)
    # ........................

    for data in products:
        jsondata = {}
        try:
            stockObj = StockRegister.objects.filter(
                warehouse_id__in=warehouseList, product=data.product_id,
                is_active=True, is_deleted=False)
            if stockObj:
                pass
            else:
                continue
            if flag:
                if warehouse != 'all' and warehouse != '' and warehouse is not None:
                    stockObj = stockObj.filter(warehouse__warehouse_code=warehouse)

                if outstock != 'all' and outstock != '' and outstock is not None and inStock != 'all' and inStock != ''\
                        and inStock is not None:
                    # stockObj = stockObj.filter(stock_in__gte=0)
                    stockObj = stockObj.filter(stock_type='stock_out')
                else:
                    if inStock != 'all' and inStock != '' and inStock is not None:
                        # stockObj = stockObj.filter(stock_in__gt=0)
                        is_persent = stock_check(stockObj, data)
                        if is_persent == False:
                            stockObj
                        else:
                            stockObj = []

                    if outstock != 'all' and outstock != '' and outstock is not None:
                        # stockObj = stockObj.filter(stock_in=0)
                        # stockObj = stockObj.filter(stock_type='stock_out')
                        is_persent = stock_check(stockObj, data)
                        if is_persent == True:
                            stockObj
                        else:
                            stockObj = []

                if Sealable != '' and Sealable is not None and nonSealable != '' and nonSealable is not None:
                    stockObj = stockObj.filter(Q(stock_type='Sellable') | Q(stock_type='NonSellable'))
                else:
                    if Sealable != '' and Sealable is not None:
                        stockObj = stockObj.filter(stock_type='Sellable')

                    if nonSealable != '' and nonSealable is not None:
                        stockObj = stockObj.filter(stock_type='NonSellable')
            # if len(categories) > 0:
            #     stockObj = stockObj.filter(product_category_id__in=categories)
            stockDataList = stockObj
            if len(stockDataList) > 0:
                pass
            else:
                continue
        except Exception:
            continue

        for stockobj in stockDataList:
            totalStock = 0
            sellable = stockObj.filter(stock_type="Sellable",
                                       is_active=True, is_deleted=False).aggregate(total=Sum('stock_in'))
            stockOut = stockObj.filter(stock_type="stock_out",
                                       is_active=True, is_deleted=False).aggregate(total=Sum('stock_out'))
            try:
                minStockLevel = StockLevelMaster.objects.filter(
                    product__pk=stockobj.product_id, is_active=True, is_deleted=False).first()
                minStockLevel = 0 if minStockLevel is None else minStockLevel.min_stock_level
                minStockLevel = int(minStockLevel)
            except Exception:
                minStockLevel = int(0)

            if sellable["total"] is not None:
                totalStockOut = 0 if stockOut["total"] is None else stockOut["total"]
                totalStock = int(sellable["total"]) - int(totalStockOut)
            # if int(stockobj.stock_in) <= int(stockobj.stock_out):
            if totalStock <= minStockLevel and  stockobj.stock_type == 'Sellable':
                jsondata['stock'] = '<a href="#" class="out-stock">Out of Stock</a>'
                incount = 'out'
            else:
                jsondata['stock'] = '<a href="#" class="in-stock">In Stock</a>'
                incount = 'in'

            if outstock is not None:
                outFlag = True
            else:
                outFlag = False
            finalJson = get_bind_product(jsondata, data, incount, categories, outFlag)
            if finalJson:
                result.append(finalJson)

        # if data.sku_code != '' and data.sku_code is not None:
        #     jsondata['sku_code'] = str(data.sku_code)
        # else:
        #     jsondata['sku_code'] = ''
        # if data.size_capacity != '' and data.size_capacity is not None:
        #     jsondata['size_capacity'] = str(data.size_capacity)
        # else:
        #     jsondata['size_capacity'] = ''
        # if data.size_capacity_unit != '' and data.size_capacity_unit is not None:
        #     jsondata['size_capacity_unit'] = str(data.size_capacity_unit)
        # else:
        #     jsondata['size_capacity_unit'] = ''
        # if data.dealer_price != '' and data.dealer_price is not None:
        #     jsondata['dealer_price'] = str(data.dealer_price)
        # else:
        #     jsondata['dealer_price'] = ''
        # if data.max_retail_price != '' and data.max_retail_price is not None:
        #     jsondata['max_retail_price'] = str(data.max_retail_price)
        # else:
        #     jsondata['max_retail_price'] = ''
        # if data.product_description_feature != '' and data.product_description_feature is not None:
        #     jsondata['product_description_feature'] = str(data.product_description_feature)
        # else:
        #     jsondata['product_description_feature'] = ''
        #
        # datas = ''
        # category = ProductCategoryMaster.objects.filter(is_active=True, is_deleted=False)
        # get_product = []
        # for PC in category:
        #     CID = data.product_category_id
        #     PCM = ProductCategoryMaster.objects.get(pk=CID)
        #     get_product.append(PCM.category_name)
        #     PID = PCM.parent
        #     try:
        #         PCM = ProductCategoryMaster.objects.get(pk=PID)
        #     except Exception:
        #         PCM = ''
        #     if PCM != '':
        #         get_product.append(PCM.category_name)
        #         PID = PCM.parent
        #     try:
        #         PCM = ProductCategoryMaster.objects.get(pk=PID)
        #     except Exception:
        #         PCM = ''
        #     if PCM != '':
        #         get_product.append(PCM.category_name)
        #         PID = PCM.parent
        #     try:
        #         PCM = ProductCategoryMaster.objects.get(pk=PID)
        #     except Exception:
        #         PCM = ''
        #     if PCM != '':
        #         get_product.append(PCM.category_name)
        #         PID = PCM.parent
        #     try:
        #         PCM1 = ProductCategoryMaster.objects.get(pk=PID)
        #     except Exception:
        #         PCM1 = ''
        #     if PCM1 != '':
        #         get_product.append(PCM1.category_name)
        #         PID = PCM1.parent
        #     break
        # get_product.reverse()
        #
        # if categories is not None:
        #     for i in get_product:
        #         datas += str(i)+','
        #     jsondata['name'] = datas.rstrip(',')
        #     if incount == 'in':
        #         result.append(jsondata)
        # else:
        #     for i in get_product:
        #         datas += str(i)+','
        #     jsondata['name'] = datas.rstrip(',')
        #     result.append(jsondata)
    count = 0
    abc = ''
    htmlformat = ''
    btncount = 0
    for obj in result:
        try:
            count += 1
            btncount += 1
            abc += '<div class="col-xs-12 col-sm-2 col-md-2" style="width:20%;"><div class="order-figger"><figure class="samsung-figger samsung-text">Samsung</figure><div class="order-detail"><h5>' + obj['name'] + '</h5><h5>' + obj['sku_code'] + ', ' + obj['size_capacity'] + ' ' + obj['size_capacity_unit'] + ' </h5><h5>MRP: <span class="price">₹ ' + obj['max_retail_price'] + '</span>, DP: <span class="dp-price">₹ ' + obj['dealer_price'] + '</span></h5><p>' + obj['product_description_feature'] + '</p>' + obj['stock'] + '</div></div></div>'
            if count == 5:
                count = 0
                htmlformat += '<div class="row order-row">' + abc + '</div>'
                abc = ''
        except Exception:
            continue
    if count != 5:
        htmlformat += '<div class="row order-row">' + abc + '</div>'
    return htmlformat


def stock_check(stockObj, data):
    flag = False
    sellable = stockObj.filter(
        stock_type="Sellable", is_active=True,
        is_deleted=False).aggregate(total=Sum('stock_in'))
    try:
        minStockLevel = StockLevelMaster.objects.filter(
            product=data, is_active=True, is_deleted=False).first()
        minStockLevel = 0 if minStockLevel is None else minStockLevel.min_stock_level
        minStockLevel = int(minStockLevel)
    except Exception:
        minStockLevel = int(0)
    stockOut = stockObj.filter(stock_type="stock_out",
                               is_active=True, is_deleted=False).aggregate(total=Sum('stock_out'))
    if sellable["total"] is not None:
        totalStockOut = 0 if stockOut["total"] is None else stockOut["total"]
        totalStock = int(sellable["total"]) - int(totalStockOut)
        if minStockLevel >= totalStock:
            flag = True
        else:
            flag = False
    return flag

def get_bind_product(jsondata, data, incount, categories=None, flagIncount=None):
    try:
        if data.sku_code != '' and data.sku_code is not None:
            jsondata['sku_code'] = str(data.sku_code)
        else:
            jsondata['sku_code'] = ''
        if data.size_capacity != '' and data.size_capacity is not None:
            jsondata['size_capacity'] = str(data.size_capacity)
        else:
            jsondata['size_capacity'] = ''
        if data.size_capacity_unit != '' and data.size_capacity_unit is not None:
            jsondata['size_capacity_unit'] = str(data.size_capacity_unit)
        else:
            jsondata['size_capacity_unit'] = ''
        if data.dealer_price != '' and data.dealer_price is not None:
            jsondata['dealer_price'] = str(data.dealer_price)
        else:
            jsondata['dealer_price'] = ''
        if data.max_retail_price != '' and data.max_retail_price is not None:
            jsondata['max_retail_price'] = str(data.max_retail_price)
        else:
            jsondata['max_retail_price'] = ''
        if data.product_description_feature != '' and data.product_description_feature is not None:
            jsondata['product_description_feature'] = str(data.product_description_feature)
        else:
            jsondata['product_description_feature'] = ''

        datas = ''
        category = ProductCategoryMaster.objects.filter(is_active=True, is_deleted=False)
        get_product = []
        for PC in category:
            CID = data.product_category_id
            PCM = ProductCategoryMaster.objects.get(pk=CID)
            get_product.append(PCM.category_name)
            PID = PCM.parent
            try:
                PCM = ProductCategoryMaster.objects.get(pk=PID)
            except Exception:
                PCM = ''
            if PCM != '':
                get_product.append(PCM.category_name)
                PID = PCM.parent
            try:
                PCM = ProductCategoryMaster.objects.get(pk=PID)
            except Exception:
                PCM = ''
            if PCM != '':
                get_product.append(PCM.category_name)
                PID = PCM.parent
            try:
                PCM = ProductCategoryMaster.objects.get(pk=PID)
            except Exception:
                PCM = ''
            if PCM != '':
                get_product.append(PCM.category_name)
                PID = PCM.parent
            try:
                PCM1 = ProductCategoryMaster.objects.get(pk=PID)
            except Exception:
                PCM1 = ''
            if PCM1 != '':
                get_product.append(PCM1.category_name)
                PID = PCM1.parent
            break
        get_product.reverse()

        if categories is not None:
            for i in get_product:
                datas += str(i) + ','
            jsondata['name'] = datas.rstrip(',')
            if incount == 'in':
                return jsondata
            elif flagIncount and incount == 'out':
                print(incount)
                return jsondata
        else:
            for i in get_product:
                datas += str(i) + ','
            jsondata['name'] = datas.rstrip(',')
            return jsondata
    except Exception:
        return None


def get_product_category_list(category):
    productCategory = ProductCategoryMaster.objects.get(
        category_name=category, is_active=True, is_deleted=False)
    productCategories = ProductCategoryMaster.objects.filter(
        parent=productCategory.pk, is_active=True, is_deleted=False)
    productCategories = [item.pk for item in productCategories]
    productCategories.append(productCategory.pk)
    return productCategories


# ========== Warehouse Search ==========


def get_warehouse_sub_category(parent):
    if parent is None:
        categoryList = ProductCategoryMaster.objects.filter(
            parent=parent, is_active=True, is_deleted=False)
        categoryList = [{"id": item.pk, "name": item.category_name} for item in categoryList]
    else:
        categoryList = ProductCategoryMaster.objects.filter(
            parent__in=parent, is_active=True, is_deleted=False)
        categoryList = [{"id": item.pk, "name": item.category_name} for item in categoryList]
    return categoryList


def get_warehouse_product_category():
    main_category = get_warehouse_sub_category(None)  # first categories
    parents = [item["id"] for item in main_category]
    category = get_warehouse_sub_category(parents)  # second categories
    parents = [item["id"] for item in category]
    sub_category = get_warehouse_sub_category(parents)  # third categories
    parents = [item["id"] for item in sub_category]
    sub_category_one = get_warehouse_sub_category(parents)  # fourth categories
    parents = [item["id"] for item in sub_category_one]
    sub_category_two = get_warehouse_sub_category(parents)  # fifth categories
    return main_category, category, sub_category, sub_category_one, sub_category_two


def get_product_all_category(request, value=None):
    if request.is_ajax():
        index = request.GET.get('index')
        value = request.GET.get('value').rstrip(' ')
        category = ProductCategoryMaster.objects.filter(
            category_name__istartswith=value, is_active=True, is_deleted=False).first()
        if category:
            value = [category.pk]
        else:
            index = None
        # ...............................
        if index == '1':
            category = get_warehouse_sub_category(value)
            parents = [item["id"] for item in category]
            sub_category = get_warehouse_sub_category(parents)
            parents = [item["id"] for item in sub_category]
            sub_category_one = get_warehouse_sub_category(parents)
            parents = [item["id"] for item in sub_category_one]
            sub_category_two = get_warehouse_sub_category(parents)
            data = {
                "list2": category,
                "list3": sub_category,
                "list4": sub_category_one,
                "list5": sub_category_two
            }
        elif index == '2':
            sub_category = get_warehouse_sub_category(value)
            parents = [item["id"] for item in sub_category]
            sub_category_one = get_sub_category(parents)
            parents = [item["id"] for item in sub_category_one]
            sub_category_two = get_sub_category(parents)
            data = {
                "list3": sub_category,
                "list4": sub_category_one,
                "list5": sub_category_two
            }
        elif index == '3':
            sub_category_one = get_sub_category(value)
            parents = [item["id"] for item in sub_category_one]
            sub_category_two = get_sub_category(parents)
            data = {
                "list4": sub_category_one,
                "list5": sub_category_two
            }
        elif index == '4':
            sub_category_two = get_sub_category(value)
            data = {
                "list5": sub_category_two
            }
        else:
            noNeed, category, sub_category, sub_category_one, sub_category_two = get_warehouse_product_category()
            data = {
                "list2": category,
                "list3": sub_category,
                "list4": sub_category_one,
                "list5": sub_category_two
            }
        return JsonResponse(data)
    else:
        category = ProductCategoryMaster.objects.filter(
            category_name__istartswith=value, is_active=True, is_deleted=False).first()
        if category:
            value = [category.pk]

        main_category = get_warehouse_sub_category(None)
        category = get_warehouse_sub_category(value)
        parents = [item["id"] for item in category]
        sub_category = get_warehouse_sub_category(parents)
        parents = [item["id"] for item in sub_category]
        sub_category_one = get_warehouse_sub_category(parents)
        parents = [item["id"] for item in sub_category_one]
        sub_category_two = get_warehouse_sub_category(parents)
        return main_category, category, sub_category, sub_category_one, sub_category_two


def email_or_sms_list(request):
    noti = Notification_role.objects.filter(is_active=True,is_deleted=False)

    if request.method=='POST':
        if 'export' in request.POST:
            result=[]
            if len(noti)>0:
                for em_sm in noti:
                    email={}
                    email['Subjects']=em_sm.action.alerts.alerts_type
                    email['Send Type']=em_sm.action.send_type
                    if em_sm.recipient_1==None:
                        em_sm.recipient_1=''
                    if em_sm.recipient_2==None:
                        em_sm.recipient_2=''
                    if em_sm.recipient_3==None:
                        em_sm.recipient_3=''
                    email['Notification Role']=str(em_sm.recipient_1+' '+em_sm.recipient_2+''+em_sm.recipient_3)
                    email['Optional Email']=em_sm.optional_email
                    result.append(email)
                response = HttpResponse(content_type='text/csv')
                response['Content-Disposition'] = 'attachment;filename=email_or_sms_list.csv'
                keys = ['Subjects','Send Type','Notification Role','Optional Email']
                writer = csv.DictWriter(response,keys)                                
                writer.writeheader()
                writer.writerows(result)
                return response
    for obj in noti:
        data=''
        if obj.recipient_1:
            data+=obj.recipient_1+','
        if obj.recipient_2:
            data+=obj.recipient_2+','
        if obj.recipient_3:
            data+=obj.recipient_3+','
        obj.__dict__['role']=data.strip(',')
    context={
    'alerts':noti,
    'listCount':len(noti)
    }
    return render(request,'send_email_format/email_or_sms_list.html',context)


def email_or_sms(request):
    if request.method=='GET':
        data=request.GET.get('ID')
        querystring=request.GET.get('querystring')
        actionid=request.GET.get('actionid')
        roleid=request.GET.get('roleid')
        alerts=''
        if data!=None and data!='':
            try:
                alerts=Alerts.objects.get(is_active=True,is_deleted=False,pk=data)
            except Exception:
                alerts=''
            if alerts!='':
                alerts.__dict__['alertstype']=querystring
                try:
                    action=Action.objects.get(pk=actionid,is_active=True,is_deleted=False,alerts_id=alerts.pk)
                except Exception:
                    action=''
                if action!='':
                    alerts.__dict__['sendtype']=action.send_type
                else:
                    alerts.__dict__['sendtype']=action
                try:
                    noti=Notification_role.objects.get(pk=roleid,is_active=True,is_deleted=False,action_id=action.pk)
                except Exception as e:
                    noti=''
                if noti!='':
                    
                    alerts.__dict__['recipient_1']=noti.recipient_1
                    alerts.__dict__['recipient_2']=noti.recipient_2
                    alerts.__dict__['recipient_3']=noti.recipient_3
                    alerts.__dict__['optionalemail']=noti.optional_email
                else:
                    alerts.__dict__['recipient_1']=noti
                    alerts.__dict__['recipient_2']=noti
                    alerts.__dict__['recipient_3']=noti
                    alerts.__dict__['optionalemail']=noti
        context={
            'alerts':alerts,
            'querystring':querystring,
            'data':data
        }
        return render(request,'send_email_format/email_or_sms.html',context)
    if request.method=='POST':
        data=None
        if request.POST.get('alerts')!='' and request.POST.get('alerts')!=None:
            data=request.POST.get('alerts')
        else:
            data=request.GET.get('ID')
        try:
            alerts=Alerts.objects.get(is_active=True,is_deleted=False,alerts_type=data)
        except Exception:
            alerts=Alerts()
        alerts.alerts_type=request.POST.get('alerts')
        alerts.save()
        try:
            action=Action.objects.get(send_type=request.POST.get('Action'),is_active=True,is_deleted=False,alerts_id=alerts.pk)
        except Exception as e:
            action=Action()
        action.send_type=request.POST.get('Action')
        action.alerts_id=alerts.pk
        action.save()
        try:
            noti=Notification_role.objects.get(action_id=action.pk,is_active=True,is_deleted=False)
        except Exception as e:
            noti=Notification_role()
        noti.recipient_1=request.POST.get('checkbox1')
        noti.recipient_2=request.POST.get('checkbox2')
        noti.recipient_3=request.POST.get('checkbox3')
        noti.optional_email=request.POST.get('optionalemail')
        noti.action_id=action.pk
        noti.save()
        messages.add_message(request, messages.SUCCESS, 'data save successfully')
        return HttpResponseRedirect('/user/email-sms-list')
    return render(request, 'send_email_format/email_or_sms.html')


def email_sms_deleted(request):
    alertid=request.GET.get('alertid')
    actionid=request.GET.get('actionid')
    roleid=request.GET.get('roleid')
    try:
        alerts=Alerts.objects.get(is_active=True,is_deleted=False,pk=alertid)
    except Exception:
        alerts=''
    if alerts!='':
        try:
            action=Action.objects.get(pk=actionid,is_active=True,is_deleted=False,alerts_id=alerts.pk)
        except Exception as e:
            action=''
        if action!='':
            action.delete()
        try:
            noti=Notification_role.objects.get(pk=roleid,is_active=True,is_deleted=False,action_id=action.pk)
        except Exception as e:
            noti=''
        if noti!='':
            noti.delete()
        notiobj=Action.objects.filter(is_active=True,is_deleted=False,alerts_id=alertid).count()
        if notiobj==0:
            alerts.delete()
        messages.add_message(request, messages.SUCCESS, 'data deleted successfully!!!')
        
    return HttpResponseRedirect('/user/email-sms-list')


def email_or_sms_format(request):
    if request.method=='POST':
        try:
            alerts=Alerts.objects.get(is_active=True,is_deleted=False,alerts_type=request.POST.get('alerts'))
        except Exception:
            alerts=''
        if alerts!='':
            try:
                action=Action.objects.get(send_type=request.POST.get('Action'),is_active=True,is_deleted=False,alerts_id=alerts.pk)
            except Exception:
                action=''
            if action!='' :
                if request.POST.get('Action')!='both':
                    noti=EmailTemplateMaster()
                    noti.action_id=action.pk
                    if request.POST.get('Action')!='sms':
                        noti.subject=request.POST.get('subject')
                    noti.email_content=request.POST.get('Content')
                    noti.save()
                else:
                    noti=EmailTemplateMaster()
                    noti.action_id=action.pk
                    noti.subject=request.POST.get('subject')
                    noti.email_content=request.POST.get('Content')
                    noti.sms_content=request.POST.get('Content_sms')
                    noti.save()
                    # noti=models.email_sms_format()
                    # noti.action_id=action.pk
                    # noti.content=request.POST.get('Content_sms')
                    # noti.save()
                messages.add_message(request, messages.SUCCESS, 'data save successfully!!!')
            else:
                messages.add_message(request, messages.ERROR, 'Selected action not found!!!')
        else:
            messages.add_message(request, messages.ERROR, 'Selected alert not found!!!')
    return render(request, 'send_email_format/email_or_sms_format.html')


def email_sms_format_list(request):
    emailobj = EmailTemplateMaster.objects.filter(is_active=True,is_deleted=False)
    return render(request,'send_email_format/email_sms_format_list.html',{'emailobj':emailobj})


def WarehouseStock(request):
    products = ProductMaster.objects.filter(is_active=True, is_deleted=False)
    stocks, search = [], ""
    # .............................................
    # print('Yesterday : ', yesterday)
    # print('Today : ', today)
    # print('Tomorrow : ', tomorrow)
    # .............................................
    if "search" in request.POST:
        search = request.POST.get('search')
        products = products.filter(
            sku_code__icontains=search,
            is_active=True, is_deleted=False)
    # .............................................
    stockLevel = StockLevelMaster.objects.filter(
        product__in=products, is_active=True, is_deleted=False)
    # ............................................
    groups = request.user.groups.all()
    groups = [group.name for group in groups]
    if "WHSUSER" in groups:
        self_warehouses = WarehouseAdmin.objects.filter(
            user=request.user, is_active=True, is_deleted=False)
        self_warehouses = [x.warehouse for x in self_warehouses]
        stockLevel = stockLevel.filter(warehouse__in=self_warehouses)
    # ............................................
    today = datetime.date.today()
    for _x_item in stockLevel:
        product = _x_item.product
        list_dict = {
            "sku_code": product.sku_code, "total": 0, "sale": 0,
            "stock_in": 0, "stock_out": 0, "non_sale": 0
        }

        totalStock = StockRegister.objects.filter(
            stock_type__in=['Sellable', 'NonSellable', 'Return'],
            product=product,created_on__date=today, is_active=True, is_deleted=False).values(
            'stock_type').annotate(count=Sum('stock_in'))

        outStock = StockRegister.objects.filter(
            product=product, stock_type='stock_out',created_on__date=today,
            is_active=True, is_deleted=False).values(
            'stock_type').annotate(count=Sum('stock_out'))

        category = find_product_category(product)
        list_dict["category"] = ' > '.join(category)
        for item in totalStock:
            if item["stock_type"] == "Sellable":
                list_dict["sale"] = item["count"]
                list_dict["total"] = list_dict["total"] + item["count"]
            elif item["stock_type"] == "NonSellable":
                list_dict["non_sale"] = item["count"]
                list_dict["total"] = list_dict["total"] + item["count"]
            elif item["stock_type"] == "Return":
                list_dict["sale"] = list_dict["sale"] + item["count"]
                list_dict["total"] = list_dict["total"] + item["count"]

        if len(outStock) > 0:
            for item in outStock:
                list_dict["stock_out"] = list_dict["stock_out"] + item["count"]
                # list_dict["stock_in"] = list_dict["total"] - list_dict["stock_out"]
                list_dict["stock_in"] = list_dict["sale"] - list_dict["stock_out"]
        else:
            list_dict["stock_in"] = list_dict["sale"]

        # ...................
        today = datetime.date.today()
        yesterday = today - datetime.timedelta(days=1)
        prev_dict = check_prev_stocks(str(yesterday), product)
        today_out = check_today_stock_out(str(today), product)
        list_dict["opening"] = prev_dict["stock_in"]
        # list_dict["closing"] = int(prev_dict["stock_in"]) - int(today_out)
        list_dict["closing"] = prev_dict["stock_in"] + int(list_dict["total"]) - int(today_out)
        # ...................
        stocks.append(list_dict)
        # print(list_dict)

    if "export" in request.POST:
        response = HttpResponse(content_type='text/csv')
        response['Content-Disposition'] = 'attachment;filename=stock_register.csv'
        final_stock_list = []
        for x in stocks:
            final_dict = {
                "SKU Code": x["sku_code"], "Category": x["category"],
                "Opening": int(x["opening"]), "Closing": int(x["closing"]),
                "Sellable": int(x["sale"]), "Non Sellable": int(x["non_sale"]),
                "Total Stock": x["total"]
            }
            final_stock_list.append(final_dict)
        keys = ['SKU Code', 'Category', 'Opening', 'Closing', 'Total Stock', 'Sellable', 'Non Sellable']
        # 'stock_in', 'stock_out',
        for item in stocks:
            del item["stock_in"], item["stock_out"]
        writer = csv.DictWriter(response, keys)
        writer.writeheader()
        writer.writerows(final_stock_list)
        return response

    paginator = Paginator(stocks, 10)
    try:
        page_index = request.GET.get("page", 1)
        data_list = paginator.page(page_index)
    except PageNotAnInteger:
        data_list = paginator.page(1)
    except EmptyPage:
        data_list = paginator.page(paginator.num_pages)
    context = {"stocks": data_list, "search": search}
    return render(request, 'warehouse/stock_register.html', context=context)


def check_today_stock_out(today_date, product):
    outStock = StockRegister.objects.filter(
        product=product, stock_type='stock_out',
        created_on__date=str(today_date),
        is_active=True, is_deleted=False).values(
        'stock_type').annotate(count=Sum('stock_out'))

    today_stock_out = 0
    if len(outStock) > 0:
        today_stock_out = outStock[0]["count"]
    return today_stock_out


def check_prev_stocks(to_date, product):
    list_dict = {
        "sku_code": product.sku_code, "total": 0, "sale": 0,
        "stock_in": 0, "stock_out": 0, "non_sale": 0
    }
    totalStock = StockRegister.objects.filter(
        stock_type__in=['Sellable', 'NonSellable', 'Return'],
        created_on__date__lte=str(to_date), product=product,
        is_active=True, is_deleted=False).values(
        'stock_type').annotate(count=Sum('stock_in'))

    outStock = StockRegister.objects.filter(
        product=product, stock_type='stock_out',
        created_on__date__lte=str(to_date),
        is_active=True, is_deleted=False).values(
        'stock_type').annotate(count=Sum('stock_out'))

    for item in totalStock:
        if item["stock_type"] == "Sellable":
            list_dict["sale"] = item["count"]
            list_dict["total"] = list_dict["total"] + item["count"]
        elif item["stock_type"] == "NonSellable":
            list_dict["non_sale"] = item["count"]
            list_dict["total"] = list_dict["total"] + item["count"]
        elif item["stock_type"] == "Return":
            list_dict["sale"] = list_dict["sale"] + item["count"]
            list_dict["total"] = list_dict["total"] + item["count"]

    if len(outStock) > 0:
        for item in outStock:
            list_dict["stock_out"] = list_dict["stock_out"] + item["count"]
            list_dict["stock_in"] = list_dict["sale"] - list_dict["stock_out"]
    else:
        list_dict["stock_in"] = list_dict["sale"]
    return list_dict


def get_sku_details(request, skucode=None):
    from_date = request.GET.get('from_date', None)
    to_date = request.GET.get('to_date', None)
    if from_date is None or from_date == "" and to_date is None or to_date == "":
        from_date = str(datetime.datetime.today().date())
        to_date = str(datetime.datetime.today().date())
    elif from_date is None or from_date == "":
        from_date = str(datetime.datetime.today().date())
    elif to_date is None or to_date == "":
        to_date = str(datetime.datetime.today().date())

    product = ProductMaster.objects.get(
        sku_code=skucode, is_active=True, is_deleted=False)
    stocks = StockRegister.objects.filter(
        product=product, created_on__gte=from_date,
        created_on__date__lte=to_date, is_active=True,
        is_deleted=False).order_by('-pk')
    context = {"stocks": stocks, "from_date": from_date, "to_date": to_date}

    # ...........................................................
    saleableStock, stockOut, availableStock = 0, 0, 0
    totalStock = stocks.filter(
        stock_type__in=['Sellable', 'Return']).values(
        'stock_type').annotate(count=Sum('stock_in'))

    outStock = stocks.filter(
        stock_type='stock_out').values(
        'stock_type').annotate(count=Sum('stock_out'))
    # ...........................................................
    category = find_product_category(product)
    context["category"] = category
    for item in stocks:
        if item.stock_type == "stock_out":
            item.__dict__["order_number"] = item.reference
        else:
            item.__dict__["order_number"] = False
    # ...........................................................

    for item in totalStock:
        if item["stock_type"] == "Sellable":
            saleableStock += item["count"]
        elif item["stock_type"] == "Return":
            saleableStock += item["count"]

    if len(outStock) > 0:
        for item in outStock:
            stockOut += item["count"]
        availableStock = saleableStock - stockOut
    else:
        availableStock = saleableStock

    if "export" in request.POST:
        response = HttpResponse(content_type='text/csv')
        response['Content-Disposition'] = 'attachment;filename=stock_register_detail.csv'
        final_stock_list = []
        for x in stocks:
            stock_action = "Stock Out" if x.stock_type == 'stock_out' else "Stock Imported"
            x_order_number = x.order_number if x.order_number else "NA"
            if x.stock_type == 'Sellable':
                x_stock_type = "Sellable"
            elif x.stock_type == 'NonSellable':
                x_stock_type = "Non Sellable"
            else:
                x_stock_type = ""

            final_dict = {
                "Date": x.created_on.date(), "Stock In": x.stock_in,
                "Stock Out": x.stock_out, "Stock Type": x_stock_type,
                "Stock Action": stock_action, "Order No": x_order_number
            }
            final_stock_list.append(final_dict)
        keys = ['Date', 'Stock In', 'Stock Out', 'Stock Type', 'Stock Action', 'Order No']
        writer = csv.DictWriter(response, keys)
        writer.writeheader()
        writer.writerows(final_stock_list)
        return response

    context["stock_in"] = saleableStock if saleableStock > 0 else 0
    context["stock_out"] = stockOut if stockOut > 0 else 0
    context["stock_available"] = availableStock if availableStock > 0 else 0
    # ...........................................................
    return render(request, 'warehouse/stock_detail.html', context=context)


def find_product_category(data):
    get_product = []
    category = ProductCategoryMaster.objects.filter(is_active=True, is_deleted=False)
    for PC in category:
        if data.product_category is not None:
            pass
        else:
            continue
        CID = data.product_category_id
        PCM = ProductCategoryMaster.objects.get(pk=CID)
        get_product.append(PCM.category_name)
        PID = PCM.parent
        try:
            PCM = ProductCategoryMaster.objects.get(pk=PID)
        except Exception:
            PCM = ''
        if PCM != '':
            get_product.append(PCM.category_name)
            PID = PCM.parent
        try:
            PCM = ProductCategoryMaster.objects.get(pk=PID)
        except Exception:
            PCM = ''
        if PCM != '':
            get_product.append(PCM.category_name)
            PID = PCM.parent
        try:
            PCM = ProductCategoryMaster.objects.get(pk=PID)
        except Exception:
            PCM = ''
        if PCM != '':
            get_product.append(PCM.category_name)
            PID = PCM.parent
        try:
            PCM1 = ProductCategoryMaster.objects.get(pk=PID)
        except Exception:
            PCM1 = ''
        if PCM1 != '':
            get_product.append(PCM1.category_name)
            PID = PCM1.parent
        break
    get_product.reverse()
    return get_product


def StockOrderDetail(request, orderID=None):
    context = get_order_details(orderID, request)
    return render(request, 'admin/stock_order_detail.html', context=context)


def get_order_details(orderID, request):
    orderHead = OrderHead.objects.get(order_number=orderID, is_active=True, is_deleted=False)
    dealerProfile = DealerProfile.objects.get(dealer=orderHead.dealer, is_active=True, is_deleted=False)
    dealerBusiness = DealerBusinessMaster.objects.get(
        brand=dealerProfile.brand, dealer=dealerProfile.dealer, is_active=True, is_deleted=False)
    context = get_order(request, orderID)
    context['dealer'] = dealerProfile
    context['dbm'] = dealerBusiness
    context['cc_status'] = False
    context["outstanding"] = orderHead.payable_amount
    context["order_id"] = orderHead.order_number
    context["order_date"] = orderHead.order_date
    context['dms_number'] = orderHead.DMS_order_number
    context["history"] = False
    context['on_hold_flag'] = False
    context['on_hold_date'] = ""

    try:
        orderPlaced = OrderEventHistory.objects.get(order_id=orderHead, order_status='Dispatched')
        today = datetime.date.today()
        order_placed_day = orderPlaced.created_on
        diff = today - order_placed_day.date()
        days = diff.days

        credit_days = dealerBusiness.credit_days
        remaining_days = credit_days - days
        if remaining_days < 0:
            remaining_days = 0
        dealerBusiness.credit_days = remaining_days
    except Exception:
        pass
    # ........................
    try:
        sr_status = ['OrderPlaced']
        order_status_detail = OrderEventHistory.objects.filter(
            order_id=orderHead, order_status=orderHead.status).last()

        """ On Hold Order details"""
        if orderHead.status == "OnHold":
            context["on_hold_flag"] = True
            context["on_hold_date"] = order_status_detail.created_on

        if orderHead.status in status_in.SRStatus():
            status_detail = {
                "head_name": "SR Name", "head_number": "SR Number",
                "date": order_status_detail.created_on,
                "name": "", "number": ""
            }
            dealerProfile = DealerProfile.objects.filter(dealer=orderHead.dealer).first()
            srDealer = SRDealer.objects.filter(dealer_id=dealerProfile).first()
            status_detail["name"] = "{0} {1}".format(srDealer.sr_id.first_name, srDealer.sr_id.last_name)
            status_detail["number"] = srDealer.sr_id.mobile
        elif orderHead.status in status_in.ManagerStatus():
            status_detail = {
                "head_name": "WH User Name", "head_number": "WH User Number",
                "date": order_status_detail.created_on,
                "name": "{0} {1}".format(orderHead.warehouse_manager.first_name,
                                         orderHead.warehouse_manager.last_name),
                "number": orderHead.warehouse_manager.mobile}
        context["status_detail"] = status_detail
    except Exception:
        pass
    # ........................

    if orderHead.status in status_in.DetailStatus():
        after_scheme, total = get_scheme(request, orderID)
        context["total"] = total
        context["details"] = after_scheme

        credit_notes = get_credit_notes(orderID, request)
        if credit_notes['grand_total'] > 0:
            context["credit_notes"] = credit_notes
            context['cc_status'] = True

    if orderHead.status in status_in.ShowHistoryStatus():
        history = OrderEventHistory.objects.filter(order_id=orderHead).order_by('pk')
        context["orderHistory"] = history
        warehouse = orderHead.warehouse_code if orderHead.warehouse_code else ""

        if orderHead.warehouse_manager:
            warehouse_manager = "{0} {1}".format(
                orderHead.warehouse_manager.first_name, orderHead.warehouse_manager.last_name)
        else:
            warehouse_manager = ""
        context["warehouse"] = warehouse
        context["manager"] = warehouse_manager
        context["history"] = True
    return context


def stock_out(request, orderHead):
    orderDetails = OrderDetails.objects.filter(
        order_id=orderHead, is_active=True, is_deleted=False)

    try:
        assign_warehouse = WarehouseMaster.objects.get(
            warehouse_code=orderHead.warehouse_code,
            is_active=True, is_deleted=False)
    except Exception:
        assign_warehouse = None
    for order in orderDetails:
        try:
            product = ProductMaster.objects.get(
                sku_code=order.sku_code, is_active=True, is_deleted=False)

            stockOut = StockRegister()
            stockOut.product = product
            stockOut.stock_in = float(0)
            stockOut.reference = orderHead.order_number
            stockOut.stock_out = float(order.quantity)
            stockOut.stock_type = 'stock_out'
            stockOut.created_by = request.user
            stockOut.modified_by = request.user
            stockOut.save()

            if assign_warehouse is not None:
                stockOut.warehouse = assign_warehouse
                stockOut.save()
        except Exception:
            continue
    return True


# def create_ledger(request, orderHead, amount, discount, note=''):
#     # create head
#     """ =============== dealer-ledger-start =============== """
#     try:
#         prevDealerLedger = Ledger.objects.filter(
#             ledger_type="Dealer", dealer=orderHead.dealer,
#             is_active=True, is_deleted=False).latest('pk')
#     except Ledger.DoesNotExist:
#         prevDealerLedger = None
#     # head will also passed here
#     currentDealerLedger = Ledger(
#         ledger_type="Dealer", remarks="Dealer Order {}".format(note),
#         order_number=orderHead.order_number, dealer=orderHead.dealer,
#         voucher_number="dealer-voucher", created_by=request.user
#     )
#     if prevDealerLedger is None:
#         balance = 0
#     else:
#         balance = prevDealerLedger.balance
#
#     currentDealerLedger.dr_amount = float(amount)
#     currentDealerLedger.balance = float(balance) + float(amount)
#     currentDealerLedger.save()
#
#     """ =============== dealer-ledger-end =============== """
#     """ =============== sales-ledger-start ============== """
#     try:
#         prevSalesLedger = Ledger.objects.filter(
#             ledger_type="Sales", dealer=orderHead.dealer,
#             is_active=True, is_deleted=False).latest('pk')
#     except Ledger.DoesNotExist:
#         prevSalesLedger = None
#     # padd ledger head
#     currentSalesLedger = Ledger(
#         ledger_type="Sales", remarks="Dealer Order",
#         order_number=orderHead.order_number, dealer=orderHead.dealer,
#         voucher_number="dealer-voucher", created_by=request.user
#     )
#     if prevSalesLedger is None:
#         balance = 0
#     else:
#         balance = prevSalesLedger.balance
#     currentSalesLedger.cr_amount = float(amount)
#     currentSalesLedger.balance = float(balance) - float(amount)
#     currentSalesLedger.save()
#
#     """ =============== sales-ledger-end =============== """
#     """ ========== First-Ledger-Complete =============== """
#
#     if discount > 0:
#
#         """ =============== credit-note-ledger-start =============== """
#         create_discount_dealer_ledger(request, orderHead, discount, note)
#         """ =============== dealer-ledger-end =============== """
#         """ ========== Second-Ledger-Complete =============== """
#     return True
#
#
# def create_transaction_ledger(request, orderHead, amount, discount, note=''):
#     """ =============== dealer-ledger-start =============== """
#     try:
#         prevDealerLedger = Ledger.objects.filter(
#             ledger_type="DealerTransaction", dealer=orderHead.dealer,
#             is_active=True, is_deleted=False).latest('pk')
#     except Ledger.DoesNotExist:
#         prevDealerLedger = None
#
#     currentDealerLedger = Ledger(
#         ledger_type="DealerTransaction",
#         remarks="Dealer Order {} {}".format(orderHead.DMS_order_number, note),
#         order_number=orderHead.order_number, dealer=orderHead.dealer,
#         voucher_number="dealer-voucher", created_by=request.user
#     )
#     if prevDealerLedger is None:
#         balance = 0
#     else:
#         balance = prevDealerLedger.balance
#     # ...............................................
#
#     # if balance < 0:
#         # dealerProfile = DealerProfile.objects.get(
#         #     dealer=orderHead.dealer, is_active=True, is_deleted=False)
#         # cdSlab = CashDiscountSlabsMaster.objects.get(
#         #     pk=dealerProfile.cd_slab_code.pk,
#         #     is_active=True, is_deleted=False)
#         # slabDiscount = cdSlab.cd1
#         # temp_balance = float(-1) * float(balance)
#         # if amount > temp_balance:
#         #     cash_discount = float(temp_balance) * float(slabDiscount) / 100
#         # else:
#         #     cash_discount = float(amount) * float(slabDiscount) / 100
#         # create_credit_note_dealer_ledger(request, orderHead, cash_discount)
#     # .....................
#     currentDealerLedger.dr_amount = float(amount) - float(discount)
#     currentDealerLedger.balance = float(balance) + (float(amount) - float(discount))
#     currentDealerLedger.save()
#
#     """ =============== dealer-ledger-end =============== """
#     """ =============== sales-ledger-start ============== """
#     # try:
#     #     prevSalesLedger = Ledger.objects.filter(
#     #         ledger_type="Sales", dealer=orderHead.dealer,
#     #         is_active=True, is_deleted=False).latest('pk')
#     # except Ledger.DoesNotExist:
#     #     prevSalesLedger = None
#     #
#     # currentSalesLedger = Ledger(
#     #     ledger_type="Sales", remarks="Dealer Order",
#     #     order_number=orderHead.order_number, dealer=orderHead.dealer,
#     #     voucher_number="dealer-voucher", created_by=request.user
#     # )
#     # if prevSalesLedger is None:
#     #     balance = 0
#     # else:
#     #     balance = prevSalesLedger.balance
#     # currentSalesLedger.cr_amount = float(amount)
#     # currentSalesLedger.balance = float(balance) - float(amount)
#     # currentSalesLedger.save()
#     #
#     # """ =============== sales-ledger-end =============== """
#     # """ ========== First-Ledger-Complete =============== """
#     #
#     # if discount > 0:
#     #     """ =============== credit-note-ledger-start =============== """
#     #     create_discount_dealer_ledger(request, orderHead, discount)
#     #     """ =============== dealer-ledger-end =============== """
#     #     """ ========== Second-Ledger-Complete =============== """
#     return True
#
#
# def create_transaction_payment_ledger(request, dealer, payment, discount):
#     """ =============== dealer-ledger-start =============== """
#     try:
#         prevDealerLedger = Ledger.objects.filter(
#             ledger_type="DealerTransaction", dealer=dealer,
#             is_active=True, is_deleted=False).latest('pk')
#     except Ledger.DoesNotExist:
#         prevDealerLedger = None
#
#     currentDealerLedger = Ledger(
#         ledger_type="DealerTransaction", remarks="Dealer Payment", dealer=dealer,
#         voucher_number="dealer-payment-voucher", created_by=request.user
#     )
#     if prevDealerLedger is None:
#         balance = 0
#     else:
#         balance = prevDealerLedger.balance
#     # ...............................................
#
#     # .....................
#     currentDealerLedger.cr_amount = float(payment)
#     currentDealerLedger.balance = float(balance) - float(payment)
#     currentDealerLedger.save()
#     latest_balance = currentDealerLedger.balance
#     currentDealerDiscountLedger = Ledger(
#         ledger_type="DealerTransaction", remarks="Credit Note Type 2", dealer=dealer,
#         voucher_number="dealer-CN2", created_by=request.user
#     )
#     currentDealerDiscountLedger.cr_amount = float(discount)
#     currentDealerDiscountLedger.balance = float(latest_balance) - float(discount)
#     currentDealerDiscountLedger.save()
#
#     """ =============== dealer-ledger-end =============== """
#     """ =============== sales-ledger-start ============== """
#     # try:
#     #     prevSalesLedger = Ledger.objects.filter(
#     #         ledger_type="Sales", dealer=orderHead.dealer,
#     #         is_active=True, is_deleted=False).latest('pk')
#     # except Ledger.DoesNotExist:
#     #     prevSalesLedger = None
#     #
#     # currentSalesLedger = Ledger(
#     #     ledger_type="Sales", remarks="Dealer Order",
#     #     order_number=orderHead.order_number, dealer=orderHead.dealer,
#     #     voucher_number="dealer-voucher", created_by=request.user
#     # )
#     # if prevSalesLedger is None:
#     #     balance = 0
#     # else:
#     #     balance = prevSalesLedger.balance
#     # currentSalesLedger.cr_amount = float(amount)
#     # currentSalesLedger.balance = float(balance) - float(amount)
#     # currentSalesLedger.save()
#     #
#     # """ =============== sales-ledger-end =============== """
#     # """ ========== First-Ledger-Complete =============== """
#     #
#     # if discount > 0:
#     #     """ =============== credit-note-ledger-start =============== """
#     #     create_discount_dealer_ledger(request, orderHead, discount)
#     #     """ =============== dealer-ledger-end =============== """
#     #     """ ========== Second-Ledger-Complete =============== """
#     return True


def AccountLedger(request, ledger):
    search_dealer = request.GET.get('order', None)
    search_dealer = 0 if search_dealer is None or search_dealer == '' else search_dealer

    from_date = request.GET.get('from_date', None)
    to_date = request.GET.get('to_date', None)
    payment_flag = True
    ledgers = Ledger.objects.filter(
        ledger_type=ledger, is_active=True, is_deleted=False)

    groups = request.user.groups.all()
    groups = [group.name for group in groups]
    if 'SR' in groups:
        payment_flag = False
        srDealer = SRDealer.objects.filter(sr_id=request.user)
        srDealers = []
        for item in srDealer:
            if item.dealer_id is not None:
                if item.dealer_id.dealer is not None:
                    srDealers.append(item.dealer_id.dealer)
        ledgers = ledgers.filter(dealer__in=srDealers)

    my_dealers = []
    dealers = [i.dealer for i in ledgers]
    dealers = list(set(dealers))
    if from_date is not None and from_date != "" and to_date is not None and to_date != "":
        ledgers = ledgers.filter(
            created_on__date__gte=from_date, created_on__date__lte=to_date)
    for _x in dealers:
        try:
            dealer = DealerProfile.objects.get(dealer=_x)
            dict_dealer = {"id": dealer.dealer.pk, "company": dealer.company_name}
            my_dealers.append(dict_dealer)
        except DealerProfile.DoesNotExist:
            continue
    if search_dealer != 0:
        ledgers = ledgers.filter(dealer__in=[int(search_dealer)])

    dealers = [i.dealer for i in ledgers]
    dealers = list(set(dealers))
    ledgerList = []
    download_data = []
    total_dealer, total_amount = 0, 0
    for dealer in dealers:
        lastLedger = ledgers.filter(dealer=dealer).latest('pk')
        ledgerList.append(lastLedger)

    for item in ledgerList:
        total_dealer += 1
        total_amount += item.balance
        try:
            dealer = DealerProfile.objects.get(dealer=item.dealer)
            try:
                payment = Payment.objects.filter(dealer=item.dealer).latest('pk')
            except Payment.DoesNotExist:
                payment = None
            payment_date = ""
            if payment:
                if payment.payment_date is not None:
                    payment_date = payment.payment_date
            item.__dict__["payment_date"] = payment_date
        except DealerProfile.DoesNotExist:
            continue
        item.__dict__["dp"] = dealer

        download_dict = {
            "company": dealer.company_name,
            "dealer": "{} {}".format(dealer.dealer.first_name, dealer.dealer.last_name),
            "mobile": dealer.dealer.mobile,
            "balance": item.balance,
            "last_transaction": item.created_on.date(),
            "payment_date": item.payment_date
        }
        download_data.append(download_dict)

    total = {"dealer": total_dealer, "amount": total_amount}

    """Export the data to a CSV file"""
    if 'export' in request.GET:
        keys = ['company', 'dealer', 'mobile', 'balance', 'last_transaction', 'payment_date']
        full_file_name = ledger.lower()
        response = download_data_in_csv(full_file_name, keys, download_data)
        return response

    paginator = Paginator(ledgerList, 10)
    try:
        page_index = request.GET.get("page", 1)
        data_list = paginator.page(page_index)
    except PageNotAnInteger:
        data_list = paginator.page(1)
    except EmptyPage:
        data_list = paginator.page(paginator.num_pages)
    context = {
        "ledgers": data_list,
        'ledger_type': ledger,
        'total': total,
        "dealers": my_dealers,
        "x_dealer": int(search_dealer),
        "from_date": from_date,
        "to_date": to_date,
        "payment_flag":payment_flag
    }
    return render(request, 'admin/ledger.html', context=context)

import re
def LedgerDetail(request, ledger, dealer_id):
    from_date = request.GET.get('from_date', None)
    to_date = request.GET.get('to_date', None)
    ledgers = Ledger.objects.filter(
        ledger_type=ledger, dealer=dealer_id,
        is_active=True, is_deleted=False).order_by('pk')
    if from_date is not None and from_date != "" and to_date is not None and to_date != "":
        ledgers = ledgers.filter(
            created_on__date__gte=from_date, created_on__date__lte=to_date)
    dealerProfile, amount = None, 0
    export_data = []
    for item in ledgers:
        try:
            dealer = DealerProfile.objects.get(dealer=item.dealer)
        except DealerProfile.DoesNotExists:
            continue
        item.__dict__["payment_date"] = ""
        # import pdb
        # pdb.set_trace()
        if ledger == 'Bank':
            payment = Payment.objects.filter(dealer=item.dealer)
            for pay in payment:
                if pay.created_on.date() == item.created_on.date() :
                    payment_date = pay.payment_date
                    item.__dict__["payment_date"] = payment_date
        # if payment:
        #     if payment.payment_date is not None:
        #         payment_date = payment.payment_date
        # item.__dict__["payment_date"] = payment_date

        item.__dict__["dp"] = dealer
        dealerProfile = dealer
        amount = item.balance

        if item.order_number:
            narration = "Against Order # {}, {}".format(item.order_number, item.remarks)

            try:
                orderHead = OrderHead.objects.get(
                    order_number=item.order_number,
                    is_active=True, is_deleted=False)
                item.__dict__["dms"] = orderHead.DMS_order_number
                narration = "Against Order # {}, DMS # {}, {}".format(item.order_number, item.dms, item.remarks)
            except:
                item.__dict__["dms"] = ""
                narration = "Against Order # {}, DMS # {}, {}".format(item.order_number, item.dms, item.remarks)
        else:
            narration = item.remarks
        export_dict = {
            "narration": narration,
            "dr": item.dr_amount,
            "cr": item.cr_amount,
            "balance": item.balance,
            "transaction_date": item.created_on.date(),
            "payment_date": item.payment_date,

        }
        export_data.append(export_dict)

    if dealerProfile is None:
        dealerProfile = DealerProfile.objects.get(dealer=dealer_id, is_active=True, is_deleted=False)

    dealerBusiness = DealerBusinessMaster.objects.get(
        brand=dealerProfile.brand, dealer=dealerProfile.dealer,
        is_active=True, is_deleted=False)

    detail = {"dp": dealerProfile, "dbm": dealerBusiness, 'amount': amount}
    activeOrder = OrderHead.objects.filter(
        dealer_id=dealer_id, status__in=status_in.OutstandingStatus(),
        is_active=True, is_deleted=False).aggregate(total=Sum('payable_amount'))
    detail["outstanding"] = activeOrder['total']

    """Export the data to a CSV file"""
    if 'export' in request.GET:
        keys = ['narration', 'dr', 'cr', 'balance', 'transaction_date']
        full_file_name = ledger.lower()
        response = download_data_in_csv(full_file_name, keys, export_data)
        return response

    paginator = Paginator(ledgers, 15)
    try:
        page_index = request.GET.get("page", 1)
        data_list = paginator.page(page_index)
    except PageNotAnInteger:
        data_list = paginator.page(1)
    except EmptyPage:
        data_list = paginator.page(paginator.num_pages)
    x_ledger = re.sub('(?!^)([A-Z][a-z]+)', r' \1', ledger).split()
    ledger = "{0} {1}".format(x_ledger[0], x_ledger[1]) if len(x_ledger) > 1 else ledger
    context = {
        "ledgers": data_list,
        'ledger_type': ledger,
        "dealer": detail,
        "from_date": from_date,
        "to_date": to_date,
        "pay_flag": ledger
    }
    return render(request, 'admin/ledger_detail.html', context=context)


def AddPayment(request):
    try:
        form_class = forms.PaymentForm
        dealers = DealerProfile.objects.filter(
            is_active=True, is_deleted=False).order_by('company_name')
        # dealers = User.objects.filter(groups__name='DEL', is_active=True).order_by('-pk')
        if request.method == "POST":
            form_class = forms.PaymentForm(request.POST)
            if form_class.is_valid():
                payment = form_class.save(commit=False)
                payment.created_by = request.user
                payment.save()
                """ Create Bank Ledger """
                # create_bank_dealer_ledger(request, payment.dealer, payment.amount)
                create_bank_ledger(request, payment.dealer, payment.amount)

                """ Payment Done """
                payment_done(request, payment.dealer, payment.amount)
                messages.add_message(request, messages.SUCCESS, 'Payment successfully updated!')
            else:
                messages.add_message(request, messages.ERROR, 'Payment failed!')
    except Exception as error:
        messages.add_message(request, messages.ERROR, str(error))
    context = {'form': form_class, "dealers": dealers}
    return render(request, 'admin/payment.html', context=context)


# def create_bank_dealer_ledger(request, dealer, amount):
#     """ =============== bank-ledger-start =============== """
#     try:
#         prevBankLedger = Ledger.objects.filter(
#             ledger_type="Bank", dealer=dealer,
#             is_active=True, is_deleted=False).latest('pk')
#     except Ledger.DoesNotExist:
#         prevBankLedger = None
#
#     currentBankLedger = Ledger(
#         ledger_type="Bank", remarks="Payment Received", dealer=dealer,
#         voucher_number="bank-voucher", created_by=request.user
#     )
#     if prevBankLedger is None:
#         balance = 0
#     else:
#         balance = prevBankLedger.balance
#     currentBankLedger.dr_amount = float(amount)
#     currentBankLedger.balance = float(balance) + float(amount)
#     currentBankLedger.save()
#
#     """ =============== bank-ledger-end =============== """
#     """ ============= dealer-ledger-start ============= """
#     try:
#         prevDealerLedger = Ledger.objects.filter(
#             ledger_type="Dealer", dealer=dealer,
#             is_active=True, is_deleted=False).latest('pk')
#     except Ledger.DoesNotExist:
#         prevDealerLedger = None
#
#     currentDealerLedger = Ledger(
#         ledger_type="Dealer", remarks="Payment Received", dealer=dealer,
#         voucher_number="bank-voucher", created_by=request.user
#     )
#     if prevDealerLedger is None:
#         balance = 0
#     else:
#         balance = prevDealerLedger.balance
#     currentDealerLedger.cr_amount = float(amount)
#     currentDealerLedger.balance = float(balance) - float(amount)
#     currentDealerLedger.save()
#
#     """ =============== dealer-ledger-end =============== """
#     """ =========== Third-Ledger-Complete =============== """
#     return True
#
#
# def create_discount_dealer_ledger(request, orderHead, discount, note=None):
#     """ =============== discount-ledger-start =============== """
#     # create head with type credit note
#     try:
#         prevDiscountLedger = Ledger.objects.filter(
#             ledger_type="Discount", dealer=orderHead.dealer,
#             is_active=True, is_deleted=False).latest('pk')
#     except Ledger.DoesNotExist:
#         prevDiscountLedger = None
#     if note:
#         message = note
#     else:
#         message = "Credit Note-Type 1"
#     # pass head
#     currentDiscountLedger = Ledger(
#         ledger_type="Discount", remarks=message,
#         order_number=orderHead.order_number, dealer=orderHead.dealer,
#         voucher_number="discount-voucher", created_by=request.user
#     )
#     if prevDiscountLedger is None:
#         balance = 0
#     else:
#         balance = prevDiscountLedger.balance
#     currentDiscountLedger.dr_amount = float(discount)
#     currentDiscountLedger.balance = float(balance) + float(discount)
#     currentDiscountLedger.save()
#
#     """ =============== discount-ledger-end =============== """
#     """ =============== dealer-ledger-start =============== """
#     try:
#         prevDealerLedger = Ledger.objects.filter(
#             ledger_type="Dealer", dealer=orderHead.dealer,
#             is_active=True, is_deleted=False).latest('pk')
#     except Ledger.DoesNotExist:
#         prevDealerLedger = None
#     # pass head
#     currentDealerLedger = Ledger(
#         ledger_type="Dealer", remarks=message,
#         order_number=orderHead.order_number, dealer=orderHead.dealer,
#         voucher_number="credit-voucher", created_by=request.user
#     )
#     if prevDealerLedger is None:
#         balance = 0
#     else:
#         balance = prevDealerLedger.balance
#     currentDealerLedger.cr_amount = float(discount)
#     currentDealerLedger.balance = float(balance) - float(discount)
#     currentDealerLedger.save()
#     """ =============== dealer-ledger-end =============== """
#     # create unadjusted account with cn1
#     return True
#
#
# def create_credit_note_dealer_ledger(request, orderHead, discount, remark=None):
#     """ =============== credit-note-ledger-start =============== """
#     if remark is None:
#         remark = 'CD(Credit Note-Type 2)'
#     try:
#         prevCreditNoteLedger = Ledger.objects.filter(
#             ledger_type="Discount", dealer=orderHead.dealer,
#             is_active=True, is_deleted=False).latest('pk')
#     except Ledger.DoesNotExist:
#         prevCreditNoteLedger = None
#
#     currentCreditNoteLedger = Ledger(
#         ledger_type="Discount", remarks=remark,
#         order_number=orderHead.order_number, dealer=orderHead.dealer,
#         voucher_number="credit-voucher", created_by=request.user
#     )
#     if prevCreditNoteLedger is None:
#         balance = 0
#     else:
#         balance = prevCreditNoteLedger.balance
#     currentCreditNoteLedger.dr_amount = float(discount)
#     currentCreditNoteLedger.balance = float(balance) + float(discount)
#     currentCreditNoteLedger.save()
#
#     """ =============== credit-note-ledger-end =============== """
#     """ =============== dealer-ledger-start =============== """
#     try:
#         prevDealerLedger = Ledger.objects.filter(
#             ledger_type="Dealer", dealer=orderHead.dealer,
#             is_active=True, is_deleted=False).latest('pk')
#     except Ledger.DoesNotExist:
#         prevDealerLedger = None
#
#     currentDealerLedger = Ledger(
#         ledger_type="Dealer", remarks=remark,
#         order_number=orderHead.order_number, dealer=orderHead.dealer,
#         voucher_number="credit-voucher", created_by=request.user
#     )
#     if prevDealerLedger is None:
#         balance = 0
#     else:
#         balance = prevDealerLedger.balance
#     currentDealerLedger.cr_amount = float(discount)
#     currentDealerLedger.balance = float(balance) - float(discount)
#     currentDealerLedger.save()
#     """ =============== dealer-ledger-end =============== """
#     return True
#
#
# def create_bank_ledger(request, dealer, amount):
#     """ =============== bank-ledger-start =============== """
#     try:
#         prevBankLedger = Ledger.objects.filter(
#             ledger_type="Bank", dealer=dealer,
#             is_active=True, is_deleted=False).latest('pk')
#     except Ledger.DoesNotExist:
#         prevBankLedger = None
#
#     currentBankLedger = Ledger(
#         ledger_type="Bank", remarks="Payment Received", dealer=dealer,
#         voucher_number="bank-voucher", created_by=request.user
#     )
#     if prevBankLedger is None:
#         balance = 0
#     else:
#         balance = prevBankLedger.balance
#     currentBankLedger.dr_amount = float(amount)
#     currentBankLedger.balance = float(balance) + float(amount)
#     currentBankLedger.save()
#     """ =============== bank-ledger-end =============== """
#     return True
#
#
# def create_dealer_ledger(request, dealer, clientPayment):
#     """ =============== dealer-ledger-start =============== """
#     try:
#         prevDealerLedger = Ledger.objects.filter(
#             ledger_type="Dealer", dealer=dealer,
#             is_active=True, is_deleted=False).latest('pk')
#     except Ledger.DoesNotExist:
#         prevDealerLedger = None
#
#     currentDealerLedger = Ledger(
#         ledger_type="Dealer", remarks="Payment Received", dealer=dealer,
#         voucher_number="discount-voucher", created_by=request.user
#     )
#     if prevDealerLedger is None:
#         balance = 0
#     else:
#         balance = prevDealerLedger.balance
#     currentDealerLedger.cr_amount = float(clientPayment)
#     currentDealerLedger.balance = float(balance) - float(clientPayment)
#     currentDealerLedger.save()
#     """ =============== dealer-ledger-end =============== """
#     return True
#
#
# def create_advance_dealer_ledger(request, dealer, clientPayment):
#     """ =============== dealer-ledger-start =============== """
#     try:
#         prevDealerLedger = Ledger.objects.filter(
#             ledger_type="Dealer", dealer=dealer,
#             is_active=True, is_deleted=False).latest('pk')
#     except Ledger.DoesNotExist:
#         prevDealerLedger = None
#
#     currentDealerLedger = Ledger(
#         ledger_type="Dealer", remarks="Advance Payment Received", dealer=dealer,
#         voucher_number="discount-voucher", created_by=request.user
#     )
#     if prevDealerLedger is None:
#         balance = 0
#     else:
#         balance = prevDealerLedger.balance
#     currentDealerLedger.cr_amount = float(clientPayment)
#     currentDealerLedger.balance = float(balance) - float(clientPayment)
#     currentDealerLedger.save()
#     """ =============== dealer-ledger-end =============== """
#     return True
#
#
# def create_advance_dealer_transaction_ledger(request, dealer, clientPayment):
#     """ =============== dealer-ledger-start =============== """
#     try:
#         prevDealerLedger = Ledger.objects.filter(
#             ledger_type="DealerTransaction", dealer=dealer,
#             is_active=True, is_deleted=False).latest('pk')
#     except Ledger.DoesNotExist:
#         prevDealerLedger = None
#
#     currentDealerLedger = Ledger(
#         ledger_type="DealerTransaction", remarks="Advance Payment Received", dealer=dealer,
#         voucher_number="discount-voucher", created_by=request.user
#     )
#     if prevDealerLedger is None:
#         balance = 0
#     else:
#         balance = prevDealerLedger.balance
#     currentDealerLedger.cr_amount = float(clientPayment)
#     currentDealerLedger.balance = float(balance) - float(clientPayment)
#     currentDealerLedger.save()
#     """ =============== dealer-ledger-end =============== """
#     return True


def payment_done(request, dealer, clientPayment):
    orderHead = OrderHead.objects.filter(
        status__in=['Dispatched', 'Delivered', 'PartialDispatched'],
        dealer=dealer, is_active=True, is_deleted=False).order_by('pk')
    discount = 0, 0
    dealerledgerFlag = True
    order = None
    create_unadjusted_ledger(request, dealer, clientPayment, True)
    for index, order in enumerate(orderHead):
        dealerProfile = DealerProfile.objects.get(dealer=dealer, is_active=True, is_deleted=False)

        orderPlaced = OrderEventHistory.objects.filter(
            order_id=order, order_status='Dispatched').first()
        if orderPlaced:
            # today = datetime.date.today()
            order_placed_day = orderPlaced.created_on

            payment_date = request.POST.get('payment_date')
            payment_date = parse_date(payment_date)
            diff = payment_date - order_placed_day.date()

            no_of_days = diff.days
            orderPayableAmount = order.payable_amount

            cdSlab = CashDiscountSlabsMaster.objects.get(
                pk=dealerProfile.cd_slab_code.pk,
                is_active=True, is_deleted=False)
            if no_of_days <= cdSlab.days_1:
                slabDiscount = cdSlab.cd1
            elif no_of_days <= cdSlab.days_2:
                slabDiscount = cdSlab.cd2
            elif no_of_days <= cdSlab.days_3:
                slabDiscount = cdSlab.cd3
            elif no_of_days <= cdSlab.days_4:
                slabDiscount = cdSlab.cd4
            else:
                slabDiscount = float(0)

            if clientPayment > orderPayableAmount:
                discount = float(orderPayableAmount) * float(slabDiscount) / 100
                order.payable_amount = float(order.payable_amount) - float(discount)
                order.save()
                orderPayableAmount = order.payable_amount
                note = 'against order# {}'.format(order.order_id)
                create_unadjusted_ledger(request, dealer, discount, False, note)
                if orderPayableAmount <= 0:
                    pass
                else:
                    create_credit_note_dealer_ledger(request, order, discount)
                    create_dealer_ledger(request, dealer, orderPayableAmount)

                    clientPayment = float(clientPayment) - float(orderPayableAmount)

                    order_payment_done = order.payable_amount
                    order.status = 'PaymentDone'
                    order.payable_amount = float(0)
                    order.save()


                    # ......... Order History .........
                    orderHistory = OrderEventHistory(
                        order_id=order,
                        order_status=order.status,
                        created_by=request.user
                    )
                    orderHistory.save()

                    # .................................
                    try:
                        # send_email_or_sms_both("Payment_Updated_Received", order)
                        send_sms_or_email(request, order, payment=float(order_payment_done))
                    except Exception:
                        pass
                    # .................................
            else:
                discount = float(clientPayment) * float(slabDiscount) / 100
                note = 'against order# {}'.format(order.order_id)
                create_unadjusted_ledger(request, dealer, discount, False, note)
                create_credit_note_dealer_ledger(request, order, discount)
                create_dealer_ledger(request, dealer, clientPayment)
                order.payable_amount = float(order.payable_amount) - float(discount)
                order.save()
                order.payable_amount = float(order.payable_amount) - float(clientPayment)
                if order.payable_amount <= 0:
                    order.status = 'PaymentDone'
                    order.payable_amount = float(0)
                    # ......... Order History .........
                    orderHistory = OrderEventHistory(
                        order_id=order,
                        order_status='PaymentDone',
                        created_by=request.user
                    )
                    orderHistory.save()
                order.save()
                clientPayment = 0
                break
    if clientPayment > 0:
        """ =============== dealer-ledger-start =============== """
        create_advance_dealer_ledger(request, dealer, clientPayment)

        """ =============== dealer-ledger-end =============== """
    return True


# def create_advance_amount_ledger(request, dealer, clientPayment, flag=None):
#     """ =============== dealer-ledger-start =============== """
#     try:
#         prevDealerLedger = Ledger.objects.filter(
#             ledger_type="DealerOrders", dealer=dealer,
#             is_active=True, is_deleted=False).latest('pk')
#     except Ledger.DoesNotExist:
#         prevDealerLedger = None
#     if flag:
#         currentDealerLedger = Ledger(
#             ledger_type="DealerOrders", remarks="Advance Payment Settled", dealer=dealer,
#             voucher_number="discount-voucher", created_by=request.user
#         )
#         if prevDealerLedger is None:
#             balance = 0
#         else:
#             balance = prevDealerLedger.balance
#         currentDealerLedger.cr_amount = float(clientPayment)
#         currentDealerLedger.balance = float(balance) - float(clientPayment)
#         currentDealerLedger.save()
#     else:
#         currentDealerLedger = Ledger(
#             ledger_type="DealerOrders", remarks="Advance Payment Received", dealer=dealer,
#             voucher_number="discount-voucher", created_by=request.user
#         )
#         if prevDealerLedger is None:
#             balance = 0
#         else:
#             balance = prevDealerLedger.balance
#         currentDealerLedger.dr_amount = float(clientPayment)
#         currentDealerLedger.balance = float(balance) + float(clientPayment)
#         currentDealerLedger.save()
#     """ =============== dealer-ledger-end =============== """
#     return True



def FinanceDashboard(request):
    activeDealer = User.objects.filter(groups__name="DEL", is_active=True).values_list('pk')
    activeDealerCount = User.objects.filter(groups__name="DEL", is_active=True).count()
    orderHeads = OrderHead.objects.filter(
        status__in=status_in.FianceDashboardStatus(), dealer__in=activeDealer,
        is_active=True, is_deleted=False).order_by('-pk')
    total_outstanding_amount = OrderHead.objects.filter(
        dealer_id__in=activeDealer, status__in=['Dispatched','Delivered','PartialDispatched', ],
        is_active=True, is_deleted=False).aggregate(Sum('payable_amount'))
    if total_outstanding_amount['payable_amount__sum'] is None:
        total_outstanding_amount['payable_amount__sum'] = 0.00
    collection = Payment.objects.filter(dealer_id__in=activeDealer).aggregate(Sum('amount'))
    # ................................................

    special = SpacialDiscount.objects.filter(
        status="Pending", is_active=True,
        is_deleted=False).order_by('-pk')
    special = [item.order_number for item in special]
    specialList = orderHeads.filter(order_number__in=special)
    remainList = orderHeads.filter().exclude(order_number__in=special)
    for order in specialList:
        if order.status == "Dispatched":
            status = OrderEventHistory.objects.filter(
                order_id_id=order.pk, order_status="Dispatched").first()
            if status:
                order.__dict__["Dispatched_Date"] = status.created_on
            else:
                order.__dict__["Dispatched_Date"] = ""
        else:
            order.__dict__['Dispatched_Date'] = ""

        try:
            _x_dealer = DealerProfile.objects.get(
                dealer=order.dealer, is_active=True, is_deleted=False)
            order.__dict__['company'] = _x_dealer.company_name
        except:
            order.__dict__['company'] = ""
    # ................................................
    for order in remainList:
        if order.status == "Dispatched":
            status = OrderEventHistory.objects.filter(
                order_id_id=order.pk, order_status="Dispatched").first()
            if status:
                order.__dict__["Dispatched_Date"] = status.created_on
            else:
                order.__dict__["Dispatched_Date"] = ""
        else:
            order.__dict__['Dispatched_Date'] = ""

        try:
            _x_dealer = DealerProfile.objects.get(
                dealer=order.dealer, is_active=True, is_deleted=False)
            order.__dict__['company'] = _x_dealer.company_name
        except:
            order.__dict__['company'] = ""
    ledgers = SpecialCDType.objects.filter(
        is_active=True, is_deleted=False, status='Pending').order_by('-pk')
    if len(ledgers) > 0:
        discountList = []
    else:
        discountList = {}
    for item in ledgers:
        try:
            dealer = DealerProfile.objects.get(dealer=item.dealer)
        except DealerProfile.DoesNotExist:
            continue
        if item.status == 'Pending':
            discount_dict = {
                "series_no": item.series_no,
                "type": item.get_discount_type_display(),
                "value": item.amount,
                "status": item.status,
                "id": item.pk,
                "company": dealer.company_name,
                "discount_date": item.created_on.date(),
                "remarks": item.remarks
            }
            discountList.append(discount_dict)
    debit_note = DebitNote.objects.filter(
        is_active=True, is_deleted=False).order_by('-pk')
    if len(debit_note) > 0:
        debitList = []
    else:
        debitList = {}
    for item in debit_note:
        try:
            dealer = DealerProfile.objects.get(dealer=item.dealer)
        except DealerProfile.DoesNotExist:
            continue
        if item.status == 'Pending':
            discount_dict = {
                # "series_no": item.series_no,
                "type": item.get_type_display(),
                "value": item.amount,
                "status": item.status,
                "id": item.pk,
                "company": dealer.company_name,
                "date": item.created_on.date(),
                "remarks": item.reference
            }
            debitList.append(discount_dict)
    context = {
        "orderList": remainList, "specialList": discountList,"debitList": debitList,
        'outstanding': total_outstanding_amount['payable_amount__sum'],
        'collection': collection['amount__sum'], 'totaldealer': activeDealerCount
    }
    return render(request, 'finance/dashboard.html', context)


def finance_all_order(request, statusobj=None):

    activeDealer = User.objects.filter(groups__name="DEL", is_active=True).values_list('pk')
    activeDealerCount = User.objects.filter(groups__name="DEL", is_active=True).count()
    orderhead = OrderHead.objects.filter(
        status__in=status_in.FianceDashboardStatus(), dealer__in=activeDealer,
        is_active=True, is_deleted=False).order_by('-pk')

    orderheadnull = OrderHead.objects.filter(
        status__in=status_in.FianceDashboardStatus(), dealer__in=activeDealer,
        is_active=True, order_date=None, is_deleted=False)
    if statusobj == 'Pending':
        special = SpacialDiscount.objects.filter(
            status="Pending", is_active=True,
            is_deleted=False).order_by('-pk')
        special = [item.order_number for item in special]
        orderhead = orderhead.filter(order_number__in=special)
        orderheadnull = orderheadnull.filter(order_number__in=special)
    else:
        pass
    orderhead=list(chain(orderhead,orderheadnull))
    if orderhead:
        for order in orderhead:
            try:
                status = OrderEventHistory.objects.get(order_id_id=order.pk, order_status='Dispatched')
                order.__dict__['Dispatched_Date'] = status.created_on
            except:
                order.__dict__['Dispatched_Date'] = '---'

            try:
                _x_dealer = DealerProfile.objects.get(
                    dealer=order.dealer, is_active=True, is_deleted=False)
                order.__dict__['company'] = _x_dealer.company_name
            except:
                order.__dict__['company'] = ""

    if request.method == 'POST':
        if 'export' in request.POST:
            result = []
            for order in orderhead:
                get_order={}
                _x_payable_amount = order.payable_amount if order.payable_amount > 0 else ""
                get_order['Order No']=order.order_number
                get_order['DMS No'] = order.DMS_order_number
                if isinstance(order.order_date, datetime.date):
                    get_order['Order Date'] = order.order_date.date()
                else:
                    get_order['Order Date'] = order.order_date
                if isinstance(order.Dispatched_Date, datetime.date):
                    get_order['Dispatched Date'] = order.Dispatched_Date.date()
                else:
                    get_order['Dispatched Date'] = order.Dispatched_Date
                get_order['Invoice Value']=order.total_amount
                get_order['Discount'] = order.discount_amount
                get_order['Payable'] = _x_payable_amount
                get_order['Status']=order.status
                get_order['SR'] = str(order.sr_id.first_name + ' ' + order.sr_id.last_name)
                get_order['Dealer']=str(order.dealer.user_code+'.'+order.company)

                result.append(get_order)
            response = HttpResponse(content_type='text/csv')
            response['Content-Disposition'] = 'attachment;filename=warehouse_user_order_list.csv'
            keys = ['Order No', 'DMS No','Order Date', 'Dispatched Date', 'Invoice Value',
                    'Discount','Payable','Status', 'SR','Dealer']
            writer = csv.DictWriter(response,keys)
            writer.writeheader()
            writer.writerows(result)
            return response
    context = {"orderhead": orderhead }
    return render(request, 'warehouse/all_orders.html', context=context)


def OrderDetail(request, orderID):
    context = get_order_details(orderID, request)
    if request.method == "POST":
        orderHead = OrderHead.objects.get(
            order_number=orderID, is_active=True, is_deleted=False)

        groups = request.user.groups.all()
        groups = [group.name for group in groups]
        if 'BusinessHead' in groups:
            status_of_discount = "Approved"
        else:
            status_of_discount = "Pending"

        discount = SpacialDiscount(
            order_number=orderID,
            comment=request.POST['comment'],
            amount=float(request.POST['amount']),
            created_by=request.user,
            dealer=orderHead.dealer,
            status=status_of_discount
        )
        discount.save()
        messages.add_message(request, messages.SUCCESS, 'Special discount sent for approval!')

    spacial = SpacialDiscount.objects.filter(
        order_number=orderID,
        is_active=True, is_deleted=False).first()
    if spacial:
        context["status"] = spacial.status
        context["discount"] = spacial.amount
    else:
        context["status"] = "Draft"
    return render(request, 'finance/order_detail.html', context=context)


def ApproveReject(request, status, orderID):
    if status == 1:
        set_status = 'Rejected'
        message_text = 'Request Successfully Rejected!'
    elif status == 2:
        set_status = 'Approved'
        message_text = 'Request Successfully Approved!'

    try:
        orderHead = OrderHead.objects.get(order_number=orderID)
        spacial = SpacialDiscount.objects.get(
            order_number=orderID, status="Pending",
            is_active=True, is_deleted=False)
        spacial.status = set_status
        spacial.save()

        """ =============== special-discount-ledger-start =============== """
        remark = "CD(Credit Note-Type 3)"
        create_credit_note_dealer_ledger(request, orderHead, spacial.amount, remark=remark)
        orderHead.payable_amount = float(orderHead.payable_amount) - float(spacial.amount)
        orderHead.save()

        messages.add_message(request, messages.SUCCESS, message_text)
    except:
        messages.add_message(request, messages.ERROR, 'Oops something went wrong please try again.')
    return redirect("/user/order-detail/{0}".format(orderID))


def CollectionLedger(request):
    from_date = request.GET.get('from_date', None)
    to_date = request.GET.get('to_date', None)
    if from_date is None and to_date is None:
        from_date = str(datetime.datetime.today().date())
        to_date = str(datetime.datetime.today().date())
    # ledgers = Ledger.objects.filter(
    #     ledger_type='Bank', created_on__gte=from_date,
    #     created_on__date__lte=to_date, is_active=True, is_deleted=False)
    # .....................
    ledgers = Payment.objects.filter(
        created_on__gte=from_date,
        created_on__date__lte=to_date,
        is_active=True, is_deleted=False).values(
        'dealer').annotate(balance=Sum('amount'))
    # .....................
    totalAmount = 0
    for item in ledgers:
        try:
            # dealer = DealerProfile.objects.get(dealer=item.dealer)
            # item.__dict__["dp"] = dealer
            dealer = DealerProfile.objects.get(dealer__id=item['dealer'])
            item["dp"] = dealer
            totalAmount += item['balance']
            item["balance"] = item['balance']

            try:
                latest_payment = Payment.objects.filter(
                    dealer__id=item['dealer'],
                    is_active=True, is_deleted=False).latest('pk')
                item["created_on"] = latest_payment.created_on
            except Ledger.DoesNotExist:
                continue
        except DealerProfile.DoesNotExist:
            continue

    context = {'amount': totalAmount, "from_date": from_date, "to_date": to_date}
    paginator = Paginator(ledgers, 10)
    try:
        page_index = request.GET.get("page", 1)
        data_list = paginator.page(page_index)
    except PageNotAnInteger:
        data_list = paginator.page(1)
    except EmptyPage:
        data_list = paginator.page(paginator.num_pages)
    context["ledgers"] = data_list
    return render(request, 'admin/collection_ledger.html', context=context)


def GlobalSearch(request):
    itemList = []
    search = request.GET.get('search')

    try:
        dealers = DealerProfile.objects.filter(
            Q(primary_contact__icontains=search) |
            Q(dealer__first_name__icontains=search) |
            Q(dealer_code__icontains=search) |
            Q(dealer__email__icontains=search) |
            Q(owner_name__icontains=search) |
            Q(email__icontains=search) |
            Q(company_name__icontains=search)
        )
        for item in dealers:
            full_name = ""
            if item.dealer:
                full_name = "{0} {1}".format(item.dealer.first_name, item.dealer.last_name)
            item_dict = {
                'label': full_name, 'id': item.dealer_id, 'del': item.dealer_code,
                'email': item.dealer.email, 'mobile': item.dealer.mobile,
                'address': item.company_address, 'company': item.company_name
            }
            itemList.append(item_dict)
    except Exception:
        pass

    if len(itemList) <= 0:
        item_dict = {
            'label': "No search results.", 'id': "0", 'del': "",
            'email': "", 'mobile': "",
            'address': "", 'company': ""
        }
        itemList.append(item_dict)
    return JsonResponse({"data": itemList})


# ============ HTML TO PDF ============


def DownloadPDF(request):
    try:
        template = get_template("report/pdf_report.html")
        data = ""
        context = {"data": data}
        html = template.render(context)
        options = {
            'page-size': 'Letter',
            'encoding': "UTF-8",
        }
        pdf = pdfkit.from_string(html, False, options)
        response = HttpResponse(pdf, content_type='application/pdf')
        response['Content-Disposition'] = 'attachment; filename="dealer.pdf"'
        return response
    except Exception as error:
        print(error)


def SendSMS(request):
    context = {}
    if request.method == "POST":
        message = request.POST.get('message')
        mobile = request.POST.get('to')

        querystring = {
            "method": settings.METHOD, "api_key": settings.API_KEY,
            "sender": settings.SENDER, "to": mobile, "message": message
        }
        response = requests.request("GET", settings.SEND_URL, params=querystring)
        response = json.loads(response.text)
        if response["status"] == 'OK':
            messages.add_message(request, messages.SUCCESS, str(response["message"]))
        elif response["status"] == 'A404':
            messages.add_message(request, messages.ERROR, str(response["message"]))
        else:
            messages.add_message(request, messages.INFO, str(response["message"]))
        context = {"message": message, "to": mobile}
    return render(request, 'admin/sms.html', context=context)


def AllUsers(request):
    group = request.GET.get('list', None)
    if group:
        allUsers = User.objects.filter(groups__name=group, is_active=True).order_by('-pk')
    else:
        allUsers = User.objects.exclude(groups__name='DEL').order_by('-pk')
    searchValue = ""

    if request.method == 'POST':
        if "btnSearch" in request.POST:
            searchValue = request.POST.get('search', None)
            if searchValue is not None:
                allUsers = allUsers.filter(
                    Q(first_name__icontains=searchValue) |
                    Q(last_name__icontains=searchValue) |
                    Q(email__icontains=searchValue) |
                    Q(groups__name__icontains=searchValue) |
                    Q(mobile__icontains=searchValue))

        try:
            custom_sr_id = request.POST.get("new_sr_id", None)
            old_sr_id = request.POST.get("old_sr_id", None)

            if custom_sr_id is not None and old_sr_id is not None:
                assigned_sr = SRDealer.objects.filter(sr_id=old_sr_id)
                sr_updated_flag = False
                for prev_sr in assigned_sr:
                    prev_sr.sr_id.is_active = False
                    prev_sr.sr_id.save()

                    prev_sr.sr_id_id = custom_sr_id
                    prev_sr.save()
                    sr_updated_flag = True
                if sr_updated_flag:
                    messages.add_message(request, messages.SUCCESS, "SR Changed Successfully!")
        except Exception as error:
            print(error)

    user_role = ""
    for user in allUsers:
        groups = user.groups.all()
        groups = [group.name for group in groups]
        if 'SR' in groups:
            user.__dict__["role"] = "SR"
            user.__dict__["userRole"] = "SR"
            user_role = "SR"
        elif 'WHS' in groups:
            user.__dict__["role"] = "Warehouse Admin"
            user.__dict__["userRole"] = "WHS"
            user_role = "WHS"
        elif 'WHSUSER' in groups:
            user.__dict__["role"] = "Warehouse Manager"
            user.__dict__["userRole"] = "WHSUSER"
            user_role = "WHSUSER"
            user.__dict__["warehouse"] = check_assigned_warehouse(user)
        elif 'Finance' in groups:
            user.__dict__["role"] = "Finance"
            user.__dict__["userRole"] = "Finance"
            user_role = "Finance"
        elif 'BusinessHead' in groups:
            user.__dict__["role"] = "Business Head"
            user.__dict__["userRole"] = "BusinessHead"
            user_role = "BusinessHead"
        else:
            user.__dict__["role"] = "Admin"
            user.__dict__["userRole"] = "Admin"
            user_role = "Admin"

    paginator = Paginator(allUsers, 10)
    try:
        page_index = request.GET.get("page", 1)
        data_list = paginator.page(page_index)
    except PageNotAnInteger:
        data_list = paginator.page(1)
    except EmptyPage:
        data_list = paginator.page(paginator.num_pages)

    context = {
        'data_list': data_list,
        'totalUsers': len(allUsers),
        'searchValue': searchValue,
        "user_role": user_role,
        "group": group
    }
    if group == "SR":
        list_of_active_sr = User.objects.filter(
            groups__name="SR", is_active=True).order_by('-pk')
        list_of_sr_detail = []
        for _x in list_of_active_sr:
            first_name = "{0}".format(_x.first_name) if _x.first_name else ""
            last_name = " {0}".format(_x.last_name) if _x.last_name else ""
            mobile = ", {0}".format(_x.mobile) if _x.mobile else ""
            code = " ({0})".format(_x.user_code) if _x.user_code else ""
            full_name = "{0}{1}{2}{3}".format(first_name, last_name, mobile, code)
            list_of_sr_detail.append({"id": _x.pk, "detail": full_name})
        context["sr_list"] = list_of_sr_detail
    return render(request, 'admin/user_list.html', context=context)


def check_assigned_warehouse(user_instance):
    try:
        assigned_warehouses = WarehouseAdmin.objects.filter(
            user=user_instance, is_active=True, is_deleted=False)
        assigned_warehouses = [x.warehouse.warehouse_code for x in assigned_warehouses]
        warehouses_code = ", ".join(map(str, assigned_warehouses))
    except Exception:
        warehouses_code = ""
    return warehouses_code


def reset_password(request):
    objdata = ''
    msg = 'testing'
    if request.method == 'POST':
        objdata = 'reset'
        try:
            userobj = User.objects.get(is_active=True, email=request.POST.get('email'))
        except Exception:
            userobj = ''
        if userobj != '':
            currentsite = get_current_site(request)
            context = {
                'user': userobj,
                'domain': currentsite.domain,
                'uid': urlsafe_base64_encode(force_bytes(userobj.pk)).decode(),
                'token': account_activation_token.make_token(userobj)
            }
            messagesobj = render_to_string('admin/email_confirmation.html', context=context)
            mail_subject = 'Password reset on {}.'.format(currentsite.domain)

            """ need to changed """
            to_email = [userobj.email]
            email = EmailMultiAlternatives(mail_subject, messagesobj, settings.DEFAULT_FROM_EMAIL, to=to_email)
            email.send()
        else:
            objdata = ''
            messages.add_message(request, messages.ERROR, 'Your Email is Invalid. Please Enter Registered Email!!!')
    context = {'objdata': objdata, 'msgobj': msg}
    return render(request, 'admin/reset_password.html', context=context)


def activate(request, uid, token):
    msg = ""
    if request.method == 'POST':
        try:
            uid = force_text(urlsafe_base64_decode(uid))
            user = User.objects.get(pk=uid, is_active=True)
        except(TypeError, ValueError, OverflowError, User.DoesNotExist):
            user = None
        if user is not None and account_activation_token.check_token(user, token):
            user.set_password(request.POST.get('pass'))
            user.save()
            logout(request)
            return redirect('/accounts/login/')
        else:
            messages.add_message(request, messages.ERROR, 'Reset password link is invalid!!')
    return render(request, 'admin/reset_password.html', {'msgobj': msg})


def Profile(request):
    try:
        userobj = User.objects.get(pk=request.user.id, is_active=True)
    except Exception as e:
        userobj = ''
    if userobj != '':
        try:
            SR = User.objects.get(pk=request.user.id, groups__name='SR', is_active=True)
        except Exception as e:
            SR = ''
            try:
                WHS = User.objects.get(pk=request.user.id, groups__name='WHS', is_active=True)
            except Exception as e:
                WHS = ''
                try:
                    WHSUSER = User.objects.get(pk=request.user.id, groups__name='WHSUSER', is_active=True)
                except Exception:
                    WHSUSER = ''
        if request.user.is_superuser:
            userobj.__dict__['role'] = 'Admin'
        elif SR != '':
            userobj.__dict__['role'] = 'SR'
        elif WHS != '':
            userobj.__dict__['role'] = 'Warehouse Admin'
        elif WHSUSER != '':
            userobj.__dict__['role'] = 'Warehouse User'
        userobj.__dict__['name'] = userobj.first_name+' '+userobj.last_name
        
    if request.method == 'POST':
        if 'img_update' in request.POST:
            doctype = request.POST.get('doctype')
            try:
                userImg = request.FILES['Myfiler']
            except Exception:
                userImg = ''
            if userImg != '':
                fs = FileSystemStorage(location=os.path.join(settings.MEDIA_ROOT, 'avatars/'), base_url=urljoin(settings.MEDIA_URL, 'avatars/'))
                filename = fs.save(userImg.name, userImg)
                uploaded_file_url = ('avatars/'+filename)
                imguser = User.objects.get(pk=request.user.id, is_active=True)
                imguser.avatar = uploaded_file_url
                imguser.save()
                return redirect('/user/profile')
        if 'btnAddMore' in request.POST:
            try:
                objuser = User.objects.get(is_active=True, pk=request.user.id)
            except Exception:
                objuser = ''
            if objuser != '':
                objuser.first_name = request.POST.get('fname')
                objuser.last_name = request.POST.get('lname')
                objuser.mobile = request.POST.get('mob')
                objuser.address_line1 = request.POST.get('address')
                objuser.city = request.POST.get('city')
                objuser.state = request.POST.get('state')
                objuser.zip_code = request.POST.get('zipcode')
                objuser.save()
                messages.add_message(request, messages.SUCCESS, 'Profile Update Sucessfully!!!')
                return redirect('/user/profile')
    context = {'userobj': userobj}
    return render(request, 'admin/profile.html', context)


def SRReport(request):
    from_date = request.GET.get('from_date', None)
    to_date = request.GET.get('to_date', None)
    if from_date is None or to_date is None:
        from_date = str(datetime.datetime.today().date().replace(day=1))
        to_date = str(datetime.datetime.today().date())
    orderHistory = OrderEventHistory.objects.filter(
        order_status__in=status_in.SRReportStatus(),
        created_on__date__gte=from_date, created_on__date__lte=to_date)
    orderHistory = [item.order_id_id for item in orderHistory]

    orders = OrderHead.objects.filter(
        order_id__in=orderHistory, is_active=True, is_deleted=False)
    orderList = []
    for order in orders:
        # try:
        #     paymentHistory = OrderEventHistory.objects.get(
        #         order_id=order, order_status='PaymentDone')
        #     payment_receive_date = paymentHistory.created_on.date()
        # except OrderEventHistory.DoesNotExist:
        #     payment_receive_date = ""

        try:
            placedHistory = OrderEventHistory.objects.filter(
                order_id=order, order_status='OrderPlaced').first()
            order_placed_date = placedHistory.created_on.date()
        except OrderEventHistory.DoesNotExist:
            order_placed_date = ""

        try:
            dealerProfile = DealerProfile.objects.get(
                dealer=order.dealer, is_active=True, is_deleted=False)
            # dealer_company_name = dealerProfile.company_name
            dealer_profile_id = dealerProfile.dealer_id
        except DealerProfile.DoesNotExist:
            # dealer_company_name = ""
            dealer_profile_id = ""

        # row_dict = {
        #     "sr_code": order.sr_id.user_code, "sr_contact": order.sr_id.mobile,
        #     "sr_name": "{} {}".format(order.sr_id.first_name, order.sr_id.last_name),
        #     "dealer_code": order.dealer.user_code, "dealer_contact": order.dealer.mobile,
        #     "dealer_name": "{} {}".format(order.dealer.first_name, order.dealer.last_name),
        #     "dealer": "{}.{}".format(order.dealer.user_code, order.dealer.first_name),
        #     "company_name": dealer_company_name, "profile_id": dealer_profile_id,
        #     "order_date": order_placed_date, "payment_date": payment_receive_date,
        #     "invoice": order.total_amount, "pending_amount": order.payable_amount
        # }
        row_dict = {
            "sr_name": "{} {}".format(order.sr_id.first_name, order.sr_id.last_name),
            "sr_contact": order.sr_id.mobile, "dealer_contact": order.dealer.mobile,
            "dealer": "{}.{}".format(order.dealer.user_code, order.dealer.first_name),
            "profile_id": dealer_profile_id, "order_date": order_placed_date,
            "invoice": order.total_amount, "pending_amount": order.payable_amount
        }
        orderList.append(row_dict)

    """Export the data to a CSV file"""
    if 'export' in request.GET:
        for item in orderList:
            del item["profile_id"]
        keys = ['sr_name', 'sr_contact', 'dealer', 'dealer_contact',
                'order_date', 'invoice', 'pending_amount']
        response = download_data_in_csv('sr_report', keys, orderList)
        return response
    context = {"from_date": from_date, "to_date": to_date}
    paginator = Paginator(orderList, 10)
    try:
        page_index = request.GET.get("page", 1)
        data_list = paginator.page(page_index)
    except PageNotAnInteger:
        data_list = paginator.page(1)
    except EmptyPage:
        data_list = paginator.page(paginator.num_pages)
    context["orders"] = data_list
    return render(request, 'reports/sr.html', context=context)


def TopSR(request):
    oType = request.GET.get('order', 0)
    from_date = request.GET.get('from_date', None)
    to_date = request.GET.get('to_date', None)
    if oType == '2':
        myStatus = status_in.TopSalesStatus()
    elif oType == '3':
        myStatus = status_in.TopOutstandingStatus()
    else:
        myStatus = status_in.TopCollectionStatus()

    if from_date is None or to_date is None:
        from_date = str(datetime.datetime.today().date().replace(day=1))
        to_date = str(datetime.datetime.today().date())
    orderHistory = OrderEventHistory.objects.filter(
        order_status__in=myStatus,
        created_on__date__gte=from_date, created_on__date__lte=to_date)
    orderHistory = [item.order_id_id for item in orderHistory]
    orderHistory = list(set(orderHistory))

    orders = OrderHead.objects.filter(
        order_id__in=orderHistory, status__in=myStatus,
        is_active=True, is_deleted=False).values('sr_id').annotate(
        invoice=Sum('total_amount'), payable=Sum('payable_amount')).order_by('-sr_id')
    topList = []
    total_amount = 0
    message_for, file_name = "", ""
    for order in orders:
        orderHeads = OrderHead.objects.filter(
            sr_id=order["sr_id"], status__in=myStatus,
            is_active=True, is_deleted=False)
        order_numbers = [order.order_number for order in orderHeads]
        dealers = [order.dealer for order in orderHeads]

        try:
            payment = Payment.objects.filter(
                dealer_id__in=dealers, is_active=True,
                is_deleted=False).aggregate(amount=Sum('amount'))
            total_collection = float(payment["amount"])
        except Exception:
            total_collection = float(0)

        srDetail = orderHeads.first().sr_id
        sr_name = "{} {}".format(srDetail.first_name, srDetail.last_name)

        row_dict = {
            "sr_contact": srDetail.mobile, "sr_name": sr_name,
            "invoice_value": order["invoice"], "payable": order["payable"],
            "outstanding": order["payable"], "orders": order_numbers
        }
        if oType == '2':
            message_for = "Total Sales"
            file_name = "sales"
            total_amount = float(total_amount) + float(order["invoice"])
        elif oType == '3':
            message_for = "Total Outstanding"
            total_amount = float(total_amount) + float(order["payable"])
            file_name = "outstanding"
        else:
            row_dict["collection"] = total_collection
            message_for = "Total Collection"
            file_name = "collections"
            total_amount = float(total_amount) + float(total_collection)
        topList.append(row_dict)

    if oType == '2':
        topList = sorted(topList, key=lambda k: k['invoice_value'], reverse=True)
    elif oType == '3':
        topList = sorted(topList, key=lambda k: k['outstanding'], reverse=True)
    else:
        topList = sorted(topList, key=lambda k: k['collection'], reverse=True)

    """Export the data to a CSV file"""
    if 'export' in request.GET:
        for item in topList:
            if oType == '1' or oType == 0:
                del item["payable"]
                del item["outstanding"]
        keys = ['sr_name', 'sr_contact', 'orders', 'invoice_value']
        if oType == '1' or oType == 0:
            keys.append('collection')
        else:
            keys.append('payable')
            keys.append('outstanding')
        full_file_name = 'top_sr_' + file_name
        response = download_data_in_csv(full_file_name, keys, topList)
        return response
    context = {
        "from_date": from_date,
        "to_date": to_date,
        "total_amount": total_amount,
        "msg": message_for,
        "total_dealer": len(topList),
        "oType": int(oType)
    }
    paginator = Paginator(topList, 10)
    try:
        page_index = request.GET.get("page", 1)
        data_list = paginator.page(page_index)
    except PageNotAnInteger:
        data_list = paginator.page(1)
    except EmptyPage:
        data_list = paginator.page(paginator.num_pages)
    context["orders"] = data_list
    return render(request, 'reports/top_sr.html', context=context)


def TopDealer(request):
    oType = request.GET.get('order', 0)
    from_date = request.GET.get('from_date', None)
    to_date = request.GET.get('to_date', None)
    if oType == '2':
        myStatus = status_in.TopSalesStatus()
    elif oType == '3':
        myStatus = status_in.TopOutstandingStatus()
    else:
        myStatus = status_in.TopCollectionStatus()

    if from_date is None or to_date is None:
        from_date = str(datetime.datetime.today().date().replace(day=1))
        to_date = str(datetime.datetime.today().date())
    orderHistory = OrderEventHistory.objects.filter(
        order_status__in=myStatus,
        created_on__date__gte=from_date, created_on__date__lte=to_date)
    orderHistory = [item.order_id_id for item in orderHistory]

    orders = OrderHead.objects.filter(
        order_id__in=orderHistory, status__in=myStatus,
        is_active=True, is_deleted=False).values(
        'dealer').annotate(invoice=Sum('total_amount'), payable=Sum(
        'payable_amount')).order_by('-dealer')
    topList = []
    total_amount = 0
    message_for, file_name = "", ""
    for order in orders:
        orderHeads = OrderHead.objects.filter(
            dealer=order["dealer"], status__in=myStatus,
            is_active=True, is_deleted=False)
        order_numbers = [order.order_number for order in orderHeads]

        try:
            payment = Payment.objects.filter(
                dealer_id=order["dealer"], is_active=True,
                is_deleted=False).aggregate(amount=Sum('amount'))
            total_collection = float(payment["amount"])
        except Exception:
            total_collection = float(0)

        try:
            dealerProfile = DealerProfile.objects.get(
                dealer_id=order["dealer"], is_active=True, is_deleted=False)
            dealer_profile_id = dealerProfile.dealer_id
            dealer = "{}.{}".format(dealerProfile.dealer.user_code, dealerProfile.dealer.first_name)
            dealer_contact = dealerProfile.dealer.mobile
        except DealerProfile.DoesNotExist:
            dealer_profile_id = dealer = dealer_contact = ""

        try:
            srDealer = SRDealer.objects.get(dealer_id=dealerProfile)
            sr_name = "{} {}".format(srDealer.sr_id.first_name, srDealer.sr_id.last_name)
            sr_contact = srDealer.sr_id.mobile
        except Exception:
            sr_name = sr_contact = ""

        row_dict = {
            "sr_contact": sr_contact, "sr_name": sr_name, "dealer_contact": dealer_contact,
            "dealer": dealer, "profile_id": dealer_profile_id, "invoice_value": order["invoice"],
            "payable": order["payable"], "outstanding": order["payable"],
            "orders": order_numbers
        }
        if oType == '2':
            message_for = "Total Sales"
            file_name = "sales"
            total_amount = float(total_amount) + float(order["invoice"])
        elif oType == '3':
            message_for = "Total Outstanding"
            total_amount = float(total_amount) + float(order["payable"])
            file_name = "outstanding"
        else:
            row_dict["collection"] = total_collection
            message_for = "Total Collection"
            file_name = "collections"
            total_amount = float(total_amount) + float(total_collection)
        topList.append(row_dict)

    if oType == '2':
        topList = sorted(topList, key=lambda k: k['invoice_value'], reverse=True)
    elif oType == '3':
        topList = sorted(topList, key=lambda k: k['outstanding'], reverse=True)
    else:
        topList = sorted(topList, key=lambda k: k['collection'], reverse=True)

    """Export the data to a CSV file"""
    if 'export' in request.GET:
        for item in topList:
            del item["profile_id"]
            if oType == '1' or oType == 0:
                del item["payable"]
                del item["outstanding"]
        keys = ['dealer', 'dealer_contact', 'sr_name', 'sr_contact', 'orders', 'invoice_value']
        if oType == '1' or oType == 0:
            keys.append('collection')
        else:
            keys.append('payable')
            keys.append('outstanding')
        full_file_name = 'top_dealer_' + file_name
        response = download_data_in_csv(full_file_name, keys, topList)
        return response
    context = {
        "from_date": from_date,
        "to_date": to_date,
        "total_amount": total_amount,
        "msg": message_for,
        "total_dealer": len(topList),
        "oType": int(oType)
    }
    paginator = Paginator(topList, 10)
    try:
        page_index = request.GET.get("page", 1)
        data_list = paginator.page(page_index)
    except PageNotAnInteger:
        data_list = paginator.page(1)
    except EmptyPage:
        data_list = paginator.page(paginator.num_pages)
    context["orders"] = data_list
    return render(request, 'reports/top_dealer.html', context=context)


def BestCollection(request):
    from_date = request.GET.get('from_date', None)
    to_date = request.GET.get('to_date', None)
    if from_date is None or to_date is None:
        from_date = str(datetime.datetime.today().date().replace(day=1))
        to_date = str(datetime.datetime.today().date())
    payments = Payment.objects.filter(
        created_on__date__gte=from_date, created_on__date__lte=to_date,
        is_active=True, is_deleted=False).values(
        'dealer').annotate(amount=Sum('amount'))
    collectionList = []
    for payment in payments:
        try:
            dealerProfile = DealerProfile.objects.get(
                dealer=payment["dealer"], is_active=True, is_deleted=False)
            dealer_profile_id = dealerProfile.dealer_id
            dealer = "{}.{}".format(dealerProfile.dealer.user_code, dealerProfile.dealer.first_name)
            dealer_contact = dealerProfile.dealer.mobile
        except DealerProfile.DoesNotExist:
            dealer_profile_id = dealer = dealer_contact = ""

        try:
            srDealer = SRDealer.objects.get(dealer_id=dealerProfile)
            sr_name = "{} {}".format(srDealer.sr_id.first_name, srDealer.sr_id.last_name)
            sr_contact = srDealer.sr_id.mobile
        except Exception:
            sr_name = sr_contact = ""

        row_dict = {
            "sr_name": sr_name, "sr_contact": sr_contact, "dealer_contact": dealer_contact,
            "dealer": dealer, "profile_id": dealer_profile_id, "amount": payment["amount"]
        }
        collectionList.append(row_dict)
    collectionList = sorted(collectionList, key=lambda k: k['amount'], reverse=True)

    """Export the data to a CSV file"""
    if 'export' in request.GET:
        for item in collectionList:
            del item["profile_id"]
        if 'export' in request.GET:
            keys = ['dealer', 'dealer_contact', 'sr_name', 'sr_contact', 'amount']
            response = download_data_in_csv('best_collection', keys, collectionList)
            return response
    context = {"from_date": from_date, "to_date": to_date}
    paginator = Paginator(collectionList, 10)
    try:
        page_index = request.GET.get("page", 1)
        data_list = paginator.page(page_index)
    except PageNotAnInteger:
        data_list = paginator.page(1)
    except EmptyPage:
        data_list = paginator.page(paginator.num_pages)
    context["collections"] = data_list
    return render(request, 'reports/best_collection.html', context=context)


def SKUSale(request):
    from_date = request.GET.get('from_date', None)
    to_date = request.GET.get('to_date', None)
    if from_date is None or to_date is None:
        from_date = str(datetime.datetime.today().date().replace(day=1))
        to_date = str(datetime.datetime.today().date())
    orderHistory = OrderEventHistory.objects.filter(
        order_status='OrderPlaced', created_on__date__gte=from_date,
        created_on__date__lte=to_date)
    orderHistory = [item.order_id for item in orderHistory]
    orderDetails = OrderDetails.objects.filter(
        order_id__in=orderHistory, is_active=True,
        is_deleted=False).values('sku_code').annotate(
        quantity=Sum('quantity'))
    skuList = sorted(orderDetails, key=lambda k: k['quantity'], reverse=True)

    """Export the data to a CSV file"""
    if 'export' in request.GET:
        keys = ['sku_code', 'quantity']
        response = download_data_in_csv('sku_sale', keys, skuList)
        return response
    context = {"from_date": from_date, "to_date": to_date}
    paginator = Paginator(skuList, 10)
    try:
        page_index = request.GET.get("page", 1)
        data_list = paginator.page(page_index)
    except PageNotAnInteger:
        data_list = paginator.page(1)
    except EmptyPage:
        data_list = paginator.page(paginator.num_pages)
    context["sales"] = data_list
    return render(request, 'reports/sku_sale.html', context=context)


def download_data_in_csv(filename, keys, dataList):
    """Export the data to a CSV file"""
    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = 'attachment;filename='+filename+'.csv'
    writer = csv.DictWriter(response, keys)
    writer.writeheader()
    writer.writerows(dataList)
    return response


# def MinStock(request):
#     products = ProductMaster.objects.filter(is_active=True, is_deleted=False)
#     skuList = []
#     for product in products:
#
#         sellable = StockRegister.objects.filter(
#             product=product, stock_type="Sellable",
#             is_active=True, is_deleted=False).aggregate(total=Sum('stock_in'))
#
#         try:
#             minStockLevel = StockLevelMaster.objects.filter(
#                 product=product, is_active=True, is_deleted=False).first()
#             minStockLevel = 0 if minStockLevel is None else minStockLevel.min_stock_level
#             minStockLevel = int(minStockLevel)
#         except Exception:
#             minStockLevel = int(0)
#
#         stockOut = StockRegister.objects.filter(
#             product=product, stock_type="stock_out",
#             is_active=True, is_deleted=False).aggregate(total=Sum('stock_out'))
#         if sellable["total"] is not None:
#             totalStockOut = 0 if stockOut["total"] is None else stockOut["total"]
#             totalStock = int(sellable["total"]) - int(totalStockOut)
#             if minStockLevel >= totalStock:
#                 category_name = reverse_category(product.product_category_id, categoryList=[])
#                 color = "danger" if minStockLevel > totalStock else "warning"
#                 sku_dict = {
#                     "category": category_name,
#                     "sku_code": product.sku_code,
#                     "available_stock": totalStock,
#                     "min_stock": minStockLevel,
#                     "class": color
#                 }
#                 skuList.append(sku_dict)
#
#     """Export the data to a CSV file"""
#     if 'export' in request.GET:
#         for item in skuList:
#             del item["class"]
#         keys = ['category', 'sku_code', 'available_stock', 'min_stock']
#         response = download_data_in_csv('min_stock', keys, skuList)
#         return response
#     context = {"products": skuList}
#     return render(request, "warehouse/min_stock.html", context=context)


def MinStock(request):
    stocks, search = [], ""
    products = ProductMaster.objects.filter(is_active=True, is_deleted=False)
    stock_level = StockLevelMaster.objects.filter(
        product__in=products, is_active=True, is_deleted=False)

    groups = [group.name for group in request.user.groups.all()]
    warehouses = WarehouseMaster.objects.filter(is_active=True, is_deleted=False)
    if 'WHSUSER' in groups:
        warehouseAssigned = WarehouseAdmin.objects.filter(
            is_active=True, is_deleted=False, user_id=request.user.id).values_list('warehouse_id')
        warehouseList = warehouses.filter(warehouse_code__in=warehouseAssigned)
        stock_level = stock_level.filter(warehouse__in=warehouseList)
    for stock in stock_level:
        product = stock.product
        list_dict = {
            "sku_code": product.sku_code, "total": 0, "sale": 0,
            "available": 0, "stock_out": 0, "non_sale": 0,
            "blocked": 0, "remaining": 0, "minimum": 0, "required": 0
        }

        try:
            stock_level = StockLevelMaster.objects.filter(
                product=product, is_active=True, is_deleted=False).first()
            list_dict["minimum"] = int(stock_level.min_stock_level)
        except:
            list_dict["minimum"] = int(0)

        totalStock = StockRegister.objects.filter(
            product=product, stock_type__in=['Sellable', 'NonSellable', 'Return'],
            is_active=True, is_deleted=False).values('stock_type').annotate(
            count=Sum('stock_in'))

        outStock = StockRegister.objects.filter(
            product=product, stock_type='stock_out',
            is_active=True, is_deleted=False).values(
            'stock_type').annotate(count=Sum('stock_out'))

        if len(totalStock) > 0:
            category = find_product_category(product)
            list_dict["category"] = ', '.join(category)
            for item in totalStock:
                if item["stock_type"] == "Sellable":
                    list_dict["sale"] = item["count"]
                    list_dict["total"] = list_dict["total"] + item["count"]
                elif item["stock_type"] == "NonSellable":
                    list_dict["non_sale"] = item["count"]
                    list_dict["total"] = list_dict["total"] + item["count"]
                elif item["stock_type"] == "Return":
                    list_dict["sale"] = list_dict["sale"] + item["count"]
                    list_dict["total"] = list_dict["total"] + item["count"]

            if len(outStock) > 0:
                for item in outStock:
                    list_dict["stock_out"] = list_dict["stock_out"] + item["count"]
                    list_dict["available"] = list_dict["sale"] - list_dict["stock_out"]
            else:
                list_dict["available"] = list_dict["sale"]


            """ NEW FROM HERE """
            order_details = OrderDetails.objects.filter(
                order_id__status__in=status_in.BlockedOrder(),
                product_id=product, is_active=True, is_deleted=False).values(
                'product_id').annotate(count=Sum('quantity'))
            for block in order_details:
                list_dict["blocked"] = list_dict["blocked"] + block["count"]
            list_dict["available"] = list_dict["sale"] - list_dict["stock_out"]
            list_dict["remaining"] = list_dict["available"] - list_dict["blocked"]
            required_stock = list_dict["blocked"] - (list_dict["available"] - list_dict["minimum"])
            list_dict["required"] = required_stock if required_stock > 0 else 0
            if list_dict["remaining"] <= list_dict["required"]:
                color = "danger" if list_dict["remaining"] < list_dict["required"] else "warning"
                list_dict["class"] = color
                stocks.append(list_dict)

    """Export the data to a CSV file"""
    if "export" in request.GET:
        final_stock_list = []
        for x in stocks:
            final_dict = {
                "SKU Code": x["sku_code"], "Category": x["category"],
                "Available Stock": int(x["available"]),
                "Remaining Stock": int(x["remaining"]),
                "Minimum Stock Level": int(x["minimum"])
            }
            final_stock_list.append(final_dict)
        keys = ["Category", "SKU Code", "Available Stock", "Remaining Stock", "Minimum Stock Level"]
        response = download_data_in_csv('min_stock', keys, final_stock_list)
        return response

    paginator = Paginator(stocks, 10)
    try:
        page_index = request.GET.get("page", 1)
        data_list = paginator.page(page_index)
    except PageNotAnInteger:
        data_list = paginator.page(1)
    except EmptyPage:
        data_list = paginator.page(paginator.num_pages)
    context = {"products": data_list, "search": search}
    return render(request, "warehouse/min_stock.html", context=context)


def PendingPayment(request):
    from_date = request.GET.get('from_date', None)
    to_date = request.GET.get('to_date', None)
    if from_date is None or to_date is None:
        from_date = str(datetime.datetime.today().date().replace(day=1))
        to_date = str(datetime.datetime.today().date())
    orderHistory = OrderEventHistory.objects.filter(
        order_status__in=status_in.PendingPaymentStatus(),
        created_on__date__gte=from_date, created_on__date__lte=to_date)
    orderHistory = [item.order_id_id for item in orderHistory]

    orders = OrderHead.objects.filter(
        order_id__in=orderHistory, is_active=True,
        is_deleted=False).values('dealer').annotate(
        total=Sum('total_amount'), payable=Sum(
            'payable_amount')).order_by('-dealer')
    topList = []
    total_pending = 0

    if request.method == "POST":
        SpecialCDApproval(id, 'status')

    for order in orders:
        total_pending += order["payable"]
        orderHeads = OrderHead.objects.filter(
            dealer=order["dealer"], is_active=True, is_deleted=False)
        order_numbers = [order.order_number for order in orderHeads]

        try:
            dealerProfile = DealerProfile.objects.get(
                dealer_id=order["dealer"], is_active=True, is_deleted=False)
            dealer_profile_id = dealerProfile.dealer_id
            dealer = "{}.{}".format(dealerProfile.dealer.user_code, dealerProfile.dealer.first_name)
            dealer_contact = dealerProfile.dealer.mobile
        except DealerProfile.DoesNotExist:
            dealer_profile_id = dealer = dealer_contact = ""

        try:
            srDealer = SRDealer.objects.get(dealer_id=dealerProfile)
            sr_name = "{} {}".format(srDealer.sr_id.first_name, srDealer.sr_id.last_name)
            sr_contact = srDealer.sr_id.mobile
        except Exception:
            sr_name = sr_contact = ""

        row_dict = {
            "sr_contact": sr_contact, "sr_name": sr_name, "dealer_contact": dealer_contact,
            "dealer": dealer, "profile_id": dealer_profile_id, "pending_amount": order["payable"],
            "order_number": order_numbers
        }
        topList.append(row_dict)
    topList = sorted(topList, key=lambda k: k['pending_amount'], reverse=True)

    """Export the data to a CSV file"""
    if 'export' in request.GET:
        for item in topList:
            del item["profile_id"]
        keys = ['dealer', 'dealer_contact', 'sr_name', 'sr_contact',
                'order_number', 'pending_amount']
        response = download_data_in_csv('pending_amount', keys, topList)
        return response

    context = {
        "from_date": from_date,
        "to_date": to_date,
        "total_pending": total_pending,
        "total_dealer": len(topList)
    }
    paginator = Paginator(topList, 10)
    try:
        page_index = request.GET.get("page", 1)
        data_list = paginator.page(page_index)
    except PageNotAnInteger:
        data_list = paginator.page(1)
    except EmptyPage:
        data_list = paginator.page(paginator.num_pages)
    context["orders"] = data_list
    return render(request, 'reports/pending.html', context=context)


# ......................................................................


# def SendEmail(request):
#     try:
#         status = "New_order_placed"
#         order = OrderHead.objects.filter(order_number=147)
#         orderDetail = OrderDetails.objects.filter(order_id=order)
#         orderList = ""
#         for item in orderDetail:
#             category_name = item.product_id.product_category.category_name
#             orderList += "{} {} {}<br>".format(item.sku_code, category_name, item.quantity)
#
#         alert = Alerts.objects.get(alerts_type=status, is_active=True, is_deleted=False)
#         action = Action.objects.get(alerts=alert, is_active=True, is_deleted=False)
#         notification = Notification_role.objects.get(action=action, is_active=True, is_deleted=False)
#         if order.status == "Dispatched":
#             role = "WHS"
#         else:
#             role = "BusinessHead"
#
#         role = User.objects.filter(is_active=True, groups__name=role).first()
#
#         # emails = [order.dealer.email, order.sr_id.email, role.email]
#         # email_list = notification.optional_email.split(",")
#         # email_list = list(filter(None, email_list))
#         # emails += email_list
#         # contacts = [order.dealer.mobile, order.sr_id.mobile, role.mobile]
#         # contacts = ",".join(map(str, contacts))
#         emails = ['kailash.c@skysoft.net.in']
#         contacts = '9891868570'
#
#         bodyHtml = EmailTemplateMaster.objects.get(action=action, is_active=True, is_deleted=False)
#         htmlContent = bodyHtml.email_content
#         htmlContent = htmlContent.replace(
#             '<dealer_name>', "{} {}".format(order.dealer.first_name, order.dealer.last_name))
#         htmlContent = htmlContent.replace('<dealer_code>', str(order.dealer.user_code))
#         htmlContent = htmlContent.replace('<order_no>', str(order.order_number))
#         htmlContent = htmlContent.replace('<price>', "₹ {}".format(order.payable_amount))
#
#         htmlContent = htmlContent.replace('<sku_product_qty>', orderList)
#
#         htmlContent = htmlContent.replace(
#             '<order_link>', '<a href="http://127.0.0.1:8000/user/order-detail/{}">View Order</a>'.format(order.order_number))
#         htmlContent = htmlContent.replace(
#             '<sr_name>', "{} {}".format(order.sr_id.first_name, order.sr_id.last_name))
#         htmlContent = htmlContent.replace('<sr_contact>', str(order.sr_id.mobile))
#
#         # ....................................................................
#         if action.send_type == 'both':
#             # send_sms(contacts, bodyHtml.sms_content)
#             send_email(bodyHtml.subject, emails, htmlContent)
#         elif action.send_type == 'sms':
#             pass
#             # send_sms(contacts, bodyHtml.sms_content)
#         else:
#             send_email(bodyHtml.subject, emails, htmlContent)
#     except Exception:
#         try:
#             emails = ['kailash.c@skysoft.net.in']
#             # emails = [order.dealer.email, order.sr_id.email]
#             send_email(order.status, emails, order.status)
#         except Exception:
#             pass


def SpecialCD(request):
    search_dealer = request.GET.get('order', None)
    search_dealer = 0 if search_dealer is None or search_dealer == '' else search_dealer

    from_date = request.GET.get('from_date', None)
    to_date = request.GET.get('to_date', None)

    if request.method == "POST":
        ledger_id = request.POST.get('ledger_id')
        status = request.POST.get('status')
        try:
            special_discount = SpecialCDType.objects.get(
                pk=ledger_id, is_active=True, is_deleted=False)
            special_discount.status = status
            special_discount.save()

            if special_discount.status == "Approved":
                """ ===== approved-special-discount ===== """
                approved_special_discount(request, special_discount)
        except SpecialCDType.DoesNotExists:
            pass

    ledgers = SpecialCDType.objects.filter(
        is_active=True, is_deleted=False).order_by('-pk')

    groups = request.user.groups.all()
    groups = [group.name for group in groups]
    if 'SR' in groups:
        srDealer = SRDealer.objects.filter(sr_id=request.user)
        srDealers = []
        for item in srDealer:
            if item.dealer_id is not None:
                if item.dealer_id.dealer is not None:
                    srDealers.append(item.dealer_id.dealer)
        ledgers = ledgers.filter(dealer__in=srDealers)

    my_dealers = []
    dealerProfiles = DealerProfile.objects.filter(
        is_active=True, is_deleted=False).order_by('-pk')
    dealers = [i.dealer for i in dealerProfiles]
    dealers = list(set(dealers))
    if from_date is not None and from_date != "" and to_date is not None and to_date != "":
        ledgers = ledgers.filter(
            created_on__date__gte=from_date, created_on__date__lte=to_date)
    for _x in dealers:
        try:
            dealer = DealerProfile.objects.get(dealer=_x)
            dict_dealer = {"id": dealer.dealer.pk, "company": dealer.company_name}
            my_dealers.append(dict_dealer)
        except DealerProfile.DoesNotExist:
            continue
    if search_dealer != 0:
        ledgers = ledgers.filter(dealer__in=[int(search_dealer)])

    discountList = []
    for item in ledgers:
        try:
            dealer = DealerProfile.objects.get(dealer=item.dealer)
        except DealerProfile.DoesNotExist:
            continue

        discount_dict = {
            "series_no": item.series_no,
            "type": item.get_discount_type_display(),
            "value": item.amount,
            "status": item.status,
            "id": item.pk,
            "company": dealer.company_name,
            "discount_date": item.created_on.date(),
            "remarks": item.remarks
        }
        discountList.append(discount_dict)

    """Export the data to a CSV file"""
    if 'export' in request.GET:
        for item in discountList:
            del item["id"]
        keys = ['series_no', 'company', 'discount_date',
                'status', 'type', 'value', 'remarks']
        full_file_name = "special_discount"
        response = download_data_in_csv(full_file_name, keys, discountList)
        return response

    paginator = Paginator(discountList, 10)
    try:
        page_index = request.GET.get("page", 1)
        data_list = paginator.page(page_index)
    except PageNotAnInteger:
        data_list = paginator.page(1)
    except EmptyPage:
        data_list = paginator.page(paginator.num_pages)
    print(data_list)
    context = {
        "ledgers": data_list,
        "dealers": my_dealers,
        "x_dealer": int(search_dealer),
        "from_date": from_date,
        "to_date": to_date
    }
    return render(request, 'admin/cd_type_3.html', context=context)


def SpecialCD1(request):
    return render(request, 'admin/rozar.html')


def approved_special_discount(request, special_discount):
    """ ===== special-discount-ledger-start ===== """
    remark = "CD(Credit Note-Type 3) {}".format(special_discount.series_no)
    dealer = special_discount.dealer
    discount = special_discount.amount
    """Ledger remarks added"""
    create_credit_note_special_discount(request, dealer, discount, remark)
    return True


def approved_debit_note(request, special_discount):
    """ ===== debit-note-approved-start ===== """
    remark = "Debit Note"
    dealer = special_discount.dealer
    discount = special_discount.amount
    create_custom_debit_note(request, dealer, discount, remark)
    return True


def AddSpecialCD(request):
    try:
        form_class = forms.SpecialTypeForm
        dealers = DealerProfile.objects.filter(
            is_active=True, is_deleted=False).order_by('company_name')
        if request.method == "POST":
            form_class = forms.SpecialTypeForm(request.POST)
            if form_class.is_valid():
                special_discount = form_class.save(commit=False)
                special_discount.created_by = request.user
                special_discount.save()

                groups = request.user.groups.all()
                groups = [group.name for group in groups]
                if "BusinessHead" in groups:
                    special_discount.status = "Approved"
                    special_discount.save()
                    """ ===== approved-special-discount ===== """
                    approved_special_discount(request, special_discount)
                    messages.add_message(request, messages.SUCCESS, 'Credit Note Type 3 Successfully Applied!')
                else:
                    messages.add_message(request, messages.SUCCESS, 'Credit Note Type 3 Successfully Created!')
                return redirect("/user/special-cd")
            else:
                messages.add_message(request, messages.ERROR, 'Oops!, Something want wrong.')
    except Exception as error:
        messages.add_message(request, messages.ERROR, str(error))
    context = {'form': form_class, "dealers": dealers}
    return render(request, 'admin/credit_note.html', context=context)


# def create_credit_note_special_discount(request, dealer, discount, remark):
#     """ =============== credit-note-ledger-start =============== """
#     try:
#         prevCreditNoteLedger = Ledger.objects.filter(
#             ledger_type="Discount", dealer=dealer,
#             is_active=True, is_deleted=False).latest('pk')
#     except Ledger.DoesNotExist:
#         prevCreditNoteLedger = None
#
#     currentCreditNoteLedger = Ledger(
#         ledger_type="Discount", remarks=remark,
#         dealer=dealer,
#         voucher_number="type-3", created_by=request.user
#     )
#     if prevCreditNoteLedger is None:
#         balance = 0
#     else:
#         balance = prevCreditNoteLedger.balance
#     currentCreditNoteLedger.dr_amount = float(discount)
#     currentCreditNoteLedger.balance = float(balance) + float(discount)
#     currentCreditNoteLedger.save()
#
#     """ =============== credit-note-ledger-end =============== """
#     """ =============== dealer-ledger-start =============== """
#     try:
#         prevDealerLedger = Ledger.objects.filter(
#             ledger_type="Dealer", dealer=dealer,
#             is_active=True, is_deleted=False).latest('pk')
#     except Ledger.DoesNotExist:
#         prevDealerLedger = None
#
#     currentDealerLedger = Ledger(
#         ledger_type="Dealer", remarks=remark,
#         dealer=dealer,
#         voucher_number="type-3", created_by=request.user
#     )
#     if prevDealerLedger is None:
#         balance = 0
#     else:
#         balance = prevDealerLedger.balance
#     currentDealerLedger.cr_amount = float(discount)
#     currentDealerLedger.balance = float(balance) - float(discount)
#     currentDealerLedger.save()
#     """ =============== dealer-ledger-end =============== """
#
#     """ =============== credit-note-ledger-end =============== """
#     """ =============== dealer-ledger-start =============== """
#     try:
#         prevDealerLedger = Ledger.objects.filter(
#             ledger_type="DealerTransaction", dealer=dealer,
#             is_active=True, is_deleted=False).latest('pk')
#     except Ledger.DoesNotExist:
#         prevDealerLedger = None
#
#     currentDealerLedger = Ledger(
#         ledger_type="DealerTransaction", remarks=remark,
#         dealer=dealer,
#         voucher_number="type-3", created_by=request.user
#     )
#     if prevDealerLedger is None:
#         balance = 0
#     else:
#         balance = prevDealerLedger.balance
#     currentDealerLedger.cr_amount = float(discount)
#     currentDealerLedger.balance = float(balance) - float(discount)
#     currentDealerLedger.save()
#     """ =============== dealer-ledger-end =============== """
#
#     outstanding = get_outstanding(dealer)
#     if outstanding < 0 or outstanding > 0:
#         orderHead = OrderHead.objects.filter(
#             dealer=dealer, status__in=status_in.OutstandingStatus(),
#             is_active=True, is_deleted=False).latest('pk')
#         """ SUBTRACT DISCOUNT TO OUTSTANDING"""
#         orderHead.payable_amount = float(orderHead.payable_amount) - float(discount)
#         orderHead.save()
#     elif outstanding == 0:
#         try:
#             orderHead = OrderHead.objects.filter(
#                 dealer=dealer, status__in=status_in.OutstandingStatus(),
#                 is_active=True, is_deleted=False).latest('pk')
#             """ SUBTRACT DISCOUNT TO OUTSTANDING"""
#             orderHead.payable_amount = float(orderHead.payable_amount) - float(discount)
#             orderHead.save()
#         except Exception:
#             pass
#     return True


def get_outstanding(dealer):
    try:
        total_outstanding_amount = OrderHead.objects.filter(
            dealer=dealer, status__in=status_in.OutstandingStatus(),
            is_active=True, is_deleted=False).aggregate(Sum('payable_amount'))
        total_outstanding_amount = float(total_outstanding_amount["payable_amount__sum"])
    except Exception:
        total_outstanding_amount = float(0)
    return total_outstanding_amount


def send_email_or_sms_both(status, order=None):
    try:
        alert = Alerts.objects.filter(alerts_type=status, is_active=True, is_deleted=False).first()
        action = Action.objects.filter(alerts=alert, is_active=True, is_deleted=False).first()
        notification = Notification_role.objects.filter(action=action, is_active=True, is_deleted=False).first()
        if order.status == "Dispatched":
            role = "WHS"
        else:
            role = "BusinessHead"

        role = User.objects.filter(is_active=True, groups__name=role).first()

        emails = [order.dealer.email, order.sr_id.email, role.email]
        email_list = notification.optional_email.split(",")
        email_list = list(filter(None, email_list))
        emails += email_list
        contacts = [order.dealer.mobile, order.sr_id.mobile, role.mobile]
        contacts = ",".join(map(str, contacts))

        bodyHtml = EmailTemplateMaster.objects.filter(action=action, is_active=True, is_deleted=False).first()
        if action.send_type == 'both':
            send_sms(contacts, bodyHtml.sms_content)
            send_email(bodyHtml.subject, emails, bodyHtml.email_content)
        elif action.send_type == 'sms':
            send_sms(contacts, bodyHtml.sms_content)
        else:
            send_email(bodyHtml.subject, emails, bodyHtml.email_content)
    except Exception:
        try:
            emails = [order.dealer.email, order.sr_id.email]
            send_email(order.status, emails, order.status)
        except Exception:
            pass


def send_sms_or_email(request, orderHead, payment=None):
    emails, mobiles, template, send_type = [], "", None, None
    try:
        msg_type = models.AlertMaster.objects.get(
            alert_type=orderHead.status, is_active=True, is_deleted=False)
        optional_emails = msg_type.optional_email.replace(' ', '').split(",")
        optional_emails = list(filter(None, optional_emails))
        send_type = msg_type.action_type
    except models.AlertMaster.DoesNotExist:
        msg_type, optional_emails = None, []

    # try:
    #     x_dealer_profile = DealerProfile.objects.get(
    #         dealer=orderHead.dealer, is_active=True, is_deleted=False)
    #     x_sr_dealer = SRDealer.objects.get(dealer_id=x_dealer_profile)
    #     sr_id = x_sr_dealer.sr_id
    # except Exception:
    #     sr_id = orderHead.sr_id

    if msg_type is not None:
        try:
            groups_name = models.SendAlert.objects.filter(
                action=msg_type, is_active=True, is_deleted=False)
            groups_name = [_x.role_name for _x in groups_name if _x.role_name not in ['SR', 'DEL']]

            users = list(User.objects.filter(is_active=True, groups__name__in=groups_name))
            users.append(orderHead.dealer), users.append(orderHead.sr_id)
            emails = [_y.email for _y in users]
            emails += optional_emails

            mobiles = [_y.mobile for _y in users]
            contacts = list(filter(None, mobiles))
            mobiles = ",".join(map(str, contacts))
        except Exception as error:
            print(error)

    if msg_type is not None:
        try:
            template = models.AlertTemplateMaster.objects.get(
                alert=msg_type, is_active=True, is_deleted=False)
        except models.AlertMaster.DoesNotExist:
            pass

    if send_type is not None and template is not None:
        if send_type == "SMS":
            sms = template.sms_content
            html_message, flag = create_alert_html_content(orderHead, send_type, sms, request, payment)
            if flag:
                send_sms(mobiles, html_message)
        elif send_type == "EMAIL":
            email = template.email_content
            html_message, flag = create_alert_html_content(orderHead, send_type, email, request, payment)
            if flag:
                send_email(template.subject, emails, html_message)
        elif send_type == "BOTH":
            sms = template.sms_content
            sms_html_message, flag = create_alert_html_content(orderHead, "SMS", sms, request, payment)
            if flag:
                send_sms(mobiles, sms_html_message)

            email = template.email_content
            email_html_message, flag = create_alert_html_content(orderHead, "EMAIL", email, request, payment)
            if flag:
                send_email(template.subject, emails, email_html_message)
    return True


def create_alert_html_content(orderHead, action, template, request, payment=None):
    try:
        replace_template = True
        orderDetails = OrderDetails.objects.filter(
            order_id=orderHead, is_active=True, is_deleted=False)

        try:
            x_dealer_profile = DealerProfile.objects.get(
                dealer=orderHead.dealer, is_active=True, is_deleted=False)
            x_sr_dealer = SRDealer.objects.get(dealer_id=x_dealer_profile)
            sr_id = x_sr_dealer.sr_id
        except Exception:
            sr_id = orderHead.sr_id

        dealer_name = "{0}".format(orderHead.dealer.first_name)
        dealer_code = "{0}".format(orderHead.dealer.user_code)
        sr_name = "{0}".format(sr_id.first_name)
        sr_contact = "{0}".format(sr_id.mobile)
        number_of_order = len(orderDetails)

        template = template.replace('<dealer_name>', dealer_name)
        template = template.replace('<dealer_code>', dealer_code)
        template = template.replace('<order_number>', str(orderHead.order_number))
        template = template.replace('<qty>', str(number_of_order))
        dealer_payment = orderHead.payable_amount if payment is None else payment
        template = template.replace('<bill_value>', "INR {0}".format(dealer_payment))

        host_name = "https://" if request.is_secure() else "http://"
        base_url = request.META.get('HTTP_HOST') or request.META.get('SERVER_NAME')
        domain_name = host_name + base_url
        order_path = domain_name + "/user/order-detail/{}".format(str(orderHead.order_number))
        template = template.replace('<link_to_order>', order_path)

        if action == "EMAIL":
            y_css = 'background: #2c64c5;padding: 8px 10px;text-decoration: none;color: #fff;border-radius: 5px;'
            # host_name = "https://" if request.is_secure() else "http://"
            # base_url = request.META.get('HTTP_HOST') or request.META.get('SERVER_NAME')
            # domain_name = host_name + base_url
            # order_path = domain_name + "/user/order-detail/{}".format(str(orderHead.order_number))
            template = template.replace('<dms_no>', str(orderHead.DMS_order_number))
            link_to_order = "<a style='{0}' href='{1}'>View Order</a>".format(y_css, order_path)
            template = template.replace('<link_to_order>', link_to_order)

            outstanding_path = domain_name + "/transactions/order_head/outstanding/?on_id={}".format(str(orderHead.dealer.pk))
            link_to_outstanding = "<a style='{0}' href='{1}'>View Outstanding</a>".format(y_css, outstanding_path)
            template = template.replace('<link_to_outstanding_balance>', link_to_outstanding)

            x_css = 'border: 1px solid;padding: 6px 12px;border-collapse: collapse;'
            table = "<table style='"+x_css+"'><thead><tr style='"+x_css+"'><th style='"+x_css+"'>SKU</th>"
            table += "<th style='"+x_css+"'>Product</th><th style='"+x_css+"'>Unit</th></tr></thead><tbody>"
            for _x in orderDetails:
                table += "<tr style='"+x_css+"'><td style='"+x_css+"'>"+_x.product_id.sku_code+"</td>"
                table += "<td style='"+x_css+"'>"+_x.product_id.product_category.category_name+"</td>"
                table += "<td style='"+x_css+"'>"+str(_x.quantity)+"</td></tr>"
            table += "</tbody></table>"
            template = template.replace('<list_of_order>', table)
            template = template.replace('<sr_name>', sr_name)
            template = template.replace('<sr_contact>', sr_contact)
    except:
        replace_template = False
    return template, replace_template


def EmailSettings(request, orderID):
    orderHead = OrderHead.objects.get(
        order_number=orderID, is_active=True, is_deleted=False)
    message = send_sms_or_email(request, orderHead)
    return HttpResponse({"msg": message})


def CustomInventory(request):
    urlobj = request.path.split('/')[2]
    ware = ''
    if urlobj == 'custom-inventory':
        ware = WarehouseMaster.objects.filter(is_active=True, is_deleted=False)
    if urlobj == 'custom-inventory-manager':
        wareadmin = WarehouseAdmin.objects.filter(
            is_active=True, is_deleted=False, user_id=request.user.id).values_list('warehouse_id')
        ware = WarehouseMaster.objects.filter(is_active=True, is_deleted=False, warehouse_code__in=wareadmin)

    warehouse_search = request.GET.get('warehouse', None)

    varfirst = request.GET.get('first', None)
    varsecond = request.GET.get('second', None)
    varthird = request.GET.get('third', None)
    varfour = request.GET.get('four', None)
    varfive = request.GET.get('five', None)

    sealable_search = request.GET.get('Sealable', None)
    non_sealable_search = request.GET.get('nonSealable', None)
    in_stock_search = request.GET.get('inStock', None)
    out_stock_search = request.GET.get('outstock', None)

    context = {
        'ware': ware,
        'Salable': sealable_search,
        'nonSalable': non_sealable_search,
        'inStock': in_stock_search,
        'outStock': out_stock_search
    }

    if varfirst == '' or varfirst == 'all' or varfirst is None:
        one, two, three, four, five = get_warehouse_product_category()
    else:
        one, two, three, four, five = get_product_all_category(request, varfirst)

    if varthird != '' and varthird != 'all' and varthird is not None:
        if varsecond != '' and varsecond != 'all' and varsecond is not None:
            x, three, four, five, y = get_product_all_category(request, varsecond)
        else:
            x, four, five, y, z = get_product_all_category(request, varthird)

    if varfour != '' and varfour != 'all' and varfour is not None:
        if varthird != '' and varthird != 'all' and varthird is not None:
            x, four, five, y, z = get_product_all_category(request, varthird)
        else:
            x, five, y, z, m = get_product_all_category(request, varfour)

    if varfive != '' and varfive != 'all' and varfive is not None:
        if varthird != '' and varthird != 'all' and varthird is not None:
            x, five, y, z, m = get_product_all_category(request, varfour)
        else:
            x, five, y, z, m = get_product_all_category(request, varfive)

    context["one"] = one
    context["two"] = two
    context["three"] = three
    context["four"] = four
    context["five"] = five
    context["varOne"] = varfirst
    context["varTwo"] = varsecond
    context["varThree"] = varthird
    context["varFour"] = varfour
    context["varFive"] = varfive
    context["varWare"] = warehouse_search

    # .......................................................................

    categories = []
    if varfive is not None and varfive != "all":
        productCategory = ProductCategoryMaster.objects.get(
            category_name__icontains=varfive, is_active=True, is_deleted=False)
        categories.append(productCategory.pk)
    elif varfour is not None and varfour != "all":
        productCategories = get_custom_product_category_list(varfour, 1)
        categories += productCategories
    elif varthird is not None and varthird != "all":
        productCategories = get_custom_product_category_list(varthird, 2)
        categories += productCategories
    elif varsecond is not None and varsecond != "all":
        productCategories = get_custom_product_category_list(varsecond, 3)
        categories += productCategories
    elif varfirst is not None and varfirst != "all":
        productCategories = get_custom_product_category_list(varfirst, 4)
        categories += productCategories

    if len(categories) > 0:
        products = ProductMaster.objects.filter(
            product_category_id__in=categories,
            is_active=True, is_deleted=False)
    else:
        products = ProductMaster.objects.filter(is_active=True, is_deleted=False)

    # ........................
    warehouseList = []
    if warehouse_search != 'all' and warehouse_search != '' and warehouse_search is not None:
        warehouseList.append(warehouse_search)
    else:
        groups = [group.name for group in request.user.groups.all()]
        if request.user.is_superuser or request.user.is_admin or 'WHS' in groups:
            # all warehouses
            warehouseList = WarehouseMaster.objects.filter(is_active=True, is_deleted=False)
            warehouseList = [item.warehouse_code for item in warehouseList]
        elif 'WHSUSER' in groups:
            # manager warehouses
            warehouseAssigned = WarehouseAdmin.objects.filter(
                is_active=True, is_deleted=False, user_id=request.user.id).values_list('warehouse_id')
            warehouseList = WarehouseMaster.objects.filter(
                is_active=True, is_deleted=False, warehouse_code__in=warehouseAssigned)
            warehouseList = [item.warehouse_code for item in warehouseList]
    # ........................
    productList = []
    for product in products:
        list_dict = {
            "sku_code": product.sku_code,
            "sellable": 0, "non_sellable": 0,
            "stock_in": 0, "stock_out": 0
        }

        search_stock_type = []
        if sealable_search is None and non_sealable_search is None and in_stock_search is None and out_stock_search is None:
            search_stock_type += ['Sellable', 'NonSellable']
        else:
            if sealable_search:
                search_stock_type.append('Sellable')
            if non_sealable_search:
                search_stock_type.append('NonSellable')
            if in_stock_search or out_stock_search:
                search_stock_type += ['Sellable', 'NonSellable']

        totalStock = StockRegister.objects.filter(
            warehouse_id__in=warehouseList, product=product,
            stock_type__in=search_stock_type, is_active=True,
            is_deleted=False).values('stock_type').annotate(count=Sum('stock_in'))

        outStock = StockRegister.objects.filter(
            product=product, stock_type='stock_out',
            is_active=True, is_deleted=False).values(
            'stock_type').annotate(count=Sum('stock_out'))

        if len(totalStock) > 0:
            category = find_product_category(product)
            list_dict["category"] = ', '.join(category)

            for item in totalStock:
                if item["stock_type"] == "Sellable":
                    list_dict["sellable"] = item["count"]
                elif item["stock_type"] == "NonSellable":
                    list_dict["non_sellable"] = item["count"]

            if len(outStock) > 0:
                for item in outStock:
                    list_dict["stock_out"] = list_dict["stock_out"] + item["count"]
                    list_dict["stock_in"] = list_dict["sellable"] - list_dict["stock_out"]
            else:
                list_dict["stock_in"] = list_dict["sellable"]

            item_dict = {
                "category": list_dict["category"], "sku_code": product.sku_code,
                "mrp": float(product.max_retail_price), "dp": float(product.dealer_price),
                "size": product.size_capacity, "size_unit": product.size_capacity_unit,
                "color": product.pattern_color, "description": product.product_description_feature
            }

            if sealable_search is None and non_sealable_search is None and in_stock_search is None and out_stock_search is None:
                if list_dict["stock_in"] > 0:
                    item_dict["stock_type"] = "Sellable"
                    item_dict["total"] = list_dict["stock_in"]
                    item_dict["x_class"] = "success"
                    productList.append(item_dict)

                if list_dict["non_sellable"] > 0:
                    second = dict(item_dict)
                    second["stock_type"] = "Non Sellable"
                    second["total"] = list_dict["non_sellable"]
                    second["x_class"] = "warning"
                    productList.append(second)

                if list_dict["stock_out"] > 0:
                    third = dict(item_dict)
                    third["stock_type"] = "Stock Out"
                    third["total"] = list_dict["stock_out"]
                    third["x_class"] = "danger"
                    productList.append(third)
            else:
                if sealable_search or in_stock_search:
                    if list_dict["stock_in"] > 0:
                        item_dict["stock_type"] = "Sellable"
                        item_dict["total"] = list_dict["stock_in"]
                        item_dict["x_class"] = "success"
                        productList.append(item_dict)

                if non_sealable_search or in_stock_search:
                    if list_dict["non_sellable"] > 0:
                        second = dict(item_dict)
                        second["stock_type"] = "Non Sellable"
                        second["total"] = list_dict["non_sellable"]
                        second["x_class"] = "warning"
                        productList.append(second)

                if out_stock_search:
                    if list_dict["stock_out"] > 0:
                        third = dict(item_dict)
                        third["stock_type"] = "Stock Out"
                        third["total"] = list_dict["stock_out"]
                        third["x_class"] = "danger"
                        productList.append(third)
    context["products"] = productList
    # ...........................................
    """Length of Search Products"""
    if len(productList) > 0:
        total_record_msg = "{0} records found".format(len(productList))
    else:
        total_record_msg = "No records found"
    context["search_record"] = total_record_msg
    # ...........................................
    return render(request, 'warehouse/custom_inventory.html', context)


def get_custom_product_category_list(category, counter):
    productCategory = ProductCategoryMaster.objects.get(
        category_name=category, is_active=True, is_deleted=False)
    productCategories = ProductCategoryMaster.objects.filter(
        parent=productCategory.pk, is_active=True, is_deleted=False)
    categories = [item.pk for item in productCategories]

    for i in range(counter):
        x_categories = get_multiple_category(categories)
        categories += x_categories
    categories.append(productCategory.pk)
    return categories


def get_multiple_category(categories):
    productCategories = ProductCategoryMaster.objects.filter(
        parent__in=categories, is_active=True, is_deleted=False)
    x_categories = [item.pk for item in productCategories]
    return x_categories


def GetCustomInventory(request):
    products = ProductMaster.objects.filter(is_active=True, is_deleted=False)
    productList = []
    for product in products:
        list_dict = {
            "sku_code": product.sku_code,
            "sellable": 0, "non_sellable": 0,
            "stock_in": 0, "stock_out": 0
        }

        totalStock = StockRegister.objects.filter(
            product=product, stock_type__in=['Sellable', 'NonSellable'],
            is_active=True, is_deleted=False).values('stock_type').annotate(
            count=Sum('stock_in'))

        outStock = StockRegister.objects.filter(
            product=product, stock_type='stock_out',
            is_active=True, is_deleted=False).values(
            'stock_type').annotate(count=Sum('stock_out'))

        if len(totalStock) > 0:
            category = find_product_category(product)
            list_dict["category"] = ', '.join(category)

            for item in totalStock:
                if item["stock_type"] == "Sellable":
                    list_dict["sellable"] = float(item["count"])
                elif item["stock_type"] == "NonSellable":
                    list_dict["non_sellable"] = float(item["count"])

            if len(outStock) > 0:
                for item in outStock:
                    list_dict["stock_out"] = float(list_dict["stock_out"]) + float(item["count"])
                    list_dict["stock_in"] = float(list_dict["sellable"]) - float(list_dict["stock_out"])
            else:
                list_dict["stock_in"] = float(list_dict["sellable"])

            item_dict = {
                "category": list_dict["category"], "sku_code": product.sku_code,
                "mrp": float(product.max_retail_price), "dp": float(product.dealer_price),
                "size": product.size_capacity, "size_unit": product.size_capacity_unit
            }

            if list_dict["stock_in"] > 0:
                item_dict["stock_type"] = "Sellable"
                item_dict["total"] = list_dict["stock_in"]
                productList.append(item_dict)

            if list_dict["non_sellable"] > 0:
                second = dict(item_dict)
                second["stock_type"] = "Non Sellable"
                second["total"] = list_dict["non_sellable"]
                productList.append(second)

            if list_dict["stock_out"] > 0:
                third = dict(item_dict)
                third["stock_type"] = "Stock Out"
                third["total"] = list_dict["stock_out"]
                productList.append(third)
    return HttpResponse(json.dumps(productList))


def get_all_product_category(request, index=None, value=None):
    if request.is_ajax():
        index = request.GET.get('index')
        value = request.GET.get('value').rstrip(' ')

        if value != '0':
            category = ProductCategoryMaster.objects.filter(
                category_name__istartswith=value, is_active=True, is_deleted=False).first()
            value = category.pk

        if index == '1' and value == '0':
            return JsonResponse({"list1": [], "list2": [], "list3": [], "list4": []})
        elif index == '1':
            sub_category_one = get_sub_category([value])
            parents = [item["id"] for item in sub_category_one]
            sub_category_two = get_sub_category(parents)

            parents = [item["id"] for item in sub_category_two]
            sub_category_three = get_sub_category(parents)
            parents = [item["id"] for item in sub_category_three]
            sub_category_four = get_sub_category(parents)
            context = {
                "list1": sub_category_one,
                "list2": sub_category_two,
                "list3": sub_category_three,
                "list4": sub_category_four
            }
            return JsonResponse(context)
        elif index == '2':
            sub_category = get_sub_category([value])

            parents = [item["id"] for item in sub_category]
            sub_category_three = get_sub_category(parents)
            parents = [item["id"] for item in sub_category_three]
            sub_category_four = get_sub_category(parents)
            context = {
                "list1": sub_category,
                "list2": sub_category_three,
                "list3": sub_category_four
            }
            return JsonResponse(context)
        elif index == '4':
            sub_category_three = get_sub_category([value])
            parents = [item["id"] for item in sub_category_three]
            sub_category_four = get_sub_category(parents)
            context = {
                "list1": sub_category_three,
                "list2": sub_category_four
            }
            return JsonResponse(context)
        elif index == '5':
            parent = ProductCategoryMaster.objects.filter(
                pk=category.parent, is_active=True, is_deleted=False)
            parent = [{"id": item.pk, "name": item.category_name} for item in parent]
            return JsonResponse({"list1": parent})
    else:
        category = ProductCategoryMaster.objects.filter(
            category_name__istartswith=value, is_active=True, is_deleted=False).first()
        value = category.pk
        if index == '1':
            sub_category_one = get_sub_category([value])
            parents = [item["id"] for item in sub_category_one]
            sub_category_two = get_sub_category(parents)
            return sub_category_one, sub_category_two
        elif index == '2':
            sub_category = get_sub_category([value])
            return sub_category
        elif index == '3':
            parent = ProductCategoryMaster.objects.filter(
                pk=category.parent, is_active=True, is_deleted=False)
            parent = [{"id": item.pk, "name": item.category_name} for item in parent]
            return parent


def get_length_of_outstock(request):
    try:
        warehouseList = []
        groups = [group.name for group in request.user.groups.all()]
        warehouses = WarehouseMaster.objects.filter(is_active=True, is_deleted=False)
        if request.user.is_superuser or request.user.is_admin or 'WHS' in groups:
            warehouseList = [item.warehouse_code for item in warehouses]
        elif 'WHSUSER' in groups:
            warehouseAssigned = WarehouseAdmin.objects.filter(
                is_active=True, is_deleted=False, user_id=request.user.id).values_list('warehouse_id')
            warehouseList = warehouses.filter(warehouse_code__in=warehouseAssigned)
            warehouseList = [item.warehouse_code for item in warehouseList]

        products = StockLevelMaster.objects.filter(
            warehouse_id__in=warehouseList, is_active=True, is_deleted=False)
        products = [_x.product for _x in products]

        outStock = StockRegister.objects.filter(
            product__in=products, stock_type='stock_out',
            is_active=True, is_deleted=False).values(
            'stock_type', 'product').annotate(count=Sum('stock_out'))
        out_stock = len(outStock)
    except:
        out_stock = 0
    return out_stock


def WarehouseReport(request):
    products = ProductMaster.objects.filter(is_active=True, is_deleted=False)
    stocks, search = [], ""
    # .............................................

    if "search" in request.GET:
        search = request.GET.get('search')
        products = products.filter(
            sku_code__icontains=search,
            is_active=True, is_deleted=False)
    # .............................................

    groups = request.user.groups.all()
    groups = [group.name for group in groups]
    if 'WHSUSER' in groups:
        warehouse = WarehouseAdmin.objects.filter(user=request.user, is_active=True, is_deleted=False)
        warehouse = [item.warehouse for item in warehouse]
        stock_level = StockLevelMaster.objects.filter(
            warehouse__in=warehouse,product__in=products, is_active=True, is_deleted=False)
    else:
        stock_level = StockLevelMaster.objects.filter(product__in=products,
             is_active=True, is_deleted=False)
    for stock in stock_level:
        product=stock.product
        list_dict = {
            "sku_code": product.sku_code, "total": 0, "sale": 0,
            "available": 0, "stock_out": 0, "non_sale": 0,
            "blocked": 0, "remaining": 0, "minimum": 0, "required": 0
        }

        try:
            stock_level = StockLevelMaster.objects.filter(
                product=product, is_active=True, is_deleted=False).first()
            list_dict["minimum"] = int(stock_level.min_stock_level)
        except:
            list_dict["minimum"] = int(0)

        totalStock = StockRegister.objects.filter(
            product=product, stock_type__in=['Sellable', 'NonSellable', 'Return'],
            is_active=True, is_deleted=False).values('stock_type').annotate(
            count=Sum('stock_in'))

        outStock = StockRegister.objects.filter(
            product=product, stock_type='stock_out',
            is_active=True, is_deleted=False).values(
            'stock_type').annotate(count=Sum('stock_out'))

        if len(totalStock) > 0:
            category = find_product_category(product)
            list_dict["category"] = ' > '.join(category)
            for item in totalStock:
                if item["stock_type"] == "Sellable":
                    list_dict["sale"] = item["count"]
                    list_dict["total"] = list_dict["total"] + item["count"]
                elif item["stock_type"] == "NonSellable":
                    list_dict["non_sale"] = item["count"]
                    list_dict["total"] = list_dict["total"] + item["count"]
                elif item["stock_type"] == "Return":
                    list_dict["sale"] = list_dict["sale"] + item["count"]
                    list_dict["total"] = list_dict["total"] + item["count"]

            if len(outStock) > 0:
                for item in outStock:
                    list_dict["stock_out"] = list_dict["stock_out"] + item["count"]
                    # list_dict["available"] = list_dict["total"] - list_dict["stock_out"]
                    list_dict["available"] = list_dict["sale"] - list_dict["stock_out"]
            else:
                list_dict["available"] = list_dict["sale"]

            """ NEW FROM HEAR """
            # =============================================
            order_details = OrderDetails.objects.filter(
                order_id__status__in=status_in.AssignedOrder(),
                product_id=product, is_active=True, is_deleted=False).values(
                'product_id').annotate(count=Sum('quantity'))
            for block in order_details:
                list_dict["blocked"] = list_dict["blocked"] + block["count"]
            # list_dict["available"] = list_dict["total"] - list_dict["non_sale"]
            list_dict["available"] = list_dict["sale"] - list_dict["stock_out"]
            list_dict["remaining"] = list_dict["available"] - list_dict["blocked"]
            # list_dict["required"] = list_dict["minimum"] - list_dict["remaining"]
            required_stock = list_dict["blocked"] - (list_dict["available"] - list_dict["minimum"])
            list_dict["required"] = required_stock if required_stock > 0 else 0
            # =============================================

            stocks.append(list_dict)

    if "export" in request.POST:
        response = HttpResponse(content_type='text/csv')
        response['Content-Disposition'] = 'attachment;filename=warehouse_report.csv'
        # keys = ['sku_code', 'sale', 'non_sale', 'total', 'available',
        #         'blocked', 'remaining', 'minimum', 'required']
        # for item in stocks:
        #     del item["stock_out"], item["category"]
        final_stock_list = []
        for x in stocks:
            # required_stock = x["required"] if x["required"] > 0 else 0
            final_dict = {
                "SKU Code": x["sku_code"], "Sellable": int(x["sale"]),
                "Non Sellable": int(x["non_sale"]), "Total Stock": int(x["total"]),
                "Stock Out": int(x["stock_out"]), "Available Stock": int(x["available"]),
                "Blocked": int(x["blocked"]), "Remaining Stock": int(x["remaining"]),
                "Minimum Stock Level": int(x["minimum"]), "Required": int(x["required"])
            }
            final_stock_list.append(final_dict)
        keys = ["SKU Code", "Sellable", "Non Sellable", "Total Stock", "Available Stock",
                "Blocked", "Remaining Stock", "Minimum Stock Level", "Required", "Stock Out"]
        writer = csv.DictWriter(response, keys)
        writer.writeheader()
        writer.writerows(final_stock_list)
        return response

    paginator = Paginator(stocks, 10)
    try:
        page_index = request.GET.get("page", 1)
        data_list = paginator.page(page_index)
    except PageNotAnInteger:
        data_list = paginator.page(1)
    except EmptyPage:
        data_list = paginator.page(paginator.num_pages)
    context = {"stocks": data_list, "search": search,
               'inventoryCount': len(stocks),}
    return render(request, 'warehouse/admin_report.html', context=context)


def DebitNoteView(request):
    search_dealer = request.GET.get('order', None)
    search_dealer = 0 if search_dealer is None or search_dealer == '' else search_dealer

    from_date = request.GET.get('from_date', None)
    to_date = request.GET.get('to_date', None)

    if request.method == "POST":
        ledger_id = request.POST.get('ledger_id')
        status = request.POST.get('status')
        try:
            special_discount = DebitNote.objects.get(
                pk=ledger_id, is_active=True, is_deleted=False)
            special_discount.status = status
            special_discount.save()

            if special_discount.status == "Approved":
                """ ===== approved-special-discount ===== """
                approved_debit_note(request, special_discount)
        except SpecialCDType.DoesNotExists:
            pass

    ledgers = DebitNote.objects.filter(
        is_active=True, is_deleted=False).order_by('-pk')

    groups = request.user.groups.all()
    groups = [group.name for group in groups]
    if 'SR' in groups:
        srDealer = SRDealer.objects.filter(sr_id=request.user)
        srDealers = []
        for item in srDealer:
            if item.dealer_id is not None:
                if item.dealer_id.dealer is not None:
                    srDealers.append(item.dealer_id.dealer)
        ledgers = ledgers.filter(dealer__in=srDealers)

    my_dealers = []
    dealerProfiles = DealerProfile.objects.filter(
        is_active=True, is_deleted=False).order_by('-pk')
    dealers = [i.dealer for i in dealerProfiles]
    dealers = list(set(dealers))
    if from_date is not None and from_date != "" and to_date is not None and to_date != "":
        ledgers = ledgers.filter(
            created_on__date__gte=from_date, created_on__date__lte=to_date)
    for _x in dealers:
        try:
            dealer = DealerProfile.objects.get(dealer=_x)
            dict_dealer = {"id": dealer.dealer.pk, "company": dealer.company_name}
            my_dealers.append(dict_dealer)
        except DealerProfile.DoesNotExist:
            continue
    if search_dealer != 0:
        ledgers = ledgers.filter(dealer__in=[int(search_dealer)])

    discountList = []
    for item in ledgers:
        try:
            dealer = DealerProfile.objects.get(dealer=item.dealer)
        except DealerProfile.DoesNotExist:
            continue

        discount_dict = {
            # "series_no": item.series_no,
            # "type": item.get_type_display(),
            "value": item.amount,
            "status": item.status,
            "id": item.pk,
            "company": dealer.company_name,
            "date": item.created_on.date(),
            "remarks": item.reference
        }
        discountList.append(discount_dict)

    """Export the data to a CSV file"""
    if 'export' in request.GET:
        for item in discountList:
            del item["id"]
        keys = ['company', 'date', 'value', 'status', 'remarks']
        full_file_name = "debit_note"
        response = download_data_in_csv(full_file_name, keys, discountList)
        return response

    paginator = Paginator(discountList, 10)
    try:
        page_index = request.GET.get("page", 1)
        data_list = paginator.page(page_index)
    except PageNotAnInteger:
        data_list = paginator.page(1)
    except EmptyPage:
        data_list = paginator.page(paginator.num_pages)
    context = {
        "ledgers": data_list,
        "dealers": my_dealers,
        "x_dealer": int(search_dealer),
        "from_date": from_date,
        "to_date": to_date
    }
    return render(request, 'admin/debit_note.html', context=context)


def AddDebitNote(request):
    try:
        form_class = forms.DebitNoteForm
        dealers = DealerProfile.objects.filter(
            is_active=True, is_deleted=False).order_by('company_name')
        if request.method == "POST":
            form_class = forms.DebitNoteForm(request.POST)
            if form_class.is_valid():
                special_discount = form_class.save(commit=False)
                special_discount.created_by = request.user
                special_discount.save()

                # create_debit_note(request, special_discount.dealer, special_discount.amount, special_discount.reference)

                groups = request.user.groups.all()
                groups = [group.name for group in groups]
                if "BusinessHead" in groups:
                    special_discount.status = "Approved"
                    special_discount.save()
                    """ ===== approved-special-discount ===== """
                    approved_debit_note(request, special_discount)
                    messages.add_message(request, messages.SUCCESS, 'Debit Note Successfully Applied!')
                else:
                    messages.add_message(request, messages.SUCCESS, 'Debit Note Successfully Created!')
                return redirect("/user/debit-note")
            else:
                messages.add_message(request, messages.ERROR, 'Oops!, Something want wrong.')
    except Exception as error:
        messages.add_message(request, messages.ERROR, str(error))
    context = {'form': form_class, "dealers": dealers}
    return render(request, 'admin/create_debit_note.html', context=context)


# def StockOut(request, sku_code):
#     status, sku_name = check_stock_out([sku_code])
#     if status:
#         message = "Product ({0}) {1}.".format(sku_code, "is In-Stock")
#     else:
#         message = "Product ({0}) {1}.".format(sku_code, "is Out of Stock")
#     context = {"status": status, "sku_code": sku_code, "message": message}
#     return HttpResponse(json.dumps(context))


def Products(request):
    search_category, productList = [], []
    if request.method == "POST":
        search_category = get_product_search_new(request)
    products = ProductMaster.objects.filter(is_active=True, is_deleted=False)
    for product in products:
        if product.product_category.category_name in search_category:
            pass
        else:
            continue

        category_name = find_product_category(product)
        category_series = ', '.join(category_name)
        item_dict = {
            "category": category_series, "sku_code": product.sku_code,
            "mrp": float(product.max_retail_price), "dp": float(product.dealer_price),
            "size": product.size_capacity, "size_unit": product.size_capacity_unit,
            "description": product.product_description_feature, "color": product.pattern_color
        }
        productList.append(item_dict)
    """Length of Search Products"""
    record_found = "{0} records found".format(len(productList)) if len(productList) > 0 else "No records found"

    category, sub_category, sub_category_one = None, None, None
    if request.method == 'POST':
        category = request.POST.get('third')
        sub_category = request.POST.get('four')
        sub_category_one = request.POST.get('five')
    if category == '' or category is None:
        one, two, three = get_product_category()
        two, three = [], []
    else:
        one, noUse, noNeed = get_product_category()
        two, three = product_category(request, '1', category)
    context = {
        "search_record": record_found, "products": productList,
        "varOne": category, "varTwo": sub_category, "one": one,
        "varThree": sub_category_one, "two": two, "three": three
    }
    return render(request, 'sr/product_list.html', context)


def DelProducts(request):
    search_category, productList = [], []
    if request.method == "POST":
        search_category = get_product_search_new(request)
    products = ProductMaster.objects.filter(is_active=True, is_deleted=False)
    for product in products:
        if product.product_category.category_name in search_category:
            pass
        else:
            continue

        category_name = find_product_category(product)
        category_series = ', '.join(category_name)
        item_dict = {
            "category": category_series, "sku_code": product.sku_code,
            "mrp": float(product.max_retail_price), "dp": float(product.dealer_price),
            "size": product.size_capacity, "size_unit": product.size_capacity_unit,
            "description": product.product_description_feature, "color": product.pattern_color
        }
        productList.append(item_dict)
    """Length of Search Products"""
    record_found = "{0} records found".format(len(productList)) if len(productList) > 0 else "No records found"

    category, sub_category, sub_category_one = None, None, None
    if request.method == 'POST':
        category = request.POST.get('third')
        sub_category = request.POST.get('four')
        sub_category_one = request.POST.get('five')
    if category == '' or category is None:
        one, two, three = get_product_category()
        two, three = [], []
    else:
        one, noUse, noNeed = get_product_category()
        two, three = product_category(request, '1', category)
    context = {
        "search_record": record_found, "products": productList,
        "varOne": category, "varTwo": sub_category, "one": one,
        "varThree": sub_category_one, "two": two, "three": three
    }
    return render(request, 'dealer/custom_product_list.html', context)
