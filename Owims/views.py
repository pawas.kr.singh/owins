from django.shortcuts import render, redirect
from django.contrib import messages
from django.http import JsonResponse
# from django.contrib.auth.models import Group
from django.contrib.auth import get_user_model
from IndospritCore.models import AllowanceMaster
from IndospiritTransactions.models import OrderHead
User = get_user_model()
from datetime import datetime


def home(request):
    remove_allowance_without_dealer()

    groups = [group.name for group in request.user.groups.all()]
    orderHead = OrderHead.objects.filter(status='Processing', is_active=True, is_deleted=False)
    # orderDate = orderHead[0].order_date
    # print(orderDate)
    for order in orderHead:
        then = order.order_date
        then = then.replace(tzinfo=None)
        now = datetime.now()
        duration = now - then
        duration_in_s = duration.total_seconds()
        hours = divmod(duration_in_s, 3600)[0]
        if hours >= 48:
            order.status = 'Deleted'
            order.save()
    if request.user.is_superuser:
        if request.user.is_admin:
            return redirect('/user/Admin_Dashboard')
        else:
            return render(request, 'home.html')
    elif 'DEL' in groups:
        return redirect('/user/Customer-Dashboard')
    elif 'SR' in groups:
        return redirect('/user/sr-dashboard')
    elif 'WHS' in groups:
        return redirect('/user/warehouse-admin-dashboard')
    elif 'WHSUSER' in groups:
        return redirect('/user/warehouse-user-dashboard')

    elif 'Finance' in groups:
        return redirect('/user/finance-dashboard')
    elif 'BusinessHead' in groups:
        return redirect('/user/head-dashboard')


def set_brand(request, brand_code):
    request.session["selected_brand"] = brand_code
    messages.add_message(request, messages.INFO, 'Brand changed!')
    return JsonResponse({'message': 'ok'})


def remove_allowance_without_dealer():
    allowances = AllowanceMaster.objects.filter(dealer=None)
    for allowance in allowances:
        allowance.delete()
    return True
