# Generated by Django 2.1.1 on 2019-04-23 12:16

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('IndospiritTransactions', '0030_debitnote_status'),
    ]

    operations = [
        migrations.AlterField(
            model_name='orderdetails',
            name='size_capacity',
            field=models.DecimalField(blank=True, decimal_places=2, max_digits=10, null=True),
        ),
        migrations.AlterField(
            model_name='orderdetails',
            name='size_capacity_unit',
            field=models.CharField(blank=True, max_length=5, null=True),
        ),
    ]
