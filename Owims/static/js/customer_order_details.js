
function replyfunction(id){
    $('#replydiv'+id.toString()).css('display','block');
    $('#reply'+id.toString()).hide();
};

function edit(counter) {
    $('#qtyShow_' + counter).css('display', 'none');
    $('#ul_' + counter).css('display', 'block');
    $('#btnEdit_' + counter).css('display', 'none');
    $('#btnUpdate_' + counter).css('display', 'block');
};

function minus(counter) {
    var qty = $('#txtOrder_' + counter).val().trim();
    var updated_qty = parseInt(qty) - 1;
    if (updated_qty <= 1) {
        updated_qty = 1;
    }
    $('#qtyShow_' + counter).text(updated_qty);
    $('#txtOrder_' + counter).val(updated_qty)
    $('#qty_' + counter).text(updated_qty)
}

function plus(counter) {
    var qty = $('#txtOrder_' + counter).val().trim();
    var updated_qty = parseInt(qty) + 1;
    if (updated_qty <= 1) {
        updated_qty = 1;
    }
    $('#qtyShow_' + counter).text(updated_qty);
    $('#txtOrder_' + counter).val(updated_qty)
    $('#qty_' + counter).text(updated_qty)
}
