from .base import *

ALLOWED_HOSTS = ['owims.do.viewyoursite.net', ]

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'owims',
        'USER': 'owims_user',
        'PASSWORD': 'k%lizU0IVB',
        'HOST': 'localhost',
        'PORT': '',
    }
}

STATIC_ROOT = os.path.join(os.path.dirname(BASE_DIR), 'static')
SITE_NAME = 'Owims Test'