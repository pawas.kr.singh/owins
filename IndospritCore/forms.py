from django import forms
from . import models
from django.forms import ValidationError
from django.contrib.auth import get_user_model
from ClassicUserAccounts.models import User


User = get_user_model()


class WarehouseMasterForm(forms.ModelForm):
    class Meta:
        model = models.WarehouseMaster
        fields = ('warehouse_code', 'warehouse_name', 'address_line1', 'address_line2', 'state', 'city', 'pin_code',
                  'contact_number')
    def __init__(self, *args, **kwargs):
        super(WarehouseMasterForm, self).__init__(*args, **kwargs)
        self.fields['warehouse_name'].label = "Warehouse name*"
        self.fields['address_line1'].label = "Address line*"
        self.fields['pin_code'].label = "Pin code*"
        self.fields['contact_number'].label = "Contact Number*"

class BrandMasterForm(forms.ModelForm):
    class Meta:
        model = models.BrandMaster
        fields = ('brand_code', 'brand_name', 'is_active', 'primary_warehouse')


class ProductCategoryMasterForm(forms.ModelForm):
    parent = forms.ModelChoiceField(queryset=models.ProductCategoryMaster.objects.all(), required=False)

    class Meta:
        model = models.ProductCategoryMaster
        fields = '__all__'
        # fields = ('category_id', 'brand', 'category_name')

    # def save(self, commit=True):
    #     obj = models.ProductCategoryMaster(
    #         brand = self.cleaned_data.get('brand', None),
    #         category_name = self.cleaned_data.get('category_name', None),
    #         path = int(self.cleaned_data.get('parent', 0)),
    #         parent = int(self.cleaned_data.get('parent', 0))
    #     )
    #     obj.save()
    #     return obj

    def clean_parent(self):
        if self.cleaned_data.get("parent", None) is not None:
            return self.cleaned_data["parent"].pk
        else:
            return None


class WarehouseAdminForm(forms.ModelForm):
    class Meta:
        model = models.WarehouseAdmin
        
        fields = ('user', 'warehouse', 'is_active')


class ProductMasterForm(forms.ModelForm):
    brand = forms.ModelChoiceField(queryset=models.BrandMaster.objects.filter(is_active=True))
    product_category = forms.ModelChoiceField(queryset=models.ProductCategoryMaster.objects.filter(is_active=True))
    product_category_code = forms.ModelChoiceField(queryset=models.SchemeCategoryMaster.objects.filter(is_active=True))
    max_retail_price = forms.DecimalField(min_value=0.01)
    dealer_price = forms.DecimalField(min_value=0.01)
    # best_buying_price = forms.DecimalField(min_value=0.01)
    # size_capacity = forms.IntegerField(min_value=0.01)

    class Meta:
        model = models.ProductMaster
        fields = ('brand', 'product_category','product_category_code', 'sku_code', 'size_capacity', 'size_capacity_unit',
                  'TaxSlabCode', 'pattern_color', 'product_description_feature', 'best_buying_price',
                  'dealer_price', 'max_retail_price')
    #
    # def clean_sku(self):
    #     cleaned_data = self.cleaned_data
    #     sku_code = self.cleaned_data.get['sku_code']
    #
    #     if sku_code and models.ProductMaster.objects.get(sku_code=sku_code):
    #         raise ValidationError("Product with same SKU already exists")
    #     return cleaned_data

    # def clean_sku(self):
    #     if self.instance.pk is None:
    #         sku_code = self.cleaned_data.get('sku_code', None)
    #         if sku_code is not None:
    #             product = models.ProductMaster()
    #             try:
    #                 models.ProductMaster.objects.get(sku_code=sku_code)
    #                 raise ValidationError("Product with same SKU already exists")
    #
    #             except product.DoesNotExist:
    #                 pass
    #         else:
    #             raise ValidationError('This field is required')
    #     else:
    #         sku_code = self.cleaned_data.get('sku_code', None)
    #         return sku_code


class ProductPriceHistoryForm(forms.ModelForm):
    class Meta:
        model = models.ProductPriceHistory
        fields = ('product', 'gst_rate', 'sgst_rate', 'cgst_rate', 'other_tax_rate', 'best_buying_price', 'dealer_price'
                  , 'max_retail_price')


class StockLevelMasterForm(forms.ModelForm):
    class Meta:
        model = models.StockLevelMaster
        fields = ('warehouse', 'product', 'min_stock_level')


class StockRegisterForm(forms.ModelForm):
    class Meta:
        model = models.StockRegister
        fields = ('warehouse', 'product', 'stock_in', 'stock_out', 'reference', 'stock_type', 'remarks')


class DealersBrandForm(forms.ModelForm):
    class Meta:
        model = models.DealersBrand
        fields = ('user', 'brand', 'is_active')


class DealerBusinessForm(forms.ModelForm):
    class Meta:
        model = models.DealerBusinessMaster
        fields = ('dealer', 'brand', 'min_order_amount', 'max_outstanding_threshold', 'credit_days', 'is_active')


class DealerSalesTargetForm(forms.ModelForm):
    monthly_target = forms.IntegerField(min_value=0)
    category = forms.ModelChoiceField(queryset=models.ProductCategoryMaster.objects.filter(is_active=True))


    class Meta:
        model = models.DealerSalesTarget
        fields = ('dealer', 'brand', 'monthly_target', 'category')


'''
class SchemeProductMasterForm(forms.ModelForm):

    class Meta:
        model = models.SchemeMaster
        fields = '__all__'
'''


class AllowanceMasterForm(forms.ModelForm):

    # discount_percent = forms.DecimalField(min_value=0, error_messages={'invalid': "This Field is required"})
    # discount_amount = forms.IntegerField(min_value=0, error_messages={'invalid': "This Field is required"})
    # dealer = forms.ModelChoiceField(queryset=User.objects.filter(groups__name="DEL"))

    class Meta:
        model = models.AllowanceMaster
        fields = ('dealer', 'brand', 'scheme_category_code', 'discount_percent', 'discount_amount')

    def __init__(self, *args, **kwargs):
        super(AllowanceMasterForm, self).__init__(*args, **kwargs)
        self.fields['scheme_category_code'].label = "Product category code"


class TieUpMasterForm(forms.ModelForm):
    # brand = forms.ModelChoiceField(queryset=models.BrandMaster.objects.filter(is_active=True))
    # discount_percent = forms.DecimalField(min_value=0, max_value=99, error_messages={'invalid': "This Field is required"})
    # discount_amount = forms.IntegerField(min_value=0, error_messages={'invalid': "This Field is required"})
    # dealer = forms.ModelChoiceField(queryset=User.objects.filter(groups__name="DEL"))

    class Meta:
        model = models.TieUpMaster
        fields = ('dealer', 'brand', 'scheme_category_code', 'discount_percent', 'discount_amount')

    def __init__(self, *args, **kwargs):
        super(TieUpMasterForm, self).__init__(*args, **kwargs)
        self.fields['scheme_category_code'].label = "Product category code"


# class CashDiscountMasterForm(forms.ModelForm):
#
#     class Meta:
#         model = models.CashDiscountMaster
#         fields = ('dealer', 'brand', 'scheme_category_code', 'discount_percent', 'discount_amount')


class SchemeMasterForm(forms.ModelForm):
    class Meta:
        model = models.SchemeMaster
        fields = ('state', 'city', 'product', 'discount_percent', 'discount_amount', 'scheme_start_date',
                  'scheme_end_date', 'scheme_name', 'scheme_code')


class SchemeProductMasterForm(forms.ModelForm):
    class Meta:
        model = models.SchemeProductMaster
        fields = ('product', 'scheme_category_code')


class SchemeCategoryMasterForm(forms.ModelForm):
    class Meta:
        model = models.SchemeCategoryMaster
        fields = ('scheme_category_code', 'scheme_category_name')


class SchemeSellThroughMasterForm(forms.ModelForm):
    state_code= forms.ModelChoiceField(queryset=models.StateMaster.objects.all())
    # city_code = forms.ChoiceField(queryset=models.CityMaster.objects.filter('state_id'))
    discount_percent = forms.DecimalField(min_value=0,error_messages={'invalid': "This Field is required"})
    discount_amount = forms.DecimalField(min_value=0,error_messages={'invalid': "This Field is required"})

    # product = forms.ModelChoiceField(queryset=models.ProductMaster.objects.all(), required=True)

    class Meta:
        model = models.SchemeSellThroughMaster
        fields = ('brand', 'state_code', 'city_code', 'product', 'discount_percent', 'discount_amount', 'scheme_start_date',
                  'scheme_end_date', 'scheme_name', 'scheme_code')

    # def __init__(self, *args, **kwargs):
    #     super().__init__(*args, **kwargs)
    #     self.fields['city_code'].queryset = models.CityMaster.objects.none()


class CashDiscountSlabsMasterForm(forms.ModelForm):
    slab_code = forms.CharField(min_length=2, required=True, error_messages={'invalid': "This Field is required"})
    cd1 = forms.DecimalField(min_value=0)
    cd2 = forms.DecimalField(min_value=0)
    cd3 = forms.DecimalField(min_value=0)
    cd4 = forms.DecimalField(min_value=0)
    days_1 = forms.IntegerField(min_value=0)
    days_2 = forms.IntegerField(min_value=0)
    days_3 = forms.IntegerField(min_value=0)
    days_4 = forms.IntegerField(min_value=0)
    class Meta:
        model = models.CashDiscountSlabsMaster
        fields = ('cd1', 'days_1', 'cd2', 'days_2', 'cd3', 'days_3', 'cd4', 'days_4', 'slab_code')


# class CashDiscountSchemeMasterForm(forms.ModelForm):
#
#     class Meta:
#         model = models.CashDiscountSchemeMaster
#         fields = ('slab', 'dealer')


class CreditNoteTypeMasterForm(forms.ModelForm):
    class Meta:
        model = models.CreditNoteTypeMaster
        fields = ('note_type_code', 'note_type_name', 'description', 'is_active')


class DocumentTypeMasterForm(forms.ModelForm):
    class Meta:
        model = models.DocumentTypeMaster
        fields = '__all__'
        # fields = ('document_name')


class CityStateRegionMasterForm(forms.ModelForm):
    parent = forms.ModelChoiceField(queryset=models.CityStateRegionMaster.objects.all(), required=False)

    class Meta:
        model = models.CityStateRegionMaster
        fields = '__all__'


class TaxSlabMasterForm(forms.ModelForm):
    # parent = forms.TaxSlabMaster(queryset=models.TaxSlabMaster.objects.all(), required=False)

    class Meta:
        model = models.TaxSlabMaster
        fields = ('gst_rate', 'igst_rate', 'sgst_rate', 'cgst_rate', 'other_tax_rate', 'slab_name')


class InventoryForm(forms.Form):
    fields = ('warehouse_code', 'city', 'brand', 'sku_code', 'saleable_stock', 'nonsaleable_stock',
              'minimum_stock_level')
