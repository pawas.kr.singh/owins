from IndospritCore.models import BrandMaster
import datetime
today = datetime.date.today()
class BrandMiddleware:
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        response = self.get_response(request)
        return response

    def process_view(self, request, view_func, *view_args, **view_kargs):
        if "/admin/" not in request.path:
            brands = BrandMaster.objects.all()
            request.brand = brands
            current_month = 1
            current_year = 2019
            if hasattr(request, "selected_brand") is False:
                if len(brands) > 0:
                    request.selected_brand = brands[0].pk
                    request.current_month = today.month
                    request.current_year = today.year
