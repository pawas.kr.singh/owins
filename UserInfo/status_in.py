def OutstandingStatus():
    statusList = ['Dispatched', 'Delivered', 'PartialDispatched', 'PaymentDone']
    return statusList


def FianceDashboardStatus():
    statusList = ['OrderPlaced', 'InWarehouse',  'Dispatched',
                  'Delivered', 'PartialDispatched']
    return statusList


def CreditNoteStatus():
    statusList = ['OrderPlaced', 'InWarehouse', 'Reject', 'OnHold', 'Dispatched',
                  'Delivered', 'PartialDispatched', 'PaymentDone']
    return statusList


def SRStatus():
    statusList = ['Draft', 'Processing', 'SchemeApplied', 'Deleted', 'OrderPlaced', 'OnHold']
    return statusList


def WarehouseStatus():
    statusList = ['InWarehouse', 'Reject', 'OnHold', 'Dispatched', 'PartialDispatched',
                  'Delivered', 'PartialDispatched', 'PaymentDone']
    return statusList


def HistoryStatus():
    statusList = ['InWarehouse', 'Reject', 'OnHold', 'Dispatched', 'PartialDispatched',
                  'Delivered', 'PartialDispatched', 'PaymentDone']
    return statusList


def ShowOutstandingStatus():
    statusList = ['OrderPlaced', 'InWarehouse', 'Reject', 'OnHold',
                  'Dispatched', 'Delivered', 'PartialDispatched', 'PaymentDone']
    return statusList


def AdminStatus():
    statusList = ['SchemeApplied','OrderPlaced','InWarehouse','OnHold','Dispatched',
                  'Delivered','PartialDispatched','Processing','Deleted', 'PaymentDone']
    return statusList


def WarehouseAdminStatus():
    # statusList = ['Dispatched', 'OrderPlaced', 'Confirm', 'InWarehouse', 'PaymentDone']
    statusList = ['Dispatched', 'OrderPlaced', 'PaymentDone', 'InWarehouse']
    return statusList


def ManagerStatus():
    statusList = ['InWarehouse', 'Dispatched', 'PaymentDone']
    return statusList


def DetailStatus():
    statusList = ['OrderPlaced', 'InWarehouse', 'Reject', 'OnHold', 'Dispatched',
                  'Delivered', 'PartialDispatched', 'PaymentDone']
    return statusList


def ShowHistoryStatus():
    statusList = ['InWarehouse', 'OnHold', 'Dispatched', 'Delivered', 'PartialDispatched', 'PaymentDone']
    return statusList


def SRHeadActiveStatus():
    statusList = ['SchemeApplied', 'OrderPlaced', 'InWarehouse', 'Dispatched',
                  'Delivered', 'PartialDispatched', 'Processing']
    return statusList


def SRHeadNullStatus():
    statusList = ['SchemeApplied', 'OrderPlaced', 'InWarehouse', 'OnHold', 'PartialDispatched', 'Processing']
    return statusList


def OutstandingFlagStatus():
    statusList = ['OrderPlaced', 'InWarehouse', 'Reject', 'OnHold', 'Dispatched',
                  'Delivered', 'PartialDispatched']
    return statusList


def SRReportStatus():
    """ Not working """
    statusList = ['Dispatched', 'PaymentDone']
    return statusList


def PendingPaymentStatus():
    """ Not working """
    statusList = ['Dispatched']
    return statusList


def TopSalesStatus():
    # statusList = ['OrderPlaced', 'InWarehouse', 'Dispatched']
    statusList = ['Dispatched']
    return statusList


def TopCollectionStatus():
    statusList = ['PaymentDone']
    return statusList


def TopOutstandingStatus():
    statusList = ['Dispatched']
    return statusList


def AdvanceOutstandingStatus():
    statusList = ['Dispatched', 'Delivered', 'PartialDispatched', 'PaymentDone']
    return statusList


def DELProfileActiveOrder():
    statusList = ['Processing', 'SchemeApplied', 'OrderPlaced', 'OnHold', 'InWarehouse',
                  'Dispatched', 'Delivered', 'PartialDispatched']
    return statusList


def BlockedOrder():
    statusList = ['OrderPlaced']
    # statusList = ['OrderPlaced', 'InWarehouse']
    return statusList

def AssignedOrder():
    # statusList = ['InWarehouse']
    statusList = ['OrderPlaced', 'InWarehouse']
    return statusList

def AssignOrder():
    # statusList = ['InWarehouse']
    statusList = ['InWarehouse']
    return statusList

def admin_active():
    statusList = ['SchemeApplied','OrderPlaced','InWarehouse','Dispatched',
                  'Delivered','PartialDispatched','Processing']
    return statusList


def admin_null_status():
    statusList = ['SchemeApplied','OrderPlaced','InWarehouse',
                  'Dispatched','Delivered','PartialDispatched','Processing', 'Deleted']
    return statusList