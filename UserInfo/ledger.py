from IndospiritTransactions.models import Ledger, OrderHead, LedgerHead, OrderEventHistory
# from UserInfo.views import *
from UserInfo import status_in
from django.db.models import Sum
from django.utils.timezone import datetime
today = datetime.today()


def create_custom_debit_note(request, dealer, discount, remark):
    """ =============== debit-note-ledger-start =============== """
    ledgerHead = LedgerHead(
        voucher_type='DebitNote',
        voucher_date=today.date()
    )
    ledgerHead.save()
    try:
        prevCreditNoteLedger = Ledger.objects.filter(
            ledger_type="Discount", dealer=dealer,
            is_active=True, is_deleted=False).latest('pk')
    except Ledger.DoesNotExist:
        prevCreditNoteLedger = None

    currentCreditNoteLedger = Ledger(
        ledger_head=ledgerHead,
        ledger_type="Discount", remarks=remark,
        dealer=dealer,
        voucher_number="debit-note", created_by=request.user
    )
    if prevCreditNoteLedger is None:
        balance = 0
    else:
        balance = prevCreditNoteLedger.balance
    currentCreditNoteLedger.cr_amount = float(discount)
    currentCreditNoteLedger.balance = float(balance) - float(discount)
    currentCreditNoteLedger.save()

    """ =============== debit-note-ledger-end =============== """
    """ =============== dealer-ledger-start =============== """
    try:
        prevDealerLedger = Ledger.objects.filter(
            ledger_type="Dealer", dealer=dealer,
            is_active=True, is_deleted=False).latest('pk')
    except Ledger.DoesNotExist:
        prevDealerLedger = None

    currentDealerLedger = Ledger(
        ledger_head=ledgerHead,
        ledger_type="Dealer", remarks=remark,
        dealer=dealer,
        voucher_number="debit-note", created_by=request.user
    )
    if prevDealerLedger is None:
        balance = 0
    else:
        balance = prevDealerLedger.balance
    currentDealerLedger.dr_amount = float(discount)
    currentDealerLedger.balance = float(balance) + float(discount)
    currentDealerLedger.save()
    """ =============== dealer-ledger-end =============== """

    """ =============== debit-note-ledger-end =============== """
    create_unadjusted_ledger_type3(request, dealer, discount, True)
    return True


def create_credit_note_special_discount(request, dealer, discount, remark):
    """ =============== credit-note-ledger-start =============== """
    ledgerHead = LedgerHead(voucher_type='CreditNote', voucher_date=today.date())
    ledgerHead.save()
    try:
        prevCreditNoteLedger = Ledger.objects.filter(
            ledger_type="Discount", dealer=dealer,
            is_active=True, is_deleted=False).latest('pk')
    except Ledger.DoesNotExist:
        prevCreditNoteLedger = None

    currentCreditNoteLedger = Ledger(
        ledger_head=ledgerHead,
        ledger_type="Discount", remarks=remark,
        dealer=dealer,
        voucher_number="type-3", created_by=request.user
    )
    if prevCreditNoteLedger is None:
        balance = 0
    else:
        balance = prevCreditNoteLedger.balance
    currentCreditNoteLedger.dr_amount = float(discount)
    currentCreditNoteLedger.balance = float(balance) + float(discount)
    currentCreditNoteLedger.save()

    """ =============== credit-note-ledger-end =============== """
    """ =============== dealer-ledger-start =============== """
    try:
        prevDealerLedger = Ledger.objects.filter(
            ledger_type="Dealer", dealer=dealer,
            is_active=True, is_deleted=False).latest('pk')
    except Ledger.DoesNotExist:
        prevDealerLedger = None

    currentDealerLedger = Ledger(
        ledger_head=ledgerHead,
        ledger_type="Dealer", remarks=remark,
        dealer=dealer,
        voucher_number="type-3", created_by=request.user
    )
    if prevDealerLedger is None:
        balance = 0
    else:
        balance = prevDealerLedger.balance
    currentDealerLedger.cr_amount = float(discount)
    currentDealerLedger.balance = float(balance) - float(discount)
    currentDealerLedger.save()
    """ =============== dealer-ledger-end =============== """

    """ =============== credit-note-ledger-end =============== """
    create_unadjusted_ledger_type3(request, dealer, discount, False, remark)
    try:
        orderHead = OrderHead.objects.filter(
            dealer=dealer, status__in=['Dispatched'],
            is_active=True, is_deleted=False).latest('pk')
    except OrderHead.DoesNotExist:
        orderHead = None

    """ SUBTRACT DISCOUNT TO OUTSTANDING"""
    if orderHead is not None:
        if float(orderHead.payable_amount) <= discount:
            orderHead.status = 'PaymentDone'
            orderHistory = OrderEventHistory(
                order_id=orderHead,
                order_status='PaymentDone',
                created_by=request.user
            )
            orderHistory.save()
        orderHead.payable_amount = float(orderHead.payable_amount) - float(discount)
        orderHead.save()
    return True


def create_unadjusted_ledger_type3(request, dealer, amount, paymentFlag, note=''):
    if paymentFlag:
        ledgerHead = LedgerHead(voucher_type='DebitNote', voucher_date=today.date())
        ledgerHead.save()
    else:
        ledgerHead = LedgerHead(voucher_type='CreditNote', voucher_date=today.date())
        ledgerHead.save()
    try:
        prevUnadjustedLedger = Ledger.objects.filter(
            ledger_type="Unadjusted", dealer=dealer,
            is_active=True, is_deleted=False).latest('pk')
    except Ledger.DoesNotExist:
        prevUnadjustedLedger = None
    if paymentFlag:
        currentUnadjustedLedger = Ledger(
            ledger_head=ledgerHead,dealer=dealer,
            ledger_type="Unadjusted", remarks="DebitNote",
            voucher_number="DebitNote", created_by=request.user
        )
    else:
        currentUnadjustedLedger = Ledger(
            ledger_head=ledgerHead,dealer=dealer,
            ledger_type="Unadjusted", remarks="Credit note type 3 {}".format(note),
            voucher_number="Credit note type 3", created_by=request.user
        )
    if prevUnadjustedLedger is None:
        udadjusted_balance = 0
    else:
        udadjusted_balance = prevUnadjustedLedger.balance
    if paymentFlag:
        currentUnadjustedLedger.dr_amount = float(amount)
        currentUnadjustedLedger.balance = float(udadjusted_balance) + float(amount)
        currentUnadjustedLedger.save()
    else:
        currentUnadjustedLedger.cr_amount = float(amount)
        currentUnadjustedLedger.balance = float(udadjusted_balance) - float(amount)
        currentUnadjustedLedger.save()


def create_advance_amount_ledger(request, dealer, clientPayment, flag=None):
    """ =============== dealer-ledger-start =============== """
    ledgerHead = LedgerHead(voucher_type='Payment', voucher_date=today.date())
    ledgerHead.save()
    try:
        prevDealerLedger = Ledger.objects.filter(
            ledger_type="DealerOrders", dealer=dealer,
            is_active=True, is_deleted=False).latest('pk')
    except Ledger.DoesNotExist:
        prevDealerLedger = None
    if flag:
        currentDealerLedger = Ledger(
            ledger_head=ledgerHead,
            ledger_type="Unadjusted", remarks="Advance Payment Settled", dealer=dealer,
            voucher_number="advance-payment", created_by=request.user
        )
        if prevDealerLedger is None:
            balance = 0
        else:
            balance = prevDealerLedger.balance
        currentDealerLedger.cr_amount = float(clientPayment)
        currentDealerLedger.balance = float(balance) - float(clientPayment)
        currentDealerLedger.save()
    else:
        currentDealerLedger = Ledger(
            ledger_type="DealerOrders", remarks="Advance Payment Received", dealer=dealer,
            voucher_number="discount-voucher", created_by=request.user
        )
        if prevDealerLedger is None:
            balance = 0
        else:
            balance = prevDealerLedger.balance
        currentDealerLedger.dr_amount = float(clientPayment)
        currentDealerLedger.balance = float(balance) + float(clientPayment)
        currentDealerLedger.save()
    """ =============== dealer-ledger-end =============== """
    return True


def create_bank_dealer_ledger(request, dealer, amount):
    """ =============== bank-ledger-start =============== """
    try:
        prevBankLedger = Ledger.objects.filter(
            ledger_type="Bank", dealer=dealer,
            is_active=True, is_deleted=False).latest('pk')
    except Ledger.DoesNotExist:
        prevBankLedger = None

    currentBankLedger = Ledger(
        ledger_type="Bank", remarks="Payment Received", dealer=dealer,
        voucher_number="bank-voucher", created_by=request.user
    )
    if prevBankLedger is None:
        balance = 0
    else:
        balance = prevBankLedger.balance
    currentBankLedger.dr_amount = float(amount)
    currentBankLedger.balance = float(balance) + float(amount)
    currentBankLedger.save()

    """ =============== bank-ledger-end =============== """
    """ ============= dealer-ledger-start ============= """
    try:
        prevDealerLedger = Ledger.objects.filter(
            ledger_type="Dealer", dealer=dealer,
            is_active=True, is_deleted=False).latest('pk')
    except Ledger.DoesNotExist:
        prevDealerLedger = None

    currentDealerLedger = Ledger(
        ledger_type="Dealer", remarks="Payment Received", dealer=dealer,
        voucher_number="bank-voucher", created_by=request.user
    )
    if prevDealerLedger is None:
        balance = 0
    else:
        balance = prevDealerLedger.balance
    currentDealerLedger.cr_amount = float(amount)
    currentDealerLedger.balance = float(balance) - float(amount)
    currentDealerLedger.save()

    """ =============== dealer-ledger-end =============== """
    """ =========== Third-Ledger-Complete =============== """
    return True


def create_discount_dealer_ledger(request, orderHead, discount, note=''):
    """ =============== discount-ledger-start =============== """
    ledgerHead = LedgerHead(voucher_type='CreditNote', voucher_date=today.date())
    ledgerHead.save()
    try:
        prevDiscountLedger = Ledger.objects.filter(
            ledger_type="Discount", dealer=orderHead.dealer,
            is_active=True, is_deleted=False).latest('pk')
    except Ledger.DoesNotExist:
        prevDiscountLedger = None

    if note:
        message = note
    else:
        message = "Credit Note-Type 1"
    currentDiscountLedger = Ledger(
        ledger_head=ledgerHead,
        ledger_type="Discount", remarks=message,
        order_number=orderHead.order_number, dealer=orderHead.dealer,
        voucher_number="discount-voucher", created_by=request.user
    )
    if prevDiscountLedger is None:
        balance = 0
    else:
        balance = prevDiscountLedger.balance
    currentDiscountLedger.dr_amount = float(discount)
    currentDiscountLedger.balance = float(balance) + float(discount)
    currentDiscountLedger.save()

    """ =============== discount-ledger-end =============== """
    """ =============== dealer-ledger-start =============== """
    try:
        prevDealerLedger = Ledger.objects.filter(
            ledger_type="Dealer", dealer=orderHead.dealer,
            is_active=True, is_deleted=False).latest('pk')
    except Ledger.DoesNotExist:
        prevDealerLedger = None
    try:
        prevUnadjustedLedger = Ledger.objects.filter(
            ledger_type="Unadjusted", dealer=orderHead.dealer,
            is_active=True, is_deleted=False).latest('pk')
    except Ledger.DoesNotExist:
        prevUnadjustedLedger = None
    currentDealerLedger = Ledger(
        ledger_head=ledgerHead,
        ledger_type="Dealer", remarks=message,
        order_number=orderHead.order_number, dealer=orderHead.dealer,
        voucher_number="credit-voucher", created_by=request.user
    )
    if prevDealerLedger is None:
        balance = 0
    else:
        balance = prevDealerLedger.balance
    currentDealerLedger.cr_amount = float(discount)
    currentDealerLedger.balance = float(balance) - float(discount)
    currentDealerLedger.save()
    """ =============== dealer-ledger-end =============== """
    currentUnadjustedLedger = Ledger(
        ledger_head=ledgerHead,
        ledger_type="Unadjusted", remarks=message,
        order_number=orderHead.order_number, dealer=orderHead.dealer,
        voucher_number="dealer-voucher", created_by=request.user
    )
    if prevUnadjustedLedger is None:
        udadjusted_balance = 0
    else:
        udadjusted_balance = prevUnadjustedLedger.balance
    currentUnadjustedLedger.cr_amount = float(discount)
    currentUnadjustedLedger.balance = float(udadjusted_balance) - float(discount)
    currentUnadjustedLedger.save()
    return True


def create_credit_note_dealer_ledger(request, orderHead, discount, remark=None):
    """ =============== credit-note-ledger-start =============== """
    ledgerHead = LedgerHead(voucher_type='CreditNote', voucher_date=today.date())
    ledgerHead.save()
    if remark is None:
        remark = 'CD(Credit Note-Type 2)'
    try:
        prevCreditNoteLedger = Ledger.objects.filter(
            ledger_type="Discount", dealer=orderHead.dealer,
            is_active=True, is_deleted=False).latest('pk')
    except Ledger.DoesNotExist:
        prevCreditNoteLedger = None

    currentCreditNoteLedger = Ledger(
        ledger_head=ledgerHead,
        ledger_type="Discount", remarks=remark,
        order_number=orderHead.order_number, dealer=orderHead.dealer,
        voucher_number="credit-voucher", created_by=request.user
    )
    if prevCreditNoteLedger is None:
        balance = 0
    else:
        balance = prevCreditNoteLedger.balance
    currentCreditNoteLedger.dr_amount = float(discount)
    currentCreditNoteLedger.balance = float(balance) + float(discount)
    currentCreditNoteLedger.save()

    """ =============== credit-note-ledger-end =============== """
    """ =============== dealer-ledger-start =============== """
    try:
        prevDealerLedger = Ledger.objects.filter(
            ledger_type="Dealer", dealer=orderHead.dealer,
            is_active=True, is_deleted=False).latest('pk')
    except Ledger.DoesNotExist:
        prevDealerLedger = None

    currentDealerLedger = Ledger(
        ledger_head=ledgerHead,
        ledger_type="Dealer", remarks=remark,
        order_number=orderHead.order_number, dealer=orderHead.dealer,
        voucher_number="credit-voucher", created_by=request.user
    )
    if prevDealerLedger is None:
        balance = 0
    else:
        balance = prevDealerLedger.balance
    currentDealerLedger.cr_amount = float(discount)
    currentDealerLedger.balance = float(balance) - float(discount)
    currentDealerLedger.save()
    """ =============== dealer-ledger-end =============== """
    return True


def create_bank_ledger(request, dealer, amount):
    """ =============== bank-ledger-start =============== """
    ledgerHead = LedgerHead(voucher_type='Payment', voucher_date=today.date())
    ledgerHead.save()
    try:
        prevBankLedger = Ledger.objects.filter(
            ledger_type="Bank", dealer=dealer,
            is_active=True, is_deleted=False).latest('pk')
    except Ledger.DoesNotExist:
        prevBankLedger = None

    currentBankLedger = Ledger(
        ledger_head=ledgerHead,
        ledger_type="Bank", remarks="Payment Received", dealer=dealer,
        voucher_number="bank-voucher", created_by=request.user
    )
    if prevBankLedger is None:
        balance = 0
    else:
        balance = prevBankLedger.balance
    currentBankLedger.dr_amount = float(amount)
    currentBankLedger.balance = float(balance) + float(amount)
    currentBankLedger.save()
    """ =============== bank-ledger-end =============== """
    return True


def create_dealer_ledger(request, dealer, clientPayment):
    """ =============== dealer-ledger-start =============== """
    ledgerHead = LedgerHead(voucher_type='Payment', voucher_date=today.date())
    ledgerHead.save()
    try:
        prevDealerLedger = Ledger.objects.filter(
            ledger_type="Dealer", dealer=dealer,
            is_active=True, is_deleted=False).latest('pk')
    except Ledger.DoesNotExist:
        prevDealerLedger = None

    currentDealerLedger = Ledger(
        ledger_head=ledgerHead,
        ledger_type="Dealer", remarks="Payment Received", dealer=dealer,
        voucher_number="discount-voucher", created_by=request.user
    )
    if prevDealerLedger is None:
        balance = 0
    else:
        balance = prevDealerLedger.balance
    currentDealerLedger.cr_amount = float(clientPayment)
    currentDealerLedger.balance = float(balance) - float(clientPayment)
    currentDealerLedger.save()
    """ =============== dealer-ledger-end =============== """
    return True


def create_advance_dealer_ledger(request, dealer, clientPayment):
    """ =============== dealer-ledger-start =============== """
    ledgerHead = LedgerHead(voucher_type='Payment', voucher_date=today.date())
    ledgerHead.save()
    try:
        prevDealerLedger = Ledger.objects.filter(
            ledger_type="Dealer", dealer=dealer,
            is_active=True, is_deleted=False).latest('pk')
    except Ledger.DoesNotExist:
        prevDealerLedger = None

    currentDealerLedger = Ledger(
        ledger_head=ledgerHead,
        ledger_type="Dealer", remarks="Advance Payment Received", dealer=dealer,
        voucher_number="discount-voucher", created_by=request.user
    )
    if prevDealerLedger is None:
        balance = 0
    else:
        balance = prevDealerLedger.balance
    currentDealerLedger.cr_amount = float(clientPayment)
    currentDealerLedger.balance = float(balance) - float(clientPayment)
    currentDealerLedger.save()
    """ =============== dealer-ledger-end =============== """
    return True


def create_advance_dealer_transaction_ledger(request, dealer, clientPayment):
    """ =============== dealer-ledger-start =============== """
    try:
        prevDealerLedger = Ledger.objects.filter(
            ledger_type="DealerTransaction", dealer=dealer,
            is_active=True, is_deleted=False).latest('pk')
    except Ledger.DoesNotExist:
        prevDealerLedger = None

    currentDealerLedger = Ledger(
        ledger_type="DealerTransaction", remarks="Advance Payment Received", dealer=dealer,
        voucher_number="discount-voucher", created_by=request.user
    )
    if prevDealerLedger is None:
        balance = 0
    else:
        balance = prevDealerLedger.balance
    currentDealerLedger.cr_amount = float(clientPayment)
    currentDealerLedger.balance = float(balance) - float(clientPayment)
    currentDealerLedger.save()
    """ =============== dealer-ledger-end =============== """
    return True



def create_unadjusted_ledger(request, dealer, amount, paymentFlag, note=''):
    if paymentFlag:
        ledgerHead = LedgerHead(voucher_type='Payment', voucher_date=today.date())
        ledgerHead.save()
    else:
        ledgerHead = LedgerHead(voucher_type='CreditNote', voucher_date=today.date())
        ledgerHead.save()
    try:
        prevUnadjustedLedger = Ledger.objects.filter(
            ledger_type="Unadjusted", dealer=dealer,
            is_active=True, is_deleted=False).latest('pk')
    except Ledger.DoesNotExist:
        prevUnadjustedLedger = None
    if paymentFlag:
        currentUnadjustedLedger = Ledger(
            ledger_head=ledgerHead,dealer=dealer,
            ledger_type="Unadjusted", remarks="Payment",
            voucher_number="dealer-voucher", created_by=request.user
        )
    else:
        currentUnadjustedLedger = Ledger(
            ledger_head=ledgerHead,dealer=dealer,
            ledger_type="Unadjusted", remarks="Credit note type 2 {}".format(note),
            voucher_number="dealer-voucher", created_by=request.user
        )
    if prevUnadjustedLedger is None:
        udadjusted_balance = 0
    else:
        udadjusted_balance = prevUnadjustedLedger.balance
    currentUnadjustedLedger.cr_amount = float(amount)
    currentUnadjustedLedger.balance = float(udadjusted_balance) - float(amount)
    currentUnadjustedLedger.save()


def create_ledger(request, orderHead, amount, discount, head_type, note=""):
    """ =============== dealer-ledger-start =============== """
    if amount > 0:
        ledgerHead = LedgerHead(voucher_type=head_type, voucher_date=today.date())
        ledgerHead.save()
        try:
            prevDealerLedger = Ledger.objects.filter(
                ledger_type="Dealer", dealer=orderHead.dealer,
                is_active=True, is_deleted=False).latest('pk')
        except Ledger.DoesNotExist:
            prevDealerLedger = None
        try:
            prevUnadjustedLedger = Ledger.objects.filter(
                ledger_type="Unadjusted", dealer=orderHead.dealer,
                is_active=True, is_deleted=False).latest('pk')
        except Ledger.DoesNotExist:
            prevUnadjustedLedger = None

        currentDealerLedger = Ledger(
            ledger_head=ledgerHead,
            ledger_type="Dealer", remarks="Dealer Order {}".format(note),
            order_number=orderHead.order_number, dealer=orderHead.dealer,
            voucher_number="dealer-voucher", created_by=request.user
        )
        if prevDealerLedger is None:
            balance = 0
        else:
            balance = prevDealerLedger.balance

        currentDealerLedger.dr_amount = float(amount)
        currentDealerLedger.balance = float(balance) + float(amount)
        currentDealerLedger.save()

        currentUnadjustedLedger = Ledger(
            ledger_head=ledgerHead,
            ledger_type="Unadjusted", remarks="Dealer Order {}".format(note),
            order_number=orderHead.order_number, dealer=orderHead.dealer,
            voucher_number="dealer-voucher", created_by=request.user
        )
        if prevUnadjustedLedger is None:
            udadjusted_balance = 0
        else:
            udadjusted_balance = prevUnadjustedLedger.balance
        currentUnadjustedLedger.dr_amount = float(amount)
        currentUnadjustedLedger.balance = float(udadjusted_balance) + float(amount)
        currentUnadjustedLedger.save()
        """ =============== dealer-ledger-end =============== """
        """ =============== sales-ledger-start ============== """
        try:
            prevSalesLedger = Ledger.objects.filter(
                ledger_type="Sales", dealer=orderHead.dealer,
                is_active=True, is_deleted=False).latest('pk')
        except Ledger.DoesNotExist:
            prevSalesLedger = None

        currentSalesLedger = Ledger(
            ledger_head=ledgerHead,
            ledger_type="Sales", remarks="Dealer Order",
            order_number=orderHead.order_number, dealer=orderHead.dealer,
            voucher_number="dealer-voucher", created_by=request.user
        )
        if prevSalesLedger is None:
            balance = 0
        else:
            balance = prevSalesLedger.balance
        currentSalesLedger.cr_amount = float(amount)
        currentSalesLedger.balance = float(balance) - float(amount)
        currentSalesLedger.save()

    """ =============== sales-ledger-end =============== """
    """ ========== First-Ledger-Complete =============== """

    if discount > 0:

        """ =============== credit-note-ledger-start =============== """
        create_discount_dealer_ledger(request, orderHead, discount, note)
        """ =============== dealer-ledger-end =============== """
        """ ========== Second-Ledger-Complete =============== """
    return True


def create_transaction_ledger(request, orderHead, amount, discount, note=""):
    """ =============== dealer-ledger-start =============== """
    try:
        prevDealerLedger = Ledger.objects.filter(
            ledger_type="DealerTransaction", dealer=orderHead.dealer,
            is_active=True, is_deleted=False).latest('pk')
    except Ledger.DoesNotExist:
        prevDealerLedger = None

    currentDealerLedger = Ledger(
        ledger_type="DealerTransaction",
        remarks="Dealer Order {} {}".format(orderHead.DMS_order_number, note),
        order_number=orderHead.order_number, dealer=orderHead.dealer,
        voucher_number="dealer-voucher", created_by=request.user
    )
    if prevDealerLedger is None:
        balance = 0
    else:
        balance = prevDealerLedger.balance
    # ...............................................

    # if balance < 0:
        # dealerProfile = DealerProfile.objects.get(
        #     dealer=orderHead.dealer, is_active=True, is_deleted=False)
        # cdSlab = CashDiscountSlabsMaster.objects.get(
        #     pk=dealerProfile.cd_slab_code.pk,
        #     is_active=True, is_deleted=False)
        # slabDiscount = cdSlab.cd1
        # temp_balance = float(-1) * float(balance)
        # if amount > temp_balance:
        #     cash_discount = float(temp_balance) * float(slabDiscount) / 100
        # else:
        #     cash_discount = float(amount) * float(slabDiscount) / 100
        # create_credit_note_dealer_ledger(request, orderHead, cash_discount)
    # .....................
    currentDealerLedger.dr_amount = float(amount) - float(discount)
    currentDealerLedger.balance = float(balance) + (float(amount) - float(discount))
    currentDealerLedger.save()

    """ =============== dealer-ledger-end =============== """
    """ =============== sales-ledger-start ============== """
    # try:
    #     prevSalesLedger = Ledger.objects.filter(
    #         ledger_type="Sales", dealer=orderHead.dealer,
    #         is_active=True, is_deleted=False).latest('pk')
    # except Ledger.DoesNotExist:
    #     prevSalesLedger = None
    #
    # currentSalesLedger = Ledger(
    #     ledger_type="Sales", remarks="Dealer Order",
    #     order_number=orderHead.order_number, dealer=orderHead.dealer,
    #     voucher_number="dealer-voucher", created_by=request.user
    # )
    # if prevSalesLedger is None:
    #     balance = 0
    # else:
    #     balance = prevSalesLedger.balance
    # currentSalesLedger.cr_amount = float(amount)
    # currentSalesLedger.balance = float(balance) - float(amount)
    # currentSalesLedger.save()
    #
    # """ =============== sales-ledger-end =============== """
    # """ ========== First-Ledger-Complete =============== """
    #
    # if discount > 0:
    #     """ =============== credit-note-ledger-start =============== """
    #     create_discount_dealer_ledger(request, orderHead, discount)
    #     """ =============== dealer-ledger-end =============== """
    #     """ ========== Second-Ledger-Complete =============== """
    return True


def create_transaction_payment_ledger(request, dealer, payment, discount):
    """ =============== dealer-ledger-start =============== """
    try:
        prevDealerLedger = Ledger.objects.filter(
            ledger_type="DealerTransaction", dealer=dealer,
            is_active=True, is_deleted=False).latest('pk')
    except Ledger.DoesNotExist:
        prevDealerLedger = None

    currentDealerLedger = Ledger(
        ledger_type="DealerTransaction", remarks="Dealer Payment", dealer=dealer,
        voucher_number="dealer-payment-voucher", created_by=request.user
    )
    if prevDealerLedger is None:
        balance = 0
    else:
        balance = prevDealerLedger.balance
    # ...............................................

    # ...............................................
    currentDealerLedger.cr_amount = float(payment)
    currentDealerLedger.balance = float(balance) - float(payment)
    currentDealerLedger.save()
    latest_balance = currentDealerLedger.balance
    currentDealerDiscountLedger = Ledger(
        ledger_type="DealerTransaction", remarks="Credit Note Type 2", dealer=dealer,
        voucher_number="dealer-CN2", created_by=request.user
    )
    currentDealerDiscountLedger.cr_amount = float(discount)
    currentDealerDiscountLedger.balance = float(latest_balance) - float(discount)
    currentDealerDiscountLedger.save()

    """ =============== dealer-ledger-end =============== """
    """ =============== sales-ledger-start ============== """
    # try:
    #     prevSalesLedger = Ledger.objects.filter(
    #         ledger_type="Sales", dealer=orderHead.dealer,
    #         is_active=True, is_deleted=False).latest('pk')
    # except Ledger.DoesNotExist:
    #     prevSalesLedger = None
    #
    # currentSalesLedger = Ledger(
    #     ledger_type="Sales", remarks="Dealer Order",
    #     order_number=orderHead.order_number, dealer=orderHead.dealer,
    #     voucher_number="dealer-voucher", created_by=request.user
    # )
    # if prevSalesLedger is None:
    #     balance = 0
    # else:
    #     balance = prevSalesLedger.balance
    # currentSalesLedger.cr_amount = float(amount)
    # currentSalesLedger.balance = float(balance) - float(amount)
    # currentSalesLedger.save()
    #
    # """ =============== sales-ledger-end =============== """
    # """ ========== First-Ledger-Complete =============== """
    #
    # if discount > 0:
    #     """ =============== credit-note-ledger-start =============== """
    #     create_discount_dealer_ledger(request, orderHead, discount)
    #     """ =============== dealer-ledger-end =============== """
    #     """ ========== Second-Ledger-Complete =============== """
    return True


def create_debit_note(request, dealer, discount, remark):
    """ =============== credit-note-ledger-start =============== """
    try:
        prevCreditNoteLedger = Ledger.objects.filter(
            ledger_type="DebitNote", dealer=dealer,
            is_active=True, is_deleted=False).latest('pk')
    except Ledger.DoesNotExist:
        prevCreditNoteLedger = None

    currentCreditNoteLedger = Ledger(
        ledger_type="DebitNote", remarks=remark,
        dealer=dealer,
        voucher_number="debit-note", created_by=request.user
    )
    if prevCreditNoteLedger is None:
        balance = 0
    else:
        balance = prevCreditNoteLedger.balance
    currentCreditNoteLedger.dr_amount = float(discount)
    currentCreditNoteLedger.balance = float(balance) + float(discount)
    currentCreditNoteLedger.save()

    """ =============== credit-note-ledger-end =============== """
    """ =============== dealer-ledger-start =============== """
    try:
        prevDealerLedger = Ledger.objects.filter(
            ledger_type="Dealer", dealer=dealer,
            is_active=True, is_deleted=False).latest('pk')
    except Ledger.DoesNotExist:
        prevDealerLedger = None

    currentDealerLedger = Ledger(
        ledger_type="Dealer", remarks=remark,
        dealer=dealer,
        voucher_number="debit-note", created_by=request.user
    )
    if prevDealerLedger is None:
        balance = 0
    else:
        balance = prevDealerLedger.balance
    currentDealerLedger.cr_amount = float(discount)
    currentDealerLedger.balance = float(balance) - float(discount)
    currentDealerLedger.save()
    """ =============== dealer-ledger-end =============== """

    # ...................................................
    """ =============== credit-note-ledger-start =============== """
    try:
        prevCreditNoteLedger = Ledger.objects.filter(
            ledger_type="Sales", dealer=dealer,
            is_active=True, is_deleted=False).latest('pk')
    except Ledger.DoesNotExist:
        prevCreditNoteLedger = None

    currentCreditNoteLedger = Ledger(
        ledger_type="Sales", remarks=remark,
        dealer=dealer,
        voucher_number="debit-note", created_by=request.user
    )
    if prevCreditNoteLedger is None:
        balance = 0
    else:
        balance = prevCreditNoteLedger.balance
    currentCreditNoteLedger.dr_amount = float(discount)
    currentCreditNoteLedger.balance = float(balance) + float(discount)
    currentCreditNoteLedger.save()

    """ =============== credit-note-ledger-end =============== """
    """ =============== dealer-ledger-start =============== """
    try:
        prevDealerLedger = Ledger.objects.filter(
            ledger_type="DealerTransaction", dealer=dealer,
            is_active=True, is_deleted=False).latest('pk')
    except Ledger.DoesNotExist:
        prevDealerLedger = None

    currentDealerLedger = Ledger(
        ledger_type="DealerTransaction", remarks=remark,
        dealer=dealer,
        voucher_number="debit-note", created_by=request.user
    )
    if prevDealerLedger is None:
        balance = 0
    else:
        balance = prevDealerLedger.balance
    currentDealerLedger.cr_amount = float(discount)
    currentDealerLedger.balance = float(balance) - float(discount)
    currentDealerLedger.save()
    """ =============== dealer-ledger-end =============== """
    # ...................................................

    # """ =============== credit-note-ledger-end =============== """
    # """ =============== dealer-ledger-start =============== """
    # try:
    #     prevDealerLedger = Ledger.objects.filter(
    #         ledger_type="DealerTransaction", dealer=dealer,
    #         is_active=True, is_deleted=False).latest('pk')
    # except Ledger.DoesNotExist:
    #     prevDealerLedger = None
    #
    # currentDealerLedger = Ledger(
    #     ledger_type="DealerTransaction", remarks=remark,
    #     dealer=dealer,
    #     voucher_number="type-3", created_by=request.user
    # )
    # if prevDealerLedger is None:
    #     balance = 0
    # else:
    #     balance = prevDealerLedger.balance
    # currentDealerLedger.cr_amount = float(discount)
    # currentDealerLedger.balance = float(balance) - float(discount)
    # currentDealerLedger.save()
    # """ =============== dealer-ledger-end =============== """

    outstanding = get_outstanding(dealer)
    if outstanding < 0 or outstanding > 0:
        orderHead = OrderHead.objects.filter(
            dealer=dealer, status__in=status_in.OutstandingStatus(),
            is_active=True, is_deleted=False).latest('pk')
        """ SUBTRACT DISCOUNT TO OUTSTANDING"""
        orderHead.payable_amount = float(orderHead.payable_amount) - float(discount)
        orderHead.save()
    elif outstanding == 0:
        try:
            orderHead = OrderHead.objects.filter(
                dealer=dealer, status__in=status_in.OutstandingStatus(),
                is_active=True, is_deleted=False).latest('pk')
            """ SUBTRACT DISCOUNT TO OUTSTANDING"""
            orderHead.payable_amount = float(orderHead.payable_amount) - float(discount)
            orderHead.save()
        except Exception:
            pass
    return True


def get_outstanding(dealer):
    try:
        total_outstanding_amount = OrderHead.objects.filter(
            dealer=dealer, status__in=status_in.OutstandingStatus(),
            is_active=True, is_deleted=False).aggregate(Sum('payable_amount'))
        total_outstanding_amount = float(total_outstanding_amount["payable_amount__sum"])
    except Exception:
        total_outstanding_amount = float(0)
    return total_outstanding_amount
